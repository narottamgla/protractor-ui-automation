/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

var logger = function() {
	var log4js = require('log4js');

	/**
	 * This method is used for logging different logs in console as well as in log file.
	 * 
	 * @author narottamc
	 * @param jsFileName
	 * @returns log object
	 */

	this.loggers = function(jsFileName) {
		var fs = require('fs');
		var log_dir="./logs/";
		  if (fs.existsSync(log_dir)) { 
		  } 
		  else
		  {
		   fs.mkdirSync(log_dir); 
		  }

		if (browser.name == 'chrome') {
			log4js.configure({
				appenders : [ {
					"type" : "dateFile",
					"pattern" : "-yyyy-MM-dd",
					"filename" : log_dir + browser.name + '.log',
					"alwaysIncludePattern" : true,
					"maxLogSize" : 10240,
					"backups" : 1,
				}, {
					type : "console"
				} ],
				replaceConsole : true
			});

			var log = log4js.getLogger(jsFileName);
			log.setLevel('ALL');
			return log;
		}
		if (browser.name == 'firefox') {

			log4js.configure({
				appenders : [ {
					"type" : "dateFile",
					"pattern" : "-yyyy-MM-dd",
					"filename" : log_dir + browser.name + '.log',
					"alwaysIncludePattern" : true,
					"maxLogSize" : 10240,
					"backups" : 1,
				}, {
					type : "console"
				} ],
				replaceConsole : true
			});
			var log = log4js.getLogger(jsFileName);
			log.setLevel('ALL');
			return log;
		}
	};
};
module.exports = new logger();