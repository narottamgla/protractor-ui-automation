/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/


var utility = function() {
	var loggerObj = require('./logger.js');
	var logger = loggerObj.loggers("utility");
	/**
	 * This function is used to hit the url in browser
	 * 
	 * @author omp
	 * @param url
	 */
	this.geturl = function(url) {
		//browser.manage().deleteAllCookies();
		browser.manage().window().maximize();
		browser.get(url);
	};

	/**
	 * This method is used to wait for given seconds.
	 * 
	 * @param seconds
	 * @author narottamc
	 */
	this.browserWaitforseconds = function(seconds) {
		browser.sleep(seconds * 1000);
	};

	/**
	 * This function used to redirect to required product
	 * 
	 * @author omp
	 * @param campaignsuitelemt
	 * @param audiencesuitelink
	 */
	this.selectProduct = function(campaignsuitelemt, audiencesuitelink) {
		campaignsuitelemt.click();
		audiencesuitelink.click();
		browser.waitForAngular();
	};

	/**
	 * This function is used to wait for second.
	 * 
	 * @author omp
	 * @returns boolean false
	 */
	var _retryOnErr = function(err) {
		browser.sleep(1000);
		return false;
	};

	/**
	 * This function is used to wait for element present up to timeout.
	 * 
	 * @author omp
	 * @param elmentFinder
	 * @param elementDesc
	 * @param timeout
	 */
	this.browserWait = function(elmentFinder, elementDesc, timeout) {

		if (!timeout) {
			timeout = 60;
		}

		browser.driver.wait(function() {
				return elmentFinder.isPresent().then(function(present) {
				if (present == true) {
				return elmentFinder.isDisplayed().then(function(value) {
						return value == true;
					});

				} else {
					return _retryOnErr();
				}
			}, _retryOnErr);

		}, timeout * 1000,
		elementDesc+'::Error waiting for element present:').then(
				function(waitRetValue) {
					return waitRetValue; // usually just `true`
				}, function(err) {
					throw err;
				});
	};
	
	/**
	 * This function is used to wait for element present up to timeout and 
	 * return true/false value based on element availability.
	 * 
	 * @author hemins
	 * @param elmentFinder
	 * @param elementDesc
	 * @param timeout
	 * @returns boolean
	 */
	var browserWaitToLoadElement = function(elmentFinder, elementDesc, timeout) {

		if (!timeout) {
			timeout = 60;
		}

		browser.driver.wait(function() {
				return elmentFinder.isPresent().then(function(present) {
				if (present == true) {
				return elmentFinder.isDisplayed().then(function(value) {
						return value == true;
					});

				} else {
					return _retryOnErr();
				}
			}, _retryOnErr);

		}, timeout * 1000,
		elementDesc+'::Error waiting for element present:').then(
				function(waitRetValue) {
					return waitRetValue; // usually just `true`
				}, function(err) {
					return false;
				});
	};
	
	/**
	 * This function is used to wait for element enabled up to timeout and 
	 * return true/false value based on element availability.
	 * 
	 * @author omprakash
	 * @param elmentFinder
	 * @param elementDesc
	 * @param timeout
	 * @returns boolean
	 */
	this.browserWaitToEnabledElement = function(elmentFinder, elementDesc, timeout) {

		if (!timeout) {
			timeout = 60;
		}

		browser.driver.wait(function() {
				return elmentFinder.isPresent().then(function(present) {
				if (present == true) {
				return elmentFinder.isEnabled().then(function(value) {
						return value == true;
					});

				} else {
					return _retryOnErr();
				}
			}, _retryOnErr);

		}, timeout * 1000,
		elementDesc+'::Error waiting for element enabled:').then(
				function(waitRetValue) {
					return waitRetValue; // usually just `true`
				}, function(err) {
					return false;
				});
	};
	/**
	 * This function is used for element present or not .
	 * 
	 * @author omp
	 * @param elementObject
	 * @returns boolean value
	 */
	this.elementCheck = function(elementObject) {

		return elementObject.isPresent().then(function(present) {
			if (present) {
				return true;
			} else {
				return false;
			}
		});
	};

	/**
	 * This function is used to select drop down option by visible text using
	 * protractor identifier.
	 * 
	 * @author narottamc
	 * @param element
	 * @param item
	 * @param milliseconds
	 */
	this.selectDropdownByText = function(element, item, milliseconds) {
		var desiredOption;
		element.findElements(by.tagName('option')).then(
				function findMatchingOption(options) {
					options.some(function(option) {
						option.getText().then(function doesOptionMatch(text) {
							if (text.indexOf(item) != -1) {
								desiredOption = option;
								return true;
							}
						});
					});
				}).then(function clickOption() {
			if (desiredOption) {
				desiredOption.click();
			}
		});
		if (typeof milliseconds != 'undefined') {
			browser.sleep(milliseconds);
		}
	};

	/**
	 * This function is used to select drop down option by index while using
	 * protractor identifier.
	 * 
	 * @author narottamc
	 * @param element
	 * @param index
	 * @param milliseconds
	 */
	this.selectDropdownByindex = function(element, index, milliseconds) {

		element.findElements(by.tagName('option')).then(function(options) {
			options[index].click();
		});
		if (typeof milliseconds != 'undefined') {
			browser.sleep(milliseconds);
		}
	};

	/**
	 * This function is used to return current day date in given format YYMMDD.
	 * 
	 * @author narottamc
	 * @returns date
	 */
	this.getDateStamp = function() {
		var moment = require('moment');

		return moment().format('YYMMDD');
	};

	/**
	 * This function is used to return current day date in given format
	 * YYMMDDHHmm.
	 * 
	 * @author narottamc
	 * @returns date
	 */
	this.getTimeStamp = function() {
		var moment = require('moment');

		return moment().format('YYMMDDHHmm');
	};

	/**
	 * This function is used to return current day date in given format
	 * DDMMYYYYHHmmSSSS.
	 * 
	 * @author narottamc
	 * @returns date
	 */
	this.getFullDateString = function() {
		var moment = require('moment');
		return moment().format('DDMMYYYYHHmmSSSS');

	};

	/**
	 * This function is used to return current day date in given format
	 * DD/MM/YYYY HH:mm:SS.
	 * 
	 * @author narottamc
	 * @returns date
	 */
	this.getTodayDate = function() {

		var moment = require('moment');
		var todaydate = moment().format('DD/MM/YYYY HH:mm:SS');
		return todaydate;

	};
	/**
	 * This function is used to return current day date in given format
	 * DD/MM/YYYY.
	 * 
	 * @author omprakash
	 * @returns date
	 */
	this.getTodayDateOnly = function() {

		var moment = require('moment');
		var todaydate = moment().format('MM/DD/YYYY');
		return todaydate;

	};
	
	/**
	 * This function is used to return next 30 days date in given format DD/MM/YYYY
	 * 
	 * 
	 * @author omprakash
	 * @returns date
	 */
	this.getMonthDate = function() {
		var moment = require('moment');
		var nextMonthDate = moment().add('day', 30);
		return nextMonthDate.format('MM/DD/YYYY');
	};

	/**
	 * This function is used to return next day date in given format DD/MM/YYYY
	 * HH:mm:SS.
	 * 
	 * @author narottamc
	 * @returns date
	 */
	this.getNextDate = function() {
		var moment = require('moment');
		var Nextdaydate = moment().add('day', 1);
		return Nextdaydate.format('DD/MM/YYYY HH:mm:SS');
	};

	/**
	 * This function is used to upload file.
	 * 
	 * @author narottamc
	 * @param browseButton
	 * @param filetoUpload
	 * @param uploadButton
	 */
	this.uploadFile = function(browseButton, filetoUpload, uploadButton) {
		var path = require('path');
		var absolutePath = path.resolve(__dirname, filetoUpload);
		browseButton.sendKeys(absolutePath);
		if (uploadButton) {
			uploadButton.click();
		}
	};

	/**
	 * This method is used to search in text box either using search button or
	 * Enter key .
	 * 
	 * @author narottamc
	 * @param element
	 * @param searchText
	 * @param searchButton
	 */
	 var searchBox = function(element, searchText, searchButton) {
		element.clear();
		element.sendKeys(searchText);
		if (searchButton) {
			searchButton.click();
		} else {
			element.sendKeys(protractor.Key.ENTER);
		}
		browser.waitForAngular();
		
	};
	/**
	 * This method is use to return element based on search.
	 * 
	 * @author vishalbha
	 * @param searchName
	 * @return element
	 */
	var advertiserLinkLocation = function(searchName) {
		return element(by.linkText(searchName));
	};
	
	/**
	 * This method is use to search element and return true if element find in search list
	 * otherwise return null;
	 * 
	 * @author hemins
	 * @param advertiserName
	 * @param elementName
	 * @returns true/null
	 */
	this.findList = function(elementSearchbox,elementName) {
		browser.waitForAngular();
		searchBox(elementSearchbox, elementName);
		browserWaitToLoadElement(element(by.linkText(elementName)),'elementName',5);
	    return advertiserLinkLocation(elementName).isPresent().then(function(value){
	    	logger.info("Searching for '"+elementName+"'");
	    	if(value){
	    		logger.info("'"+elementName +"' is found in search list.");
	    		return true;
	    	} else if(!value){
	    		logger.info("'"+elementName +"' is not found in search list.");
	    		return null;
	    	}
	    });
	};
	
	/**
	 * This method used to accept confirm Alert.
	 * 
	 * @author vishalbha
	 */
	this.confirmAlert = function() {
			browser.switchTo().alert().then(function(alert) {
			alert.accept();
		}, function(err) {
			logger.error(err);
		});
	};

	/**
	 * This function is used to switch on frame.
	 * 
	 * @author vishalbha
	 */
	
	this.switchToFrame = function(element) {
		browser.waitForAngular();
		browser.switchTo().frame(element);
	};

	/**
	 * This function is used to switch on new Window.
	 * 
	 * @author vishalbha
	 */
	var parentHandle = {};
	var popUpHandle = {};
	this.switchToNewWindow = function() {
		parentHandle = browser.getWindowHandle();
		browser.getAllWindowHandles().then(function(handles) {
			popUpHandle = handles[1];
			browser.switchTo().window(popUpHandle);
		});
	};

	/**
	 * This function is used to switch on Old Window.
	 * 
	 * @author vishalbha
	 */
	this.switchToMainWindow = function() {
		browser.close();
		browser.getAllWindowHandles().then(function(handles) {
			browser.switchTo().window(parentHandle);
		});
	};
	
	/**
	 * This function is used to set isAngularSite true or false as per argumrnt pass.
	 * 
	 * @param console
	 * @author narottamc
	 */
	this.isAngularPage=function(console)
	{
		if(console.toUpperCase()=='NEW'){
			   isAngularSite(true);
				}
				else{
					isAngularSite(false);
		         }	
	       };
};

module.exports = new utility();