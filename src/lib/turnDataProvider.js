/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

var turnDataProvider = function() {
	var fs = require('fs');
	var nconf=require('nconf');
	
	
	/**
	 * This method is used to read data from Json file using language and dataname filters
	 * 
	 * @author omp
	 * @param dataProviderFile
	 * @param language
	 * @param dataName
	 * @returns result object
	 */
	this.getDataProvider = function(dataProviderFile,environment,language,dataName){
		var data=null,result=null;
		data = JSON.parse(fs.readFileSync(dataProviderFile, 'utf8'));
		result=data[environment][language][dataName];
		return result;
	};
	
	
	/**
	 * This method is used to write Json data to Json file using language and dataname 
	 * filters with the help of key & value pair.
	 * 
	 * @author narottamc
	 * @param dataProviderFile
	 * @param language
	 * @param dataName
	 * @param key
	 * @param value
	 */
	this.writeDataProvider=function(dataProviderFile,environment,language,dataName,key,value)
	{
		nconf.argv()
	    .env()
	     .file({ file: dataProviderFile});
		
		nconf.set(environment+':'+language+':'+dataName+':'+key,value);
		
		
		 nconf.save(function (err) {
			   fs.readFile(dataProviderFile,'utf-8', function (err, data) {
				 if (err) throw err;
			    });
			  });
	};
		
};

module.exports = new turnDataProvider();
