/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

var assert = function() {
 var loggerObj = require('./logger.js');
 var logger = loggerObj.loggers("assert");
	/**
	 * This method is used to assert true value.
	 * 
	 * @author hemins
	 * @param trueValue
	 * @param msg
	 */
	this.assertTrue = function(trueValue, msg) {
		trueValue.then(function(value) {
			if (!value) {
				logger.error(msg);
			}
		});
		expect(trueValue).toBe(true);
	};

	/**
	 * This method is used to assert false value.
	 * 
	 * @author hemins
	 * @param falseValue
	 * @param msg
	 */
	this.assertFalse = function(falseValue, msg) {
		falseValue.then(function(value) {
			if (value) {
				logger.error(msg);
			}
		});
		expect(falseValue).toBe(false);
	};

	/**
	 * This method is used to assert null value.
	 * 
	 * @author hemins
	 * @param nullValue
	 * @param msg
	 */
	this.assertNull = function(nullValue, msg) {
		nullValue.then(function(value) {
			if (value != null) {
				logger.error(msg);
			}
		});
		expect(nullValue).toBeNull();
	};

	/**
	 * This method is used to assert not null value.
	 * 
	 * @author hemins
	 * @param nullValue
	 * @param msg
	 */
	this.assertNotNull = function(nullValue, msg) {
		nullValue.then(function(value) {
			if (value == null) {
				logger.error(msg);
			}
		});
		expect(nullValue).not.toBeNull();
	};


	/**
	 * This method is used to assert undefined value.
	 * 
	 * @author hemins
	 * @param undefinedValue
	 * @param msg
	 */
	this.assertUndefined = function(undefinedValue, msg) {
		undefinedValue.then(function(value) {
			if (value == undefined) {
				logger.error(msg);
			}
		});
		expect(undefinedValue).toBe(undefined);
	};

	/**
	 * This method is used to compare string are equal or not.
	 * 
	 * @author hemins
	 * @param actual
	 * @param expected
	 * @param msg
	 */
	this.assertEqual = function(expected, actual, msg) {
		expected.then(function(expectedValue){
				if(expectedValue != actual){
					logger.error(msg);
				}
			});

		expect(expected).toEqual(actual);

	};

	/**
	 * This method is used to compare string are equal or not.
	 * 
	 * @author hemins
	 * @param actual
	 * @param expected
	 * @param msg
	 */
	this.assertNotEqual = function(expected, actual, msg) {

		expected.then(function(expectedValue){
			if(expectedValue == actual){
				logger.error(msg);
			}
		});

		expect(expected).not.toEqual(actual);

	};
	
	/**
	 * This method is used to assert string contains given value.
	 * 
	 * @author hemins
	 * @param actual
	 * @param expected
	 * @param msg
	 */
	this.assertContain = function(expected,actual,msg) {
		expected.then(function(expectedValue){
			if(actual.indexOf(expectedValue) == -1){
				logger.error(msg);
			}
		});
		expect(expected).toContain(actual);   
	};
	
	/**
	 * This method is used to assert string not contains given value.
	 * 
	 * @author hemins
	 * @param actual
	 * @param expected
	 * @param msg
	 */
	this.assertNotContain = function(expected,actual,msg) {
		expected.then(function(expectedValue){
			if(actual.indexOf(expectedValue) != -1){
				logger.error(msg);
			}
		});
		
		expect(expected).not.toContain(actual);   
	};
	
	/**
	 * This method is used to assert list contains given value.
	 * 
	 * @author narottamc
	 * @param actual
	 * @param expected
	 * @param msg
	 */
	this.assertListContain=function(expected,actual,msg){
     var matchFound='';
		for(var i=0;i<expected.length;i++){
			if(expected[i]==actual){
				matchFound='YES';
				}
			}
		if(matchFound !='YES'){
			logger.error(msg);
			}
		expect(expected).toContain(actual);   
	};
	
	/**
	 * This method is used to assert list contains given value.
	 * 
	 * @author narottamc
	 * @param actual
	 * @param expected
	 * @param msg
	 */
	this.assertListNotContain=function(expected,actual,msg){
		var matchFound='';
		for(var i=0;i<expected.length;i++){
			if(expected[i]==actual){
				matchFound='YES';
				}
			}
		if(matchFound =='YES'){
			logger.error(msg);
			}
		expect(expected).not.toContain(actual);  
		
	};
	
	/**
	 * This method is used to verify true value.
	 * 
	 * @author hemins
	 * @param trueValue
	 * @param msg
	 */
	this.verifyTrue = function(trueValue, msg) {
		trueValue.then(function(value) {
			if (!value) {
				logger.warn(msg);
			}
		});
	};

	/**
	 * This method is used to verify false value.
	 * 
	 * @author hemins
	 * @param falseValue
	 * @param msg
	 */
	this.verifyFalse = function(falseValue, msg) {
		falseValue.then(function(value) {
			if (value) {
				logger.warn(msg);
			}
		});
	};

	/**
	 * This method is used to verify null value.
	 * 
	 * @author hemins
	 * @param nullValue
	 * @param msg
	 */
	this.verifyNull = function(nullValue, msg) {
		nullValue.then(function(value) {
			if (value != null) {
				logger.warn(msg);
			}
		});
	};

	/**
	 * This method is used to verify not null value.
	 * 
	 * @author hemins
	 * @param nullValue
	 * @param msg
	 */
	this.verifyNotNull = function(nullValue, msg) {
		nullValue.then(function(value) {
			if (value == null) {
				logger.warn(msg);
			}
		});
	};


	/**
	 * This method is used to verify undefined value.
	 * 
	 * @author hemins
	 * @param undefinedValue
	 * @param msg
	 */
	this.verifyUndefined = function(undefinedValue, msg) {
		undefinedValue.then(function(value) {
			if (value == undefined) {
				logger.warn(msg);
			}
		});
	};

	/**
	 * This method is used to compare string are equal or not.
	 * 
	 * @author hemins
	 * @param actual
	 * @param expected
	 * @param msg
	 */
	this.verifyEqual = function(expected, actual, msg) {

			expected.then(function(expectedValue){
				if(expectedValue != actual){
					logger.warn(msg);
				}
			});

	};

	/**
	 * This method is used to compare string are equal or not.
	 * 
	 * @author hemins
	 * @param actual
	 * @param expected
	 * @param msg
	 */
	this.verifyNotEqual = function(actual, expected, msg) {

		actual.then(function(actualValue){
			expected.then(function(expectedValue){
				if(actualValue == expectedValue){
					logger.warn(msg);
				}
			});
		});
	};
	
	/**
	 * This method is used to assert string contains given value.
	 * 
	 * @author hemins
	 * @param actual
	 * @param expected
	 * @param msg
	 */
	this.verifyContain = function(actual,expected,msg) {
		actual.then(function(actualValue){
			expected.then(function(expectedValue){
				if(actualValue.indexOf(expectedValue) == -1){
					logger.warn(msg);
				}
			});
		}); 
	};
	
	/**
	 * This method is used to assert string not contains given value.
	 * 
	 * @author hemins
	 * @param actual
	 * @param expected
	 * @param msg
	 */
	this.verifyNotContain = function(actual,expected,msg) {
		actual.then(function(actualValue){
			expected.then(function(expectedValue){
				if(actualValue.indexOf(expectedValue) != -1){
					logger.warn(msg);
				}
			});
		});
	};
};
module.exports = new assert()