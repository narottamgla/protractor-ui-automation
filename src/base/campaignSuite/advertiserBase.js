var advertiserBase = function() {
	var advertiserPage = require('../../pageObject/campaignSuite/advertiserPO.js');
	var utility=require('../../lib/utility.js');
	var turnDataFilter=require('../../lib/turnDataProvider.js');
	
	 protract=protractor.getInstance();
	
	this.gotoAdvertiser = function() {
		console.log("goto advertiser");
		utility.browserWait(advertiserPage.advertiserstabLink,'Adevertisers tab link ');	
		advertiserPage.advertiserstabLink.click();
					
		};
	

	this.redirectToNewadvertiser = function() {
		console.log("new advertiser");
		utility.browserWait(advertiserPage.actionDropdown,'Action dropdown');		
		utility.browserWaitforseconds(1);
		advertiserPage.actionDropdown.click().then(function(){
			console.log("new....");
			utility.browserWait(advertiserPage.newUnderAction,'New Action');	
		
				advertiserPage.newUnderAction.click().then(function(){
					utility.browserWait(advertiserPage.newAdvertiser,'New Advertiser tab');
				
					advertiserPage.newAdvertiser.click();
					
					console.log("After Click adv Link");
					});
				});	
		
		browser.waitForAngular();
		};
		
		
		this.clickNewAdvertiserPageCancelButton = function(){
			advertiserPage.advActionDropDown.click();
			advertiserPage.newAdvertiserActionLink.click();
			advertiserPage.advCancelButton.click();
			
		};
		
		this.createNewadvertiser = function(advdata) {
			utility.browserWait(advertiserPage.marketnameDropdown);

		this.createNewadvertiser = function(Path,lng,dataname) {
			var advdata = turnDataFilter.getDataProvider(Path,lng,dataname);
			var purl=advdata.primaryurl+'1233';
			turnDataFilter.WriteDataProvider(Path,lng,dataname,'primaryurl',purl);
			
		    utility.browserWait(advertiserPage.marketnameDropdown,'Market dropdown');

			advertiserPage.marketnameDropdown.sendKeys(advdata.Market);
			//utility.browserWait(2);
			utility.browserWaitforseconds(2);
			
			advertiserPage.accountnameDropdown.sendKeys(advdata.accountname);	
			
			
			//console.log("updated adv Name"+advdata.advertiserName);
			advertiserPage.newadertiserName.sendKeys(advdata.advertiserName);
			advertiserPage.primaryUrl.sendKeys(advdata.primaryurl);
			advertiserPage.tpaAdvName.sendKeys(advdata.TPAadverstiser);
			
			
			//var test=protract.findElement(protractor.By.model('vo.localCurrencyId'));
		  //  utility.selectDropdownByText(test,advdata.currency,1000);
			
			//advertiserPage.currencyDropdown.$('[value='+advdata.currency+']').click();
		    advertiserPage.currencyDropdown.sendKeys('Malaysian Ringgit');
		 
			
			utility.browserWait(advertiserPage.saveButton,'Adevertiser Save button');
		//	advertiserPage.saveButton.click();
			
			protract.waitForAngular();
			
		};
		
		
		this.searchAdvertiser = function(advdata) {
			utility.browserWait(advertiserPage.searchtextBox,'Advertiser Serach Box');
			advertiserPage.searchtextBox.sendKeys(advdata.advertiserName);
			utility.browserWait(advertiserPage.searchgoButton);
			advertiserPage.searchgoButton.click();
			//utility.browserWait(3);
			
			
		};
	
};



module.exports = new advertiserBase()