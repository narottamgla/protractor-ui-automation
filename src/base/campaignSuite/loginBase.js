var loginBase = function() {
	var loginPage = require('../../pageObject/campaignSuite/loginPO.js');

	this.login = function(logindata) {
		loginPage.username.sendKeys(logindata.username);
		loginPage.password.sendKeys(logindata.password);
		loginPage.loginbutton.click();
	};

};

module.exports = new loginBase()