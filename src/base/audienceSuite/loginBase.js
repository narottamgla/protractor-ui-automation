var loginBase = function() {
	var loginPO = require('../../pageObject/audiencesuite/loginPO.js');
	var utilityObj=require('../../lib/utility.js');
	var loggerObj = require('../../lib/logger.js');
	var logger=loggerObj.loggers("loginBase");
	var assertsObj = require('../../lib/assert.js');
	
	/**
	 * This method is use to login.
	 * 
	 * @author hemins
	 * @param logindata
	 */
	this.login = function(logindata) {
		browser.manage().deleteAllCookies();
		utilityObj.browserWaitforseconds(3);
		if(!loginPO.isUserNameTextBoxPresent()){
			driver.navigate().refresh();
			utilityObj.browserWaitforseconds(3);
			if(!loginPO.isUserNameTextBoxPresent()){
				driver.navigate().refresh();
				utilityObj.browserWaitforseconds(3);
			}
		}
		loginPO.enterUserName(logindata.username);
		loginPO.enterPassword(logindata.password);
		loginPO.clickLoginButton();
		browser.waitForAngular();		
	};
	
	/**
	 * This method is use to return Username textbox is present or not in login page.
	 * 
	 * @author hemins
	 * @returns boolean
	 */
	this.isUserNamePresent = function(){
		return loginPO.isUserNameTextBoxPresent();
	};
	
	/**
	 * This method is use to redirect to audience suite product.
	 * 
	 * @author omp
	 */
	this.redirectToAudienceSuite=function(){
		utilityObj.elementCheck(loginPO.returnAudienceSuitProduct()).then(function(val){
			if(val){
			
			}
			else{
				utilityObj.elementCheck(loginPO.returnCampaignSuitProduct()).then(function(value){
					if(value){
						utilityObj.selectProduct(loginPO.returnCampaignSuitProduct(),loginPO.returnAudienceSuiteLink());
					}	
				});
			}
		});
	};
	
	/**
	 * This method is use to return Audience Suite Link is present or not.
	 * 
	 * @author hemins
	 * @returns boolean
	 */
	this.checkAudienceSuite = function(){
		return loginPO.isAudienceSuiteLinkPresent().then(function(value){
			if(value){
				logger.info("Redirect to Audience Suite Project.");
				return value;
			}else{
				logger.info("Not redirect to Audience Suite Project.");
				return value;
			}
		});
		return loginPO.isAudienceSuiteLinkPresent();
	};

	/**
	 * This method is use to redirect to Campaign suite product.
	 * 
	 * @author narottamc
	 */
	this.redirectToCampaignSuite=function(){
		utilityObj.elementCheck(loginPO.returnAudienceSuitProduct()).then(function(val){
			if(val){
				utilityObj.selectProduct(loginPO.returnAudienceSuitProduct(),loginPO.returnCampaignSuiteLink());
				utilityObj.browserWaitforseconds(4);
				loginPO.warningPresent().then(function(resultVal){
				if(resultVal){
					loginPO.warningClose();
				}	
				});
			}
			else{
				
				};
			});
		};
		
	/**
	 * This method is use to return Campaign Suite Link is present or not.
	 * 
	 * @author narottamc
	 * @returns boolean
	 */
	this.checkCampaignSuite = function(){
		return loginPO.isCampaignSuiteLinkPresent().then(function(value){
			if(value){
				logger.info("Redirect to Campaign Suite Project.");
				return value;
			}else{
				logger.info("Not redirect to Campaign Suite Project.");
				return value;
			}
		});
		return loginPO.isCampaignSuiteLinkPresent();
	};
	/**
	 * This method is use to check logout UserBox is present or not on Audience Suite/Campaign page.
	 * 
	 * @author narottamc
	 * @param user
	 * @param projectName
	 * @returns boolean
	 */
	this.isLogoutUserBoxPresent = function(user,projectName) {
		return loginPO.isLogoutUserBoxPresent(projectName).then(function(value){
			if(value){
				logger.info("Login Sucessfully with "+user+" user.");
				return value;
			}else {
				logger.info("Login Unsucessfully with "+user+" user.");
				return value;
			}
		});
	};

	/**
	 * This method is use to logout from Audience Suite/Campaign Suite Project.
	 * 
	 * @author narottamc
	 * @param user
	 * @param projectName
	 */
	this.logout = function(user,projectName) {
		loginPO.isLogoutLinkPresent().then(function(value){
			if(value){
				loginPO.clickLogoutLink();
				loginPO.isLoginLinkPresent().then(function(){
					loginPO.clickLoginLink();
					logger.info("Logout Sucessfully with "+user+".");
				});
			}
			else{
				loginPO.isLogoutUserBoxPresentNew(projectName).then(function(){
					loginPO.clickLogoutUserBox(projectName);
					loginPO.clickLogoutLink();
					loginPO.isLoginLinkPresent().then(function(){
						loginPO.clickLoginLink();
						logger.info("Logout Sucessfully with "+user+".");
					});			
				});
			}
		});
		browser.manage().deleteAllCookies();
	};

};
module.exports = new loginBase();