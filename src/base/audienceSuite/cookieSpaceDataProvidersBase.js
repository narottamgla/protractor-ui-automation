var cookieSpaceDataProvidersBase = function(){
	
	var cookieSpaceDataProvidersPO = require('../../pageObject/audiencesuite/cookieSpaceDataProvidersPO.js');
	var utilityObj = require('../../lib/utility.js');
	var turnDataFilterObj = require('../../lib/turnDataProvider.js');
	var pathObj  =require( '../../testData/audienceSuite/path.js');
	var assertsObj = require('../../lib/assert.js');
	var loggerObj = require('../../lib/logger.js');
	var logger = loggerObj.loggers("cookieSpaceDataProvidersBase");
	
	/**
	 * This Method is used for redirection to Data Provider Tab.
	 * 
	 *  @author hemins
	 */
	this.redirectToCookieSpaceDataProvidersTab = function(){
		cookieSpaceDataProvidersPO.clickDataMenuLink();
		cookieSpaceDataProvidersPO.clickCookieSpaceDataProviderSubLink();
		assertsObj.verifyTrue(verifyPageHeaderText("Data Provider"),"Not redirect to Data Provider Tab.");
		verifyPageHeaderText("Data Provider").then(function(value){
			 if(value){
				 logger.info("Redirected to Data Provider Page.");
			 }
		 });
	};
	
	/**
	 * This method is use to return element list in new console page.
	 * 
	 * @author narottamc
	 */
	this.returnElementList=function(){
		return cookieSpaceDataProvidersPO.dataProvidersElementList();
	};
	
	/**
	 * This method is used check page header text of Data Provider page and match page
	 * header text and return true value if page header text is Data Provider otherwise 
	 * return false value.
	 * 
	 * @author hemins
	 * @returns boolean
	 */
	var verifyPageHeaderText = function(name){
		return cookieSpaceDataProvidersPO.getCookieSpaceDataProviderHeaderText().then(function(value){
			if(value == name){
				return true;
			}else{
				return false;
			}
		});
	};
	
	/**
	 * This method is use to Click on New Data Provider and click Cancel button on
	 * create Cookie Space Data Provider page.
	 * 
	 * @author hemins
	 */
	this.newCookieSpaceDataProviderWithCancelButton = function(advHeaderText) {
		cookieSpaceDataProvidersPO.getCookieSpaceDataProviderHeaderText().then(function() {
			logger.info("Clicking cancel button of Action->new Cookie Space Data Provider page.");
		});
		cookieSpaceDataProvidersPO.clickActionDropDown();
		cookieSpaceDataProvidersPO.clickNewDataProviderActionLink();
		utilityObj.browserWaitforseconds(3);
		isAngularSite(false);
		cookieSpaceDataProvidersPO.clickCancelButton();
		utilityObj.browserWaitforseconds(8);
		//assertsObj.verifyTrue(verifyPageHeaderText("Data Provider"),"Data Provider header text is not found on Cookie Space Data Provider page.");
	};
	
	/**
	 * This method is use to create Cookie Space Data Provider.
	 * 
	 * @author hemins
	 * @param cookieSpaceDataProviderTestData
	 * @param cookieSpaceDataProviderName
	 * @param testDataSetName
	 */
	this.createCookieSpaceDataProvider = function(cookieSpaceDataProviderTestData, cookieSpaceDataProviderName,testDataSetName){
		cookieSpaceDataProvidersPO.isNewDataProviderLinkPresent().then(function(){
			logger.info("Creating Cookie Space Data Provider.");
		});
		cookieSpaceDataProvidersPO.clickOnNewDataProviderLink();
		isAngularSite(false);
		cookieSpaceDataProvidersPO.selectCookieSpace(cookieSpaceDataProviderTestData.cookieSpace);
		cookieSpaceDataProvidersPO.enterCookieSpaceDataProviderName(cookieSpaceDataProviderName);
		utilityObj.uploadFile(cookieSpaceDataProvidersPO.returnLogoBrowserButton(),pathObj.getLogoPath());
		cookieSpaceDataProvidersPO.enterNotes(cookieSpaceDataProviderTestData.notes);
		cookieSpaceDataProvidersPO.clickSave();
		
		turnDataFilterObj.writeDataProvider(pathObj.getCookieSpaceDataProviderTestDataFilePath(), browser.env,browser.language,testDataSetName, 'createdCookieSpaceDataProvider', cookieSpaceDataProviderName);
		cookieSpaceDataProvidersPO.returnSearchElement().isPresent().then(function(){
			assertsObj.verifyTrue(verifyPageHeaderText("Data Provider"),"Cookie Space Data Provider header text is not found on Cookie Space Data Provider page.");
			logger.info("Created Cookie Space Data Provider.");
		});
		
	};
	
	/**
	 * This method is use to edit Cookie Space Data Provider.
	 * 
	 * @author hemins
	 * @param cookieSpaceDataProviderTestData
	 * @param cookieSpaceDataProviderName
	 * @param testDataSetName
	 */
	this.editCookieSpaceDataProvider = function(cookieSpaceDataProviderTestData, cookieSpaceDataProviderName,testDataSetName){
		cookieSpaceDataProvidersPO.getCookieSpaceDataProviderHeaderText().then(function(){
			logger.info("Editing Cookie Space Data Provider.");
		});
		cookieSpaceDataProvidersPO.clickIconMenu();
		cookieSpaceDataProvidersPO.clickEditLink();
		isAngularSite(false);
		cookieSpaceDataProvidersPO.enterCookieSpaceDataProviderName(cookieSpaceDataProviderName);
		cookieSpaceDataProvidersPO.enterNotes(cookieSpaceDataProviderTestData.editedNotes);
		cookieSpaceDataProvidersPO.clickSave();
		turnDataFilterObj.writeDataProvider(pathObj.getCookieSpaceDataProviderTestDataFilePath(), browser.env,browser.language,testDataSetName, 'updatedCookieSpaceDataProvider', cookieSpaceDataProviderName);
		cookieSpaceDataProvidersPO.returnSearchElement().isPresent().then(function(){
			assertsObj.verifyTrue(verifyPageHeaderText("Data Provider"),"Cookie Space Data Provider header text is not found on Cookie Space Data Provider page.");
			logger.info("Edited Cookie Space Data Provider.");
		});
		
	};
	
	/**
	 * This method is use to return Cookie Space Data Provider Name of view Cookie Space Data Provider page.
	 * 
	 * @author hemins
	 * @param cookieSpaceDataProviderName
	 */
	this.returnCookieSpaceDataProviderNameOfViewPage = function(cookieSpaceDataProviderName){
		isAngularSite(false);
		return cookieSpaceDataProvidersPO.getExpectedDataProviderName().then(function(value){
			logger.info("Viewing Cookie Space Data Provider.");
			assertsObj.verifyEqual(cookieSpaceDataProvidersPO.getExpectedDataProviderName(),cookieSpaceDataProviderName,
			"Not match cookie space data provider name in View Cookie Space Data Provider page.");
			logger.info("Viewed Cookie Space Data Provider.");
			console.log(value);
			return value;
		});	
	};
	
	/**
	 * This method is use to redirect view data provider page.
	 * 
	 * @author hemins
	 */
	this.redirectToViewCookieSpaceDataProviderPage = function() {
		cookieSpaceDataProvidersPO.clickIconMenu();
			logger.info("Redirecting to view data provider page.");
			cookieSpaceDataProvidersPO.clickViewLink();
	};
	
	/**
	 * This Method is use to delete Cookie Space Data Provider.
	 * 
	 * @author hemins
	 * @param cookieSpaceDataProviderTestData
	 */
	this.deleteCookieSpaceDataProvider = function(cookieSpaceDataProviderTestData){
		cookieSpaceDataProvidersPO.getCookieSpaceDataProviderHeaderText().then(function(value){
			logger.info("Deleting Cookie Space Data Provider.");
			assertsObj.verifyTrue(verifyPageHeaderText("Data Provider"),"Data Provider header text is not found on Cookie Space Data Provider page.");
			cookieSpaceDataProvidersPO.clickIconMenu();
			cookieSpaceDataProvidersPO.clickDeleteLink();
			cookieSpaceDataProvidersPO.clickConfirmDeleteAlert();
		});
		browser.waitForAngular();
		cookieSpaceDataProvidersPO.getCookieSpaceDataProviderHeaderText().then(function(value){
			logger.info("Deleted Cookie Space Data Provider.");
		});
	};
		
	/**
	 * This method is use to click on Action -> Show Deleted cookie space data provider
	 * 
	 * @author hemins
	 */
	this.clickShowCookieSpaceDeletedDataProvider = function() {
		browser.waitForAngular();
		cookieSpaceDataProvidersPO.isActionDropDownPresent().then(function(){
			assertsObj.verifyTrue(verifyPageHeaderText("Data Provider"),"Data Provider header text is not found on Cookie Space Data Provider page.");
			logger.info("Clicking on Show Deleted Cookie Space Data Provider.");
			cookieSpaceDataProvidersPO.clickActionDropDown();
		});
		browser.waitForAngular();
		cookieSpaceDataProvidersPO.clickShowDeleted();
	};
	
	/**
	 * This method is use to redirect to view audit log page.
	 * 
	 * @author hemins
	 */
	this.gotoViewAuditLog = function() {
		browser.waitForAngular();
		cookieSpaceDataProvidersPO.isIconMenuPresent().then(function(){
			logger.info("Redirect to View Audit Log page of Advertiser.");
			cookieSpaceDataProvidersPO.clickIconMenu();			
			cookieSpaceDataProvidersPO.clickAuditLogLink();
			browser.waitForAngular();
			utilityObj.switchToNewWindow();
			assertsObj.verifyTrue(verifyPageHeaderText('Audit Log'),'Audit Log page header text is not found.');
		});				
	};
	
	/**
	 * This method is use to search field name in view audit log page.
	 * 
	 * @author hemins
	 * @param searchField
	 */
	this.searchAuditLog = function(searchField) {
		
		cookieSpaceDataProvidersPO.isAuditLogSearchBoxPresent().then(function(){
			logger.info("Searching for field name in View Audit Log Page.");
			assertsObj.verifyTrue(verifyPageHeaderText('Audit Log'),'Audit Log page header text is not found in Cookie Space Data Provider\'s Audit Log page.');
			cookieSpaceDataProvidersPO.searchAuditLog(searchField);
		});
	};
	
	/**
	 * This method is use to get field name from view audit log page.
	 * 
	 * @author hemins
	 * @returns String
	 */
	this.getFieldNameValue = function() {
		return cookieSpaceDataProvidersPO.getFieldNameText().then(function(value){
			logger.info("Getting field name value from Audit log page.");
			return value;
		});		
	};
	
	/**
	 * This method is use to click on Action -> Hide Deleted Cookie Space Data Provider
	 * 
	 * @author hemins
	 */
	this.clickHideDeletedCookieSpaceDataProvider = function(dataProviderType) {
		browser.waitForAngular();
		cookieSpaceDataProvidersPO.isActionDropDownPresent().then(function(){
			assertsObj.verifyTrue(verifyPageHeaderText('Data Provider'),'Data Provider page header text is not found in Cookie Space Data Provider Page.');
			logger.info("Clicking on Hide Deleted Cookie Space Data Provider.");
			cookieSpaceDataProvidersPO.clickActionDropDown();
		});

		cookieSpaceDataProvidersPO.clickHideDeleted();
	};
	
	/**
	 *  This method used for sorting by data provider name.
	 * 
	 * @author hemins
	 */	
	this.sortByCookieSpaceDataProviderName= function(){
		cookieSpaceDataProvidersPO.clickDataProviderNameColumn();
		logger.info("Click on Cookie Space Data Provider Name column for sorting.");
	};
	
	/**
	 * This method is used to verify that records are sort by Name or not.
	 * 
	 * @author hemins
	 * @return String
	 */
	this.assertNameColumn = function() {
		assertsObj.verifyNotNull(cookieSpaceDataProvidersPO.returnReverseDataProviderName(),"Not Find column sorting with Cookie Space Data Provider Name.");
		return cookieSpaceDataProvidersPO.returnReverseDataProviderName();
	};
	
	/**
	 * This method is use to return search Textbox element of Cookie Space Data Provider tab.
	 * 
	 * @author hemins
	 * @returns Element
	 */
	this.returnSearchElement = function(){
		return cookieSpaceDataProvidersPO.returnSearchElement();
	};
	
};
module.exports = new cookieSpaceDataProvidersBase();