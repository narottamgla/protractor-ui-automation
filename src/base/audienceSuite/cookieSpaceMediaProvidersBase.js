var cookieSpaceMediaProvidersBase = function() {
		
	var cookieSpaceMediaProvidersPO = require('../../pageObject/audiencesuite/cookieSpaceMediaProvidersPO.js');
	var utilityObj = require('../../lib/utility.js');
	var turnDataFilterObj = require('../../lib/turnDataProvider.js');
	var pathObj  =require( '../../testData/audienceSuite/path.js');
	var assertsObj = require('../../lib/assert.js');
	var loggerObj = require('../../lib/logger.js');
	var logger = loggerObj.loggers("cookieSpaceMediaProvider");
	
	/**
	 * This method is used check page header text of Cookie Space Media Provider page and match page
	 * header text and return true value if page header text is Media Provider otherwise 
	 * return false value.
	 * 
	 * @author hemins
	 * @returns boolean
	 */
	var verifyPageHeaderText = function(name){
		return cookieSpaceMediaProvidersPO.getPageHeaderText().then(function(value){
			if(value == name){
				return true;
			}else{
				return false;
			}
		});
	};
	
	/**
	 * This method is use to return element list in new console page.
	 * 
	 * @author hemins
	 */
	
	this.returnElementList=function()
	{
		return cookieSpaceMediaProvidersPO.csmpElementList();
	};
	
	
	/**
	 * This Method is used for redirection to Cookie Space Media Provider Tab.
	 * 
	 *  @author hemins
	 */
	this.redirectToCookieSpaceMediaProvidersTab = function(){
		cookieSpaceMediaProvidersPO.clickMediaMenuLink();
		browser.waitForAngular();
		cookieSpaceMediaProvidersPO.clickMediaProviderSubMenuLink();
		assertsObj.verifyTrue(verifyPageHeaderText("Media Provider"),"Not redirect to Cookie Space Media Provider Tab.");
		verifyPageHeaderText("Media Provider").then(function(value){
			 if(value){
				 logger.info("Redirected to Cookie Space Media Provider Page.");
			 }
		 });
	};
	/**
	 * This method is use to click Action -> New Media Provider and click Cancel button.
	 * 
	 * @author hemins
	 * @param advHeaderText
	 */
	this.newMediaProviderWithCancelButton = function(advHeaderText) {
		cookieSpaceMediaProvidersPO.getPageHeaderText().then(function() {
			logger.info("Clicking cancel button of Action->new advertiser page.");
		});
		utilityObj.browserWaitforseconds(2);
		cookieSpaceMediaProvidersPO.clickActionDropDown();
		cookieSpaceMediaProvidersPO.clickNewMediaProviderLink();
		cookieSpaceMediaProvidersPO.clickCancelBtn();
		assertsObj.verifyTrue(verifyPageHeaderText("Media Provider"),"Media Provider header text is not found on Cookie Space Media Providers page.");
	};
	
	/**
	 * This method is use to Create New Cookie Space Media Provider.
	 * 
	 * @author hemins
	 * @param mediaProviderData
	 * @param mediaProviderName
	 * @param testDataSetName
	 */
	this.createNewCookieSpaceMediaProviders = function(mediaProviderData, mediaProviderName,testDataSetName) {
		cookieSpaceMediaProvidersPO.getPageHeaderText().then(function() {
			logger.info("Creating Cookie Space Media Providers("+mediaProviderName+").");
			cookieSpaceMediaProvidersPO.clickNewMediaProviderPlusLink();
			isAngularSite(false);
			cookieSpaceMediaProvidersPO.selectCookieSpace(mediaProviderData.cookieSpace);
			cookieSpaceMediaProvidersPO.enterMediaProviderName(mediaProviderName);
			utilityObj.uploadFile(cookieSpaceMediaProvidersPO.selectLogo(),pathObj.getMediaProviderLogoPath());
			cookieSpaceMediaProvidersPO.enterNotes(mediaProviderData.notes);
			cookieSpaceMediaProvidersPO.clickSaveBtn();
			turnDataFilterObj.writeDataProvider(pathObj.getCookieSpaceMediaProviderTestDataFilePath(), browser.env, browser.language,testDataSetName, 'createdMediaProviderName', mediaProviderName);
			cookieSpaceMediaProvidersPO.getPageHeaderText().then(function() {
				assertsObj.verifyTrue(verifyPageHeaderText("Media Provider"),"Media Provider header text is not found on Cookie Space Media Providers page.");
				logger.info("Created Cookie Space Media Providers("+mediaProviderName+").");
			});
		});
	};
	
	/**
	 * This method is use to Edit created Cookie Space Media Provider.
	 * 
	 * @author hemins
	 * @param mediaProviderTestData
	 * @param updatedMediaProviderName
	 * @param testDataSetName
	 */			
	this.editCookieSpaceMediaProvider=function(mediaProviderTestData,updatedMediaProviderName,testDataSetName){
		cookieSpaceMediaProvidersPO.isIconMenuPresent().then(function() {
			logger.info("Redirect to Edit Media Provider("+updatedMediaProviderName+").");
			cookieSpaceMediaProvidersPO.clickIconMenu();
			cookieSpaceMediaProvidersPO.clickEditLink();
			cookieSpaceMediaProvidersPO.enterMediaProviderName(updatedMediaProviderName);
			cookieSpaceMediaProvidersPO.clickSaveBtn();
			assertsObj.verifyTrue(verifyPageHeaderText("Media Provider"),
					"Media Provider header text is not found on Cookie Space Media Provider page.");	
		});
		cookieSpaceMediaProvidersPO.getPageHeaderText().then(function() {
			logger.info("Edited created Media Provider("+updatedMediaProviderName+").");
			turnDataFilterObj.writeDataProvider(pathObj.getCookieSpaceMediaProviderTestDataFilePath(), browser.env, browser.language,testDataSetName, 'updatedMediaProviderName', updatedMediaProviderName);
		});
	};
	
	/**
	 * This method is use to Delete Cookie Space Media Provider for Turn and non Turn Market.
	 * 
	 * @author hemins
	 * @param mediaProviderTestData
	 */
	this.deleteCookieSpaceMediaProvider=function(mediaProviderTestData){
		cookieSpaceMediaProvidersPO.isIconMenuPresent().then(function() {
			logger.info("Redirect to Delete Cookie Space Media Provider("+mediaProviderTestData.updatedMediaProviderName+").");
			cookieSpaceMediaProvidersPO.clickIconMenu();
			cookieSpaceMediaProvidersPO.clickDeleteLink();
			utilityObj.browserWaitforseconds(4);			
			cookieSpaceMediaProvidersPO.clickConfirmDeleteAlert();
			utilityObj.browserWaitforseconds(3);
		});
		cookieSpaceMediaProvidersPO.getPageHeaderText().then(function() {
			logger.info("Deleted Cookie Space Media Provider("+mediaProviderTestData.updatedMediaProviderName+").");
		});		
	};
	
	/**
	 * This method is use to click on Action -> Show Deleted
	 * 
	 * @author hemins
	 */
	this.clickShowDeletedCookieSpaceMediaProvider = function() {
		browser.waitForAngular();
		cookieSpaceMediaProvidersPO.checkActionDropdownPresent().then(function(){
			assertsObj.verifyTrue(verifyPageHeaderText('Media Provider'),'Cookie Space Media Provider page header text is not found.');
			logger.info("Clicking on Action->Show Deleted of Media Provider.");
			cookieSpaceMediaProvidersPO.clickActionDropDown();
		});
		browser.waitForAngular();
		cookieSpaceMediaProvidersPO.clickShowDeletedLink();
		browser.waitForAngular();
	};
	
	/**
	 * This method is use to click on Action -> Hide Deleted
	 * 
	 * @author hemins
	 */
	this.clickHideDeletedMediaProvider = function() {
		browser.waitForAngular();
		cookieSpaceMediaProvidersPO.checkActionDropdownPresent().then(function(){
			assertsObj.verifyTrue(verifyPageHeaderText('Media Provider'),'Cookie Space Media Provider page header text is not found.');
			logger.info("Clicking on Action->Hide Deleted of Media Provider.");
			cookieSpaceMediaProvidersPO.clickActionDropDown();
		});
		browser.waitForAngular();
		cookieSpaceMediaProvidersPO.clickHideDeletedLink();
	};
	
	/**
	 * This method is use to redirect to view audit log page of Media provider.
	 * 
	 * @author hemins
	 */
	this.gotoCookieSpaceMediaProviderViewAuditLog = function() {
		browser.waitForAngular();
		cookieSpaceMediaProvidersPO.isIconMenuPresent().then(function(){
			logger.info("Redirect to View Audit Log page of Media Provider.");
			cookieSpaceMediaProvidersPO.clickIconMenu();			
			cookieSpaceMediaProvidersPO.clickAuditLogLink();
			utilityObj.switchToNewWindow();
			assertsObj.verifyTrue(verifyPageHeaderText('Audit Log'),'Audit Log page header text is not found on Cookie Space Media Provider page.');
		});				
	};
	
	/**
	 * This method is use to search field name in view audit log page of Media Provider.
	 * 
	 * @author hemins
	 * @param searchField
	 */
	this.searchCookieSpaceMediaProviderAuditLog = function(searchField) {	
		cookieSpaceMediaProvidersPO.isAuditLogSearchBoxPresent().then(function(){
			logger.info("Searching for field name in View Audit Log Page of Media Provider.");
			assertsObj.verifyTrue(verifyPageHeaderText('Audit Log'),'Audit Log page header text is not found.');
			cookieSpaceMediaProvidersPO.searchAuditLogTextBox(searchField);
		});
	};
	
	/**
	 * This method is use to get field name from view audit log page of Cookie Space Media Provider.
	 * 
	 * @author hemins
	 * @returns value
	 */
	this.getFieldNameValue = function() {	
		return cookieSpaceMediaProvidersPO.getFieldNameText().then(function(value){
			logger.info("Getting field name value from Audit log page of Cookie Space Media Provider.");
			return value;
		});		
	};
	
	/**
	 * This method is use to redirect to Cookie Space Media Provider Obtain pixel tab.
	 * 
	 * @author hemins
	 */
	this.gotoCookieSpaceMediaProviderObtainPixels = function(){
		cookieSpaceMediaProvidersPO.getPageHeaderText().then(function() {
			logger.info("Click on obtain Pixels Media Provider");		
			cookieSpaceMediaProvidersPO.clickObtainPixelsLink();
			browser.switchTo().defaultContent();	
		});		
	};
	
	/**
	 * This method is used to return header text of Obtain Pixel Pop-up & verify ID Sync and Pop-up text.
	 * 
	 * @author narottamc
	 * @param mediaProviderTestData
	 * @returns value
	 */
	this.returnMediaProviderObtainPixelHeaderName = function(mediaProviderTestData){
		return cookieSpaceMediaProvidersPO.getObtainPixelsName().then(function(value){
			assertsObj.verifyEqual(cookieSpaceMediaProvidersPO.getObtainPixelsName(),mediaProviderTestData.obtainPixelsName,"Cookie Space Media Provider: "+mediaProviderTestData.impressionSharing +"is not found for obtain pixel.");
			assertsObj.verifyEqual(cookieSpaceMediaProvidersPO.getIDSyncText(),mediaProviderTestData.idSync,"Cookie Space Media Provider: "+mediaProviderTestData.idSync +"is not found for obtain pixel.");
			cookieSpaceMediaProvidersPO.clickNewCloseBtn();
			utilityObj.browserWaitforseconds(3);
			return value;
			});	
	};
	
	/**
	 *This method is used to click on Icon->View Configuration of cookie space media Provider tab and verify the Advertiser text,Advertiser Feedback text
	 *  in New angular page.
	 * 
	 * @author hemins
	 * @param mediaProviderTestData
	 */	
	this.viewConfigurationMediaProvider=function(mediaProviderTestData){
		browser.waitForAngular();
		cookieSpaceMediaProvidersPO.getPageHeaderText().then(function(){
				logger.info("Clicking to 'IconMenu-> View configuration.");
				cookieSpaceMediaProvidersPO.clickIconMenu();
				cookieSpaceMediaProvidersPO.clickViewConfigurationLink();
				isAngularSite(false);
				cookieSpaceMediaProvidersPO.getConfigurationPageHeaderText().then(function(){
					assertsObj.verifyEqual(cookieSpaceMediaProvidersPO.getViewConfAdvertiserText(),mediaProviderTestData.expandAdvertisers,"Media Provider:" +mediaProviderTestData.expandAdvertisers+" is not found on Expand Page.");
					assertsObj.verifyEqual(cookieSpaceMediaProvidersPO.expandAdvertiserFeedbackText(),mediaProviderTestData.expandAdvertisersFeedback,"Media Provider:" +mediaProviderTestData.expandAdvertisersFeedback+" is not found on Expand Page.");
				});	
		});
	};
	
	/**
	 *  This method is used to check close button of View configuration cookie space media Provider tab displayed or not in angular page.
	 * 
	 * @author hemins
	 * @returns boolean
	 */
	 this.checkViewConfigurationCloseButton=function(){
		 isAngularSite(false);
		 return cookieSpaceMediaProvidersPO.isViewConfigurationCloseButtonDisplayed();
	 };
	
	/**
	 *  This method is used to click close button of View configuration media Provider tab in angular page.
	 * 
	 * @author hemins
	 */
	this.clickMediaProviderViewConfigurationCloseButton = function(){
		 isAngularSite(false);
		 cookieSpaceMediaProvidersPO.isViewConfigurationCloseButtonDisplayed().then(function(){
			 cookieSpaceMediaProvidersPO.clickViewConfigurationCloseButton();
			logger.info("Clicking Expand close button of media Provider done.");
		});
	};
	
	/**
	 * This method is use to return search element.
	 * 
	 * @author hemins
	 */
	this.returnSearchElement = function(){
		return cookieSpaceMediaProvidersPO.returnSearchElement();
	}
	
	/**
	 *  This method used for sorting.
	 * 
	 * @author hemins
	 */
	
	this.sortByMediaProviderName= function(){
		cookieSpaceMediaProvidersPO.clickMediaProviderNameColumn();
		logger.info("Click on Media Provider Name column for sorting.");
	};
	
	/**
	 * This method is used to verify that records are sort by Name or not.
	 * 
	 * @author hemins
	 * @return
	 */
	this.assertNameColumn = function() {
		assertsObj.verifyNotNull(cookieSpaceMediaProvidersPO.returnReverseMediaProviderName(),"Not Find column sorting with Media Provider Name.");
		return cookieSpaceMediaProvidersPO.returnReverseMediaProviderName();
	};
}
module.exports = new cookieSpaceMediaProvidersBase();