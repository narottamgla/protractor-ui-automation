/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

var mediaProviderBase = function() {
	var mediaProviderPO = require('../../pageObject/audiencesuite/mediaProviderPO.js');
	var utilityObj = require('../../lib/utility.js');
	var turnDataFilterObj = require('../../lib/turnDataProvider.js');
	var pathObj  =require( '../../testData/audienceSuite/path.js');
	var assertsObj = require('../../lib/assert.js');
	var loggerObj = require('../../lib/logger.js');
	var logger = loggerObj.loggers("mediaProviderBase");
	
	/**
	 * This method is use to check search box on Media Provider tab.
	 * 
	 * @author narottamc
	 */

	this.returnSearchElement = function() {
		return mediaProviderPO.returnSearchTextField();
	};
	
	/**
	 * This method is use to Redirect to Media Provider tab.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	this.gotoMediaProviderTab = function(){
		browser.waitForAngular();
		isAngularSite(false);
		mediaProviderPO.clickMediaProvidersMenuLink();
			if(browser.user.toUpperCase()=='CS'){
				mediaProviderPO.clickMediaProvidersSubLink();
			}
			assertsObj.verifyTrue(varifyPageHeaderText("Media Provider"),"Media provider header text is not found on Media provider page.");
		};

	/**
	 * This method is use to click on Cancel button of New Media provider Create page.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	this.newMediaProviderWithCancelButton = function() {
		mediaProviderPO.getMediaProviderPageHeader().then(function() {
			logger.info("Clicking cancel button of Action->new Media Provider page.");
		});
		mediaProviderPO.clickActionDropdown();
		mediaProviderPO.clickNewMediaProviderActionLink();
		isAngularSite(false);
		mediaProviderPO.clickNewMediaProviderCancelButton();
		assertsObj.verifyTrue(varifyPageHeaderText("Media Provider"),"Media provider header text is not found on Media provider page.");
	};
	
	 /**
	 * This method is use to add data to 'Base pixel tab' of  Create New Media Provider for Turn and Non Turn Market.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 * @param  mediaProviderTestData
	 */
	var addBasePixelMediaProvider=function(mediaProviderTestData){
		var userType = browser.user;
			if(!(mediaProviderTestData.isCheckedServerToserver.toUpperCase()=='CHECKED' ||mediaProviderTestData.isCheckedServerToserver.toUpperCase()=='YES' ||mediaProviderTestData.isCheckedServerToserver.toUpperCase()=='TRUE')||(userType.toUpperCase()!='CS'))
			{
				mediaProviderPO.clickOnBasePixel();
				utilityObj.browserWaitforseconds(1);
				mediaProviderPO.setBaseSyntx(mediaProviderTestData.baseSyntaxt);
				mediaProviderPO.enterTrailingSuffixTxtBox(mediaProviderTestData.trailingSyntax);
				mediaProviderPO.setFieldDelimiter(mediaProviderTestData.fieldDelimeter);
			}	
	};
	
	 /**
	 * This method is use to add data to 'Ad performance->Enable data collection' of  Create New Media Provider for Turn and Non Turn Market.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 * @param  mediaProviderTestData
	 */
	var enableAPIDataCollectionMP=function(mediaProviderTestData){
		if(mediaProviderTestData.isEnableAPIDataCollection.toUpperCase()=='CHECKED' ||mediaProviderTestData.isEnableAPIDataCollection.toUpperCase()=='YES' ||mediaProviderTestData.isEnableAPIDataCollection.toUpperCase()=='TRUE'){
			mediaProviderPO.CheckEnableCollectAPIDataCheckBox();
				if(mediaProviderTestData.isCheckedIdTypesTurnIDCheckBox.toUpperCase()=='CHECKED' ||mediaProviderTestData.isCheckedIdTypesTurnIDCheckBox.toUpperCase()=='YES' ||mediaProviderTestData.isCheckedIdTypesTurnIDCheckBox.toUpperCase()=='TRUE'){
					mediaProviderPO.CheckIdTypesTurnIDCheckBox();
				}
				if(mediaProviderTestData.isCheckedIdTypesDeviceIDCheckBox.toUpperCase()=='CHECKED' ||mediaProviderTestData.isCheckedIdTypesDeviceIDCheckBox.toUpperCase()=='YES' ||mediaProviderTestData.isCheckedIdTypesDeviceIDCheckBox.toUpperCase()=='TRUE'){
					mediaProviderPO.CheckIdTypesDeviceIDCheckBox();
					var deviceTypeList=mediaProviderTestData.deviceListinAPI.split(",");
					for(var k=0;k<deviceTypeList.length;k++){
						mediaProviderPO.selectDeviceTypesInAPIData(deviceTypeList[k]);
					}
				}};
		};
	
	 /**
	 * This method is use to add data to 'Ad performance tab' of  Create New Media Provider for Turn and Non Turn Market.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 * @param  mediaProviderTestData
	 */
	var addPerformanceMediaProvider=function(mediaProviderTestData){
		var userType = browser.user;
		var mediaChannelList=mediaProviderTestData.mediaChannel.split(",");	
			for(var i=0;i<mediaChannelList.length;i++){
				mediaProviderPO.selectMediaChannel(mediaChannelList[i]);
			}
				mediaProviderPO.clicknextButton();
				utilityObj.browserWaitforseconds(1);
				var pixelTypeList=mediaProviderTestData.pixeltype.split(",");	
				for(var j=0;j<pixelTypeList.length;j++){
					mediaProviderPO.selectElementsSentInPixel(pixelTypeList[j]);
				}
				enableAPIDataCollectionMP(mediaProviderTestData);
				if((mediaProviderTestData.isCheckedAnalyticsOnly.toUpperCase()=='CHECKED' ||mediaProviderTestData.isCheckedAnalyticsOnly.toUpperCase()=='YES' ||mediaProviderTestData.isCheckedAnalyticsOnly.toUpperCase()=='TRUE')&&(userType.toUpperCase()=='CS')){
				mediaProviderPO.selectAnalyticsOnly();
				}		
		};
	 
	   /**
		 * This method is use to add data to 'Details tab' of  Create New Media Provider for Turn and Non Turn Market.
		 * 
		 * @author narottamc
		 * @updated vishalbha
		 * @param  mediaProviderTestData
		 * @param  mediaProviderName
		 */
	var addDetailsMediaProvider=function(mediaProviderTestData,mediaProviderName){
		isAngularSite(false);
		var userType = browser.user;
			if(userType.toUpperCase()=='CS'){
				mediaProviderPO.selectMarketName(mediaProviderTestData.market);
				mediaProviderPO.selectParentMediaProviderId(mediaProviderTestData.parentMediaprovider);	
				if(mediaProviderTestData.isCheckedServerToserver.toUpperCase()=='CHECKED' ||mediaProviderTestData.isCheckedServerToserver.toUpperCase()=='YES' ||mediaProviderTestData.isCheckedServerToserver.toUpperCase()=='TRUE'){
					mediaProviderPO.clickServerToServer();
				}	
			}
			mediaProviderPO.enterMediaproviderNameTxtBox(mediaProviderName);
			utilityObj.uploadFile(mediaProviderPO.browseMediaproviderlogo(),pathObj.getMediaProviderLogoPath());
		};

	/**
	 * This method is use to Create New Media Provider for Turn and Non Turn Market.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 * @param  mediaProviderTestData
	 * @param  mediaProviderName
	 */
	this.createNewMediaProvider = function(mediaProviderTestData,mediaProviderName, typeOfMarket) {
		mediaProviderPO.clickNewMediaProviderLink();
		logger.info("Creating New Media Provider with "+typeOfMarket+"");
		isAngularSite(false);
		addDetailsMediaProvider(mediaProviderTestData,mediaProviderName);
		addPerformanceMediaProvider(mediaProviderTestData);
		addBasePixelMediaProvider(mediaProviderTestData);
		mediaProviderPO.clickSaveButton();
		utilityObj.browserWaitforseconds(3);
		assertsObj.verifyTrue(varifyPageHeaderText("Media Provider"),
				"Media Provider header text is not found on Media Provider page.");
		mediaProviderPO.getMediaProviderPageHeader().then(function(){
			logger.info("Created New Media Provider with "+typeOfMarket+"");
			updateMediaProviderTestDataFile(mediaProviderTestData,'createdMediaProviderName',mediaProviderName);
		});
	};

	/**
	 * This method is use to return element list in new console page.
	 * 
	 * @author vishalbha
	 */
	
	this.returnElementList=function(){
		return mediaProviderPO.mediaProviderelementList();
	};
	/**
	 * This method is use to write in media provider test data Json file.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 * @param  mediaProviderTestData
	 * @param  jsonKeyName
	 * @param  jsonKeyValue
	 */
	 var updateMediaProviderTestDataFile=function(mediaProviderTestData,jsonKeyName,jsonKeyValue){
		 if(mediaProviderTestData.market.toUpperCase()=='TURN'){
			 turnDataFilterObj.writeDataProvider(pathObj.getMediaProviderTestDataFilePath(), browser.env,
			 browser.language, 'createmediaproviderturnmarket',jsonKeyName,jsonKeyValue);
			}
		 	else
		 	{
			turnDataFilterObj.writeDataProvider(pathObj.getMediaProviderTestDataFilePath(), browser.env,
				browser.language, 'createmediaprovidernonturnmarket',jsonKeyName,jsonKeyValue);
		 	}		
	 };

	/**
	 * This method is use to Edit created Media Provider.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 * @param updatedMediaProviderName
	 */			
	this.editCreatedMediaProvider=function(mediaProviderTestData,updatedMediaProviderName){
		        mediaProviderPO.getMediaProviderPageHeader().then(function() {
				logger.info("Redirect to Edit Media Provider.");
				mediaProviderPO.clickOnIconMenu();
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				mediaProviderPO.clickonEditlink();
				isAngularSite(false);
				mediaProviderPO.enterMediaproviderNameTxtBox(updatedMediaProviderName);
				mediaProviderPO.clickSaveButton();
				assertsObj.verifyTrue(varifyPageHeaderText("Media Provider"),"Media Provider header text is not found on Media Provider page.");	
			});
			mediaProviderPO.getMediaProviderPageHeader().then(function() {
			logger.info("Editing created Media Provider done.");
			updateMediaProviderTestDataFile(mediaProviderTestData,'updatedMediaProviderName',updatedMediaProviderName);	
			});
		};
	
	/**
	 * This method is use to redirect to view Media Provider page.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	this.gotoViewMediaProvider = function() {
		mediaProviderPO.clickOnIconMenu();
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(1);
		mediaProviderPO.clickViewlink();
	};
	
	/**
	 * This method is use to View created Media Provider.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 * @param mediaProviderName
	 */
	this.viewCreatedMediaProvider=function(mediaProviderName){	
		isAngularSite(false);
			mediaProviderPO.getExpectedMediaProvidersNameViewPage().then(function() {
				logger.info("Viewing created Media Provider.");
				assertsObj.verifyEqual(mediaProviderPO.getExpectedMediaProvidersNameViewPage(),mediaProviderName,
				"Not match Media Provider in View Media Provider page.");
			});
			mediaProviderPO.getExpectedMediaProvidersNameViewPage().then(function(){
				logger.info("Viewed created Media Provider.");
			});
			return mediaProviderPO.getExpectedMediaProvidersNameViewPage();
	   };
		
	/**
	 * This method is use to Delete created Media Provider for Turn and non Turn Market.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	this.deleteCreatedMediaProvider=function(){	
		mediaProviderPO.getMediaProviderPageHeader().then(function() {
			logger.info("Redirect to Delete created Media Provider.");
			mediaProviderPO.clickOnIconMenu();
			utilityObj.browserWaitforseconds(3);
			mediaProviderPO.clickOnDeletelink();
			mediaProviderPO.clickConfirmDeleteAlert();
		});
		mediaProviderPO.getMediaProviderPageHeader().then(function() {
			logger.info("Deleting created Media Provider done.");
		});		
	};
	
	/**
	 * This method is use to click on Action -> Show Deleted
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	this.clickShowDeletedMediaProvider = function(){
	browser.waitForAngular();
		mediaProviderPO.checkActionDropdownPresent().then(function(){
			assertsObj.verifyTrue(varifyPageHeaderText('Media Provider'),'Media Provider page header text is not found.');
			logger.info("Clicking on Action->Show Deleted of Media Provider.");
			mediaProviderPO.clickActionDropdown();
		});
		browser.waitForAngular();
		mediaProviderPO.clickOnShowDeleted();
	};
	
	/**
	 * This method is use to click on Action -> Hide Deleted
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	this.clickHideDeletedMediaProvider = function() {
		browser.waitForAngular();
		mediaProviderPO.checkActionDropdownPresent().then(function(){
			assertsObj.verifyTrue(varifyPageHeaderText('Media Provider'),'Media Provider page header text is not found.');
			logger.info("Clicking on Action->Hide Deleted of Media Provider.");
			mediaProviderPO.clickActionDropdown();
		});
		browser.waitForAngular();
		mediaProviderPO.clickOnHideDeleted();
	};
	
	/**
	 * This method is use to redirect to view audit log page of Media provider.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 * @param name
	 */
	this.gotoMediaProviderViewAuditLog = function() {
		browser.waitForAngular();
		logger.info("Redirect to View Audit Log page of Media Provider.");
		mediaProviderPO.clickOnIconMenu();	
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(1);
		mediaProviderPO.clickonViewAuditLoglink();
		utilityObj.switchToNewWindow();
		isAngularSite(false);
		assertsObj.verifyTrue(varifyAuditLogPageHeaderText('Audit Log'),'Audit Log page header text is not found on Media Provider page.');
	};
	
	/**
	 * This method is use to search field name in view audit log page of Media Provider.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 * @param searchField
	 */
	this.searchMediaProviderAuditLog = function(searchField) {	
		mediaProviderPO.isAuditLogSearchBoxPresent().then(function(){
			logger.info("Searching for field name in View Audit Log Page of Media Provider.");
			assertsObj.verifyTrue(varifyAuditLogPageHeaderText('Audit Log'),'Audit Log page header text is not found.');
			mediaProviderPO.searchAuditLogTextBox(searchField);
		});
	};
	
	/**
	 * This method is use to get field name from view audit log page of Media Provider.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 * @returns value
	 */
	this.getFieldNameValue = function() {	
		return mediaProviderPO.getFieldNameText().then(function(value){
			logger.info("Getting field name value from Audit log page of Media Provider.");
			return value;
		});		
	};
	
	/**
	 * This method is use to redirect to Media Provider Obtain pixel tab.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	this.gotoMediaProviderObtainPixels = function(){
		mediaProviderPO.getMediaProviderPageHeader().then(function() {
			logger.info("Click on obtain Pixels Media Provider");		
			mediaProviderPO.clickOnIconMenu();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			mediaProviderPO.clickOnObtainPixelsLink();
			browser.switchTo().defaultContent();	
			});		
	};
	
	/**
	 * This method is used to return header text of Obtain Pixel Pop-up & verify AudienceSyncId,ImpressionSharing,clickSharing
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 * @param mediaProviderTestData
	 * @returns value
	 */
	this.returnMediaProviderObtainPixelHeaderName = function(mediaProviderTestData){
		return mediaProviderPO.getObtainPixelsName().then(function(value){
			assertsObj.verifyEqual(mediaProviderPO.expectedImpressionSharing(),mediaProviderTestData.impressionSharing,"Media Provider: "+mediaProviderTestData.impressionSharing +"is not found for obtain pixel.");
			assertsObj.verifyEqual(mediaProviderPO.expectedClickSharing(),mediaProviderTestData.clickSharing,"Media Provider: "+mediaProviderTestData.clickSharing +"is not found for obtain pixel.");
			mediaProviderPO.clickCloseOnObtainPixels();
			return value;
		});	
	};
	
	/**
	 *  This method used for sorting by Media Provider name.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	
	this.sortByMediaProviderName= function(){
		mediaProviderPO.clickMediaProviderNameColumn();
		browser.waitForAngular();
		logger.info("Click on Media Provider Name column for sorting.");
	};
	
	/**
	 * This method is used to verify that records are sort by Media Provider Name or not.
	 * 
	 * @author narottamc
	 * @return
	 */
	this.assertMediaProviderNameColumn = function() {
		assertsObj.verifyNotNull(mediaProviderPO.returnReverseMediaProviderName(),"Not Find column sorting with Media Provider Name.");
		return mediaProviderPO.returnReverseMediaProviderName();
	};
		
	/**
	 *  This method is use to clear search box.
	 * 
	 * @author narottamc
	 */
	this.clearSearchMediaProvider =function(){
		mediaProviderPO.getMediaProviderPageHeader().then(function() {
			logger.info("Clearing Search Textbox and click on search button.");
			mediaProviderPO.clearSearchBox();			
			mediaProviderPO.clickSearchCancel();
			browser.waitForAngular();
		});
	};
	
	/**
	 *  This method is use to select market Filter for Media Provider.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 * @param marketName
	 */
	this.selectMarketFilterMediaProvider =function(marketName){
		mediaProviderPO.getMediaProviderPageHeader().then(function() {
			utilityObj.browserWaitforseconds(3);
			logger.info("Selecting Media Provider Market Filter.");
			assertsObj.verifyTrue(mediaProviderPO.checkFilterFlipper(),"Filter Button is not available.");
			mediaProviderPO.clickFilterFlipper();
			browser.waitForAngular();
			mediaProviderPO.clickOnClearFilter();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);		
			mediaProviderPO.clickOnSelectLink().then(function(){
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2);
				mediaProviderPO.enterMarketName(marketName).then(function(){
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2);
				mediaProviderPO.clickOnMarketFilter(marketName);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
				});
			});			
		});	
	};
	
	/**
	 *  This method take first row market name from List Item Column "Market".
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	this.getMarketNameMediaProvider = function(){
		return mediaProviderPO.getMarketNameFromColumn().then(function(value){
		return value;	
		});
	};
		
	/**
	 *  This method is used for Redirect to Expand collapse page of Media provider & Verify the AdvertiserFeedbackText & AdvertiserText.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 * @param  mediaProviderTestData
	 */
	this.expandMediaProviders=function(mediaProviderTestData){
		mediaProviderPO.getMediaProviderPageHeader().then(function(){
			logger.info("Clicking to 'Action-> Expand ALL Rows' of Media Provider.");
			mediaProviderPO.clickActionDropdown();
			mediaProviderPO.clickExpandMediaProvidersLink();
		});
		mediaProviderPO.expandAdvertiserText().then(function(){
			assertsObj.verifyEqual(mediaProviderPO.expandAdvertiserText(),mediaProviderTestData.expandAdvertisers,"Media Provider:" +mediaProviderTestData.expandAdvertisers+" is not found on Expand Page.");
			assertsObj.verifyEqual(mediaProviderPO.expandAdvertiserFeedbackText(),mediaProviderTestData.expandAdvertisersFeedback,"Media Provider:" +mediaProviderTestData.expandAdvertisersFeedback+" is not found on Expand Page.");
		});
	};
	
	/**
	 *  This method is used to click close button of Expanded Media Provider tab.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	this.clickingMediaProviderExpandCloseButton = function(){
		mediaProviderPO.isExpandMediaProviderCloseButtonDisplayed().then(function(){
			mediaProviderPO.clickExpandMediaProviderCloseButton();
			logger.info("Clicking Expand close button of Media Provider done.");
		});
	};
	
	/**
	 *  This method is used to check close button of Expanded Data contract tab dispalyed or not.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 * @returns boolean
	 */
	this.checkExpandCloseButton=function(){
		return mediaProviderPO.isExpandMediaProviderCloseButtonDisplayed();
	};
	
	/**
	 *  This method is used to click on Action->Collapse All Rows link of Media Provider tab.
	 *  
	 * @author narottamc
	 * @updated vishalbha
	 */
	this.collapseMediaProvider=function(){
		mediaProviderPO.getMediaProviderPageHeader().then(function(){
			logger.info("Clicking to 'Action-> Collapse ALL Rows' of Media Provider.");
				mediaProviderPO.clickActionDropdown();
				mediaProviderPO.clickExpandMediaProvidersLink();
				mediaProviderPO.clickActionDropdown();
				mediaProviderPO.clickCollapseMediaProvidersLink();
		});
	};	  
	
	/**
	 * This method is use to verify page header of Media Provider page.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 * @param name
	 * @returns boolean
	 */
	var varifyPageHeaderText = function(name) {
		return mediaProviderPO.getMediaProviderPageHeader().then(function(value) {
			if (value == name) {
				return true;
			} else {
				return false;
			};
		});
	};
	
	/**
	 * This method is use to verify page header of Media Provider page.
	 * 
	 * @author vishalbha
	 * @param name
	 * @returns boolean
	 */
	var varifyAuditLogPageHeaderText = function(name) {
		return mediaProviderPO.getMediaProviderAuditLogPageHeader().then(function(value) {
			if (value == name) {
				return true;
			} else {
				return false;
			};
		});
	};
	
	/**
	 *This method is used to click on Icom->View link of media Provider tab and verify the Rate Schedule text,Taxonomy text,
	 *  pixel option text,Events text and  media Provider configuration text in New angular page.
	 * 
	 * @author vishalbha
	 * @param mediaProviderTestData
	 * @returns Data contract configuration text
	 */	
	this.viewConfigurationMediaProvider=function(mediaProviderTestData){
		browser.waitForAngular();
			mediaProviderPO.getMediaProviderPageHeader().then(function(){
				logger.info("Clicking to 'IconMenu-> View configuration.");
				mediaProviderPO.clickOnIconMenu();
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				mediaProviderPO.clickViewConfigurationLink();
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				isAngularSite(false);
				mediaProviderPO.getConfigurationPageHeaderText().then(function(){
					assertsObj.verifyEqual(mediaProviderPO.expandAdvertiserText(),mediaProviderTestData.expandAdvertisers,"Media Provider:" +mediaProviderTestData.expandAdvertisers+" is not found on Expand Page.");
					assertsObj.verifyEqual(mediaProviderPO.expandAdvertiserFeedbackText(),mediaProviderTestData.expandAdvertisersFeedback,"Media Provider:" +mediaProviderTestData.expandAdvertisersFeedback+" is not found on Expand Page.");
				});	
		});
	};
	
	/**
	 *  This method is used to click close button of View configuration media Provider tab in angular page.
	 * 
	 * @author vishalbha
	 */
	this.clickMediaProviderViewConfigurationCloseButton = function(){
		 isAngularSite(false);
		 mediaProviderPO.isViewConfigurationCloseButtonDisplayed().then(function(){
			 mediaProviderPO.clickViewConfigurationCloseButton();
			logger.info("Clicking Expand close button of media Provider done.");
		});
	};
	
	/**
	 *  This method is used to check close button of View configuartion media Provider tab dispalyed or not in angular page.
	 * 
	 * @author vishalbha
	 * @returns boolean
	 */
	 this.checkViewConfigurationCloseButton=function(){
		 isAngularSite(false);
		 return mediaProviderPO.isViewConfigurationCloseButtonDisplayed();
		};
	};
	module.exports = new mediaProviderBase();

