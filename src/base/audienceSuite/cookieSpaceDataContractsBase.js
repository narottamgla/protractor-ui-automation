var cookieSpaceDataContractsBase = function(){
		
	var cookieSpaceDataContractsPO = require('../../pageObject/audiencesuite/cookieSpaceDataContractsPO.js');
	var utilityObj = require('../../lib/utility.js');
	var turnDataFilterObj = require('../../lib/turnDataProvider.js');
	var pathObj  =require( '../../testData/audienceSuite/path.js');
	var assertsObj = require('../../lib/assert.js');
	var loggerObj = require('../../lib/logger.js');
	var logger = loggerObj.loggers("cookieSpaceDataContractsBase");
	
	
	/**
	 * This method is used check page header text of Cookie Space Data Contract page and match page
	 * header text and return true value if page header text is Data Provider otherwise 
	 * return false value.
	 * 
	 * @author hemins
	 * @returns boolean
	 */
	var verifyPageHeaderText = function(name){
		return cookieSpaceDataContractsPO.getCookieSpaceDataContractPageHeaderText().then(function(value){
			if(value == name){
				return true;
			}else{
				return false;
			}
		});
	};
	
	/**
	 * This method is use to return element list in new console page.
	 * 
	 * @author narottamc
	 */
	
	this.returnElementList=function(){
		return cookieSpaceDataContractsPO.dataContractsElementList();
	};
	
	/**
	 * This Method is used for redirection to Cookie Space Data Contract Tab.
	 * 
	 *  @author omprakash
	 */
	this.redirectToCookieSpaceDataContractsTab = function(){
		cookieSpaceDataContractsPO.clickOnDataLink();
		browser.waitForAngular();
		cookieSpaceDataContractsPO.clickOnCookieSpaceDataContractsSubLink();
		assertsObj.verifyTrue(verifyPageHeaderText("Data Contract Listing"),"Not redirect to Cookie Space Data Contracts Tab.");
		verifyPageHeaderText("Data Contract Listing").then(function(value){
			 if(value){
				 logger.info("Redirected to Cookie Space Data Contracts Page.");
			 }
		 });
	};
	
	
	/**
	 * This function is used to enter all details for general tab of creation of Cookie Space Data Contract
	 * 
	 * @author hemins
	 */
	var generalTabEnter = function(cookieSpaceDataContractTestData,cookieSpaceDataContractName){
		cookieSpaceDataContractsPO.checkForShadowPage();
		cookieSpaceDataContractsPO.enterNewCookieSpaceDataContractName(cookieSpaceDataContractName);
		cookieSpaceDataContractsPO.selectCurrencyType(cookieSpaceDataContractTestData.currency);
		if((cookieSpaceDataContractTestData.availability).toUpperCase() == 'ALL'){
			cookieSpaceDataContractsPO.clickAllAdvertiser();
		}
		else if((cookieSpaceDataContractTestData.availability).toUpperCase() == 'SINGLE'){
	
			cookieSpaceDataContractsPO.clickSingleAdvertiser().then(function(){
				utilityObj.browserWaitforseconds(3);
				cookieSpaceDataContractsPO.clickSingleAdvertiser();
				cookieSpaceDataContractsPO.clickonFlextagDataTypeFrame();
				cookieSpaceDataContractsPO.selectSingleAdvertiser(cookieSpaceDataContractTestData.singleAdvertiserName);
			});
		}
		else if((cookieSpaceDataContractTestData.availability).toUpperCase() == 'ANALYTICS'){
			cookieSpaceDataContractsPO.clickavailabilityAnalytics().then (function(){
				utilityObj.browserWaitforseconds(3);
				cookieSpaceDataContractsPO.clickavailabilityAnalytics();
			});
		}		
		
	};
	
	/**
	 * This function is used to fill details of rate schedule tab of Cookie Space Data Contract tab
	 * 
	 * @author hemins
	 */
	var rateSheduleTabDetailsEnter=function(cookieSpaceDataContractTestData){
		var startDate='';
		var endDate='';
		cookieSpaceDataContractsPO.selectAudienceDataCost(cookieSpaceDataContractTestData.audienceDataCost);
		if(cookieSpaceDataContractTestData.startDate==''){
			startDate=utilityObj.getTodayDateOnly();
		}else{
			startDate=cookieSpaceDataContractTestData.startDate;
		}
		
		if(cookieSpaceDataContractTestData.endDate==''){
			endDate=utilityObj.getMonthDate();
		}
		else{
			endDate=cookieSpaceDataContractTestData.endDate;
		}
		cookieSpaceDataContractsPO.selectStartDate(startDate);
		cookieSpaceDataContractsPO.selectEndDate(endDate);
		cookieSpaceDataContractsPO.selectRateType(cookieSpaceDataContractTestData.rateType);
		if((cookieSpaceDataContractTestData.rateType).toUpperCase() == "FLAT FEE"){
			cookieSpaceDataContractsPO.enterFlatfee(cookieSpaceDataContractTestData.rate1);	
		}
		else if((cookieSpaceDataContractTestData.rateType).toUpperCase() == "CPM (USAGE CPM)"){
			cookieSpaceDataContractsPO.enterCPMValue(cookieSpaceDataContractTestData.rate1);
		}
		else if((cookieSpaceDataContractTestData.rateType).toUpperCase() == "CPUU + CPM"){
			cookieSpaceDataContractsPO.enterCPUUValue(cookieSpaceDataContractTestData.rate1);
			cookieSpaceDataContractsPO.enterCPMValue(cookieSpaceDataContractTestData.rate2);
		}
		else if((cookieSpaceDataContractTestData.rateType).toUpperCase() == "COST PER STAMP"){
			cookieSpaceDataContractsPO.enterCostPerStamp(cookieSpaceDataContractTestData.rate1);
		}
		else if((cookieSpaceDataContractTestData.rateType).toUpperCase() == "REVENUE SHARE %"){
			cookieSpaceDataContractsPO.enterRevenueShare(cookieSpaceDataContractTestData.rate1);
		}
	};
	
	/**
	 * This function is used to select collection type.
	 * 
	 * @author hemins
	 */
	var selectCollectionType = function(cookieSpaceDataContractTestData,getTaxonomyPath){
		cookieSpaceDataContractsPO.clickOnDataTypeTab();
		cookieSpaceDataContractsPO.selectCollectiontype(cookieSpaceDataContractTestData.collectiontype);
		cookieSpaceDataContractsPO.clickOnFrame();	
		if((cookieSpaceDataContractTestData.collectiontype).toUpperCase()=='API'){
			if((cookieSpaceDataContractTestData.idType).toUpperCase()=='DEVICE ID'){
				cookieSpaceDataContractsPO.selectCollectiontype(cookieSpaceDataContractTestData.idType);
				cookieSpaceDataContractsPO.checkIFAInDeviceIdType();
				cookieSpaceDataContractsPO.checkPlatformIdInDeviceIdType();
			}
			if((cookieSpaceDataContractTestData.checkedCategory).toUpperCase()=='CHECKED'){
				cookieSpaceDataContractsPO.clickOnCategoriesCheckbutton().then(function(){
				importTaxonomy(getTaxonomyPath);
				});
			}
			else if((cookieSpaceDataContractTestData.checkedKeyword).toUpperCase()=='CHECKED'){
				cookieSpaceDataContractsPO.clickOnKeywordsCheckbutton();
			}
			else if((cookieSpaceDataContractTestData.checkedKeys).toUpperCase()=='CHECKED'){
				cookieSpaceDataContractsPO.clickOnKeysCheckbutton().then (function(){					
				cookieSpaceDataContractsPO.enterKeyInTextBox(cookieSpaceDataContractTestData.keys);
				});
			}
		}
		else if((cookieSpaceDataContractTestData.collectiontype).toUpperCase()=='FILE'){
				cookieSpaceDataContractsPO.selectCollectiontype(cookieSpaceDataContractTestData.collectiontype).then(function(){
				importTaxonomy(getTaxonomyPath);
				
				});
		}
		else if((cookieSpaceDataContractTestData.collectiontype).toUpperCase()=='PIXEL'){
				cookieSpaceDataContractsPO.selectCollectiontype(cookieSpaceDataContractTestData.collectiontype);
				cookieSpaceDataContractsPO.selectPixelType(cookieSpaceDataContractTestData.pixelType);
				cookieSpaceDataContractsPO.clickOnKeysCheckbutton().then (function(){
				cookieSpaceDataContractsPO.enterKeyInTextBox(cookieSpaceDataContractTestData.keys);
				});
		};
	};
	
	/**
	 * This function is used to import taxonomy.
	 * 
	 * @author hemins
	 */	
	var importTaxonomy = function(getTaxonomyPath){
		isAngularSite(false);
		cookieSpaceDataContractsPO.ClickOnImportTaxonomy();
		browser.waitForAngular();
		cookieSpaceDataContractsPO.switchToFrame();
		utilityObj.browserWaitforseconds(2);		
		utilityObj.uploadFile(element(by.css('.textInput')),getTaxonomyPath);
		utilityObj.browserWaitforseconds(3);
		cookieSpaceDataContractsPO.clickOnApplyButton();
		utilityObj.browserWaitforseconds(3);		
	};
	
	/**
	 * This function is used to create Cookie Space Data Contract from New button.
	 * 
	 * @author hemins
	 * @param cookieSpaceDataContractTestData
	 * @param cookieSpaceDataContractName
	 * @param getTaxonomyPath
	 * @param testDataName
	 */
	this.createCookieSpaceDataContracts = function(cookieSpaceDataContractTestData,cookieSpaceDataContractName,getTaxonomyPath,testDataName){
		isAngularSite(false);
		cookieSpaceDataContractsPO.getFormHeader().then(function() {
			logger.info("Creating Cookie Space Data Contract with "+cookieSpaceDataContractTestData.availability +" availability and "+cookieSpaceDataContractTestData.collectiontype+ " collectiontype.");
			generalTabEnter(cookieSpaceDataContractTestData,cookieSpaceDataContractName);
			selectCollectionType(cookieSpaceDataContractTestData,getTaxonomyPath);	
			browser.switchTo().defaultContent();
			cookieSpaceDataContractsPO.clickOnRateSchedulesTabLink();
			rateSheduleTabDetailsEnter(cookieSpaceDataContractTestData);
			assertsObj.verifyTrue(cookieSpaceDataContractsPO.rateScheduleHeaderText('New Data Contract Rate Schedules'),"Cookie Space Data Contracts header text is not found on Cookie Space Data Contract page.");
			cookieSpaceDataContractsPO.clickOnSaveButton();
			turnDataFilterObj.writeDataProvider(pathObj.getCookieSpaceDataContractsTestDataFilePath(), browser.env, browser.language,testDataName ,'createdDC',cookieSpaceDataContractName);
		});
		cookieSpaceDataContractsPO.isSearchBoxPresent().then(function(value) {
			logger.info("Created Cookie Space Data Contract with "+cookieSpaceDataContractTestData.availability +" availability and "+cookieSpaceDataContractTestData.collectiontype+ " collectiontype.");
		});
	};
	
	/**
	 * This method is use to return search Text box element of Cookie Space Data Contracts tab.
	 * 
	 * @author hemins
	 * @returns Element
	 */
	this.returnSearchElement = function(){
		cookieSpaceDataContractsPO.clickOnClearSearch();
		return cookieSpaceDataContractsPO.returnSearchElement();
	};
	
	/**
	 * This function used to select Cookie Space Data Provider from popup.
	 * 
	 * @author hemins
	 * @param cookieSpaceDataContractTestData
	 */	
	this.selectCookieSpaceDataContractFromNewButton=function(cookieSpaceDataContractTestData){
		cookieSpaceDataContractsPO.clickOnNewCookieSpaceDataContractsButton();
		browser.waitForAngular();			
		utilityObj.browserWaitforseconds(5);
		cookieSpaceDataContractsPO.selectDataProviderfromDropDown(cookieSpaceDataContractTestData.dataProviderName);
		browser.waitForAngular();			
		utilityObj.browserWaitforseconds(3);
		cookieSpaceDataContractsPO.clickOnFrameNextButton();	
	};
	
	/**
	 * This method is use to click on icon Menu->Delete Link.
	 * 
	 * @author hemins
	 * @param cookieSpaceDataContractName
	 */
	this.deleteCreatedCookieSpaceDataContract=function(cookieSpaceDataContractName){
		cookieSpaceDataContractsPO.isIconMenuPresent().then(function() {
			logger.info("Deleting created "+cookieSpaceDataContractName +" Cookie Space Data Contract.");
			cookieSpaceDataContractsPO.clickOnIconMenu();
			cookieSpaceDataContractsPO.clickOnDeleteLink();
			cookieSpaceDataContractsPO.clickConfirmDeleteAlert();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
		});
		cookieSpaceDataContractsPO.getCookieSpaceDataContractPageHeaderText().then(function() {
			logger.info("Deleted created "+cookieSpaceDataContractName +" Cookie Space Data Contract.");
		});	
	};
	
	/**
	* This method is use to edit created cookie space data contract.
	* 
	* @author hemins
	* @param  dataName
	* @param  updatedCookieSpaceDataContractName
	*/
	this.editCreatedCookieSpaceDataContract=function(dataName,updatedCookieSpaceDataContractName) {
		cookieSpaceDataContractsPO.isIconMenuPresent().then(function() {
			logger.info("Editing Cookie Space Data Contract.");
			cookieSpaceDataContractsPO.clickOnIconMenu();
			cookieSpaceDataContractsPO.clickOnEditLink();
			isAngularSite(false);
			cookieSpaceDataContractsPO.enterNewCookieSpaceDataContractName(updatedCookieSpaceDataContractName);
			cookieSpaceDataContractsPO.clickOnSaveButton();
		});
		cookieSpaceDataContractsPO.isSearchBoxPresent().then(function() {
			logger.info("Edited Cookie Space Data Contract.");
			turnDataFilterObj.writeDataProvider(pathObj.getCookieSpaceDataContractsTestDataFilePath(), browser.env,browser.language,dataName,'updatedDC',updatedCookieSpaceDataContractName);		
		});
	};
	
	/**
	* This method is use to redirect to View page of cookie space data contract.
	* 
	* @author hemins
	*/
	this.gotoViewCookieSpaceDataContracts = function() {
		cookieSpaceDataContractsPO.isIconMenuPresent().then(function() {
			cookieSpaceDataContractsPO.clickOnIconMenu();
			cookieSpaceDataContractsPO.clickOnViewLink();
		});
	};
		
	/**
	* This method is use to View created cookie space data contract.
	* 
	* @author hemins
	* @param  updatedCookieSpaceDataContractName
	* @returns datacontractanme
	*/
	this.viewCreatedCookieSpaceDataContract=function(updatedCookieSpaceDataContractName){
		isAngularSite(false);
		cookieSpaceDataContractsPO.getExpectedDataContractNameViewPage().then (function(){
			logger.info("Viewing created Cookie Space Data Contract.");
			assertsObj.verifyEqual(cookieSpaceDataContractsPO.getExpectedDataContractNameViewPage(),updatedCookieSpaceDataContractName,
			"Not match Cookie Space Data Contract in View Cookie Space Data Contract page.");	
		});
		cookieSpaceDataContractsPO.getExpectedDataContractNameViewPage().then(function() {
					logger.info("Viewed created Cookie Space Data Contract.");
		});
		return cookieSpaceDataContractsPO.getExpectedDataContractNameViewPage();
	};
	
	/**
	 *  This method is used to click on Action->Expanded link of Cookie Space Data Contract tab and verify the Rate Schedule text,Taxonomy text,
	 *  pixel option text,Events text and  Data contract configuration text.
	 *  
	 * @author hemins
	 * @param cookieSpaceDataContractTestData
	 * @returns Data contract configuration text
	 */
	this.expandCookieSpaceDataContracts=function(cookieSpaceDataContractTestData)	{
		cookieSpaceDataContractsPO.getCookieSpaceDataContractPageHeaderText().then(function(){
			logger.info("Clicking to 'Action-> Expand ALL Rows' of Cookie Space Data Contracts.");
			cookieSpaceDataContractsPO.clickOnActionDropDownList();
			cookieSpaceDataContractsPO.clickExpandDataContarctsLink();
			cookieSpaceDataContractsPO.getExpandDatacontractConfText().then(function(){
				assertsObj.verifyEqual(cookieSpaceDataContractsPO.getExpandDatacontractConfText(),cookieSpaceDataContractTestData.expandConf,"Cookie Space Data Contract:" +cookieSpaceDataContractTestData.expandConf+" is not found for Expand.");
				assertsObj.verifyEqual(cookieSpaceDataContractsPO.getExpandRateScedulesText(),cookieSpaceDataContractTestData.expandRateSch,"Cookie Space Data Contract:" +cookieSpaceDataContractTestData.expandRateSch+" is not found for Expand.");
				assertsObj.verifyEqual(cookieSpaceDataContractsPO.getExpandTaxonomyText(),cookieSpaceDataContractTestData.expandTaxonomy,"Cookie Space Data Contract:" +cookieSpaceDataContractTestData.expandTaxonomy+" is not found for Expand.");
				assertsObj.verifyEqual(cookieSpaceDataContractsPO.getExpandPixelOptionsText(),cookieSpaceDataContractTestData.expandAPIOptions,"Cookie Space Data Contract:" +cookieSpaceDataContractTestData.expandAPIOptions+" is not found for Expand.");
				assertsObj.verifyEqual(cookieSpaceDataContractsPO.getExpandEventsText(),cookieSpaceDataContractTestData.expandEvents,"Cookie Space Data Contract:" +cookieSpaceDataContractTestData.expandEvents+" is not found for Expand.");
			});	
		});
	};
	
	/**
	 *  This method is used to click on Action->Collapse All Rows link of Cookie Space Data contract tab.
	 *  
	 * @author hemins
	 */
	this.collapseCookieSpaceDataContracts=function() {
		cookieSpaceDataContractsPO.getCookieSpaceDataContractPageHeaderText().then(function(){
			logger.info("Clicking to 'Action-> Collapse ALL Rows' of Cookie Space Data Contracts.");
			cookieSpaceDataContractsPO.clickOnActionDropDownList();
			cookieSpaceDataContractsPO.clickExpandDataContarctsLink();
			cookieSpaceDataContractsPO.clickOnActionDropDownList();
			cookieSpaceDataContractsPO.clickCollapseDataContarctsLink();
		});
	};		
	
	/**
	 *  This method is used to click close button of Expanded Cookie Space Data contract tab.
	 * 
	 * @author hemins
	 */
	this.clickingCookieSpaceDataContractExpandCloseButton = function(){
		 cookieSpaceDataContractsPO.isExpandCloseButtonDisplayed().then(function() {
			cookieSpaceDataContractsPO.clickExpandCloseButton();
			logger.info("Clicking Expand close button of Cookie Space Data Contract done.");
		});
	};
	
	/**
	 *  This method is used to check close button of Expanded Cookie Space Data contract tab displayed or not.
	 * 
	 * @author hemins
	 * @returns boolean
	 */
	this.checkExpandCloseButton=function(){
		return cookieSpaceDataContractsPO.isExpandCloseButtonDisplayed();
	};
	
	/**
	 *  This method is used to click on Icom->View link of Data contract tab and verify the Rate Schedule text,Taxonomy text,
	 *  pixel option text,Events text and  Data contract configuration text in New angular page.
	 *  
	 * @author narottamc
	 * @param dataContractTestData
	 * @returns Data contract configuration text
	 */
	this.viewConfigurationDataContracts=function(cookieSpaceDataContractTestData){
		browser.waitForAngular();
		cookieSpaceDataContractsPO.getCookieSpaceDataContractPageHeaderText().then(function(){
			logger.info("Clicking to 'IconMenu-> View configuration.");
			cookieSpaceDataContractsPO.clickOnIconMenu();
			cookieSpaceDataContractsPO.clickViewConfigurationLink();
			isAngularSite(false);
			cookieSpaceDataContractsPO.getConfigurationPageHeaderText().then(function(){
				assertsObj.verifyEqual(cookieSpaceDataContractsPO.getViewConfRateScedulesText(),cookieSpaceDataContractTestData.expandRateSch,"Cookie Space Data Contract:" +cookieSpaceDataContractTestData.expandRateSch+" is not found for Expand.");
				assertsObj.verifyEqual(cookieSpaceDataContractsPO.getViewConfTaxonomyText(),cookieSpaceDataContractTestData.expandTaxonomy,"Cookie Space Data Contract:" +cookieSpaceDataContractTestData.expandTaxonomy+" is not found for Expand.");
				if(cookieSpaceDataContractTestData.collectiontype.toUpperCase()=='PIXEL'){
				assertsObj.verifyEqual(cookieSpaceDataContractsPO.getViewConfPixelOptionsText(),cookieSpaceDataContractTestData.expandPixelOptions,"Data Contract:" +cookieSpaceDataContractTestData.expandPixelOptions+" is not found for Expand.");
				}
				assertsObj.verifyEqual(cookieSpaceDataContractsPO.getViewConfExpandEventsText(),cookieSpaceDataContractTestData.expandEvents,"Cookie Space Data Contract:" +cookieSpaceDataContractTestData.expandEvents+" is not found for Expand.");
					});	
			});
	};
	
	/**
	 *  This method is used to check close button of View configuartion Data contract tab dispalyed or not in angular page.
	 * 
	 * @author narottamc
	 * @returns boolean
	 */
	this.checkViewConfigurationCloseButton=function(){
		isAngularSite(false);
		return cookieSpaceDataContractsPO.isViewConfigurationCloseButtonDisplayed();
	};
	
	/**
	 *  This method is used to click close button of View configuration Data contract tab in angular page.
	 * 
	 * @author narottamc
	 */
	this.clickingDataContractViewConfigurationCloseButton = function(){
		 isAngularSite(false);
		 cookieSpaceDataContractsPO.isViewConfigurationCloseButtonDisplayed().then(function(){
			 cookieSpaceDataContractsPO.clickViewConfigurationCloseButton();
			logger.info("Clicking Expand close button of Data Contract done.");
		});
	};
	
	/**
	 * This method is use to click on icon Menu->Obtain pixel link.
	 * 
	 * @author hemins
	 */
	this.gotoCookieSpaceDataContractObtainPixels = function(){
		cookieSpaceDataContractsPO.isIconMenuPresent().then(function() {
			logger.info("Click on obtain Pixels Cookie Space Data Contract");		
			cookieSpaceDataContractsPO.clickOnIconMenu();
			cookieSpaceDataContractsPO.clickOnObtainPixelsLink();
			browser.switchTo().defaultContent();	
		});		
	};
	
	/**
	 * This method is use to return Obtain pixel page header name.
	 * 
	 * @author hemins
	 * @returns Obtain pixel page header name
	 */
	this.returnCookieSpaceDataContractObtainPixelHeaderName = function(){
		return cookieSpaceDataContractsPO.getObtainPixelsName().then(function(value){
			cookieSpaceDataContractsPO.clickCloseOnObtainPixels();
			return value;
		});	
	};
	
	/**
	 * This method is use to redirect to Cookie Space Data contract view audit Log page.
	 * 
	 * @author hemins
	 */
	this.gotoCookieSpaceDataContractViewAuditLog = function() {
		browser.waitForAngular();
		cookieSpaceDataContractsPO.isIconMenuPresent().then(function(){
			logger.info("Redirect to View Audit Log page of Cookie Space Data Contract.");
			cookieSpaceDataContractsPO.clickOnIconMenu();
			browser.waitForAngular();
			cookieSpaceDataContractsPO.clickOnViewAuditLink();
			browser.waitForAngular();
			utilityObj.switchToNewWindow();
			assertsObj.verifyTrue(verifyPageHeaderText('Audit Log'),'Audit Log page header text is not found on Cookie Space Data Contarct page.');
		});				
	};

	/**
	 * This method is use to search 'searchField' on view Audit log page.
	 * 
	 * @author hemins
	 * @param searchField
	 */
	this.searchCookieSpaceDataContractAuditLog = function(searchField) {
		isAngularSite(false);
		cookieSpaceDataContractsPO.isAuditLogSearchBoxPresent().then(function(){
			logger.info("Searching for field name in View Audit Log Page of Cookie Space Data Contract.");
			assertsObj.verifyTrue(verifyPageHeaderText('Audit Log'),'Audit Log page header text is not found.');
			cookieSpaceDataContractsPO.searchAuditLogTextBox(searchField);
		});
	};	
	
	/**
	 * This method is use to get field name from view audit log page of Cookie Space Data Contract.
	 * 
	 * @author hemins
	 * @returns value
	 */
	this.getFieldNameValue = function() {	
		return cookieSpaceDataContractsPO.getFieldNameText().then(function(value){
			logger.info("Getting field name value from Audit log page of Cookie Space Data Contract.");
			return value;
		});		
	};	
	
	/**
	 * This method is use to click on Action -> Show Deleted
	 * 
	 * @author hemins
	 */
	this.clickShowDeletedCookieSpaceDataContract = function() {
		browser.waitForAngular();
		cookieSpaceDataContractsPO.checkActionDropdownPresent().then(function(){
			assertsObj.verifyTrue(verifyPageHeaderText('Data Contract Listing'),'Cookie Space Data Contarct page header text is not found.');
			logger.info("Clicking on Action->Show Deleted of Cookie Space Data Contract.");
			cookieSpaceDataContractsPO.clickOnActionDropDownList();
		});
		browser.waitForAngular();
		cookieSpaceDataContractsPO.clickOnShowDeletedLink();
	};	
	
	/**
	 * This method is use to click on Action -> Hide Deleted
	 * 
	 * @author hemins
	 */
	this.clickHideDeletedCookieSpaceDataContract = function() {
		browser.waitForAngular();
		cookieSpaceDataContractsPO.checkActionDropdownPresent().then(function(){
			assertsObj.verifyTrue(verifyPageHeaderText('Data Contract Listing'),'Cookie Space Data Contract page header text is not found.');
			logger.info("Clicking on Action->Hide Deleted of Cookie Space Data Contract.");
			cookieSpaceDataContractsPO.clickOnActionDropDownList();
		});
		browser.waitForAngular();
		cookieSpaceDataContractsPO.clickOnHideDeletedLink();
	};	

	/**
	* This method is used to sort cookie space data contract list by Cookie Space Data contract name.
	* 
	* @author hemins
	*/		
	this.sortByCookieSpaceDataContractName = function(){
		cookieSpaceDataContractsPO.clickCookieSpaceDataContractNameColumn();
		logger.info("Click on Cookie Space Data Contract Name column for sorting.");
	};

	/**
	 * This method is used to verify that records are sort by Cookie Space Data Contract Name or not.
	 * 
	 * @author hemins
	 * @return returnReverseCookieSpaceDataContractName
	 */
	this.assertCookieSpaceDataContractNameColumn = function() {
		assertsObj.verifyNotNull(cookieSpaceDataContractsPO.returnReverseCookieSpaceDataContractName(),"Not Find column sorting with Cookie Space Data Contract Name.");
		return cookieSpaceDataContractsPO.returnReverseCookieSpaceDataContractName();
	};
	
	/**
	 *  This method use to select first record check box in displayed cookie space data contract list.
	 * 
	 * @author hemins
	 */
	this.selectDeletedDataContract=function(){
		cookieSpaceDataContractsPO.checkFirstRecordCheckbox();
	};
	
	/**
	 *  This method use to click on Action->Undo delete link & confirm alert.
	 * 
	 * @author hemins
	 */
	this.undoDeleteCookieSpaceDataContract = function() {
		cookieSpaceDataContractsPO.getCookieSpaceDataContractPageHeaderText().then(function(){
			logger.info("Clicking to 'Action-> Undo delete' of Cookie Space Data Contracts.");
			cookieSpaceDataContractsPO.clickOnActionDropDownList();
			cookieSpaceDataContractsPO.clickOnUndoDeleteLink();
			cookieSpaceDataContractsPO.clickConfirmDeleteAlert();
		});
	};

};
module.exports = new cookieSpaceDataContractsBase()