/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

var segmentBase = function(){
	
	var segmentPO = require('../../pageObject/audiencesuite/segmentPO.js');
	var utilityObj = require('../../lib/utility.js');
	var turnDataFilterObj = require('../../lib/turnDataProvider.js');
	var assertsObj = require('../../lib/assert.js');
	var loggerObj = require('../../lib/logger.js');
	var logger = loggerObj.loggers("segmentBase");
	
	/**
	 * This Method is use for redirection to segment Tab.
	 * 
	 *  @author hemins
	 */
	this.redirectToSegmentTab = function(){
		browser.navigate().refresh();
		browser.waitForAngular();
		segmentPO.clickSegmentLink();
		assertsObj.verifyTrue(varifyPageHeaderText("Segments"),"Not redirect to Segment Tab.");
		varifyPageHeaderText("Segments").then(function(value){
			 if(value){
				 logger.info("Redirected to Segment Page.");
			 }
		 });
		 
	};
	
	/**
	 * This Method is use for redirection to segment Tab in campaign suite.
	 * 
	 *  @author omp
	 */
	this.redirectToSharedSegmentTab = function(){
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(2);
		segmentPO.warningPresent().then(function(resultVal){
			if(resultVal){
				segmentPO.warningClose();
			}	
			});
		browser.navigate().refresh();
		browser.waitForAngular();
		segmentPO.clickDataLink().then(function(){
			browser.waitForAngular();			
			segmentPO.clickSharedSegmentLink();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			assertsObj.verifyTrue(varifyPageHeaderText("Segments"),"Not redirect to Segment Tab.");
		});		
		varifyPageHeaderText("Segments").then(function(value){
			 if(value){
				 logger.info("Redirected to Segment Page.");
			 }
		 });
	};
	/**
	 * This method is use check page header text of segment page and match page
	 * header text and return true value if page header text is segment otherwise 
	 * return false value.
	 * 
	 * @author hemins
	 * @returns boolean
	 */
	var varifyPageHeaderText = function(name){
		return segmentPO.getSegmentPageHeaderText().then(function(value){
			if(value == name){
				return true;
			}else{
				return false;
			}
		});
	};
		
	/**
	 * This method is use to create segment of different type based on JSON Data.
	 * 
	 * @author hemins
	 * @param segmentTestData
	 * @param segmentName
	 * @param testDataSetName
	 */
	this.createSegment = function(segmentPath,segmentTestData, segmentName,testDataSetName){
		segmentPO.isNewSegmentBtnPresent().then(function(){
			assertsObj.verifyTrue(varifyPageHeaderText("Segments"),"Segments header text is not found on Segments page.");
			if((segmentTestData.dataContract).toUpperCase() == "IMPRESSION SHARING"){
				logger.info("Creating New Segment with "+segmentTestData.availability+" availability and "+segmentTestData.dataContract+" segment rules.");
			}else{
				logger.info("Creating New Segment with "+segmentTestData.availability+" availability and "+segmentTestData.segmentRules+" segment rules.");
			}
		});		
		segmentPO.clickNewSegmentBtn().then(function(){
			isAngularSite(false);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			if((browser.user).toUpperCase()=="CS"){
				segmentPO.selectMarketDropDown(segmentTestData.market);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
			}

			segmentPO.enterSagmentName(segmentName);
			selectAvailability(segmentTestData);
				browser.waitForAngular().then(function(){
					selectMediaProvider(segmentTestData);
				});								
			browser.waitForAngular();
			selectSegmentFilter(segmentTestData);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			selectSegmentRules(segmentTestData);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			segmentPO.clickSaveBtn().then(function(){
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
				browser.waitForAngular();
				browser.navigate().refresh();
				browser.waitForAngular();
			});
			turnDataFilterObj.writeDataProvider(segmentPath, browser.env, browser.language,testDataSetName, 'createdSegment', segmentName);
			varifyPageHeaderText("Segments").then(function(){
				if((segmentTestData.dataContract).toUpperCase() == "IMPRESSION SHARING"){
					logger.info("Created New Segment with "+segmentTestData.availability+" availability and "+segmentTestData.dataContract+" segment rules.");
				}else{
					logger.info("Created New Segment with "+segmentTestData.availability+" availability and "+segmentTestData.segmentRules+" segment rules.");
				}
			});
		});
		
	};

	/**
	 * This method is use to select segment availability based on JSON data 
	 * like All Advertisers, Single Advertiser and Analytics Only.
	 * 
	 * @param segmentTestData
	 */
	var selectAvailability = function(segmentTestData){
		if((segmentTestData.availability).toUpperCase() == "ALL ADVERTISERS"){
			segmentPO.clickAllAdvertiserRbt();
		}else if((segmentTestData.availability).toUpperCase() == "SINGLE ADVERTISER"){
			segmentPO.clickSingleAdvertiserRbt();
			segmentPO.selectSingleAdvertiser(segmentTestData.advertiser);
		}else if((segmentTestData.availability).toUpperCase() == "ANALYTICS ONLY"){
			segmentPO.clickAnalyticsRbt();
		}		
	};
	
	/**
	 * This method is use to select media provider based on JSON data 
	 * like Display, Video, Mobile, Social and Site Personalization.
	 * 
	 * @author hemins
	 * @param segmentTestData
	 */
	var selectMediaProvider = function(segmentTestData){
		//When availability is Analytics Only then no selection of media provider will come.
		utilityObj.browserWaitforseconds(3);
		if((segmentTestData.availability).toUpperCase() != "ANALYTICS ONLY"){
			if((segmentTestData.mediaProviderType).toUpperCase() == "DISPLAY"){
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				segmentPO.clickDisplayMPTab().then(function(){
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(1);
					clickMediaProviderCheckBox(segmentTestData);
				});			
			}else if((segmentTestData.mediaProviderType).toUpperCase() == "VIDEO"){
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				segmentPO.clickVideoMPTab().then(function(){
					clickMediaProviderCheckBox(segmentTestData);
				});			
			}else if((segmentTestData.mediaProviderType).toUpperCase() == "MOBILE"){
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				segmentPO.clickMobileMPTab().then(function(){
					clickMediaProviderCheckBox(segmentTestData);
				});
			}else if((segmentTestData.mediaProviderType).toUpperCase() == "SOCIAL"){
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				segmentPO.clickSocialMPTab().then(function(){
					clickMediaProviderCheckBox(segmentTestData);
				});
			}else if((segmentTestData.mediaProviderType).toUpperCase() == "SITE PERSONALIZATION"){
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				segmentPO.clickSitePersonalizationMPTab().then(function(){
					clickMediaProviderCheckBox(segmentTestData);
				});
			}else{
				logger.info("Please give proper mediaProviderType in segment JSON File");
			}
		}
	};
	
	/**
	 * 	In this method, If availability is All Advertisers then select first media provider from table.
	 *  other wise find for Turn media provider.
	 *  
	 *  @author hemins
	 *  @param segmentTestData
	 */
	var clickMediaProviderCheckBox = function(segmentTestData){

		if((segmentTestData.availability).toUpperCase() == "ALL ADVERTISERS"){
			segmentPO.checkMediaProvider();
			segmentPO.isPixcelIdPresent().then(function(value){
				if(value){
					var pixvalue=null; 
					if(segmentTestData.pixelID){
					pixvalue=segmentTestData.pixelID;
					}
					else{
					pixvalue=123;
					}
					segmentPO.enterPixelId(pixvalue);
				}
			});
			
		}
		else{
			segmentPO.checkTurnMediaProviderCheckBox();
		}
	};
	
	/**
	 * This method is use to select segment filter based on JSON Data 
	 * like Geographies and Postal code.
	 * 
	 * @author hemins
	 * @param segmentTestData
	 */
	var selectSegmentFilter = function(segmentTestData){		
		if((segmentTestData.segmentFilter).toUpperCase() == "YES"){
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			segmentPO.clickExpandSegmentFilter();
			browser.waitForAngular();
			segmentPO.clickSegmentFilterLabel();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			segmentPO.switchToFrame();
			if((segmentTestData.segmentFilterType).toUpperCase() == "GEOGRAPHIES"){
				segmentPO.clickTargetContinentsRdb();
				segmentPO.selectGeoChkBox();
			}else if((segmentTestData.segmentFilterType).toUpperCase() == "POSTAL CODES"){
				segmentPO.clickTargetPostalCodesRdb();
				segmentPO.selectCountryDropDown(segmentTestData.country);
				segmentPO.enterEntries(segmentTestData.Entries);
			}else{
				logger.info("Plese give proper segmentFilter in segment JSON file.");
			}
			segmentPO.clickApplyBtn();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			browser.switchTo().defaultContent();
		}
	};
	
	/**
	 * This method is use to select Segment Rules basd on JSON Data
	 * like Key/Value, Keywords, Taxonomy, Individual Pages, Impression Sharing and Click Sharing.
	 * 
	 * @author hemins
	 * @param segmentTestData
	 */
	var selectSegmentRules = function(segmentTestData){
		if((segmentTestData.segmentRules).toUpperCase() == "KEY/VALUE"){
			enterDataForKeyValueSegmentRules(segmentTestData);
		}else if((segmentTestData.segmentRules).toUpperCase() == "KEYWORDS"){
			enterDataForKeywordSegmentRules(segmentTestData);
		}else if((segmentTestData.segmentRules).toUpperCase() == "TAXONOMY" || (segmentTestData.segmentRules).toUpperCase() == "INDIVIDUAL PAGES"){
			enterDataForTexonomyAndIndividualPageSegmentRules(segmentTestData);
		}else if((segmentTestData.dataContract).toUpperCase() == "IMPRESSION SHARING" || (segmentTestData.dataContract).toUpperCase() == "CLICK SHARING"){
			enterDataForImpressionAndClickSharingSegmentRules(segmentTestData);
		}else{
			logger.info("Please give proper segmentRules or dataContract in segment JSON file.");
		}
	};
	
	/**
	 * This method is use enter data for key/value type of segment rules.
	 * 
	 * @author hemins
	 * @param segmentTestData
	 */
	var enterDataForKeyValueSegmentRules = function(segmentTestData){
		segmentPO.selectNagtedDropDown(segmentTestData.nagtedValue);
		segmentPO.selectDataContractDropDown(segmentTestData.dataContract);
		segmentPO.selectSegmentRuleTypeDropDown(segmentTestData.segmentRules);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(1);
		segmentPO.selectFlexKeyDropDown(segmentTestData.flaxTagName);			
		if((segmentTestData.flexTagOperands).toUpperCase() == "IN A LIST OF"){
			segmentPO.enterFlexKeyValueTxtArea(segmentTestData.flaxTagKeyValue);
		}else if((segmentTestData.flexTagOperands).toUpperCase() == "IN RANGE OF"){
			segmentPO.selectFlexOptionsDropDown(segmentTestData.flexTagOperands);
			segmentPO.enterMinValue(segmentTestData.minValue);
			segmentPO.enterMaxValue(segmentTestData.minValue);
		}else{
			segmentPO.selectFlexOptionsDropDown(segmentTestData.flexTagOperands);
			segmentPO.segmFlexTagExpressionValueTxtBox(segmentTestData.value);
		}
	};
	
	/**
	 * This method is use to enter data for Keyword type of segment rules.
	 * 
	 * @author hemins
	 * @param segmentTestData
	 */
	var enterDataForKeywordSegmentRules = function(segmentTestData){
		segmentPO.selectNagtedDropDown(segmentTestData.nagtedValue);
		segmentPO.selectDataContractDropDown(segmentTestData.dataContract);
		segmentPO.selectSegmentRuleTypeDropDown(segmentTestData.segmentRules);
		segmentPO.enterKeywordTxtArea(segmentTestData.segmentKeywords);
	};
	
	/**
	 * This method is use to enter data for Taxonomy and Individual Page type of Segment rules.
	 * 
	 * @author hemins
	 * @param segmentTestData
	 */
	var enterDataForTexonomyAndIndividualPageSegmentRules = function(segmentTestData){
		segmentPO.selectNagtedDropDown(segmentTestData.nagtedValue);
		segmentPO.selectDataContractDropDown(segmentTestData.dataContract);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(2);
		segmentPO.clickCategories();
		browser.waitForAngular();
		segmentPO.switchToFrame();
		segmentPO.clickSearchButtion();
		browser.waitForAngular();
		segmentPO.selectCategories();
		segmentPO.clickBuildRuleBtn();
		browser.switchTo().defaultContent();
		if((segmentTestData.segmentRules).toUpperCase() == "INDIVIDUAL PAGES"){
			segmentPO.selectFrequenceDropDown(segmentTestData.frequency);
			segmentPO.enterFrequenceDropDown(segmentTestData.frequencyValue);
			segmentPO.selectPeriodDropDropDown(segmentTestData.period);
		}
	};
	
	/**
	 * This method is use to enter data for Impression and Click sharing type of segment rules.
	 * 
	 * @author hemins
	 * @param segmentTestData
	 */
	var enterDataForImpressionAndClickSharingSegmentRules = function(segmentTestData){
		segmentPO.selectNagtedDropDown(segmentTestData.nagtedValue);
		segmentPO.selectDataContractDropDown(segmentTestData.dataContract);
		if((segmentTestData.segmentRules).toUpperCase() != 'ADVERTISER'){
			segmentPO.selectSegmentRuleTypeDropDown(segmentTestData.segmentRules);
			segmentPO.clickTargetObjectBtn();
			utilityObj.browserWaitforseconds(1);
			segmentPO.switchToFrame();
			segmentPO.clickAddOption();
			segmentPO.clickApplyBtn();
			browser.switchTo().defaultContent();
		}
		segmentPO.selectFrequenceDropDown(segmentTestData.frequency);
		segmentPO.enterFrequenceDropDown(segmentTestData.frequencyValue);
		segmentPO.selectPeriodDropDropDown(segmentTestData.period);
	};
	
	/**
	 * This method is used to edit segment name.
	 * 
	 * @author hemins
	 * @param segmentTestData
	 * @param segmentName
	 * @param testDataSetName
	 */
	this.editSegment = function(segmentPath,segmentTestData,segmentName,testDataSetName){
		varifyPageHeaderText("Segments").then(function(){
			assertsObj.verifyTrue(varifyPageHeaderText("Segments"),"Segments header text is not found on Segments page.");
			if((segmentTestData.dataContract).toUpperCase() == "IMPRESSION SHARING"){
				logger.info("Editing New Segment with "+segmentTestData.availability+" availability and "+segmentTestData.dataContract+" segment rules.");
			}else{
				logger.info("Editing New Segment with "+segmentTestData.availability+" availability and "+segmentTestData.segmentRules+" segment rules.");
			}
		});
		
		segmentPO.clickIconMenu();
		segmentPO.clickEditLink();
		isAngularSite(false);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(1);
		segmentPO.enterSagmentName(segmentName);
		segmentPO.clickSaveBtn();
			browser.waitForAngular();
			browser.navigate().refresh();
			browser.waitForAngular();
	
		turnDataFilterObj.writeDataProvider(segmentPath,browser.env, browser.language,testDataSetName, 'updatedSegment',segmentName);
		
		varifyPageHeaderText("Segments").then(function(){
			if((segmentTestData.dataContract).toUpperCase() == "IMPRESSION SHARING"){
				logger.info("Edited New Segment with "+segmentTestData.availability+" availability and "+segmentTestData.dataContract+" segment rules.");
			}else{
				logger.info("Edited New Segment with "+segmentTestData.availability+" availability and "+segmentTestData.segmentRules+" segment rules.");
			}
		});
		
	};
	
	/**
	 * This method is use to copy existing segment.
	 * 
	 * @author hemins
	 * @param segmentTestData
	 */
	this.copySegment = function(segmentTestData){
		
		segmentPO.clickIconMenu();
		segmentPO.clickCopyLink();
		isAngularSite(false);
		browser.waitForAngular();
		browser.navigate().refresh();
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(4);
		segmentPO.clickSaveBtn();
			browser.waitForAngular();
			browser.navigate().refresh();
			browser.waitForAngular();

		
		varifyPageHeaderText("Segments").then(function(){
			if((segmentTestData.dataContract).toUpperCase() == "IMPRESSION SHARING"){
				logger.info("Copied Segment with "+segmentTestData.availability+" availability and "+segmentTestData.dataContract+" segment rules.");
			}else{
				logger.info("Copied Segment with "+segmentTestData.availability+" availability and "+segmentTestData.segmentRules+" segment rules.");
			}
		});
		
	};
	
	/**
	 * This method is use to delete segment.
	 * 
	 * @author hemins
	 * @param segmentTestData
	 */
	this.deleteSegment = function(segmentTestData){
		varifyPageHeaderText("Segments").then(function(){
			assertsObj.verifyTrue(varifyPageHeaderText("Segments"),"Segments header text is not found on Segments page.");
			if((segmentTestData.dataContract).toUpperCase() == "IMPRESSION SHARING"){
				logger.info("Deleting New Segment with "+segmentTestData.availability+" availability and "+segmentTestData.dataContract+" segment rules.");
			}else{
				logger.info("Deleting New Segment with "+segmentTestData.availability+" availability and "+segmentTestData.segmentRules+" segment rules.");
			}
		});
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			segmentPO.clickIconMenu();
			segmentPO.clickDeleteLink();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);			
			segmentPO.clickConfirmDeleteAlert();
			browser.waitForAngular();
	
		
		varifyPageHeaderText("Segments").then(function(){
			if((segmentTestData.dataContract).toUpperCase() == "IMPRESSION SHARING"){
				logger.info("Deleted New Segment with "+segmentTestData.availability+" availability and "+segmentTestData.dataContract+" segment rules.");
			}else{
				logger.info("Deleted New Segment with "+segmentTestData.availability+" availability and "+segmentTestData.segmentRules+" segment rules.");
			}
		});
	};
	
	/**
	 * This method is use to click on Action -> Show Deleted segment
	 * 
	 * @author hemins
	 * @param segmentTestData
	 */
	this.clickShowDeletedSegment = function(segmentTestData) {
		browser.waitForAngular();
		segmentPO.isActionDropDownPresent().then(function(){
			assertsObj.verifyTrue(varifyPageHeaderText("Segments"),"Segments header text is not found on Segments page.");
			if((segmentTestData.dataContract).toUpperCase() == "IMPRESSION SHARING"){
				logger.info("Clicking on Show Deleted Segment for "+segmentTestData.availability+" availability and "+segmentTestData.dataContract+" segment rules.");
			}else{
				logger.info("Clicking on Show Deleted Segment for "+segmentTestData.availability+" availability and "+segmentTestData.segmentRules+" segment rules.");
			}
			segmentPO.clickActionDropDown();
		});
		browser.waitForAngular();
		segmentPO.clickShowDeletedLink();
	};
	
	/**
	 * This method is use to redirect to view audit log page.
	 * 
	 * @author hemins
	 */
	this.gotoViewAuditLog = function() {
		browser.waitForAngular();
		segmentPO.isIconMenuPresent();
			logger.info("Redirect to View Audit Log page of Segment.");
			segmentPO.clickIconMenu();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			segmentPO.clickAuditLogLink();
			isAngularSite(false);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			utilityObj.switchToNewWindow();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			assertsObj.verifyTrue(varifyPageHeaderText('Audit Log'),'Audit Log page header text is not found.');
				
	};
	
	/**
	 * This method is use to search field name in view audit log page.
	 * 
	 * @author hemins
	 * @param searchField
	 */
	this.searchAuditLog = function(searchField) {
		
		segmentPO.isAuditLogSearchBoxPresent().then(function(){
			logger.info("Searching for field name in View Audit Log Page.");
			assertsObj.verifyTrue(varifyPageHeaderText('Audit Log'),'Audit Log page header text is not found.');
			segmentPO.searchAuditLog(searchField);
		});
	};
	
	/**
	 * This method is use to get field name from view audit log page.
	 * 
	 * @author hemins
	 * @returns String
	 */
	this.getFieldNameValue = function() {
		return segmentPO.getFieldNameText().then(function(value){
			logger.info("Getting field name value from Audit log page.");
			return value;
		});		
	};
	
	/**
	 * This method is use to return search Textbox element of segment tab.
	 * 
	 * @author hemins
	 * @returns Element
	 */
	this.returnSearchElement = function(){
		return segmentPO.returnSearchElement();
	};
	
	/**
	 * This method is use to click on Action -> Hide Deleted segment
	 * 
	 * @author hemins
	 * @param segmentTestData
	 */
	this.clickHideDeletedSegment = function(segmentTestData) {
		browser.waitForAngular();
		segmentPO.isActionDropDownPresent().then(function(){
			assertsObj.verifyTrue(varifyPageHeaderText('Segments'),'Segments page header text is not found.');
			if((segmentTestData.dataContract).toUpperCase() == "IMPRESSION SHARING"){
				logger.info("Clicking on Show Deleted Segment for "+segmentTestData.availability+" availability and "+segmentTestData.dataContract+" segment rules.");
			}else{
				logger.info("Clicking on Show Deleted Segment for "+segmentTestData.availability+" availability and "+segmentTestData.segmentRules+" segment rules.");
			}
			segmentPO.clickActionDropDown().then(function(){
				segmentPO.clickHideDeletedLink();
			});
		});

		
	};
	
	/**
	 *  This method is use to clear searching.
	 * 
	 * @author hemins
	 */
	this.clearSearchBoxText =function(){
		segmentPO.returnSearchElement().isPresent().then(function() {
			logger.info("Clearing Search Textbox Text and click on search button.");
			return segmentPO.clearSearchBoxText();
		});
	};
	
	/**
	 *  This method is use to select market Filter.
	 * 
	 * @author hemins
	 * @param advData
	 */
	this.selectMarketFilter =function(marketName){
			logger.info("Selecting Market Filter.");
			assertsObj.verifyTrue(segmentPO.checkFilterFlipper(),"Filter Button is not available");
			segmentPO.clickFilterFlipper();
			segmentPO.clickOnClearFilter();
			browser.waitForAngular();
			segmentPO.clickOnSelectLink().then(function(){
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2);
				segmentPO.enterMarketName(marketName).then(function(){
					browser.waitForAngular();
					segmentPO.clickOnMarketFilter(marketName);
					segmentPO.clickSearchCancel();
					utilityObj.browserWaitforseconds(3);
					browser.waitForAngular();
				});
			});
	
	};
	
	/**
	 * This Method is used to clear filter.
	 * 
	 * @author hemins
	 */
	this.clearAllFilter = function(){
		segmentPO.clickFilterFlipper();
		segmentPO.clickOnClearFilter();
	};
	
	/**
	 *  This method take first row market name from List Item Column "Market".
	 * 
	 * @author hemins
	 */
	this.getMarketName = function(){
			return segmentPO.getMarketNameFromColumn().then(function(value){
				return value;	
		});
	};
	
	/**
	 *  This method used for sorting by segment name.
	 * 
	 * @author hemins
	 */	
	this.sortBySegmentName= function(){
		segmentPO.clickSegmentNameColumn();
		browser.waitForAngular();
		logger.info("Click on segment Name column for sorting.");
	};
	
	/**
	 * This method is used to verify that records are sort by Name or not.
	 * 
	 * @author hemins
	 * @return
	 */
	this.assertNameColumn = function() {
		assertsObj.verifyNotNull(segmentPO.returnReverseSegmentName(),"Not Find column sorting with segment Name.");
		return segmentPO.returnReverseSegmentName();
	};
	
	/**
	 * This function is use to return element list in new console page
	 * @author omp
	 *
	 */
	this.returnElementList=function()
	{
		return segmentPO.segmentElementList();
	};
};

module.exports = new segmentBase();