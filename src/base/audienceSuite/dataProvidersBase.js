/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/
var dataProvidersBase = function(){
	
	var dataProvidersPO = require('../../pageObject/audiencesuite/dataProvidersPO.js');
	var utilityObj = require('../../lib/utility.js');
	var turnDataFilterObj = require('../../lib/turnDataProvider.js');
	var pathObj  =require( '../../testData/audienceSuite/path.js');
	var assertsObj = require('../../lib/assert.js');
	var loggerObj = require('../../lib/logger.js');
	var logger = loggerObj.loggers("dataProvidersBase");

	/**
	 * This Method is used for redirection to Data Provider Tab.
	 * 
	 *  @author hemins
	 */
	this.redirectToDataProvidersTab = function(){
		dataProvidersPO.clickDataMenuLink();
		browser.waitForAngular();
		dataProvidersPO.clickDataProviderSubLink();
		assertsObj.verifyTrue(verifyPageHeaderText("Data Provider"),"Not redirect to Data Provider Tab.");
		verifyPageHeaderText("Data Provider").then(function(value){
			 if(value){
				 logger.info("Redirected to Data Provider Page.");
			 }
		 });
	};
	
	/**
	 * This method is used check page header text of Data Provider page and match page
	 * header text and return true value if page header text is Data Provider otherwise 
	 * return false value.
	 * 
	 * @author hemins
	 * @returns boolean
	 */
	var verifyPageHeaderText = function(name){
		return dataProvidersPO.getDataProvideraPageHeaderText().then(function(value){
			if(value == name){
				return true;
			}else{
				return false;
			}
		});
	};
	
	/**
	 * This method is use to return element list in new console page.
	 * 
	 * @author narottamc
	 */
	
	this.returnElementList=function(){
		return dataProvidersPO.dataProvidersElementList();
	};
	
	/**
	 * This method is use to create Data Provider of Standard and Flextag type.
	 * 
	 * @author hemins
	 * @param dataProviderTestData
	 * @param dataProviderName
	 * @param dataProviderType
	 */
	this.createDataProvider = function(testDataPath,dataProviderTestData, dataProviderName,testDataSetName){
		dataProvidersPO.isNewDataProviderLinkPresent().then(function(){
			if((dataProviderTestData.dataType).toUpperCase() == "STANDARD"){
				logger.info("Creating "+dataProviderTestData.dataType+" type of Data Provider.");
			}else if((dataProviderTestData.dataType).toUpperCase() == "FLEXTAG"){
				logger.info("Creating "+dataProviderTestData.dataType+" type of Data Provider with "+dataProviderTestData.flextagViewerEvent+" event.");
			}else{
				logger.info("Please give dataType as either Standard or Flextag in data provider JSON File.");
			}
		});
		if((dataProviderTestData.dataType).toUpperCase() == "STANDARD"){
			dataProvidersPO.clickOnNewDataProviderLink();
			utilityObj.browserWaitforseconds(2);
			
		}else if((dataProviderTestData.dataType).toUpperCase() == "FLEXTAG"){
			dataProvidersPO.clickActionDropDown();
			dataProvidersPO.clickNewDataProviderActionLink();
			utilityObj.browserWaitforseconds(2);
		}	
		isAngularSite(false);
		if((browser.user).toUpperCase() =="CS"){
			dataProvidersPO.selectMarket(dataProviderTestData.market);
			if((dataProviderTestData.dataType).toUpperCase() == "STANDARD"){
				dataProvidersPO.selectParentDataProvider(dataProviderTestData.parentDataProvider);
			}
		}
		if((dataProviderTestData.dataType).toUpperCase() == "FLEXTAG"){
			dataProvidersPO.selectDataType(dataProviderTestData.dataType);
			dataProvidersPO.enterDataProviderName(dataProviderName);
			dataProvidersPO.enterTopLevelDomains(dataProviderTestData.topLevelDomains);
			utilityObj.uploadFile(dataProvidersPO.returnLogoBrowserButton(),pathObj.getLogoPath());
			dataProvidersPO.enterNotes(dataProviderTestData.notes);
			dataProvidersPO.clickEventCaptureTab();
			dataProvidersPO.selectFlextagViewerEventsTab(dataProviderTestData.flextagViewerEvent);
			dataProvidersPO.clickAddManualEvent();
			if((dataProviderTestData.flextagViewerEvent).toUpperCase() == "JAVASCRIPT"){
				dataProvidersPO.enterViewerName(dataProviderTestData.javascriptName);
				dataProvidersPO.enterVariable(dataProviderTestData.javascriptVariable);
			}else if((dataProviderTestData.flextagViewerEvent).toUpperCase() == "HTML"){
				dataProvidersPO.enterViewerName(dataProviderTestData.htmlName);
				dataProvidersPO.selectHtmlElementType(dataProviderTestData.htmlElementType);
				dataProvidersPO.selectHtmlIdentifier(dataProviderTestData.htmlIdentifier);
				dataProvidersPO.enterHtmlExpression(dataProviderTestData.htmlExpression);
				dataProvidersPO.selectHtmlAttribute(dataProviderTestData.htmlAttribute);
			}else{
				logger.info("Please give flextagViewerEvent as either Javascript or HTML in data provider JSON File.");
			}
			dataProvidersPO.clickSave();
		}else if((dataProviderTestData.dataType).toUpperCase() == "STANDARD"){
			dataProvidersPO.enterDataProviderName(dataProviderName);
			utilityObj.uploadFile(dataProvidersPO.returnLogoBrowserButton(),pathObj.getLogoPath());
			dataProvidersPO.enterNotes(dataProviderTestData.notes);
			dataProvidersPO.clickSave();
		}
		turnDataFilterObj.writeDataProvider(testDataPath,browser.env,browser.language,testDataSetName, 'createdDataProvider', dataProviderName);
		dataProvidersPO.returnSearchElement().isPresent().then(function(){
			assertsObj.verifyTrue(verifyPageHeaderText("Data Provider"),"Data Provider header text is not found on Data Provider page.");
			if((dataProviderTestData.dataType).toUpperCase() == "STANDARD"){
				logger.info("Created "+dataProviderTestData.dataType+" type of Data Provider.");
			}else if((dataProviderTestData.dataType).toUpperCase() == "FLEXTAG"){
				logger.info("Created "+dataProviderTestData.dataType+" type of Data Provider with "+dataProviderTestData.flextagViewerEvent+" event.");
			}else{
				logger.info("Please give dataType as either Standard or Flextag in data provider JSON File.");
			}
		});
	};
	
	/**
	 * This method is use to Edit Data Provider of Standard and Flextag type.
	 * 
	 * @author hemins
	 * @param dataProviderTestData
	 * @param dataProviderName
	 * @param dataProviderType
	 */
	this.editDataProvider = function(testDataPath,dataProviderTestData,dataProviderName,testDataSetName){
		browser.waitForAngular();
		dataProvidersPO.getDataProvideraPageHeaderText().then(function(){
			if((dataProviderTestData.dataType).toUpperCase() == "STANDARD"){
				logger.info("Editing "+dataProviderTestData.dataType+" type of Data Provider.");
			}else if((dataProviderTestData.dataType).toUpperCase() == "FLEXTAG"){
				logger.info("Editing "+dataProviderTestData.dataType+" type of Data Provider with "+dataProviderTestData.flextagViewerEvent+" event.");
			}else{
				logger.info("Please give dataType as Standard or Flextag in dataProvider JSON File.");
			}
		});
		dataProvidersPO.clickIconMenu();
		utilityObj.browserWaitforseconds(2);
		browser.waitForAngular();
		dataProvidersPO.clickEditLink();
		utilityObj.browserWaitforseconds(1);
		isAngularSite(false);
		dataProvidersPO.enterDataProviderName(dataProviderName);
		utilityObj.browserWaitforseconds(3);
		dataProvidersPO.clickSave();
		turnDataFilterObj.writeDataProvider(testDataPath,browser.env, browser.language,testDataSetName, 'updatedDataProvider',dataProviderName);
		dataProvidersPO.returnSearchElement().isPresent().then(function(){
			if((dataProviderTestData.dataType).toUpperCase() == "STANDARD"){
				logger.info("Edited "+dataProviderTestData.dataType+" type of Data Provider.");
			}else if((dataProviderTestData.dataType).toUpperCase() == "FLEXTAG"){
				logger.info("Edited "+dataProviderTestData.dataType+" type of Data Provider with "+dataProviderTestData.flextagViewerEvent+" event.");
			}else{
				logger.info("Please give dataType as Standard or Flextag in dataProvider JSON File.");
			}
			assertsObj.verifyTrue(verifyPageHeaderText("Data Provider"),"Data Provider header text is not found on Data Provider page.");
		});
	};
	
	/**
	 * This method is use to return data provider name of Standard and Flextag type of view data provider page.
	 * 
	 * @author hemins
	 * @param dataProviderName
	 * @param dataProviderType
	 */
	this.returnDataProviderNameOfViewPage = function(dataProviderName,dataProviderType){
		isAngularSite(false);
		return dataProvidersPO.getExpectedDataProviderName().then(function(value){
			logger.info("Viewing "+dataProviderType+" type of Data Provider.");
			assertsObj.verifyEqual(dataProvidersPO.getExpectedDataProviderName(),dataProviderName,
			"Not match data provider name in View Data Provider page.");
			logger.info("Viewed "+dataProviderType+" type of Data Provider.");
			console.log(value);
			return value;
		});	
	};
	
	/**
	 * This Method is use to delete Standard and Flextag type of data provider.
	 * 
	 * @author hemins
	 * @param dataProviderType
	 */
	this.deleteDataProvider = function(dataProviderTestData){
		dataProvidersPO.getDataProvideraPageHeaderText().then(function(value){
			if((dataProviderTestData.dataType).toUpperCase() == "STANDARD"){
				logger.info("Deleting "+dataProviderTestData.dataType+" type of Data Provider.");
			}else if((dataProviderTestData.dataType).toUpperCase() == "FLEXTAG"){
				logger.info("Deleting "+dataProviderTestData.dataType+" type of Data Provider with "+dataProviderTestData.flextagViewerEvent+" event.");
			}else{
				logger.info("Please give dataType as Standard or Flextag in dataProvider JSON File.");
			}
			assertsObj.verifyTrue(verifyPageHeaderText("Data Provider"),"Data Provider header text is not found on Data Provider page.");
			dataProvidersPO.clickIconMenu();
			dataProvidersPO.clickDeleteLink();
			browser.waitForAngular();
        	 dataProvidersPO.clickConfirmDeleteAlert();
		});
		
		dataProvidersPO.getDataProvideraPageHeaderText().then(function(value){
			if((dataProviderTestData.dataType).toUpperCase() == "STANDARD"){
				logger.info("Deleted "+dataProviderTestData.dataType+" type of Data Provider.");
			}else if((dataProviderTestData.dataType).toUpperCase() == "FLEXTAG"){
				logger.info("Deleted "+dataProviderTestData.dataType+" type of Data Provider with "+dataProviderTestData.flextagViewerEvent+" event.");
			}else{
				logger.info("Please give dataType as Standard or Flextag in dataProvider JSON File.");
			}
		});
	};
	
	/**
	 * This method is use to click on Action -> Show Deleted data provider
	 * 
	 * @author hemins
	 * @param dataProviderType
	 */
	this.clickShowDeletedDataProvider = function(dataProviderType) {
		browser.waitForAngular();
		dataProvidersPO.isActionDropDownPresent().then(function(){
			assertsObj.verifyTrue(verifyPageHeaderText("Data Provider"),"Data Provider header text is not found on Data Provider page.");
			logger.info("Clicking on Show Deleted Data Provider for "+dataProviderType+" type.");
			dataProvidersPO.clickActionDropDown();
		});
		browser.waitForAngular();
		dataProvidersPO.clickShowDeleted();
		browser.waitForAngular();
	};
	
	/**
	 * This method is use to redirect to view audit log page.
	 * 
	 * @author hemins
	 */
	this.gotoViewAuditLog = function() {
		browser.waitForAngular();
		dataProvidersPO.isIconMenuPresent().then(function(){
			logger.info("Redirect to View Audit Log page of Advertiser.");
			dataProvidersPO.clickIconMenu();			
			dataProvidersPO.clickAuditLogLink();
			browser.waitForAngular();
			utilityObj.switchToNewWindow();
			assertsObj.verifyTrue(verifyPageHeaderText('Audit Log'),'Audit Log page header text is not found.');
		});				
	};
	
	/**
	 * This method is use to search field name in view audit log page.
	 * 
	 * @author hemins
	 * @param searchField
	 */
	this.searchAuditLog = function(searchField) {
		utilityObj.browserWaitforseconds(3);
		dataProvidersPO.isAuditLogSearchBoxPresent().then(function(){
			logger.info("Searching for field name in View Audit Log Page.");
			assertsObj.verifyTrue(verifyPageHeaderText('Audit Log'),'Audit Log page header text is not found.');
			dataProvidersPO.searchAuditLog(searchField);
		});
	};
	
	/**
	 * This method is use to get field name from view audit log page.
	 * 
	 * @author hemins
	 * @returns String
	 */
	this.getFieldNameValue = function() {
		return dataProvidersPO.getFieldNameText().then(function(value){
			logger.info("Getting field name value from Audit log page.");
			return value;
		});		
	};
	
	/**
	 * This method is use to click on Action -> Hide Deleted Data Provider
	 * 
	 * @author hemins
	 * @param dataProviderType
	 */
	this.clickHideDeletedDataProvider = function(dataProviderType) {
		browser.waitForAngular();
		dataProvidersPO.isActionDropDownPresent().then(function(){
			assertsObj.verifyTrue(verifyPageHeaderText('Data Provider'),'Data Provider page header text is not found.');
			logger.info("Clicking on Hide Deleted Data Provider for "+dataProviderType+" type.");
			dataProvidersPO.clickActionDropDown();
		});

		dataProvidersPO.clickHideDeleted();
	};
	
	/**
	 * This method is use to return search Textbox element of Data Provider tab.
	 * 
	 * @author hemins
	 * @returns Element
	 */
	this.returnSearchElement = function(){
		return dataProvidersPO.returnSearchElement();
	};
	
	/**
	 * This method is use to redirect view data provider page.
	 * 
	 * @author hemins
	 */
	this.redirectToViewDataProviderPage = function() {
		    dataProvidersPO.clickIconMenu();
			logger.info("Redirecting to view data provider page.");
			dataProvidersPO.clickViewLink();
	};
	
	/**
	 *  This method is use to clear searching.
	 * 
	 * @author hemins
	 */
	this.clearSearch =function(){
		dataProvidersPO.getDataProvideraPageHeaderText().then(function() {
			logger.info("Clearing Search Textbox and click on search button.");
			dataProvidersPO.clearSearchBox();
		});
	};
	
	/**
	 *  This method is use to select market Filter.
	 * 
	 * @author hemins
	 * @param marketName
	 */
	this.selectMarketFilter =function(marketName){
		dataProvidersPO.getDataProvideraPageHeaderText().then(function() {
			logger.info("Selecting Market Filter.");
			assertsObj.verifyTrue(utilityObj.elementCheck(dataProvidersPO.checkFilterFlipper()),"Filter Button is not available");
			dataProvidersPO.clickFilterFlipper();
			dataProvidersPO.clickOnClearFilter();
			dataProvidersPO.clickOnSelectLink();
			dataProvidersPO.enterMarketName(marketName);
			dataProvidersPO.clickOnMarketFilter(marketName);
		});	
	};
	
	
	/**
	 * This method is use to clear market filter in new console page.
	 * 
	 * @author narottamc
	 */
	
	this.clearNewFilter=function(){
		dataProvidersPO.isMarketFilterPresent().then(function(value){
			if(value){dataProvidersPO.clickOnClearFilter();
			};
		});
	};
	/**
	 *  This method is use to select market Filter in list page.
	 * 
	 * @author narottamc
	 * @param marketName
	 */
	this.selectMarketFilterNew =function(marketName){
		dataProvidersPO.getDataProvideraPageHeaderText().then(function() {
			logger.info("Selecting Market Filter.");
			dataProvidersPO.clickNewFilterButton();
			utilityObj.browserWaitforseconds(1);
			dataProvidersPO.selectNewMarketTypeFilter();
			dataProvidersPO.enterNewMarketNameFilter(marketName).then(function(){;
			utilityObj.browserWaitforseconds(2);
			dataProvidersPO.clickOnNewFilter(marketName);
			utilityObj.browserWaitforseconds(2);
			dataProvidersPO.clickCancelSearchIcon();
			});
		});	
	};
	
	/**
	 *  This method take first row market name from List Item Column "Market".
	 * 
	 * @author hemins
	 */
	this.getMarketName = function(){
			return dataProvidersPO.getMarketNameFromColumn().then(function(value){
				return value;	
		});
	};
	
	/**
	 *  This method used for sorting by data provider name.
	 * 
	 * @author hemins
	 */
	
	this.sortByDataProviderName= function(){
		dataProvidersPO.clickDataProviderNameColumn();
		logger.info("Click on Data Provider Name column for sorting.");
	};
	
	/**
	 * This method is used to verify that records are sort by Name or not.
	 * 
	 * @author hemins
	 * @return
	 */
	this.assertNameColumn = function() {
		assertsObj.verifyNotNull(dataProvidersPO.returnReverseDataProviderName(),"Not Find column sorting with Data Provider Name.");
		return dataProvidersPO.returnReverseDataProviderName();
	};
	
	/**
	 * This method is used to redirect to Obtain Pixel Pop-up.
	 * 
	 * @author hemins
	 */
	this.gotoObtainPixel = function(){
		dataProvidersPO.getDataProvideraPageHeaderText().then(function() {
			logger.info("Goto Obtain Pixel page for Flextag data provider.");
			dataProvidersPO.clickObtainPixel();
			browser.switchTo().defaultContent();
		});
	};
	
	/**
	 * This method is used to return header text of Obtain Pixel Pop-up.
	 * 
	 * @author hemins
	 */
	this.returnObtainPixelHeaderName = function(){
		return dataProvidersPO.getObtainPixelHeaderText().then(function(value){
			logger.info("Getting Obtain Pixel Header Name.");
			dataProvidersPO.clickObtainPixelCloseButton();
			return value;
		});
	};
};

module.exports = new dataProvidersBase();