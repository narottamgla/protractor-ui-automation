
	var dataContractBase = function(){
		
	var dataContractPO = require('../../pageObject/audiencesuite/dataContractsPO.js');
	var utilityObj = require('../../lib/utility.js');
	var turnDataFilterObj = require('../../lib/turnDataProvider.js');
	var pathObj  =require( '../../testData/audienceSuite/path.js');
	var assertsObj = require('../../lib/assert.js');
	var loggerObj = require('../../lib/logger.js');
	var logger = loggerObj.loggers("dataContractBase");
	
	
	/**
	 * This method is use to redirect view data contract page.
	 * 
	 * @author narottamc
	 */
	this.redirectToViewDataContractsPage = function() {
		dataContractPO.clickIconMenu().then(function(){
			logger.info("Redirecting to view data Contract page.");
			dataContractPO.clickViewLink();
		});
	};
	
	/**
	 * This Method is used for redirection to Data Provider Tab.
	 * 
	 *  @author narottamc
	 */
	this.redirectToDataContractsTab = function(){
		browser.waitForAngular();
		browser.navigate().refresh();
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		dataContractPO.clickOnDataLink().then(function(){
		browser.waitForAngular();
		dataContractPO.clickOnDataContractsSubLink();
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		});
	};
	

	/**
	 * This Method is used for redirection to CS Data Provider Tab.
	 * 
	 *  @author narottamc
	 */
	this.redirectToCSDataContractsTab = function(){
		dataContractPO.clickOnDataLink();
		browser.waitForAngular();
		dataContractPO.clickOnDataContractsSubLink();
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
	
	};
	
	
		
	/**
	 * This function is used to enter all details for general tab of creation of data contract
	 * 
	 * @author narottamc
	 * @param dataContractTestData,dataContractName
	 */
	var generalTabEnter=function(dataContractTestData,dataContractName){
		dataContractPO.enterNewDataContractName(dataContractName);
		if(dataContractTestData.parentDataContract!=''){
			dataContractPO.selectParentDataContract(dataContractTestData.parentDataContract);
		}
		dataContractPO.selectCurrencyType(dataContractTestData.currency);
		if((dataContractTestData.availability).toUpperCase() == 'ALL'){
			dataContractPO.clickAllAdvertiser();
		}
		else if((dataContractTestData.availability).toUpperCase() == 'SINGLE'){
			dataContractPO.clickSingleAdvertiser().then(function(){
				utilityObj.browserWaitforseconds(3);
				dataContractPO.clickSingleAdvertiser();
				dataContractPO.clickonFlextagDataTypeFrame();
				dataContractPO.selectSingleAdvertiser(dataContractTestData.singleAdvertiserName);
			});
		}
		else if((dataContractTestData.availability).toUpperCase() == 'ANALYTICS'){
			dataContractPO.clickavailabilityAnalytics().then (function(){
				utilityObj.browserWaitforseconds(3);
				dataContractPO.clickavailabilityAnalytics();
			});
		}		
	};
	
	/**
	 * This function is used to fill details of rate schedule tab of data contract tab
	 * 
	 * @author narottamc
	 * @param dataContractTestData
	 */
	var rateSheduleTabDetailsEnter=function(dataContractTestData){
		var startDate='';
		var endDate='';
		var userType = browser.user;
		if(userType=='CS'){
			dataContractPO.selectAudienceDataCost(dataContractTestData.audienceDataCost);
		};
		
		if(dataContractTestData.startDate==''){
			startDate=utilityObj.getTodayDateOnly();
		}
		else{
			startDate=dataContractTestData.startDate;
		}
		if(dataContractTestData.endDate==''){
			endDate=utilityObj.getMonthDate();
		}
		else{
			endDate=dataContractTestData.endDate;
		}
		dataContractPO.selectStartDate(startDate);
		dataContractPO.selectEndDate(endDate);
		dataContractPO.selectRateType(dataContractTestData.rateType);
		if((dataContractTestData.rateType).toUpperCase() == "FLAT FEE"){
			dataContractPO.enterFlatfee(dataContractTestData.rate1);	
		}
		else if((dataContractTestData.rateType).toUpperCase() == "CPM (USAGE CPM)"){
			dataContractPO.enterCPMValue(dataContractTestData.rate1);
		}
		else if((dataContractTestData.rateType).toUpperCase() == "CPUU + CPM"){
			dataContractPO.enterCPUUValue(dataContractTestData.rate1);
			dataContractPO.enterCPMValue(dataContractTestData.rate2);
		}
		else if((dataContractTestData.rateType).toUpperCase() == "COST PER STAMP"){
			dataContractPO.enterCostPerStamp(dataContractTestData.rate1);
		}
		else if((dataContractTestData.rateType).toUpperCase() == "REVENUE SHARE %"){
			dataContractPO.enterRevenueShare(dataContractTestData.rate1);
		}
	};
	
	
	/**
	 * This function used to fill the key event details for html/javaScript
	 *
	 * @author narottamc
	 * @param row,key,eventName,transform,val1,val2
	 */
	var eventDetailsFill=function(row,key,eventName,transform,val1,val2){
		dataContractPO.clickOnFlexDataTypeAddNewKey().then(function(){
			var rownumber=row+1;
			dataContractPO.enterKey(rownumber,key);
			dataContractPO.enterEventName(rownumber,eventName);
			dataContractPO.selectTransform(rownumber,transform);
			browser.waitForAngular();
			if((transform.toUpperCase())!='NONE'){
				if((transform.toUpperCase())!='IN RANGE'){
					dataContractPO.eneterTransformValue(rownumber,val1);
				}
				else{
					dataContractPO.clickonFlextagDataTypeFrame();
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(1);
					dataContractPO.eneterTransformMinRange(rownumber,val1);
					dataContractPO.eneterTransformMaxRange(rownumber,val2);
				}
			}
			});
		
		
	};
	/**
	 * This function is used the fill the key details in datatype
	 * 
	 * @author narottamc
	 * @param dataContractTestData
	 */
	var keyDetailsEnter=function(dataContractTestData){
		if((dataContractTestData.html).toUpperCase()=='YES'){
				if((dataContractTestData.row1).toUpperCase()=='YES'){
					browser.waitForAngular();
					eventDetailsFill(1,dataContractTestData.key1,dataContractTestData.eventName1,dataContractTestData.transformation1,dataContractTestData.value11,dataContractTestData.value12);
					if((dataContractTestData.row2).toUpperCase()=='YES'){
						browser.waitForAngular();
					eventDetailsFill(2,dataContractTestData.key2,dataContractTestData.eventName2,dataContractTestData.transformation2,dataContractTestData.value21,dataContractTestData.value22);
					if((dataContractTestData.row3).toUpperCase()=='YES'){
						browser.waitForAngular();
						eventDetailsFill(3,dataContractTestData.key3,dataContractTestData.eventName3,dataContractTestData.transformation3,dataContractTestData.value31,dataContractTestData.value32);
						if((dataContractTestData.row4).toUpperCase()=='YES'){
							browser.waitForAngular();
							eventDetailsFill(4,dataContractTestData.key4,dataContractTestData.eventName4,dataContractTestData.transformation4,dataContractTestData.value41,dataContractTestData.value42);
							if((dataContractTestData.row5).toUpperCase()=='YES'){
								browser.waitForAngular();
								eventDetailsFill(5,dataContractTestData.key5,dataContractTestData.eventName5,dataContractTestData.transformation5,dataContractTestData.value51,dataContractTestData.value52);
								}
							}
						}
					}
				
				}		
		
		} //Ending of HTML key values entry
		if((dataContractTestData.javaScript).toUpperCase()=='YES'){
			dataContractPO.javaScriptTabClick().then(function(){
				if((dataContractTestData.jrow1).toUpperCase()=='YES'){
					browser.waitForAngular();
				eventDetailsFill(1,dataContractTestData.jkey1,dataContractTestData.jeventName1,dataContractTestData.jtransformation1,dataContractTestData.jvalue11,dataContractTestData.jvalue12);
					if((dataContractTestData.jrow2).toUpperCase()=='YES'){
						browser.waitForAngular();
					eventDetailsFill(2,dataContractTestData.jkey2,dataContractTestData.jeventName2,dataContractTestData.jtransformation2,dataContractTestData.jvalue21,dataContractTestData.jvalue22);
					if((dataContractTestData.jrow3).toUpperCase()=='YES'){
						browser.waitForAngular();
						eventDetailsFill(3,dataContractTestData.jkey3,dataContractTestData.jeventName3,dataContractTestData.jtransformation3,dataContractTestData.jvalue31,dataContractTestData.jvalue32);
						if((dataContractTestData.jrow4).toUpperCase()=='YES'){
							browser.waitForAngular();
							eventDetailsFill(4,dataContractTestData.jkey4,dataContractTestData.jeventName4,dataContractTestData.jtransformation4,dataContractTestData.jvalue41,dataContractTestData.jvalue42);
							if((dataContractTestData.jrow5).toUpperCase()=='YES'){
								browser.waitForAngular();
								eventDetailsFill(5,dataContractTestData.jkey5,dataContractTestData.jeventName5,dataContractTestData.jtransformation5,dataContractTestData.jvalue51,dataContractTestData.jvalue52);
								}
							}
						}
					}
				}
				});
		} //Ending of JavaScript key values entry
		
		
	};
	
	
	/**
	 * This function used to create taxonomy node tree
	 * 
	 * @author narottamc
	 * @param dataContractTestData
	 */
	
	var taxonomyData=function(dataContractTestData){
		if((dataContractTestData.taxonomy).toUpperCase()=='YES'){
		dataContractPO.clickOnFlexDataTypeAddTaxonomy().then(function(){
			if((dataContractTestData.parent)!=''){
			dataContractPO.clickOnFlexDataTypeAddParent().then(function(){				
				dataContractPO.enterParenrtTaxonomy(dataContractTestData.parent).then(function(){
					if((dataContractTestData.child)!=''){
						dataContractPO.clickChildTaxonomyButton().then(function(){
							utilityObj.confirmAlert();
							dataContractPO.enterParenrtTaxonomy(dataContractTestData.child).then( function(){
								dataContractPO.clickOnChildNode();
							});
						});
					}
				});
					
			});
		}			
		});
		}
	};
	
	/**
	 * This function is used to create piggyback
	 * 
	 * @author narottamc
	 * @param dataContractTestData	
	 */
	var addPiggyback=function(dataContractTestData){
		
		dataContractPO.clickOnFlexDataTypeAddPiggyBackBtn();
		dataContractPO.selectFlexDataTypepiggyBackType(dataContractTestData.piggyBackType);
			dataContractPO.enterFlexDataTypePiggyBackName(dataContractTestData.piggyName).then(function(){
				if((dataContractTestData.piggyBackType).toUpperCase()==' CAMPAIGN SUITE BEACON'){
					dataContractPO.enterPiggyBackAdvertiser(dataContractTestData.advertiser);
					dataContractPO.enterPiggyBackBeacon(dataContractTestData.beacon);
					dataContractPO.enterPiggyBackOrderId(dataContractTestData.orderId);
					dataContractPO.enterPiggyBackOrderValue(dataContractTestData.orderValue);
					dataContractPO.enterPiggyBackCurrency(dataContractTestData.currencyCode);				
				}
				if((dataContractTestData.piggyBackType).toUpperCase()=='CUSTOM HTML'){
					dataContractPO.selectFlexDataTypeInsertPageEvent(dataContractTestData.insertPageEvents);
					utilityObj.browserWaitforseconds(1);
				}
				
				if((dataContractTestData.piggyBackType).toUpperCase()==' AUDIENCE SUITE DATA COLLECTION PIXEL'){
					dataContractPO.enterPiggyBackPixelId(dataContractTestData.pixcelId);
					dataContractPO.enterPiggyBackTaxonomyId(dataContractTestData.taxonomyId);
					dataContractPO.enterPiggyBackKeywords(dataContractTestData.keyword);
					dataContractPO.enterPiggyBackKeywordPairs(dataContractTestData.keyValuePair);
					dataContractPO.enterPiggyBackDPUserId(dataContractTestData.dataProviderUser);
					dataContractPO.enterPiggyBackOutput(dataContractTestData.outPut);
				}
				if((dataContractTestData.piggyBackType).toUpperCase()==' AUDIENCE SUITE INDIVIDUAL PAGE PIXEL'){
					dataContractPO.enterPiggyBackPixelId(dataContractTestData.pixcelId);
					dataContractPO.enterPiggyBackOrderId(dataContractTestData.orderId);
					dataContractPO.enterPiggyBackOrderValue(dataContractTestData.orderValue);
				}				
				
				if((dataContractTestData.piggyBackType).toUpperCase()=='GOOGLE SALES ACTIVITY TAG'){
					dataContractPO.enterPiggyBackSubdomain(dataContractTestData.subDomamain);
					dataContractPO.enterPiggyBackAdvertiser(dataContractTestData.advertiserId);
					dataContractPO.enterPiggyBackGroupTag(dataContractTestData.groupTag);
					dataContractPO.enterPiggyBackActivityTag(dataContractTestData.ActivityTag);
					dataContractPO.enterPiggyBackOrderId(dataContractTestData.orderId);
					dataContractPO.enterPiggyBackKeywordU(dataContractTestData.u);
					dataContractPO.enterPiggyBackUValue(dataContractTestData.uvalue);
					dataContractPO.enterPiggyBackQuantity(dataContractTestData.quantity);
					dataContractPO.enterPiggyBackCost(dataContractTestData.cost);
				}
				if((dataContractTestData.piggyBackType).toUpperCase()=='GOOGLE COUNTER TAG'){
					dataContractPO.enterPiggyBackSubdomain(dataContractTestData.subDomamain);
					dataContractPO.enterPiggyBackAdvertiser(dataContractTestData.advertiserId);
					dataContractPO.enterPiggyBackGroupTag(dataContractTestData.groupTag);
					dataContractPO.enterPiggyBackActivityTag(dataContractTestData.ActivityTag);
					dataContractPO.enterPiggyBackOrderId(dataContractTestData.orderId);
					dataContractPO.enterPiggyBackKeywordU(dataContractTestData.u);
					dataContractPO.enterPiggyBackUValue(dataContractTestData.uvalue);
				}
				dataContractPO.clickSavePiggyBackButton();
			});
		};
		
	/**
	 * This function is used the call the functions of datatype
	 * 
	 * @author narottamc
	 * @param dataContractTestData
	 */
	var dataTypeDetailsEnter=function(dataContractTestData){		
		keyDetailsEnter(dataContractTestData);
		taxonomyData(dataContractTestData);
		if((dataContractTestData.piggyBackType).toUpperCase()=='GOOGLE COUNTER TAG'){
		addPiggyback(dataContractTestData.googleCounterTag);
		}
		else if((dataContractTestData.piggyBackType).toUpperCase()=='GOOGLE SALES ACTIVITY TAG'){
			addPiggyback(dataContractTestData.googleSalesActivityTag);
		}
		else if((dataContractTestData.piggyBackType).toUpperCase()==' AUDIENCE SUITE INDIVIDUAL PAGE PIXEL'){
			addPiggyback(dataContractTestData.turnAudienceSuiteIndividualPagePixel);	
				}
		else if((dataContractTestData.piggyBackType).toUpperCase()==' AUDIENCE SUITE DATA COLLECTION PIXEL'){
			addPiggyback(dataContractTestData.turnAudienceSuiteDataCollectionPixel);
		}
		else if((dataContractTestData.piggyBackType).toUpperCase()=='CUSTOM HTML'){
			
			addPiggyback(dataContractTestData.customHtml);
		}
		else if((dataContractTestData.piggyBackType).toUpperCase()==' CAMPAIGN SUITE BEACON'){
			
			addPiggyback(dataContractTestData.turnCampaignSuiteBeacon);
		}
		utilityObj.browserWaitforseconds(2);
		if((dataContractTestData.piggyBackType1).toUpperCase()=='GOOGLE COUNTER TAG'){
			addPiggyback(dataContractTestData.googleCounterTag);
			}
			else if((dataContractTestData.piggyBackType1).toUpperCase()=='GOOGLE SALES ACTIVITY TAG'){
				addPiggyback(dataContractTestData.googleSalesActivityTag);
			}
			else if((dataContractTestData.piggyBackType1).toUpperCase()==' AUDIENCE SUITE INDIVIDUAL PAGE PIXEL'){
				addPiggyback(dataContractTestData.turnAudienceSuiteIndividualPagePixel);	
					}
			else if((dataContractTestData.piggyBackType1).toUpperCase()==' AUDIENCE SUITE DATA COLLECTION PIXEL'){
				addPiggyback(dataContractTestData.turnAudienceSuiteDataCollectionPixel);
			}
			else if((dataContractTestData.piggyBackType1).toUpperCase()=='CUSTOM HTML'){
				addPiggyback(dataContractTestData.customHtml);
				
			}
			else if((dataContractTestData.piggyBackType1).toUpperCase()==' CAMPAIGN SUITE BEACON'){
				
				addPiggyback(dataContractTestData.turnCampaignSuiteBeacon);
			}
		};
	
	/**
	 * This function is used to select data Provider from filter option
	 * 
	 * @author narottamc
	 * @param dataProvider
	 */
		
	this.selectDPFromfilter=function(dataProvider){
		dataContractPO.getDataContractPageHeaderText().then(function(){
				logger.info("Selecting Data Contract Market Filter.");
				dataContractPO.clickOnClearFilter();
				utilityObj.browserWaitforseconds(2);
				browser.waitForAngular();
				dataContractPO.clickNewFilterButton();
				dataContractPO.selectNewDataProviderTypeFilter();
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2);
				dataContractPO.enterNewDataProviderNameFilter(dataProvider);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
				dataContractPO.clickOnNewMarketFilter(dataProvider);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);		
		});
	};
	
	/**
	 * This function is used click on new data contract button
	 * 
	 * @author narottamc
	 */
	this.clickNewDataContractsButton=function(){
		dataContractPO.clickOnNewDataContractsButton();
	};
			
	/**
	 * This function used to select dataprovider from popup
	 * 
	 * @author vishalbha
	 * @param dataContractTestData
	 */
	
	this.selectDataContractFromNewButton=function(dataContractTestData){
		utilityObj.isAngularPage(browser.console);
		dataContractPO.clickOnNewDataContractsButton();
		utilityObj.browserWaitforseconds(5);
		dataContractPO.selectDataProviderfromDropDown(dataContractTestData.dataProviderName);
		utilityObj.browserWaitforseconds(2);
		dataContractPO.clickOnFrameNextButton();
	};
	
	/**
	 * This function is used to select collection type .
	 * 
	 * @author vishalbha
	 * @param dataContractTestData,getTaxonomyPath
	 */
	var selectCollectionType = function(dataContractTestData,getTaxonomyPath){
		dataContractPO.selectCollectiontype(dataContractTestData.collectiontype);
		dataContractPO.clickOnFrame();	
		if((dataContractTestData.collectiontype).toUpperCase()=='API'){
			if((dataContractTestData.idType).toUpperCase()=='DEVICE ID'){
				dataContractPO.selectCollectiontype(dataContractTestData.idType);
				dataContractPO.checkIFAInDeviceIdType();
				dataContractPO.checkPlatformIdInDeviceIdType();
			}
			if((dataContractTestData.checkedCategory).toUpperCase()=='CHECKED'){
				dataContractPO.clickOnCategoriesCheckbutton().then(function(){
				browser.waitForAngular();	
				importTaxonomy(getTaxonomyPath);
				});
			}
			else if((dataContractTestData.checkedKeyword).toUpperCase()=='CHECKED'){
				dataContractPO.clickOnKeywordsCheckbutton();
			}
			else if((dataContractTestData.checkedKeys).toUpperCase()=='CHECKED'){
				dataContractPO.clickOnKeysCheckbutton().then (function(){					
				dataContractPO.enterKeyInTextBox(dataContractTestData.keys);
				});
			}
		}
		else if((dataContractTestData.collectiontype).toUpperCase()=='FILE'){
				dataContractPO.selectCollectiontype(dataContractTestData.collectiontype).then(function(){
				browser.waitForAngular();
				importTaxonomy(getTaxonomyPath);
				
				});
		}
		else if((dataContractTestData.collectiontype).toUpperCase()=='PIXEL'){
				dataContractPO.selectCollectiontype(dataContractTestData.collectiontype);
				dataContractPO.selectPixelType(dataContractTestData.pixelType);
				dataContractPO.clickOnKeysCheckbutton().then (function(){
				dataContractPO.enterKeyInTextBox(dataContractTestData.keys);
				});
		};
	};
	
	/**
	 * This function is used to selectPiggybackRules 
	 * 
	 * @author vishalbha
	 * @param dataContractTestData
	 */
	
	var selectPiggybackRules = function(dataContractTestData){
		dataContractPO.selectEventTypeRule(dataContractTestData.eventRule);
		dataContractPO.selectTransformationeRule(dataContractTestData.transformationRule);
		dataContractPO.selectRuleValue(dataContractTestData.ruleValue);
	};
	
	/**
	 * This function is used to import taxonomy 
	 * 
	 * @author vishalbha
	 * @param getTaxonomyPath
	 */
	
	var importTaxonomy = function(getTaxonomyPath){
		isAngularSite(false);
		dataContractPO.ClickOnImportTaxonomy();
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);	
		dataContractPO.switchToFrame();
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);	
		utilityObj.uploadFile(element(by.css('.textInput')),getTaxonomyPath);
		 browser.waitForAngular();
		dataContractPO.clickOnApplyButton();
		utilityObj.browserWaitforseconds(3);		
	};
	
	/**
	 * This function is used to create standard and flextag type of data contract with cross device duplication.
	 * 
	 * @author hemins
	 * @param dataContractTestData,dataContractName,getTaxonomyPath,testDataName
	 */
	this.createDataContracts = function(testDataPath,dataContractTestData,dataContractName,getTaxonomyPath,testDataName){
		isAngularSite(false);
		dataContractPO.isDataContractNameTxtBoxPresent().then(function(){
			logger.info("Creating "+ dataContractTestData.dataContractstype +" Data Contract with "+dataContractTestData.availability +" availability and "+dataContractTestData.availability+ " collectiontype.");
			generalTabEnter(dataContractTestData,dataContractName);
			if(dataContractTestData.parentDataContract ==''){	
				dataContractPO.clickOnDataTypeTab();
			}
			if(	(dataContractTestData.dataContractstype).toUpperCase()=='STANDARD'){
				selectCollectionType(dataContractTestData,getTaxonomyPath);
			}else{
				dataTypeDetailsEnter(dataContractTestData);
				selectPiggybackRules(dataContractTestData);
			}
			browser.switchTo().defaultContent();
			dataContractPO.clickOnRateSchedulesTabLink();
			rateSheduleTabDetailsEnter(dataContractTestData);
			assertsObj.verifyTrue(dataContractPO.rateScheduleHeaderText('New Data Contract Rate Schedules'),"Data Contracts header text is not found on Data Contract page.");
			
			if(((dataContractTestData.crossDevice).toUpperCase() == "YES") && ((browser.user).toUpperCase()=='CS')){
				crossDeviceDuplication(dataContractTestData);
				selectCorrectCrossDevice(dataContractTestData,dataContractName);
			}
			utilityObj.browserWaitforseconds(2);
			dataContractPO.clickOnSaveButton();
			turnDataFilterObj.writeDataProvider(testDataPath, browser.env, browser.language,testDataName ,'createdDC',dataContractName);
		});
		dataContractPO.isSearchBoxPresent().then(function() {
			logger.info("Created "+dataContractTestData.dataContractstype +" Data Contract with "+dataContractTestData.availability +" availability and "+dataContractTestData.collectiontype+ " collectiontype.");
		});
	 };
	 
	 /**
	  * This method is use to remove mapping from all source data contract.
	  * 
	  * @author hemins
	  * @param dataContractTestData
	  */
	 this.removeMappingSourceDataContract = function(dataContractTestData){
		 for(var i=0;i<4;i++){
			 utilityObj.findList(dataContractPO.returnSearchElement(),dataContractTestData.selectDest[i].value);
			 browser.waitForAngular();
			 utilityObj.browserWaitforseconds(4);
			 dataContractPO.clickOnIconMenu();
			 dataContractPO.clickOnEditLink();
			 dataContractPO.isDataContractNameTxtBoxPresent().then(function(){
				 dataContractPO.clickRemoveRowBtnIfPresent();
			 });
		     utilityObj.browserWaitforseconds(2);
			 dataContractPO.clickOnSaveButton();
		 };
		 dataContractPO.clickOnClearAllFilter();
		 
	 };
 
	 /**
		 * This method use to call different method to select data for cross device duplication and verify validation according to it.
		 * 
		 * @author hemins
		 * @param dataContractTestData
		 * @param dataContractName
		 */
		var crossDeviceDuplication= function(dataContractTestData,dataContractName){
			selectSameTypeGraphProvider(dataContractTestData);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			verifySameGraphProvider(dataContractTestData,dataContractName);
			browser.waitForAngular();
			selectSameDestinationContract(dataContractTestData);
			browser.waitForAngular();
			verifySameDestinationDataContract(dataContractTestData,dataContractName);
			browser.waitForAngular();
			sameGraphAndSameDestination(dataContractTestData);
			browser.waitForAngular();
			verifySameGraphAndDestinationDataContract(dataContractTestData,dataContractName);
			browser.waitForAngular();
			withoutSelectGraphandDestination(dataContractTestData);
			browser.waitForAngular();
			verifyWithoutSelectGraphandDestinationDataContract(dataContractTestData,dataContractName);
			browser.waitForAngular();
			/*selectDifferentCollectionTypeDestinationDC(dataContractTestData);
			browser.waitForAngular();
			verifySelectDifferentCollectionTypeDestinationDC(dataContractTestData,dataContractName);*/
		};
		
		/**
		 * This method is use to select same type of graph provider for cross device duplication.
		 * 
		 * @author hemins
		 * @param dataContractTestData
		 */
		var selectSameTypeGraphProvider = function(dataContractTestData){
			if((dataContractTestData.sameGraphProvider).toUpperCase() == "YES"){
				dataContractPO.clickOnGeneralTab().then(function(){
					browser.waitForAngular();
				    utilityObj.browserWaitforseconds(2);
					dataContractPO.clickEnableForCrossDeviceDuplication();
					logger.info("Checking Vaildation to select Same Type of Graph Provider.");
					for(var j=1;j<=2;j++){
						dataContractPO.selectSameTypeOfGraphProvider(dataContractTestData.selectGraph[0].value,j);
						browser.waitForAngular();
						dataContractPO.selectDiffDataContract(dataContractTestData.selectDest[j-1].value,j);	
						if(j<2){
							dataContractPO.clickRowAddBtn();
							browser.waitForAngular();
						};
					};
					utilityObj.browserWaitforseconds(2);
					dataContractPO.clickOnSaveButton();
				});
			}
		};
		
		/**
		 * This method is use to select same destination data contract name for cross device duplication.
		 * 
		 * @author hemins
		 * @param dataContractTestData
		 */
		var selectSameDestinationContract = function(dataContractTestData){
			if((dataContractTestData.sameDestinationContractsName).toUpperCase() == "YES"){
				dataContractPO.clickOnGeneralTab().then(function(){
					dataContractPO.clickEnableForCrossDeviceDuplication();
					logger.info("Checking Vaildation to select Same Type of Destination Data Contracts Name.");	
					for(var j=1;j<=2;j++){
						dataContractPO.selectDiffGraphProvider(dataContractTestData.selectGraph[j-1].value,j);						
						browser.waitForAngular();
						dataContractPO.selectSameTypeOfDestinationDataContracts(dataContractTestData.selectDest[0].value,j);
						if(j<2){
							dataContractPO.clickRowAddBtn();
							browser.waitForAngular();
						}
					};
					utilityObj.browserWaitforseconds(2);
					dataContractPO.clickOnSaveButton();	

				});
			}	
		};
		
		/**
		 * This method is use to select same type of graph provider and destination data contract for cross device duplication.
		 * 
		 * @author hemins
		 * @param dataContractTestData
		 */
		var sameGraphAndSameDestination = function(dataContractTestData){
			if((dataContractTestData.sameGraphAndSameDestination).toUpperCase() == "YES"){
				dataContractPO.clickOnGeneralTab().then(function(){
					dataContractPO.clickEnableForCrossDeviceDuplication();
					logger.info("Checking Vaildation to select Same Type of Destination Data Contracts Name and Graph Provider.");
					for(var j=1;j<=2;j++){
						dataContractPO.selectSameTypeOfGraphProvider(dataContractTestData.selectGraph[0].value,j);
						dataContractPO.selectSameTypeOfDestinationDataContracts(dataContractTestData.selectDest[0].value,j);
						if(j<2){
							dataContractPO.clickRowAddBtn();
							browser.waitForAngular();
						}
					};
					utilityObj.browserWaitforseconds(2);
					dataContractPO.clickOnSaveButton();
					
				});
			}
		};
		
		/**
		 * This method is use to add cross device without select graph provider and destination contract name for cross device duplication.
		 * 
		 * @author hemins
		 * @param dataContractTestData
		 */
		var withoutSelectGraphandDestination = function(dataContractTestData){
			if((dataContractTestData.withoutselectGraphandDestination).toUpperCase() == "YES"){
				dataContractPO.clickOnGeneralTab().then(function(){
					dataContractPO.clickEnableForCrossDeviceDuplication();
					browser.waitForAngular();
					logger.info("Checking Vaildation to without select any Destination Data Contracts Name and Graph Provider.");
					for(var j=1;j<2;j++){
						dataContractPO.clickRowAddBtn();
						browser.waitForAngular();
					};	
					utilityObj.browserWaitforseconds(2);
					dataContractPO.clickOnSaveButton();
				});
			}
		};
		
		/**
		 * This method is use to select different data collection type of destination data contract for cross device duplication.
		 * 
		 * @author hemins
		 * @param dataContractTestData
		 */
		var selectDifferentCollectionTypeDestinationDC = function(dataContractTestData){
			if((dataContractTestData.selectDifferentCollectionTypeDestinationDC).toUpperCase() == "YES"){
				dataContractPO.clickOnGeneralTab().then(function(){
					browser.waitForAngular();
					dataContractPO.clickEnableForCrossDeviceDuplication();
					browser.waitForAngular();
					logger.info("Checking Vaildation to select different Destination Data Contracts data Type from Source Data Contracts data type.");
					for(var j=1;j<=2;j++){
						dataContractPO.selectDiffGraphProvider(dataContractTestData.selectGraph[j-1].value,j-1);
						browser.waitForAngular();
						dataContractPO.selectDiffDataContract(dataContractTestData.selectDest[j+1].value,j+1);	
						if(j<2){
							dataContractPO.clickRowAddBtn();
							browser.waitForAngular();
						}
					};
					utilityObj.browserWaitforseconds(2);
					dataContractPO.clickOnSaveButton();
				});
			}
		};
		
		/**
		 * This method is use to remove mapping for cross device duplication.
		 * 
		 * @author hemins
		 */
		 var removeRows = function(){
			for(var j=1;j<=2;j++){
				dataContractPO.clickRemoveRowBtn(j);
				browser.waitForAngular();
			}
		 };
	
		/**
		  * This Method is use to verify validation of select same type of graph for a source data contract.
		  * 
		  * @author hemins
		  * @param dataContractTestData
		  * @param dataContractName
		  */
		 var verifySameGraphProvider = function(dataContractTestData,dataContractName){
			 if((dataContractTestData.sameGraphProvider).toUpperCase() == "YES"){
				 dataContractPO.verifyErrMessage(dataContractTestData.errorMesCheck[0].errorMessage).then(function(value){
					 assertsObj.verifyEqual(dataContractPO.verifyErrMessage(dataContractTestData.errorMesCheck[0].errorMessage),dataContractTestData.errorMesCheck[0].errorMessage,"Select Same Graph Provider error Message is not found.");
					 removeRows();
				 });
			 }
		 };
		 
		 /**
		  * This Method is use to verify validation of select same destination data contract for a source data contract.
		  * 
		  * @author hemins
		  * @param dataContractTestData
		  * @param dataContractName
		  */
		 var verifySameDestinationDataContract = function(dataContractTestData,dataContractName){
			 if((dataContractTestData.sameDestinationContractsName).toUpperCase() == "YES"){
				 dataContractPO.verifyErrMessage(dataContractTestData.errorMesCheck[1].errorMessage).then(function(value){
					 assertsObj.verifyEqual(dataContractPO.verifyErrMessage(dataContractTestData.errorMesCheck[1].errorMessage),dataContractTestData.errorMesCheck[1].errorMessage,"Select Same Destination Data Contracts error Message is not found.");	
					 removeRows();
				 });
			 }
		 };
		 
		 /**
		  * This Method is use to verify validation of same type of graph and destination data contract.
		  * 
		  * @author hemins
		  * @param dataContractTestData
		  * @param dataContractName
		  */
		 var verifySameGraphAndDestinationDataContract = function(dataContractTestData,dataContractName){
			 if((dataContractTestData.sameGraphAndSameDestination).toUpperCase() == "YES"){
				 dataContractPO.verifyErrMessage(dataContractTestData.errorMesCheck[0].errorMessage).then(function(value){
					 assertsObj.verifyEqual(dataContractPO.verifyErrMessage(dataContractTestData.errorMesCheck[0].errorMessage),dataContractTestData.errorMesCheck[0].errorMessage,"Select Same Graph Provider error Message is not found.");	
					 assertsObj.verifyEqual(dataContractPO.verifyErrMessage(dataContractTestData.errorMesCheck[1].errorMessage),dataContractTestData.errorMesCheck[1].errorMessage,"Select Same Destination Data Contracts error Message is not found.");
					 removeRows();
				 });
			 }
			
		 };
		 
		 /**
		  * This Method is use to verify validation of without selecting graph and destination data contract.
		  * 
		  * @author hemins
		  * @param dataContractTestData
		  * @param dataContractName
		  */
		 var verifyWithoutSelectGraphandDestinationDataContract = function(dataContractTestData,dataContractName){
			 if((dataContractTestData.withoutselectGraphandDestination).toUpperCase() == "YES"){
				 dataContractPO.verifyErrMessage(dataContractTestData.errorMesCheck[2].errorMessage).then(function(value){
					 assertsObj.verifyEqual(dataContractPO.verifyErrMessage(dataContractTestData.errorMesCheck[2].errorMessage),dataContractTestData.errorMesCheck[2].errorMessage,"No Graph Provider error Message is not found.");	
					 assertsObj.verifyEqual(dataContractPO.verifyErrMessage(dataContractTestData.errorMesCheck[3].errorMessage),dataContractTestData.errorMesCheck[3].errorMessage,"No Destination Data Contracts error Message is not found.");
					 removeRows();
				 });
			 }
		 };
		 
		 /**
		  * This Method is use to verify validation of Different Collection Type of Destination Data Contract.
		  * 
		  * @author hemins
		  * @param dataContractTestData
		  * @param dataContractName
		  */
		 var verifySelectDifferentCollectionTypeDestinationDC = function(dataContractTestData,dataContractName){
			 if((dataContractTestData.selectDifferentCollectionTypeDestinationDC).toUpperCase() == "YES"){
				 dataContractPO.verifyErrMessage(dataContractTestData.errorMesCheck[4].errorMessage).then(function(value){
					for(var i=1;i<=2;i++){
						 assertsObj.verifyEqual(dataContractPO.verifyErrMessage(dataContractTestData.errorMesCheck[i+3].errorMessage),dataContractTestData.errorMesCheck[i+3].errorMessage,"Error Message is not found.");	
					};
					removeRows();
				 });
			 }
		 };
		 
		 /**
		 * This function to select correct value from cross device duplication after fired validation. 
		 * 
		 * @author hemins
		 * @param dataContractTestData
		 * @param dataContractName
		 */
		 var selectCorrectCrossDevice = function(dataContractTestData,dataContractName){
			if((dataContractTestData.crossDevice).toUpperCase() == 'YES'){
				dataContractPO.clickOnGeneralTab().then(function(){
					logger.info("Select Same Destination Data Contracts Data Type as Source Data Contracts");
					dataContractPO.clickEnableForCrossDeviceDuplication();
					for(var j=1;j<=2;j++){
						dataContractPO.selectDiffGraphProvider(dataContractTestData.selectGraph[j-1].value,j);
						dataContractPO.selectDiffDataContract(dataContractTestData.selectDest[j].value,j);	
						if(j<2){
							dataContractPO.clickRowAddBtn();
						}
					};							
				});
			};	
		 };
	
		/**
		 * This method is use to match header text.
		 * 
		 * @author vishalbha
		 * @param name
		 * @returns boolean
		 */
		var verifyPageHeaderText = function(name) {
			return dataContractPO.getPageHeader().then(function(value) {
				if (value == name) {
					return true;
				}else{
					return false;
				};
			});
		};
		
		/**
		 * This method is use to match header text for audit log page.
		 * 
		 * @author narottamc
		 * @param name
		 * @returns boolean
		 */
		var verifyAuditLogPageHeaderText = function(name){
			return dataContractPO.getAuditLogPageHeader().then(function(value){
				if(value == name){
					return true;
				}else{
					return false;
				}
			});
		};
		
		/**
		 * This method is use to return search Textbox element of data Contracts tab.
		 * 
		 * @author vishalbha
		 * @returns Element
		 */
		this.returnSearchElement = function(){
			return dataContractPO.returnSearchElement();
		};
		
		/**
		 * This method is use to return element list in new console page.
		 * 
		 * @author narottamc
		 */
		
		this.returnElementList=function(){
			return dataContractPO.dataContractElementList();
		};
		
		/**
		 * This method is use to clear filter .
		 * 
		 * @author vishalbha
		 * @returns Element
		 */
		this.removeFilter = function(){
			 dataContractPO.clickOnClearFilter();
			 utilityObj.browserWaitforseconds(2);
		};
		
		/**
		  * This function is used edit mapping for cross device.
		  * 
		  * @author hemins
		  */
		 var editCrossDeviceMapping = function(dataContractTestData){
			 browser.waitForAngular();
			 dataContractPO.clickRowAddBtn();
			 browser.waitForAngular();
			dataContractPO.selectDiffGraphProvider(dataContractTestData.selectGraph[2].value,1);
			dataContractPO.selectDiffDataContract(dataContractTestData.selectDest[0].value,1);	
		 };
		
		/**
		* This method is use to edit created data contract.
		* 
		* @author narottamc
		* @param  dataName,updatedDataContractName
		* @param  updatedDataContractName
		*/
		this.editCreatedDataContract=function(testDataPath,dataContractTestData,dataName,updatedDataContractName){
			    dataContractPO.isIconMenuPresent().then(function() {
				logger.info("Redirect to Edit Data Contract.");
				dataContractPO.clickOnIconMenu();
				utilityObj.browserWaitforseconds(2);
				dataContractPO.clickOnEditLink();
				utilityObj.browserWaitforseconds(2);
				isAngularSite(false);
				browser.waitForAngular();
				dataContractPO.enterNewDataContractName(updatedDataContractName);
				if(((dataContractTestData.crossDevice.toUpperCase()=='YES')||(dataContractTestData.crossDevice.toUpperCase()=='ON')||(dataContractTestData.crossDevice.toUpperCase()=='TRUE')) && ((browser.user).toUpperCase()=='CS')){
				editCrossDeviceMapping(dataContractTestData);
				}
				utilityObj.browserWaitforseconds(2);
				dataContractPO.clickOnSaveButton();
				utilityObj.browserWaitforseconds(2);
			});
				dataContractPO.getDataContractPageHeaderText().then(function() {
					logger.info("Editing created Data Contract done.");
					turnDataFilterObj.writeDataProvider(testDataPath, browser.env,browser.language,dataName,'updatedDC',updatedDataContractName);		
				});
		};
		
		/**
		* This method is use to redirect to View page of data contract.
		* 
		* @author narottamc
		*/
		this.gotoViewDataContracts = function() {
			 dataContractPO.isIconMenuPresent().then(function() {
				 dataContractPO.clickOnIconMenu();
				 dataContractPO.clickOnViewLink();
				});
		};
			
		/**
		* This method is use to View created data contract and Cross Device Data (Graph and Destination Data Contract).
		* 
		* @author hemins
		* @param  dataContractTestData
		* @returns datacontractanme
		* 
		*/
		this.viewCreatedDataContract=function(dataContractTestData)	{
			isAngularSite(false);
			dataContractPO.getExpectedDataContractNameViewPage().then(function() {
				logger.info("Viewing("+dataContractTestData.updatedDC+") Data Contract.");
				assertsObj.verifyEqual(dataContractPO.getExpectedDataContractNameViewPage(),dataContractTestData.updatedDC,
				"Not match Data Contract in View Data Contract page.");		
                if(((dataContractTestData.crossDevice.toUpperCase()=='YES')||(dataContractTestData.crossDevice.toUpperCase()=='ON')||(dataContractTestData.crossDevice.toUpperCase()=='TRUE')) &&((browser.user).toUpperCase()=='CS')){				
				for(var i=0;i<3;i++){
					assertsObj.verifyEqual(dataContractPO.returnGraph((dataContractTestData.selectGraph[i]).value),(dataContractTestData.selectGraph[i]).value,
					"Graph("+(dataContractTestData.selectGraph[i]).value+") is not found in view data contract page.");
					assertsObj.verifyEqual(dataContractPO.returnDestinationDataContracts((dataContractTestData.selectDest[i]).value),(dataContractTestData.selectDest[i]).value,
					"Dastination Data Contract("+(dataContractTestData.selectDest[i]).value+") is not found in view data contract page.");
				}
				}
			});
			dataContractPO.getExpectedDataContractNameViewPage().then(function() {
						logger.info("Viewed("+dataContractTestData.updatedDC+") Data Contract.");
			});
			return dataContractPO.getExpectedDataContractNameViewPage();
		};
		
		/**
		 * This method is use to remove cross dvice mapping from source and destination data contract.
		 * 
		 * @author hemins
		 * @param dataContractName
		 * @param dataContractType
		 */
		this.deleteCrossDeviceMapping = function(dataContractName, dataContractType){
			dataContractPO.isSearchBoxPresent().then(function() {
				 if(dataContractType.toUpperCase() == 'DESTINATION'){
					utilityObj.findList(dataContractPO.returnSearchElement(),dataContractName);
					browser.waitForAngular();
					dataContractPO.clickOnClearFilter();
					utilityObj.browserWaitforseconds(2);
					dataContractPO.isIconMenuPresent().then(function() {
						logger.info("Deleting mapping from "+dataContractName +" Data Contract.");
						 dataContractPO.clickOnIconMenu();
						 dataContractPO.clickOnEditLink();
					});
					dataContractPO.clickRemoveMapping();
					dataContractPO.clickRemoveMapping();
					utilityObj.browserWaitforseconds(2);
					dataContractPO.clickOnSaveButton();
				 }else if(dataContractType.toUpperCase() == 'SOURCE'){					 
					utilityObj.findList(dataContractPO.returnSearchElement(),dataContractName);
					dataContractPO.isIconMenuPresent().then(function() {
						logger.info("Deleting mapping from "+dataContractName +" Data Contract.");
						 dataContractPO.clickOnIconMenu();
						 dataContractPO.clickOnEditLink();
					});
					dataContractPO.clickRemoveMapping();
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					dataContractPO.clickOnSaveButton();
				 }
			});		
		};
	
		/**
		 * This method is use to delete Data Contract.
		 * 
		 * @author hemins
		 * @param dataContractName
		 */
		this.deleteCreatedDataContract=function(dataContractName){
     		utilityObj.findList(dataContractPO.returnSearchElement(),dataContractName);
			dataContractPO.isIconMenuPresent().then(function() {
				logger.info("Deleting "+dataContractName +" Data Contract.");
				 dataContractPO.clickOnIconMenu();
				 dataContractPO.clickOnDeleteLink();
				 browser.waitForAngular();
				dataContractPO.clickConfirmDeleteAlert();						
			});
			dataContractPO.getDataContractPageHeaderText().then(function() {
				logger.info("Deleted "+dataContractName +" Data Contract.");
			});	
		};
		
		/**
		 * This method is use to click on icon Menu->Obtain pixel link.
		 * 
		 * @author narottamc
		 */
		this.gotoDataContractObtainPixels = function(){
			dataContractPO.isIconMenuPresent().then(function() {
				logger.info("Click on obtain Pixels Data Contract");		
				dataContractPO.clickOnIconMenu();
				dataContractPO.clickOnObtainPixelsLink();
				browser.switchTo().defaultContent();	
				});		
		};
		
		/**
		 * This method is use to return Obtain pixel page header name.
		 * 
		 * @author narottamc
		 * @returns Obtain pixel page header name
		 */
		this.returnDataContractObtainPixelHeaderName = function(){
			return dataContractPO.getObtainPixelsName().then(function(value){
				dataContractPO.clickCloseOnObtainPixels();
				return value;
				});	
			};
		
		/**
		 * This method is use to redirect to Data contract view audit Log page.
		 * 
		 * @author narottamc
		 */
		this.gotoDataContractViewAuditLog = function() {
			browser.waitForAngular();
			dataContractPO.isIconMenuPresent().then(function(){
				logger.info("Redirect to View Audit Log page of Data Contract.");
				dataContractPO.clickOnIconMenu();			
				dataContractPO.clickOnViewAuditLink();
				browser.waitForAngular();
				isAngularSite(false);
				utilityObj.switchToNewWindow();				
				assertsObj.verifyTrue(verifyAuditLogPageHeaderText('Audit Log'),'Audit Log page header text is not found on Data Contarct page.');
				});				
			};
		
			/**
			 * This method is use to search 'searchField' on view Audit log page.
			 * 
			 * @author narottamc
			 * @param searchField
			 */
		this.searchDataContractAuditLog = function(searchField) {	
			dataContractPO.isAuditLogSearchBoxPresent().then(function(){
				logger.info("Searching for field name in View Audit Log Page of Data Contract.");
				assertsObj.verifyTrue(verifyAuditLogPageHeaderText('Audit Log'),'Audit Log page header text is not found.');
				dataContractPO.searchAuditLogTextBox(searchField);
				});
			};	
			
			/**
			 * This method is use to get field name from view audit log page of Data Contract.
			 * 
			 * @author narottamc
			 * @returns value
			 */
		this.getFieldNameValue = function() {	
			return dataContractPO.getFieldNameText().then(function(value){
				logger.info("Getting field name value from Audit log page of Data Contract.");
				return value;
				});		
			};	
			
			/**
			 * This method is use to click on Action -> Show Deleted
			 * 
			 * @author narottamc
			 */
		this.clickShowDeletedDataContract = function() {
			browser.waitForAngular();
			dataContractPO.checkActionDropdownPresent().then(function(){
					assertsObj.verifyTrue(verifyPageHeaderText('Data Contracts'),'Data Contarct page header text is not found.');
					logger.info("Clicking on Action->Show Deleted of Data Contract.");
					browser.waitForAngular();
					dataContractPO.clickOnActionDropDownList();
				});
				browser.waitForAngular();
				dataContractPO.clickOnShowDeletedLink();
				browser.waitForAngular();
			};	
			
			/**
			 * This method is use to click on Action -> Hide Deleted
			 * 
			 * @author narottamc
			 */
		this.clickHideDeletedDataContract = function() {
			browser.waitForAngular();
			dataContractPO.checkActionDropdownPresent().then(function(){
				assertsObj.verifyTrue(verifyPageHeaderText('Data Contracts'),'Data Contract page header text is not found.');
				logger.info("Clicking on Action->Hide Deleted of Data Contract.");
				dataContractPO.clickOnActionDropDownList();
				});
				browser.waitForAngular();
				dataContractPO.clickOnHideDeletedLink();
			};	
		
		/**
		* This method is used to sort data contract list by Data contract name..
		* 
		* @author narottamc
		*/		
		this.sortByDataContractName=function(){
			dataContractPO.clickDataContractNameColumn();
			logger.info("Click on Data Contract Name column for sorting.");
		};
		
		/**
		 * This method is used to verify that records are sort by Data Contract Name or not.
		 * 
		 * @author narottamc
		 * @return returnReverseDataContractName
		 */
		this.assertDataContractNameColumn = function() {
			assertsObj.verifyNotNull(dataContractPO.returnReverseDataContractName(),"Not Find column sorting with Data Contract Name.");
			return dataContractPO.returnReverseDataContractName();
		};
		
		/**
		 *  This method is used to click on Action->Expanded link of Data contract tab and verify the Rate Schedule text,Taxonomy text,
		 *  pixel option text,Events text and  Data contract configuration text.
		 *  
		 * @author narottamc
		 * @param dataContractTestData
		 * @returns Data contract configuration text
		 */
		this.expandDataContracts=function(dataContractTestData){
			dataContractPO.getDataContractPageHeaderText().then(function(){
				logger.info("Clicking to 'Action-> Expand ALL Rows' of Data Contracts.");
				browser.waitForAngular();
				dataContractPO.clickOnActionDropDownList();
				dataContractPO.clickExpandDataContarctsLink();
				dataContractPO.getExpandDatacontractConfText().then(function(){
					assertsObj.verifyEqual(dataContractPO.getExpandDatacontractConfText(),dataContractTestData.expandConf,"Data Contract:" +dataContractTestData.expandConf+" is not found for Expand.");
					assertsObj.verifyEqual(dataContractPO.getExpandRateScedulesText(),dataContractTestData.expandRateSch,"Data Contract:" +dataContractTestData.expandRateSch+" is not found for Expand.");
					assertsObj.verifyEqual(dataContractPO.getExpandTaxonomyText(),dataContractTestData.expandTaxonomy,"Data Contract:" +dataContractTestData.expandTaxonomy+" is not found for Expand.");
					if(dataContractTestData.collectiontype.toUpperCase()=='PIXEL'){
					assertsObj.verifyEqual(dataContractPO.getExpandPixelOptionsText(),dataContractTestData.expandPixelOptions,"Data Contract:" +dataContractTestData.expandPixelOptions+" is not found for Expand.");
					}
					assertsObj.verifyEqual(dataContractPO.getExpandEventsText(),dataContractTestData.expandEvents,"Data Contract:" +dataContractTestData.expandEvents+" is not found for Expand.");
						});	
				});
		};
		
		/**
		 *  This method is used to click on Action->Collapse All Rows link of Data contract tab.
		 *  
		 * @author narottamc
		 */
		this.collapseDataContracts=function(){
			dataContractPO.getDataContractPageHeaderText().then(function(){
				logger.info("Clicking to 'Action-> Collapse ALL Rows' of Data Contracts.");
				dataContractPO.clickOnActionDropDownList();
				dataContractPO.clickExpandDataContarctsLink();
				dataContractPO.clickOnActionDropDownList();
				dataContractPO.clickCollapseDataContarctsLink();
		});
	};		
		
		/**
		 *  This method is used to click close button of Expanded Data contract tab.
		 * 
		 * @author narottamc
		 */
		this.clickingDataContractExpandCloseButton = function(){
			 dataContractPO.isExpandCloseButtonDisplayed().then(function(){
				dataContractPO.clickExpandCloseButton();
				logger.info("Clicking Expand close button of Data Contract done.");
			});
		};
		
		/**
		 *  This method is used to check close button of Expanded Data contract tab dispalyed or not.
		 * 
		 * @author narottamc
		 * @returns boolean
		 */
		this.checkExpandCloseButton=function()
		{
			return dataContractPO.isExpandCloseButtonDisplayed();
		};
		
		/**
		 *  This method is used to click on Icom->View link of Data contract tab and verify the Rate Schedule text,Taxonomy text,
		 *  pixel option text,Events text and  Data contract configuration text in New angular page.
		 *  
		 * @author narottamc
		 * @param dataContractTestData
		 * @returns Data contract configuration text
		 */
		this.viewConfigurationDataContracts=function(dataContractTestData){
			browser.waitForAngular();
			dataContractPO.getDataContractPageHeaderText().then(function(){
				logger.info("Clicking to 'IconMenu-> View configuration.");
				dataContractPO.clickOnIconMenu();
				dataContractPO.clickViewConfigurationLink();
				isAngularSite(false);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
				dataContractPO.getConfigurationPageHeaderText().then(function(){
					assertsObj.verifyEqual(dataContractPO.getViewConfRateScedulesText(),dataContractTestData.expandRateSch,"Data Contract:" +dataContractTestData.expandRateSch+" is not found for Expand.");
					assertsObj.verifyEqual(dataContractPO.getViewConfTaxonomyText(),dataContractTestData.expandTaxonomy,"Data Contract:" +dataContractTestData.expandTaxonomy+" is not found for Expand.");
					if(dataContractTestData.collectiontype.toUpperCase()=='PIXEL'){
					assertsObj.verifyEqual(dataContractPO.getViewConfPixelOptionsText(),dataContractTestData.expandPixelOptions,"Data Contract:" +dataContractTestData.expandPixelOptions+" is not found for Expand.");
					}
					assertsObj.verifyEqual(dataContractPO.getViewConfExpandEventsText(),dataContractTestData.expandEvents,"Data Contract:" +dataContractTestData.expandEvents+" is not found for Expand.");
						});	
				});
		};
		
		/**
		 *  This method is used to click close button of View configuration Data contract tab in angular page.
		 * 
		 * @author narottamc
		 */
		this.clickingDataContractViewConfigurationCloseButton = function(){
			 isAngularSite(false);
			 dataContractPO.isViewConfigurationCloseButtonDisplayed().then(function(){
				dataContractPO.clickViewConfigurationCloseButton();
				logger.info("Clicking Expand close button of Data Contract done.");
			});
		};
		
		/**
		 *  This method is used to check close button of View configuartion Data contract tab dispalyed or not in angular page.
		 * 
		 * @author narottamc
		 * @returns boolean
		 */
		this.checkViewConfigurationCloseButton=function(){
			isAngularSite(false);
			return dataContractPO.isViewConfigurationCloseButtonDisplayed();
		};
			
		/**
		 *  This method is use to select market Filter for Data Contract.
		 * 
		 * @author narottamc
		 * @param marketName
		 */
		this.selectMarketFilterDataContract =function(marketName){
				dataContractPO.getDataContractPageHeaderText().then(function() {
					logger.info("Selecting Data Contract Market Filter.");
					dataContractPO.clickNewFilterButton();
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					dataContractPO.selectNewMarketTypeFilter();
					dataContractPO.enterNewMarketNameFilter(marketName);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(1);
					dataContractPO.clickOnNewMarketFilter(marketName);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
				});				
		};
		
		/**
		 *  This method is use to clear search box.
		 * 
		 * @author narottamc
		 */
		this.clearSearchDataContract =function(){
			dataContractPO.getDataContractPageHeaderText().then(function() {
				logger.info("Clearing Search Textbox and click on search button.");
				dataContractPO.clearSearchBox();
				dataContractPO.clickClearSearchIcon();
			});
		};
		
		/**
		 *  This method take first row market name from List Item Column "Market".
		 * 
		 * @author narottamc
		 */
		this.getMarketNameDataContract = function(){
				return dataContractPO.getMarketNameFromColumn().then(function(value){
					return value;	
			});
		};
		
		/**
		 *  This method take first row Data Provider name from List Item Column "Data Provider".
		 * 
		 * @author narottamc
		 */
		this.getDataProviderNameDataContract = function(){
				return dataContractPO.getDataProviderNameFromColumn().then(function(value){
					return value;	
			});
		};
		
		/**
		 *  This method use to select first record check box in dispalyed data contract list.
		 * 
		 * @author narottamc
		 */
		this.selectDeletedDataContract=function(){
			dataContractPO.checkFirstRecordCheckbox();
		};
		
		/**
		 *  This method use to click on Action->Undo delete link & confirm alert.
		 * 
		 * @author narottamc
		 */
		this.undoDeleteDataContract=function(){
			dataContractPO.getDataContractPageHeaderText().then(function(){
				logger.info("Clicking to 'Action-> Undo delete' of Data Contracts.");
				dataContractPO.clickOnActionDropDownList();
				dataContractPO.clickOnUndoDeleteLink();
				 browser.waitForAngular();
				 dataContractPO.clickConfirmDeleteAlert();
			});
		};
	};
	module.exports = new dataContractBase();