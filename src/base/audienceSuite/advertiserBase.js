/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

var advertiserBase = function() {
			var advertiserPO = require('../../pageObject/audiencesuite/advertiserPO.js');
			var utilityObj = require('../../lib/utility.js');
			var turnDataFilterObj = require('../../lib/turnDataProvider.js');
			var pathObj  =require( '../../testData/audienceSuite/path.js');
			var assertsObj = require('../../lib/assert.js');
			var loggerObj = require('../../lib/logger.js');
			var logger = loggerObj.loggers("advertiserBase");
			
			/**
			 * This method is use to check search box on advertiser tab.
			 * 
			 * @author vishalbha
			 */
			this.returnSearchElement = function(){
				return advertiserPO.returnSearchElement();
			};
			
			/**
			 * This method is use to return element list in new console page.
			 * 
			 * @author narottamc
			 */
			
			this.returnElementList=function()
			{
				return advertiserPO.AdvertiserelementList();
			};
			
			/**
			 * This method is use to redirect on advertiser tab.
			 * 
			 * @author vishalbha
			 */
			this.gotoAdvertiser = function() {
				browser.waitForAngular();
				advertiserPO.clickAdvertiserMenuLink();
				browser.waitForAngular();
				advertiserPO.clickAdvertiserSubMenuLink();
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
				};
		
			/**
			 * This method is use to Create New advertiser and click Cancel button on
			 * Advertiser page.
			 * 
			 * @author vishalbha
			 */
			this.newAdvertiserWithCancelButton = function(advHeaderText) {
				advertiserPO.getPageHeader().then(function() {
					logger.info("Clicking cancel button of Action->new advertiser page.");
				});
				advertiserPO.clickActionDropDown();
				advertiserPO.clickNewAdvertiserActionMenu();
				advertiserPO.clickNewAdvertiserCancleButton();
				assertsObj.verifyTrue(verifyPageHeaderText("Advertisers"),"Advertiser header text is not found on Advertiser page.");
			};
		
			/**
			 * This method is use to Create New advertiser.
			 * 
			 * @author vishalbha
			 */
			this.createNewAdvertiser = function(advertiserData, advertiserName,checkAdvertiserType) {
				advertiserPO.getPageHeader().then(function() {
				logger.info("Creating Advertiser.");
				advertiserPO.clickNewAdvertiserLink();
				isAngularSite(false);
				var userType = browser.user;
				if(userType=='CS'){
				advertiserPO.selectMarket(advertiserData.market);
				}
				if(checkAdvertiserType == "AdvertiserWithMediaProvider"){
					advertiserPO.enterAdvertiserName(advertiserName);
					advertiserPO.selectMediaProvider(advertiserData.mediaProvider);
					advertiserPO.enterMediaProviderId(advertiserData.mediaProviderId);
					turnDataFilterObj.writeDataProvider(pathObj.getAdvertiserTestDataFilePath(), browser.env,browser.language,'createadvertiser', 'createadvertiserwithmediaprovider', advertiserName);
				}else{
					advertiserPO.enterAdvertiserName(advertiserName);
					turnDataFilterObj.writeDataProvider(pathObj.getAdvertiserTestDataFilePath(), browser.env, browser.language,'createadvertiser', 'createdAdvertiser', advertiserName);
				};	
				advertiserPO.clickSaveButton();
				assertsObj.verifyTrue(verifyPageHeaderText("Advertisers"),"Advertiser header text is not found on Advertiser page.");
			});
				advertiserPO.getPageHeader().then(function() {
				logger.info("Created Advertiser.");
				//utilityObj.isAngularPage("New");
			});
		};
					
			/**
			 * This method is use to redirect view advertiser page.
			 * 
			 * @author vishalbha
			 */
			this.gotoViewAdvertiser = function() {
				//utilityObj.isAngularPage(browser.console);
				advertiserPO.clickIconMenu();
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				advertiserPO.clickViewLink();
			
			};
			
			/**
			 * This method used to edit advertiser name.
			 * 
			 * @author narottamc
			 * @param dataName
			 * @param updatedAdvName
			 */
			this.editAdvertiser = function(dataName, updatedAdvName) {
				//utilityObj.isAngularPage(browser.console);
				advertiserPO.getPageHeader().then(function() {
				logger.info("Editing Advertiser.");
				advertiserPO.clickIconMenu();
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				advertiserPO.clickEditLink();
				isAngularSite(false);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				advertiserPO.enterAdvertiserName(updatedAdvName);
				turnDataFilterObj.writeDataProvider(pathObj.getAdvertiserTestDataFilePath(),browser.env, browser.language, dataName, 'updatedAdvertiser',updatedAdvName);
				advertiserPO.clickSaveButton();
				assertsObj.verifyTrue(verifyPageHeaderText("Advertisers"),"Advertiser header text is not found on Advertiser page.");	
				});
				advertiserPO.getPageHeader().then(function(){
					logger.info("Edited Advertiser.");	
					isAngularSite(false);
				});		
			};
			
			/**
			 * This method is use to view advertiser page.
			 * 
			 * @author vishalbha
			 * @param advData
			 */
			this.viewAdvertiserName = function(advData) {
				isAngularSite(false);
				advertiserPO.getAdvertiserNameViewPage().then(function(){
					logger.info("Viewing Created Advertiser.");
					assertsObj.verifyEqual(advertiserPO.getAdvertiserNameViewPage(),advData,
					"Not match advertiser in View Advertiser page.");
				});
				advertiserPO.getAdvertiserNameViewPage().then(function(){
					logger.info("Viewed Created Advertiser.");
				});	
			 	 return advertiserPO.getAdvertiserNameViewPage();
			};
				
			/**
			 * This method is use to delete advertiser name in list item.
			 * 
			 * @author vishalbha
			 */
			this.deleteAdvertiser = function() {
				advertiserPO.getPageHeader().then(function() {
					logger.info("Deleting created Advertiser.");
					assertsObj.verifyTrue(verifyPageHeaderText("Advertisers"),"Advertiser header text is not found on Advertiser page.");
					advertiserPO.clickIconMenu();
					browser.waitForAngular();
					advertiserPO.clickDeleteLink();
					browser.waitForAngular();					
					advertiserPO.clickConfirmDeleteAlert();						
					});
				advertiserPO.getPageHeader().then(function() {
					logger.info("Deleted created Advertiser.");
				});
			};
				
			/**
			 * This method is use to pass advertiser name into search box.
			 * 
			 * @author vishalbha
			 */
			this.searchItemList = function(advertiser) {
				utilityObj.searchBox(advertiserPO.returnSearchElement(), advertiser);
			};
					
			/**
			 *  This method is use to clear searching.
			 * 
			 * @author vishalbha
			 */
			this.clearSearch =function(){
				advertiserPO.getPageHeader().then(function() {
					logger.info("Clearing Search Textbox and click on search button.");
					advertiserPO.clearSearchBox();				
					advertiserPO.clickClearSearchIcon();
				});
			};
			
			/**
			 * This method is use to click on Action -> Show Deleted
			 * 
			 * @author hemins
			 */
			this.clickShowDeletedAdvertiser = function() {
				browser.waitForAngular();
				advertiserPO.checkActionDropdownPresent().then(function(){
					assertsObj.verifyTrue(verifyPageHeaderText('Advertisers'),'Advertiser page header text is not found.');
					logger.info("Clicking on Show Deleted Advertiser.");
					advertiserPO.clickActionDropDown();
				});
				browser.waitForAngular();
				advertiserPO.clickShowDeletedLink();
			};
			
			/**
			 * This method is use to redirect to view audit log page.
			 * 
			 * @author hemins
			 */
			this.gotoViewAuditLog = function() {
				browser.waitForAngular();
					logger.info("Redirect to View Audit Log page of Advertiser.");
					advertiserPO.clickIconMenu();
					browser.waitForAngular();
					advertiserPO.clickAuditLogLink();
					utilityObj.switchToNewWindow();
					isAngularSite(false);
					browser.waitForAngular();
					assertsObj.verifyTrue(verifyAuditLogPageHeaderText('Audit Log'),'Audit Log page header text is not found.');
				};
			
			/**
			 * This method is use to match header text.
			 * 
			 * @author hemins
			 * @param name
			 * @returns boolean
			 */
			var verifyPageHeaderText = function(name){
				return advertiserPO.getPageHeader().then(function(value){
					if(value == name){
						return true;
					}else{
						return false;
					}
				});
			};
			
			/**
			 * This method is use to match header text for auditlog page.
			 * 
			 * @author narottamc
			 * @param name
			 * @returns boolean
			 */
			var verifyAuditLogPageHeaderText = function(name){
				return advertiserPO.getAuditLogPageHeader().then(function(value){
					if(value == name){
						return true;
					}else{
						return false;
					}
				});
			};
				
			/**
			 * This method is use to search field name in view audit log page.
			 * 
			 * @author hemins
			 * @param searchField
			 */
			this.searchAuditLog = function(searchField) {
				advertiserPO.waitForAuditLogSearchBox().then(function(){
					logger.info("Searching for field name in View Audit Log Page.");
					assertsObj.verifyTrue(verifyAuditLogPageHeaderText('Audit Log'),'Audit Log page header text is not found.');
					advertiserPO.returnAuditLogSearchElement(searchField);
				});
			};
			
			/**
			 * This method is use to get field name from view audit log page.
			 * 
			 * @author hemins
			 * @returns
			 */
			this.getFieldNameValue = function() {
				
				return advertiserPO.getFieldNameText().then(function(value){
					logger.info("Getting field name value from Audit log page.");
					return value;
				});		
			};
			
			/**
			 * This method is use to click on Action -> Hide Deleted
			 * 
			 * @author hemins
			 */
			this.clickHideDeletedAdvertiser = function() {
				browser.waitForAngular();
				advertiserPO.checkActionDropdownPresent().then(function(){
					assertsObj.verifyTrue(verifyPageHeaderText('Advertisers'),'Advertiser page header text is not found.');
					logger.info("Clicking on Hide Deleted Advertiser.");
					advertiserPO.clickActionDropDown();
				});
				browser.waitForAngular();
				advertiserPO.clickHideDeletedLink();
			};
			
			/**
			 *  This method is use to select market Filter.
			 * 
			 * @author vishalbha
			 * @param advData
			 */
			this.selectMarketFilter =function(marketName){
					advertiserPO.getPageHeader().then(function() {
					utilityObj.browserWaitforseconds(2);
					advertiserPO.clickOnClearFilter();
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					advertiserPO.clickNewFilterButton();
					browser.waitForAngular();					
					advertiserPO.selectNewMarketTypeFilter();
					browser.waitForAngular();
					advertiserPO.enterNewMarketNameFilter(marketName).then(function(){
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					advertiserPO.clickOnNewMarketFilter(marketName);	
					});
					
				});				
			};
			
			/**
			 *  This method take market name from List Item Column "Market".
			 * 
			 * @author vishalbha
			 */
			this.marketNameCount = function(){
					return advertiserPO.getMarketNameFromColumn().then(function(value){
					return value;	
				});	
			};
			
			/**
			 *  This method used for sorting .
			 * 
			 * @author vishalbha
			 */
			
			this.sortByAdvertiserName= function(){
				advertiserPO.clickAdvertiserNameColumn();
				logger.info("Click on Advertiser Name column for sorting.");
			};
			
			/**
			 * This method is used to verify that records are sort by Name or not.
			 * 
			 * @author vishalbha
			 * @return
			 */
			this.assertNameColumn = function() {
				assertsObj.verifyNotNull(advertiserPO.returnReverseAdvName(),"Not Find column sorting with Advertiser Name.");
				return advertiserPO.returnReverseAdvName();
			};
			
			/**
			 * This method is used to click on Edit metadata link.
			 * 
			 * @author narottamc
			 */
			this.clickEditAdvertiserMetaDataLink=function(){
				browser.waitForAngular();
				logger.info("Redirect to Edit Metadata page of Advertiser.");
				advertiserPO.clickIconMenu();			
				advertiserPO.clickEditMetaDataLink();	
			};
			
			/**
			 * This method is used to upload advertiser Meta data file.
			 * 
			 * @param  editMetaDataxlsFilePath
			 * @author narottamc
			 */
			this.browseAdvertiserMetaDataFile=function(editMetaDataxlsFilePath){
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2);
				utilityObj.uploadFile(advertiserPO.returnMetaDatabrowsebutton(),editMetaDataxlsFilePath);
				utilityObj.browserWaitforseconds(1);
				advertiserPO.clickMetaDataApplyButton();
				utilityObj.browserWaitforseconds(1);
			};
			
			/**
			 * This method is used to assert Edit link on edit metadata page.
			 * 
			 * @author narottamc
			 */
			this.assertEditMetaDataEditButton=function(){
				return advertiserPO.getEditMetaDataPageTitleText().then(function(){
					logger.info("Uploading MetaData file for Advertiser done.");
				return advertiserPO.isEditMetaDataEditButtonPresent();
				});
			};
			
			/**
			 * This method is used to click cancel button edit metadata page.
			 * 
			 * @author narottamc
			 */
			this.clickEditMetaDataCancelButton=function(){
				utilityObj.browserWaitforseconds(2);
			    advertiserPO.clickEditMetaDataCancelButton();
			    utilityObj.browserWaitforseconds(2);
			    browser.switchTo().defaultContent();
			};
			
			/**
			 * This method is used to verify Insertion order,Package,lineItems & creatives and their sub childs on edit meta data page.
			 * 
			 * @param advertiserTestData
			 * @author narottamc
			 */
			this.verifyAdvertiserEditedMetaData=function(advertiserTestData){
				advertiserPO.switchToEditMetaDataFrame();
				utilityObj.browserWaitforseconds(5);
				advertiserPO.clickIOPlusButton();
				advertiserPO.clickPackagePlusButton();
				advertiserPO.clickEditMetaDataLIPlusButton();
				advertiserPO.clickEditMetaDataCreativePlusButton();
				for(var i=1;i<5;i++){
					assertsObj.verifyEqual(advertiserPO.getEditMetaDataText(i),advertiserTestData.editMetaDataText[i-1].value,'Actual value:'+advertiserPO.getEditMetaDataText(i)+'not matching with expected value:'+advertiserTestData.editMetaDataText[i-1].value);
					assertsObj.verifyEqual(advertiserPO.getEditMetaDataSubText(i),advertiserTestData.editMetaDataSubText[i-1].value,'Actual value not matching with expected value:'+advertiserTestData.editMetaDataSubText[i-1].value);
				}
			};
			
			/**
			 * This method is used to assert Edit metadata page title..
			 * 
			 * @author narottamc
			 */
			this.assertEditMetaDataPageTitle=function(){
				return advertiserPO.getEditMetaDataPageTitleText();		
			};
			
			/**
			 * This method is used to claer if there is any exsting filter.
			 * 
			 * @author narottamc
			 */
			this.clearExistingFilter=function(){
				if(browser.user.toUpperCase()=='CS'){
				advertiserPO.clickOnClearFilter();
				}
			};	
		};
		module.exports = new advertiserBase();

