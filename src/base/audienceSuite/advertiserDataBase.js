/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

var advertiserDataBase = function() {
	var advertiserDataPO = require('../../pageObject/audiencesuite/AdvertiserDataPO.js');
	var utilityObj = require('../../lib/utility.js');
	var turnDataFilterObj = require('../../lib/turnDataProvider.js');
	var pathObj = require('../../testData/audienceSuite/path.js');
	var assertsObj = require('../../lib/assert.js');
	var loggerObj = require('../../lib/logger.js');
	var logger = loggerObj.loggers("advertiserDataBase");

	/**
	 * This method is use to return element list in new console page.
	 * 
	 * @author hemins
	 */
	this.returnElementList=function(){
		return advertiserDataPO.AdvertiserDataElementList();
	};
	
	this.returnElementListAdv=function(){
		return advertiserDataPO.AdvertiserElementList();
	};
	
	/**
	 * This method is use to clear advertiser and market filter in new console page.
	 * 
	 * @author narottamc
	 */
	this.clearNewFilter=function(){
		advertiserDataPO.isMarketFilterPresent().then(function(value){
			if(value){advertiserDataPO.clickOnClearFilter();
			};
		});
			advertiserDataPO.isAdvertiserFilterPresent().then(function(value){
				if(value){advertiserDataPO.clickOnClearFilter();
				}
		});
	};
	
	/**
	 * This method is use to check search box on advertiser tab.
	 * 
	 * @author vishalbha
	 */
	this.returnSearchElement = function() {
		return advertiserDataPO.returnSearchElement();
	};

	/**
	 * This method is use to redirect on advertiser tab.
	 * 
	 * @author vishalbha
	 */
	this.gotoAdvertiser = function() {
		browser.waitForAngular();
		advertiserDataPO.clickAdvertiserMenuLink();
		browser.waitForAngular();
		advertiserDataPO.clickAdvertiserSubMenuLink();
		assertsObj.verifyTrue(varifyPageHeaderText('Advertisers'),'Advertiser Data page header text is not found.');
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(2);
	};

	/**
	 * This method is use to redirect on advertiser data tab.
	 * 
	 * @author narottamc
	 */
	this.gotoAdvertiserData = function() {
		browser.navigate().refresh();
		browser.waitForAngular();
		advertiserDataPO.clickAdvertiserMenuLink();
		browser.waitForAngular();
		advertiserDataPO.clickAdvertiserDataSubLink();
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(5);
		assertsObj.verifyTrue(varifyPageHeaderText('Advertiser Data'),'Advertiser Data page header text is not found.');
		
	};

	/**
	 * This method is use to match header text.
	 * 
	 * @author hemins
	 * @param name
	 * @returns boolean
	 */
	var varifyPageHeaderText = function(name) {
		return advertiserDataPO.getPageHeader().then(function(value) {
			if (value == name) {
				return true;
			} else {
				return false;
			};
		});
	};

	/**
	 * This method is use to clicking on New advertiser link.
	 * 
	 * @author vishalbha
	 */
	this.clickOnCreatedAdvertiser = function() {
		advertiserDataPO.clickAdvertiserLink();
	};

	/**
	 * This method is use to Create New advertiser.
	 * 
	 * @author vishalbha
	 */
	this.createNewAdvertiser = function(advertiserData, advertiserName,testDataSet) {
		advertiserDataPO.getPageHeader().then(function() {
			logger.info("Creating Advertiser.");
			advertiserDataPO.clickNewAdvertiserLink();
			isAngularSite(false);
			if ((browser.user).toUpperCase() == 'CS') {
				advertiserDataPO.selectMarket(advertiserData.market);
			}
			advertiserDataPO.enterAdvertiserName(advertiserName);
			turnDataFilterObj.writeDataProvider(pathObj.getAdvertiserDataTestDataFilePath(),browser.env, browser.language,testDataSet,'createdAdvertiserName', advertiserName);
			advertiserDataPO.clickOnApplyButton();
			assertsObj.verifyTrue(varifyPageHeaderText("Advertisers"),"Advertiser header text is not found on Advertiser page.");
		});
		advertiserDataPO.getPageHeader().then(function() {
			logger.info("Created Advertiser.");
			//utilityObj.isAngularPage(browser.console);
		});
	};

	/**
	 * This method is use to click on Cancel button of New Advertiser Data.
	 * 
	 * @author vishalbha
	 */
	this.clickNewAdvertiserDataPageCancelButton = function() {
		advertiserDataPO.getPageHeader().then(function() {
			logger.info("Clicking cancel button of Action->new advertiser data page.");
		});
		advertiserDataPO.clickActionDropDown();
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(1);
		advertiserDataPO.clickNewAdvertiserActionMenu();
		advertiserDataPO.clickNewAdvertiserCancleButton();
		assertsObj.verifyTrue(varifyPageHeaderText("Advertiser Data"),"Advertiser header text is not found on Advertiser data page.");
	};

	/**
	 * This method is use to create new advertiser data.
	 * 
	 * @author vishalbha
	 * @param
	 */
	this.createNewAdvertiserData = function(advertiserTestData, advertiserName,getTaxonomyPath,testDataSet) {
		advertiserDataPO.getPageHeader().then(function() {
			logger.info("Creating Advertiser Data with "+advertiserTestData.dataType+" DataType.");
		});
		advertiserDataPO.clickAdvertiserDataButton();
		isAngularSite(false);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(1);	
		advertiserDataPO.enterAdvertiserDataName(advertiserName);
		advertiserDataPO.selectDataType(advertiserTestData.dataType).then(function(){
			browser.waitForAngular();
		if((advertiserTestData.dataType).toUpperCase() == "CATEGORY/KEYWORDS"){
			if ((advertiserTestData.withCategory).toUpperCase()=="CHECKED") {
				advertiserDataPO.selectCategory().then(function(){
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(1);
				advertiserDataPO.importTaxonomy();
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				advertiserDataPO.switchToFrame();
				utilityObj.uploadFile(element(by.css('.textInput')),getTaxonomyPath);
				advertiserDataPO.clickOnApplyButton();
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				});
			}else {
				advertiserDataPO.selectKeyword();
			}
		}else if((advertiserTestData.dataType).toUpperCase() == "DEFAULT"){
			advertiserDataPO.selectDataPixelType(advertiserTestData.dataPixelType);
		}else if((advertiserTestData.dataType).toUpperCase() == "CLICK"){
				advertiserDataPO.selectDataPixelType(advertiserTestData.dataPixelType);
				advertiserDataPO.clickOnClientPiggybacksTab();
				advertiserDataPO.clickOnCreatePiggybackButton();	
				advertiserDataPO.selectMediaProvider(advertiserTestData.mediaProviderType);
				advertiserDataPO.enterUrl(advertiserTestData.Url);
		//TODO 
		/**
		 * Beacon functionality is pending because for beacon we need Advertiser which
		 * was created from campaign suite.
		 */
		}else if((advertiserTestData.dataType).toUpperCase() == "SITE INTERACTION"){
				advertiserDataPO.selectDataPixelType(advertiserTestData.dataPixelType);
				advertiserDataPO.clickOnAdvanceOptionLink();
				advertiserDataPO.clickOrderIdCheckButton();
				advertiserDataPO.clickOptOutCheckButton();
				advertiserDataPO.clickOnClientPiggybacksTab();
				advertiserDataPO.clickOnCreatePiggybackButton();
				advertiserDataPO.selectClientDataType(advertiserTestData.clientDataType);
				advertiserDataPO.selectMediaProvider(advertiserTestData.mediaProviderType);
				advertiserDataPO.enterHTML(advertiserTestData.HTML);
		}
		});
		utilityObj.browserWaitforseconds(4);
		turnDataFilterObj.writeDataProvider(pathObj.getAdvertiserDataTestDataFilePath(), browser.env,browser.language, testDataSet,'createdAdvertiserDataName', advertiserName);
		browser.waitForAngular();
		advertiserDataPO.clickAdvertiserDataSaveButton();	
		advertiserDataPO.getPageHeader().then(function() {
			logger.info("Created Advertiser Data with "+advertiserTestData.dataType+" DataType.");
			//utilityObj.isAngularPage(browser.console);
		});
	};
	
	/**
	 * This method is use to edit advertiser data.
	 * 
	 * @author narottamc
	 * @param updatedAdevertiserData
	 */
	this.editAdvertiserData = function(updatedAdevertiserDataName,testDataSet) {
		advertiserDataPO.iconMenuPresentAdvData().then(function() {
			logger.info("Redirect to Edit Advertiser Data.");
			advertiserDataPO.clickIconMenuAdvData();
			browser.waitForAngular();
			 utilityObj.browserWaitforseconds(2);
			advertiserDataPO.clickEditLink();
			advertiserDataPO.enterAdvertiserDataName(updatedAdevertiserDataName);
			advertiserDataPO.clickAdvertiserDataSaveButton();
			assertsObj.verifyTrue(varifyPageHeaderText("Advertiser Data"),"Advertiser header text is not found on Advertiser data page.");
		});
		advertiserDataPO.getPageHeader().then(function() {
			logger.info("Editing Advertiser Data Done .");
	turnDataFilterObj.writeDataProvider(pathObj.getAdvertiserDataTestDataFilePath(), browser.env,browser.language,testDataSet,'updatedAdvertiserDataName',updatedAdevertiserDataName);
		});
	};

	
	/**
	 * This method is use to redirect to view advertiser data page.
	 * 
	 * @author narottamc
	 */
	this.gotoViewAdvertiserData = function() {
		//utilityObj.isAngularPage(browser.console);
		advertiserDataPO.clickIconMenuAdvData();
			browser.waitForAngular();
			 utilityObj.browserWaitforseconds(1);
			advertiserDataPO.clickViewLink();
		
	};

	/**
	 * This method is use to view advertiser data.
	 * 
	 * @author narottamc
	 * @param advertiserData
	 * @returns AdvertiserDataName
	 */
	this.viewAdvertiserDataName = function(advertiserData) {
		isAngularSite(false);
		advertiserDataPO.getAdvertiserDataNameViewPage().then(function() {
			logger.info("Viewing created Advertiser Data.");
			assertsObj.verifyEqual(advertiserDataPO.getAdvertiserDataNameViewPage(),advertiserData,"Not match advertiser data in View Advertiser data page.");
		});
		advertiserDataPO.getAdvertiserDataNameViewPage().then(function() {
			logger.info("Viewed created Advertiser Data.");
		});
		return advertiserDataPO.getAdvertiserDataNameViewPage();
	};
	
	/**
	 * This method is use to delete advertiser data or advertiser.
	 * 
	 * @author narottamc
	 * @param type
	 */
	this.deleteAdvertiser = function(type) {
		if (type.toUpperCase() == 'ADVERTISER DATA') {
			advertiserDataPO.iconMenuPresentAdvData().then(function() {
				logger.info("Redirect to Delete Advertiser Data");
				advertiserDataPO.clickIconMenuAdvData();
				browser.waitForAngular();
				 utilityObj.browserWaitforseconds(1);
			});
		}else if (type.toUpperCase() == 'ADVERTISER') {
			advertiserDataPO.iconMenuPresentAdv().then(function() {
				logger.info("Redirect to Delete Advertiser");
				advertiserDataPO.clickIconMenuAdv();
			});
		}
		advertiserDataPO.clickDeleteLink();
		utilityObj.browserWaitforseconds(4);	
			browser.switchTo().defaultContent();
			advertiserDataPO.clickConfirmDeleteAlert();		
		advertiserDataPO.getPageHeader().then(function() {
			logger.info("Deleting created::" + type + " Done.");
		});
	};
	
	/**
	 * This method is use to obtain Pixels.
	 * 
	 * @author vishalbha
	 */
	this.gotoObtainPixels = function(){
		advertiserDataPO.getPageHeader().then(function() {
			logger.info("Click on obtain Pixels Advertiser Data");		
			advertiserDataPO.clickIconMenuAdvData();
			browser.waitForAngular();
			 utilityObj.browserWaitforseconds(1);
			advertiserDataPO.clickObtainPixels();
			browser.switchTo().defaultContent();	
		});
		return advertiserDataPO.getObtainPixelsName().then(function(value){
			advertiserDataPO.clickCloseOnObtainPixels();
			return value;
		});	
	};
	
	/**
	 * This method is use to check deleted Advertiser data in Show Deleted tab.
	 * 
	 * @author vishalbha
	 */
	this.clickShowDeletedAdvertiser = function() {
		browser.waitForAngular();
		advertiserDataPO.checkActionDropdownPresent().then(function(){
			assertsObj.verifyTrue(varifyPageHeaderText('Advertiser Data'),'Advertiser data page header text is not found.');
			logger.info("Clicking on Show Deleted Advertiser.");
			advertiserDataPO.clickActionDropDown();
		});
		browser.waitForAngular();
		advertiserDataPO.clickShowDeletedLink();
	};
	
	/**
	 * This method is use to redirect to view audit log page and verify Edited text.
	 * 
	 * @author vishalbha
	 */	
	this.gotoViewAuditLog = function(){
		browser.waitForAngular();
		advertiserDataPO.iconMenuPresentAdv().then(function(){
			advertiserDataPO.clickIconMenuAdvData();
			browser.waitForAngular();
			 utilityObj.browserWaitforseconds(1);
			advertiserDataPO.clickAuditLogLink();
			utilityObj.switchToNewWindow();
			isAngularSite(false);
			assertsObj.verifyTrue(verifyAuditLogPageHeaderText('Audit Log'),'Audit Log page header text is not found.');
		});				
	};
	
	/**
	 * This method is use to match header text for auditlog page.
	 * 
	 * @author hemins
	 * @param name
	 * @returns boolean
	 */
	var verifyAuditLogPageHeaderText = function(name){
		return advertiserDataPO.getAuditLogPageHeader().then(function(value){
			if(value == name){
				return true;
			}else{
				return false;
			}
		});
	};

	/**
	 * This method is use to search field name in view audit log page.
	 * 
	 * @author vishalbha
	 * @param searchField
	 */
	this.searchAuditLog = function(searchField) {
		browser.waitForAngular();
		advertiserDataPO.waitForAuditLogSearchBox().then(function(){
				logger.info("Searching for field name in View Audit Log Page.");
				assertsObj.verifyTrue(verifyAuditLogPageHeaderText('Audit Log'),'Audit Log page header text is not found.');
				advertiserDataPO.returnAuditLogSearchElement(searchField);
		});
	};
	
	/**
	 * This method is use to get field name from view audit log page.
	 * 
	 * @author vishalbha
	 * @return value
	 */
	this.getFieldNameValue = function() {
		return advertiserDataPO.getFieldNameText().then(function(value){
			logger.info("Getting field name value from Audit log page.");
			return value;
		});		
	};
	
	/**
	 * This method is use to search field name in view audit log page.
	 * 
	 * @author vishalbha
	 */
	this.clickHideDeletedAdvertiserData = function() {
	browser.waitForAngular();
		advertiserDataPO.checkActionDropdownPresent().then(function(){
			assertsObj.verifyTrue(varifyPageHeaderText('Advertiser Data'),'Advertiser data page header text is not found.');
			logger.info("Clicking on Hide Deleted Advertiser data.");
			advertiserDataPO.clickActionDropDown();
		});
		browser.waitForAngular();
		advertiserDataPO.clickHideDeletedLink();
	};

	/**
	 *  This method is use to clear searching.
	 * 
	 * @author vishalbha
	 */
	this.clearSearch =function(){
		advertiserDataPO.getPageHeader().then(function() {
			logger.info("Clearing Search Textbox and click on search button.");
			advertiserDataPO.clearSearchBox();	
		});
	};
	
	/**
	 *  This method is use to select market Filter.
	 * 
	 * @author vishalbha
	 * @param marketName
	 */
	this.selectMarketFilter =function(marketName){
				advertiserDataPO.getPageHeader().then(function() {
				logger.info("Selecting Market Filter.");
				advertiserDataPO.clickNewFilterButton();
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				advertiserDataPO.selectNewMarketTypeFilter();
				browser.waitForAngular();
				advertiserDataPO.enterNewMarketNameFilter(marketName).then(function() {
				utilityObj.browserWaitforseconds(3);
				advertiserDataPO.clickOnNewFilter(marketName);
				browser.waitForAngular();
				});
		});
		

	};
	
	/**
	 *  This method is use to select Advertiser Filter in new list page.
	 * 
	 * @author narottamc
	 * @param advertiserName
	 */
	this.filterByAdvertiser = function(advertiserName){
			advertiserDataPO.getPageHeader().then(function() {
			advertiserDataPO.clickNewFilterButton();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			advertiserDataPO.selectNewAdvertiserTypeFilter();
			advertiserDataPO.enterNewAdvetiserNameFilter(advertiserName);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(5);
			advertiserDataPO.clickOnNewFilter(advertiserName);
			browser.waitForAngular();
		});
	};

	/**
	 *  This method is use to select Advertiser Filter.
	 * 
	 * @author vishalbha
	 * @param advertiserdata
	 */
	this.selectAdvertiserFilter = function(advertiserdata){
		advertiserDataPO.getPageHeader().then(function() {
			logger.info("Selecting Advertiser Filter.");
			advertiserDataPO.isFlipperPresent();
			advertiserDataPO.clickOnClearFilter();
			advertiserDataPO.clickOnSelectAdvertiserLink();
			advertiserDataPO.enterFilterData(advertiserdata);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(4);
			advertiserDataPO.selectFilterData(advertiserdata);
			browser.waitForAngular();
		});
	};
	
	/**
	 *  This method used for sorting .
	 * 
	 * @author vishalbha
	 */	
	this.sortByAdvertiserName= function(){
		advertiserDataPO.clickAdvertiserNameColumn();
			logger.info("Click on Advertiser Name column for sorting.");
	};
	
	/**
	 * This method is used to verify that records are sort by Name or not.
	 * 
	 * @author vishalbha
	 * @return boolean
	 */
	this.assertNameColumn = function() {
		assertsObj.verifyNotNull(advertiserDataPO.returnReverseAdvName(),"Not Find column sorting with Advertiser Name.");
			return advertiserDataPO.returnReverseAdvName();
	};
		
};
module.exports = new advertiserDataBase();