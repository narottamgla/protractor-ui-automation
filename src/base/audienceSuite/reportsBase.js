/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

var reportsBase = function(){
	var reportPO = require('../../pageObject/audiencesuite/reportsPO.js');
	var utilityObj = require('../../lib/utility.js');
	var turnDataFilterObj = require('../../lib/turnDataProvider.js');
	var pathObj = require('../../testData/audienceSuite/path.js');
	var assertsObj = require('../../lib/assert.js');
	var loggerObj = require('../../lib/logger.js');
	var logger = loggerObj.loggers("reportsBase");

	/**
	 * This method is use to redirect on Reporting tab.
	 * 
	 * @author vishalbha
	 */
	this.gotoReportingTab = function() {
		browser.waitForAngular();
		reportPO.clickOnReportingTab();
	};
	
	/**
	 * This method is use to return Reporting tab Link is present or not.
	 * 
	 * @author vishalbha
	 * @returns boolean
	 */
	this.checkReportingtabisPresent = function(){
		return reportPO.checkReportingtabPresent().then(function(value){
			if(value){
				logger.info("Redirect to Reporting tab.");
				return value;
			}else{
				logger.info("Not redirect to Reporting tab.");
				return value;
			}
		});
		return reportPO.checkReportingtabPresent();
	};
	
	/**
	 * This method is use to click on Sub tab of Reporting tab.
	 * 
	 * @author vishalbha
	 * @param userType
	 */
	this.clickOnSubTab = function(userType){
		browser.waitForAngular();
		if((userType).toUpperCase()=='CS'){
			reportPO.checkPivotReportTabPresent().then(function(){
				reportPO.clickOnPivotReportTab();
				logger.info("Redirect Reporting tab -> Pivot Report tab.")
				assertsObj.verifyTrue(reportPO.checkPivotReportTabPresent(),"Pivot Reports tab is not available");
			});
			
			reportPO.checkInsightsReportTabPresent().then(function(){
				reportPO.clickOnInsightsReportTab();
				logger.info("Redirect Pivot Report tab -> Insights Report tab.");
				utilityObj.browserWaitforseconds(5);
				assertsObj.verifyTrue(reportPO.checkInsightsReportTabPresent(),"Insight Reports tab is not available");
			});
			
			reportPO.checkMyReportsTabPresent().then(function(){
				reportPO.clickOnMyReportsTab();
				logger.info("Redirect Insights Report tab -> My Reports tab.");
				assertsObj.verifyTrue(reportPO.checkMyReportsTabPresent(),"My Reports tab is not available");
			});
			utilityObj.browserWaitforseconds(6);
			reportPO.checkOnPivotReportVelocityPagePresent().then(function(){
				reportPO.checkOnPivotReportVelocityPagePresent();
				reportPO.clickOnPivotReportVelocityPageTab();
				logger.info("Redirect Reporting tab -> Pivot Report tab.")
				assertsObj.verifyTrue(reportPO.checkMyReportsTabPresent(),"Pivot Reports tab is not available");
			});
					
		};
		if((userType).toUpperCase()=='DPMWITHACCESS'){
			utilityObj.browserWaitforseconds(5);
			
			reportPO.checkInsightsReportTabPresent().then(function(){
				reportPO.clickOnInsightsReportTab();
				logger.info("Redirect Reporting tab -> Insights Report tab.");
				utilityObj.browserWaitforseconds(5);
				assertsObj.verifyTrue(reportPO.checkInsightsReportTabPresent(),"Insight Reports tab is not available");
			});
			
			reportPO.checkMyReportsTabPresent().then(function(){
				reportPO.clickOnMyReportsTab();
				logger.info("Redirect Insights Report tab -> My Reports tab.");
				assertsObj.verifyTrue(reportPO.checkMyReportsTabPresent(),"My Reports tab is not available");
			});
			
			reportPO.checkPivotReportTabPresent().then(function(){
				assertsObj.verifyTrue(reportPO.checkOnPivotReportVelocityPagePresent(),"Pivot Reports tab is not available");
				reportPO.clickOnPivotReportVelocityPageTab();
				logger.info("Redirect My Reporting tab -> Pivot Report tab.")
				
			});
		};
		if((userType).toUpperCase()=='DPM'){
			
			utilityObj.browserWaitforseconds(5);
			
			reportPO.checkInsightsReportTabPresent().then(function(){
				reportPO.clickOnInsightsReportTab();
				logger.info("Redirect Reporting tab -> Insights Report tab.");
				utilityObj.browserWaitforseconds(5);
				assertsObj.verifyTrue(reportPO.checkInsightsReportTabPresent(),"Insight Reports tab is not available");
			});
			
			reportPO.checkMyReportsTabPresent().then(function(){
				reportPO.clickOnMyReportsTab();
				logger.info("Redirect Insights Report tab -> My Reports tab.");
				assertsObj.verifyTrue(reportPO.checkMyReportsTabPresent(),"My Reports tab is not available");
			});
			
			reportPO.checkPivotReportTabPresent().then(function(){
				assertsObj.verifyTrue(reportPO.checkOnPivotReportVelocityPagePresent(),"Pivot Reports tab is not available");
				reportPO.clickOnPivotReportVelocityPageTab();
				logger.info("Redirect My Reporting tab -> Pivot Report tab.")
				
			});
		};
	};
};
module.exports = new reportsBase();

