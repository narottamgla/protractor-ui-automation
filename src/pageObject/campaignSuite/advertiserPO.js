var AdvertiserPO = function() {
	
	
    
	this.advertiserstabLink=element(by.linkText('ADVERTISERS')); //main tab link
	this.advertisersLink=element(by.linkText('Advertisers'));   //Sub tab link
	this.actionDropdown=element(by.id('div-viwActLbl'));
	this.newUnderAction=element(by.id('act-mnu-new'));
	this.newAdvertiser=element(by.id('act-mnu-newAdvrtiser'));
	this.marketnameDropdown=element(by.model('accountMarket'));
	this.accountnameDropdown=element(by.model('vo.accountName'));
	this.newadertiserName=element(by.model('vo.name'));
	this.primaryUrl=element(by.model('vo.primaryUrl'));
	this.tpaAdvName=element(by.model('vo.tpaAdvertiserName'));
	this.currencyDropdown=element(by.model('vo.localCurrencyId'));
	
	
	this.saveButton=element(by.id('btn-save'));
	
	
	//Search Created advertiser
	this.searchtextBox=element(by.css('.searchText'));
	
	this.searchgoButton=element(by.css('.goBtn'));
	
	
	
	
	
};

module.exports = new AdvertiserPO();