/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

var mediaProviderPO = function() {

	var utilityObj = require('../../lib/utility.js');
    var mediaProvidersMenuLink = element(by.linkText('MEDIA')); // main menu link of Media Providers tab.
    var mediaProvidersSubLink= element(by.linkText('Media Providers')); // sub link of main menu of Media Provider.
    var newMediaProviderLink= element(by.css('.new-advertiser-button.new-media-provider-button>ul>li>a'));
    // Link for page which creates new media provider.
    var marketIdDropDown= element(by.id('marketId'));//id for market name from dropdown.
    var parentMediaProviderIdDropDown= element(by.id('parentMediaProviderId'));//id for parent market name from dropdown.
    var mediaproviderNameTxtBox=element(by.id('mediaProviderName'));// id for mediaproviderName textbox
    var servertocerversupport=element(by.id('typeServerToServer'));	// id for server-to-server support
    var analyticsOnlyCheckbox=element(by.id('typeAnalyticsOnly'));// location for Analytics only check box 
    var baseSyntaxTxtBox=element(by.id('basePixelUrl'));// location for Base syntax text box
    var trailingSuffixTxtBox=element(by.id('basePixelUrlSuffix'));//location for trailing syntax text box.
    var fieldDelimiterTxtBox=element(by.id('basePixelUrlDelimiter'));	// location for Bas syntax text box
    var adPerformanceNextBtn=element(by.linkText('2.   Ad Performance')); // location for Next button
    var backButton=element(by.name('back'));// location for back button 
    var saveButton=element(by.css('.saveButton.save-button'));// location for save button button 
    var cancelButton=element(by.name('cancel'));// location for cancel button
    var actionDropdown=element(by.css('.action-dropDown'));// location of action dropdown
    var newMediaProviderActionLink=element(by.linkText('New Media Provider'));// Action dropdown Link for page which creates new media provider
    var searchTextField=element(by.name('search'));
    var searchSubmitButton=element(by.id('searchBtn'));
    var mediaproviderPageHeaderLocation=element(by.css('.titletext-wrapper>h2,.title-text.ng-binding'));// Location of advertiser page header.
    var mediaproviderlogo=element(by.id('mediaProviderImageFileName'));
    var basePixel=element(by.linkText('3.   Base Pixel'));
    var clickonEditlink=element(by.linkText('Edit'));//Edit link media provider
    var clickonAuditLoglink=element(by.linkText('View Audit Log'));//View Audit Log media provider.
    var searchAuditLogTextBox = element(by.name('auditLogSearch'));//Search audit log of Media Provider.
    var fieldNameLocation = element(by.css('.listView tr:nth-child(2) td:nth-child(4)'));
    var iconMenu=element(by.css('.icon-menu>img'));// click on icon of Media Provider.
    var clickviewlink=element(by.linkText('View'));// Click on View Link of Media Provider.
    var viewEditedtext=element(by.css('.title-text'));// View Edited Media provider Text
    var clickOnDeletelink=element(by.linkText('Delete'));// delete link of Media provider.
    var showDeleted=element(by.linkText('Show Deleted'));  //Show Deleted link of Media Provider.
    var hideDeleted=element(by.linkText('Hide Deleted')); //Hide Deleted link of Media provider.    
    var closedFlipper = element(by.css(".adv-flipper.closed"));
	var clearFilterLink = element(by.linkText("Clear all filters"));//clear all filter button.
	var selectOption = element(by.linkText("Select..."));//select link for Market.
	var selectMarketFilter = element(by.css(".dhx_combo_input"));//select market filter
    var selectmarket=element(by.css('#marketId'));//select market name from drop down on create Media provider page.
    var selectFilterLink=element(by.css('.selectFilterLink'));// Select market filter.
    var marketNameInListItem = element(by.css('#records-list>tbody>tr>td:nth-of-type(6)')); // market list 
    var mediaprovidername=element(by.linkText('Name'));//Media provider sorting by name link.
    var expandMediaProviders=element(by.linkText('Expand All Rows'));//Expand all row of Media Provider List.
    var collapseMediaProviders=element(by.linkText('Collapse All Rows'));//Collapse all row of Media Provider list.
    var expandAdvertiserText=element(by.css(".expanded-row:nth-child(1)>.row-label"));
    var expandMediaProviderCloseButton=element(by.css(".expand-close>img"));
    var expandAdvertiserFeedbackText=element(by.css(".expanded-row:nth-child(2)>.row-label"));
    var obtainPixelslink=element(by.partialLinkText("Obtain Pixel"));//obtain pixel link.
    var obtainpixelclose = element(by.xpath('.//table[@class="wizardButtons"]/tbody/tr/td/input'));//obtain pixel frame close button.
    var syncID=element(by.xpath("//tr/td/h5[1]"));//syncID location on Obtain pixel on Obtain pixel page.
    var impressionSharing=element(by.xpath("//tr/td/h5[2]"));//impressionSharing location on Obtain pixel page.
    var clickSharing=element(by.xpath("//tr/td/h5[3]"));//clickSharing location on Obtain pixel page.
    var obtainPixelsName=element(by.xpath("//*[@id='TB_window']/div/table/tbody/tr[1]/td/table/tbody/tr/td/h3/div")); // obtain pixel
    var enableCollectAPIData=element(by.css("#collectAPIData"));//Enable API data collection check box.
    var idTypesTurnID=element(by.css("#turnId"));// TurnId check box.
    var idTypesDeviceID=element(by.css(" #deviceId")); //Device id check box.
    var mediaProviderNameColumn =element(by.linkText('Name'));//column name for sorting media provider by name.
    var reverseMediaProviderLocation= element(by.css('.selected.reverse'));//reverse sorting of Media provider.
    
	// New Console..
    
	var newMediaProviderPlusBtn = element(by.xpath("//a[@class='tif-plus btn-action-plus pull-left' and not (contains(@style,'display: none;'))]"));
	var mpNewsearchTxtBox=element(by.model("param"));
	var mpNewElementList = element.all(by.xpath("//td[@ng-repeat='column in columns']/span|//header[@class='title-text ng-binding']"));
	var mpNewIconMenu=element(by.xpath("(//button[@type='button'])[position()=1]"));
	var mpNewListPageRecord=element(by.xpath("(//td[@ng-repeat='column in columns'])[position()=1]"));
	var mpNewConfirmDeleteAlert=element(by.css('[ng-click="deleteConfirm()"]'));
	var mpNewActionDropDown=element(by.xpath("//button[@class='btn btn-dropdown dropdown-toggle pull-right ng-binding']"));	
	var mpNewobtainPixelsName=element(by.xpath("//div[@id='obtainPixelModal']/div[1]/h4[1]")); // obtain pixel
	var mpNewImpressionSharing=element(by.css('.provider-pixel.ng-binding:nth-last-of-type(2)'));//impressionSharing location on Obtain pixel page.
    var mpNewClickSharing=element(by.css(".provider-pixel.ng-binding:nth-of-type(2)"));//clickSharing location on Obtain pixel page.
    var mpNewcloseButton = element(by.xpath("//div/button[@class='btn btn-default ng-binding'][@ng-click='close()']"));//obtain pixel frame close button.
    var mpNewNameSorting=element(by.xpath("(//div[@class='relative ng-scope ng-binding'])[position()=2]"));
    var mpViewConfigurationLink=element(by.linkText('View Configuration'));
    var mpNewConfPageHeaderText=element(by.xpath("//div[@id='viewConfigModal']/div[1]/h4")); 
    var mpNewAdvertiser=element(by.css('.view-configuration-row:nth-child(1)>div:nth-child(1)'));//Expand->Advertiser text.
	var mpNewAdvertiserFeedback=element(by.css('.view-configuration-row:nth-child(2)>div:nth-child(1)'));//Expand->Advertiser Feedback.
	var mpNewExpandPixelOptions=element(by.css('.view-configuration-row:nth-child(3)>div:nth-child(1)'));//Expand->Pixel option text.
	var mpNewExpandEvents=	element(by.css('.view-configuration-row:nth-child(4)>div:nth-child(1)'));//Expand->Events text.
	var mpNewFilterMarket=element(by.id('tapMarketName'));
	var mpNewFilterClose=element(by.css('.tif-delete.remove-filter'));
	var mpNewFilterDropdown=element(by.id('filter-dropdown'));
	var mpMarketLink=element(by.linkText('Market'));
	var mpNewMarketColoumn=element(by.xpath('//tbody/tr[1]/td[4]'));
	var mpSearchCancel=element(by.css('.turn-clear'));
		
    
	this.clickSearchCancel=function(){  
			if(mpSearchCancel.isPresent()){
			return mpSearchCancel.click();
			}
		};	
			
    this.getViewConfAdvertiserText = function() {
		utilityObj.browserWait(mpNewAdvertiser,'mpNewAdvertiser');	
		return mpNewAdvertiser.getText();
	};
	this.getViewConfTaxonomyText = function() {
		utilityObj.browserWait(mpNewAdvertiserFeedback,'mpNewAdvertiserFeedback');	
	};
	this.getViewConfPixelOptionsText = function() {
		utilityObj.browserWait(mpNewExpandPixelOptions,'mpNewExpandPixelOptions');	
		return mpNewExpandPixelOptions.getText();
	};
	this.getViewConfExpandEventsText = function() {
		utilityObj.browserWait(mpNewExpandEvents,'mpNewExpandEvents');	
		return mpNewExpandEvents.getText();
	};
	
	this.isViewConfigurationCloseButtonDisplayed=function(){
		return mpNewcloseButton.isDisplayed();
	};
	
	this.clickViewConfigurationCloseButton=function() {   
		utilityObj.browserWait(mpNewcloseButton,'mpNewcloseButton');
		return mpNewcloseButton.click();
	};
	
	this.clickConfirmDeleteAlert=function()
	{   utilityObj.browserWait(mpNewConfirmDeleteAlert,'mpNewConfirmDeleteAlert');
		return mpNewConfirmDeleteAlert.click();
	};
	
	this.clickMediaProvidersMenuLink = function() {
		utilityObj.browserWait(mediaProvidersMenuLink, 'mediaProvidersMenuLink');
		return mediaProvidersMenuLink.click();
		};
	
	this.clickMediaProvidersSubLink = function() {
		utilityObj.browserWait(mediaProvidersSubLink, 'mediaProvidersSubLink');
		return mediaProvidersSubLink.click();
	};
	
	this.clickNewMediaProviderLink = function() {
			return newMediaProviderPlusBtn.click();
	};
	
	this.selectMarketDropDown = function(optionName) {
		utilityObj.browserWait(marketIdDropDown, 'marketIdDropDown');
		return marketIdDropDown.sendKeys(optionName);
	};
	
	this.selectParentMediaProviderId = function(optionName) {
		return parentMediaProviderIdDropDown.sendKeys(optionName);
	};
	
	this.enterMediaproviderNameTxtBox = function(mediaProviderName) {
		mediaproviderNameTxtBox.clear();
		return mediaproviderNameTxtBox.sendKeys(mediaProviderName);
	};
	
	this.clickServerToServer = function() {
		return servertocerversupport.click();
	};
	
	/**
	 * This method is use to select diffient mediaChannels according to argument.
	 * 
	 * @author narottamc
	 * @param  mediaChannel
	 */
	this.selectMediaChannel = function(mediaChannel) {
		if (mediaChannel.toUpperCase() == "DISPLAY") {
			return element(by.id('mediaChannelLogo1')).click();
		} else if (mediaChannel.toUpperCase() == "VIDEO") {
			return element(by.id('mediaChannelLogo2')).click();
		} else if (mediaChannel.toUpperCase() == "SOCIAL") {
			return element(by.id('mediaChannelLogo3')).click();
		} else if (mediaChannel.toUpperCase() == "MOBILE") {
			return element(by.id('mediaChannelLogo4')).click();
		} else if (mediaChannel.toUpperCase() == "SEARCH") {
			return element(by.id('mediaChannelLogo5')).click();
		} else if (mediaChannel.toUpperCase() == "SITE PERSONILIZATION") {
			return element(by.id('mediaChannelLogo6')).click();
		}
	};
	
	/**
	 * This method is use to select diffent pixelTypes according to argument.
	 * 
	 * @author narottamc
	 * @param  pixelType
	 */
	this.selectElementsSentInPixel = function(pixelType) {
		if (pixelType.toUpperCase() == "COMPANY ID") {
			return element(by.css('#typeCompanyId')).click();
		} else if (pixelType.toUpperCase() == "PACKAGE ID") {
			return element(by.css('#typePackageId')).click();
		} else if (pixelType.toUpperCase() == "PUBLISHER ID") {
			return element(by.css('#typePublisherId')).click();
		} else if (pixelType.toUpperCase() == "OFFICE ID") {
			return element(by.css('#typeOfficeId')).click();
		} else if (pixelType.toUpperCase() == "LINE ITEM ID") {
			return element(by.css('#typeLineItemId')).click();
		} else if (pixelType.toUpperCase() == "CREATIVE SIZE") {
			return element(by.css('#typeCreativeSize')).click();
		} else if (pixelType.toUpperCase() == "CLIENT ID") {
			return element(by.css('#typeClientId')).click();
		} else if (pixelType.toUpperCase() == "CREATIVE ID") {
			return element(by.css('#typeCreativeId')).click();
		} else if (pixelType.toUpperCase() == "INVENTORY COST") {
			return element(by.css('#typeInventoryCost')).click();
		} else if (pixelType.toUpperCase() == "USERID") {
			return element(by.css('#typeUserId')).click();
		} else if (pixelType.toUpperCase() == "AGENCY FEE") {
			return element(by.css('#typeAgencyFee')).click();
		} else if (pixelType.toUpperCase() == "INSERTION ORDER ID") {
			return element(by.css('#typeInsertionOrderId')).click();
		} else if (pixelType.toUpperCase() == "BID AMOUNT") {
			return element(by.css('#typeBidAmount')).click();
		} else if (pixelType.toUpperCase() == "INVENTORY SOURCE ID") {
			return element(by.css('#typeInventorySourceId')).click();
		} else if (pixelType.toUpperCase() == "MEDIA CHANNEL ID") {
			return element(by.css('#typeMediaChannelId')).click();
		} else if (pixelType.toUpperCase() == "SUB DOMAIN") {
			return element(by.css('#typeSubdomain')).click();
		} else if (pixelType.toUpperCase() == "EXTERNAL DATA COST") {
			return element(by.css('#typeExternalDataCost')).click();
		} else if (pixelType.toUpperCase() == "DSP FEE") {
			return element(by.css('#typeDspFee')).click();
		}
	};
	
	/**
	 * This method is use to select different device type according to argument .
	 * 
	 * @author narottamc
	 * @param  deviceType
	 */
	this.selectDeviceTypesInAPIData = function(deviceType) {
	if (deviceType.toUpperCase() == "IFA") {
			return element(by.css('#ditIfa')).click();
		} else if (deviceType.toUpperCase() == "PLATEFORM ID") {
			return element(by.css('#ditPlatformId')).click();
		} else if (deviceType.toUpperCase() == "MAC ADDRESS") {
			return element(by.css('#ditMacAddress')).click();
		} else if (deviceType.toUpperCase() == "DEVICE ID") {
			return element(by.css('#ditDeviceId')).click();
		}
	};
	
	this.selectAnalyticsOnly = function() {
		return analyticsOnlyCheckbox.click();
	};
	
	this.setBaseSyntx = function(baseSyntax) {
		baseSyntaxTxtBox.clear();
		return baseSyntaxTxtBox.sendKeys(baseSyntax);
	};
	
	this.enterTrailingSuffixTxtBox = function(baseSyntax) {
		trailingSuffixTxtBox.clear();
		return trailingSuffixTxtBox.sendKeys(baseSyntax);
	};
	
	this.setFieldDelimiter = function(fieldDelimiter) {
		fieldDelimiterTxtBox.clear();
		return fieldDelimiterTxtBox.sendKeys(fieldDelimiter);
	};
	
	this.clicknextButton = function() {
		return adPerformanceNextBtn.click();
	};
	this.clickBackButton = function() {
		utilityObj.browserWait(backButton, 'backButton');
		return backButton.click();
	};
	
	this.clickSaveButton = function() {
		return saveButton.click();
	};
	this.clickNewMediaProviderCancelButton = function() {
		utilityObj.browserWait(cancelButton, 'cancelButton');
		return cancelButton.click();
	};

	this.clickActionDropdown = function() {
		utilityObj.browserWait(mpNewActionDropDown,"mpNewActionDropDown");
		return mpNewActionDropDown.click();
	};
	
	this.checkActionDropdownPresent = function() {
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		return mpNewActionDropDown.isPresent();
	};
	
	this.clickNewMediaProviderActionLink = function() {
		utilityObj.browserWait(newMediaProviderActionLink,
				'newMediaProviderActionLink');
		return newMediaProviderActionLink.click();
	};
	
	 this.mediaProviderelementList=function()
	 {
		 return mpNewElementList;
	 };
	  
	 
	this.returnSearchTextField = function() 
	{
			utilityObj.browserWait(mpNewsearchTxtBox,"mpNewsearchTxtBox");
			return mpNewsearchTxtBox;
	};
	
	this.returnSearchSubmitButton = function() {
		utilityObj.browserWait(searchSubmitButton, 'searchSubmitButton');
		return searchSubmitButton;
	};
		
	this.browseMediaproviderlogo = function(logopath) {
		return mediaproviderlogo;
	};
	this.clickOnBasePixel = function() {
	return basePixel.click();
	};
	
	this.clickonEditlink = function() {
		utilityObj.browserWait(clickonEditlink, 'clickonEditlink');
		return clickonEditlink.click();
	};
	
	this.clickonViewAuditLoglink = function() {
		utilityObj.browserWait(clickonAuditLoglink, 'clickonAuditLoglink');
		return clickonAuditLoglink.click();
	};
	
	this.isAuditLogSearchBoxPresent = function() {
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		return searchAuditLogTextBox.isPresent();
	};
	
	this.returnAuditLogSearchElement = function() {
		return searchAuditLogTextBox;
	};
	
	this.getFieldNameText = function() {
		return fieldNameLocation.getText();
	};
	
	this.searchAuditLogTextBox = function(fieldName) {
		searchAuditLogTextBox.sendKeys(fieldName);
		searchAuditLogTextBox.sendKeys(protractor.Key.ENTER);
	};
	
	this.clickOnIconMenu = function() {
			 mpNewListPageRecord.click().then(function(){
			 browser.waitForAngular();
			 utilityObj.browserWaitforseconds(2);
			 browser.driver.executeScript('arguments[0].click()',mpNewIconMenu.getWebElement());
			 utilityObj.browserWaitforseconds(2);
			 });
	};
	
	this.isIconMenuPresent = function() {
		return iconMenu.isPresent();
		};
	
	this.clickViewlink = function() {
		utilityObj.browserWait(clickviewlink, 'clickviewlink');
		return clickviewlink.click();
	};
	
	this.getExpectedMediaProvidersNameViewPage = function() {
		utilityObj.browserWait(viewEditedtext, 'viewEditedtext');
		return viewEditedtext.getText();
	};
	this.clickOnDeletelink = function() {
		utilityObj.browserWait(clickOnDeletelink, 'clickOnDeletelink');
		return clickOnDeletelink.click();
	};
	
	this.clickOnShowDeleted = function() {
		utilityObj.browserWait(showDeleted, 'showDeleted');
		return showDeleted.click();
	};
	
	this.clickOnHideDeleted = function() {
		utilityObj.browserWait(hideDeleted, 'hideDeleted');
		return hideDeleted.click();
	};
	this.clickOnHideDeleted = function() {
		utilityObj.browserWait(hideDeleted, 'hideDeleted');
		return hideDeleted.click();
	};
	
	this.clearFilterOption = function() {
		utilityObj.browserWait(clearfilterlink, 'clearfilterlink');
		return clearfilterlink.click();
	};
	
	this.selectMarketName = function(marketName) {
		return selectmarket.sendKeys(marketName);
	};
	
	this.clickSelectFilterLink = function() {
		utilityObj.browserWait(selectFilterLink, 'selectFilterLink');
		return selectFilterLink.click();
	};
	
	this.clickMediaProvidernameLink = function() {
		utilityObj.browserWait(mediaprovidername, 'mediaprovidername');
		return mediaprovidername.click();
	};
	
	this.clickExpandMediaProvidersLink = function() {
		utilityObj.browserWait(expandMediaProviders, 'expandMediaProviders');
		return expandMediaProviders.click();
	};
	this.clickCollapseMediaProvidersLink = function() {
		utilityObj.browserWait(collapseMediaProviders, 'collapseMediaProviders');
		return collapseMediaProviders.click();
	};
		
	this.expandAdvertiserText = function() {
			return mpNewAdvertiser.getText();
	};
	
	this.expandAdvertiserFeedbackText = function() {
			return mpNewAdvertiserFeedback.getText();
	};
	
	this.clickExpandMediaProviderCloseButton = function() {
		utilityObj.browserWait(expandMediaProviderCloseButton,
				'expandMediaProviderCloseButton');
		return expandMediaProviderCloseButton.click();
	};
	
	this.isExpandMediaProviderCloseButtonDisplayed=function()
	{
		return expandMediaProviderCloseButton.isDisplayed();	
	};
	
	this.clickOnObtainPixelsLink = function() {
		utilityObj.browserWait(obtainPixelslink, 'obtainPixelslink');
		return obtainPixelslink.click();
	};
	
	this.clickCloseOnObtainPixels = function() {
			utilityObj.browserWait(mpNewcloseButton, 'mpNewcloseButton');
			return mpNewcloseButton.click();
	};
	
	this.expectedAudienceSyncId = function() {
		utilityObj.browserWait(syncID, 'syncID');
		return syncID.getText();
	};
	
	this.expectedImpressionSharing = function() {
			utilityObj.browserWait(mpNewImpressionSharing, 'mpNewImpressionSharing');
			return mpNewImpressionSharing.getText();
	};
	
	this.expectedClickSharing = function() {
			utilityObj.browserWait(mpNewClickSharing, 'mpNewClickSharing');
			return mpNewClickSharing.getText();
	};
	
	this.getObtainPixelsName = function() {
			utilityObj.browserWait(mpNewobtainPixelsName,"mpNewobtainPixelsName");
				return mpNewobtainPixelsName.getText();
	};
	
	this.getMediaProviderPageHeader = function() {
		utilityObj.browserWait(mediaproviderPageHeaderLocation,"mediaproviderPageHeaderLocation");
		utilityObj.browserWaitforseconds(2);
		return mediaproviderPageHeaderLocation.getText();
		};
	
	this.getMediaProviderAuditLogPageHeader = function() {
		utilityObj.browserWait(mediaproviderPageHeaderLocation,"mediaproviderPageHeaderLocation");
		
		return mediaproviderPageHeaderLocation.getText();
	};	
	
	this.CheckEnableCollectAPIDataCheckBox = function() {
		return enableCollectAPIData.click();
	};
	
	this.CheckIdTypesTurnIDCheckBox = function() {
		return idTypesTurnID.click();
	};
	
	this.CheckIdTypesDeviceIDCheckBox = function(){ 
		return idTypesDeviceID.click();
	};

	this.clickMediaProviderNameColumn = function() {		
			utilityObj.browserWait(mpNewNameSorting,'mpNewNameSorting');
			mpNewNameSorting.click();
	};
	
	this.clickViewConfigurationLink=function(){
		utilityObj.browserWait(mpViewConfigurationLink,'mpViewConfigurationLink');
		mpViewConfigurationLink.click();
	};
	
	this.getConfigurationPageHeaderText=function(){
		utilityObj.browserWait(mpNewConfPageHeaderText,'mpNewConfPageHeaderText');
		return mpNewConfPageHeaderText.getText();
	};
	
	this.returnReverseMediaProviderName = function() {
		return reverseMediaProviderLocation.isPresent();
	};
	
	this.clearSearchBox = function() {
			utilityObj.browserWait(mpNewsearchTxtBox,"mpNewsearchTxtBox");
			utilityObj.browserWaitforseconds(2);
			mpNewsearchTxtBox.clear();	
			return mpNewsearchTxtBox.sendKeys(protractor.Key.ENTER);
	};
	
	this.checkFilterFlipper = function() {
			return mpNewFilterMarket.isPresent();
	};
	
	this.clickFilterFlipper = function() {
		return closedFlipper.isPresent().then(function(value) {
			if (value) {
				return closedFlipper.click();
			} else {
				return false;
			}
		});
	};
	
	this.clickOnClearFilter = function() {
			return mpNewFilterMarket.isPresent().then(function(value){
				if(value){
					browser.driver.executeScript('arguments[0].style.opacity="1"',mpNewFilterClose.getWebElement()).then(function(){
						browser.driver.executeScript("arguments[0].click();",mpNewFilterClose.getWebElement());
					});
					
				}else{
					return false;
				}
				
			});	
	};
	
	this.clickOnSelectLink = function() {
			utilityObj.browserWait(mpNewFilterDropdown,'mpNewFilterDropdown');
			return mpNewFilterDropdown.click().then(function(){
				return mpMarketLink.click();
			});
	};
	
	this.enterMarketName = function(marketName) {
			mpNewFilterMarket.clear();
			return mpNewFilterMarket.sendKeys(marketName);
	};
	
	this.clickOnMarketFilter = function(marketType) {
			utilityObj.browserWait(element(by.xpath("//a[text()='"+marketType+"']")),'Market element filter');
			element(by.xpath("//a[text()='"+marketType+"']")).click();
			return browser.waitForAngular();
	};
	
	this.getMarketNameFromColumn = function() {
			utilityObj.browserWait(mpNewMarketColoumn,'mpNewMarketColoumn');
			return mpNewMarketColoumn.getText();
	};
	
	
};

module.exports = new mediaProviderPO();
