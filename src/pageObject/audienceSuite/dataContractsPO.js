	
	var DataContractsPO = function(){
		var loggerObj = require('../../lib/logger.js');
		var logger = loggerObj.loggers("dataContractPO");
		
		var utilityObj = require('../../lib/utility.js');
		var dcPageHeaderLocation = element(by.css(".title-text.ng-binding,.titletext-wrapper>h2"));// Location of Data Contract page header.
		var dcSearchElement = element(by.id('search'));
		var dcDataLink = element(by.linkText('DATA')); //Main Menu Link of Data tab.
		var dcCSDataLink = element(by.linkText('Data'));		
		var dcDataContractSubLink = element(by.linkText('Data Contracts')); //click on Data Contracts sub Menu Link of Data tab.
		var dcNewDataContractsButton = element(by.linkText('New Data Contract'));// Click on New Data Contract button.
		var dcActionDropDownButton = element(by.css('.action-dropDown'));// location of action dropdown.
		var dcNewDataContractsFromActionLink= element(by.css('.options>li:nth-child(1)>a'));//Select New Data Contracts from Action Drop down list.
		var dcSelectDataProviderFromFrame = 'TB_window';//data Provider frame id.
		var dcSelectDataProviderDropDown = element(by.id('data-provider-select'));//select data provider from frame drop down.
		var dcFrameNextButton = element(by.id('modal-button-next'));//click on Next button after selecting data Provider.
	// General tab	
		var dcGeneralTab = element(by.linkText('1.   General'));//click on general button.
		var dcNewDataContractNameTxtBox = element(by.id('dataContractName'));//enter New Data Contract Name.
		var dcParentDataContractDropDown = element(by.id('parentDataContractId'));//select parent Data Contracts.
		var dcCurrencyDropDown = element(by.id('localCurrencyId'));//Select Currency from drop down
		var dcAllAdvertiserRdBtn = element(by.id('availabilityAll'));//Select All Advertiser Radio Button.
		var dcSingleAdvertiserRdBtn = element(by.id('availabilitySingle'));//Select Single Advertiser Radio Button.
		var dcSingleAdvertiserDropDown=element(by.id('singleAdvertiserId'));//Single advertiser drop down 
		var dcavailabilityAnalyticsRdBtn = element(by.id('availabilityAnalytics'));//Select Analytics Only Radio Button.
		var dcEnableForCrossDeviceDuplicationCheckBtn = element(by.id('optionalCheckbox-DupContract'));//Cross device duplication checkbox location.
		var dcCrossDeviceGraphProviderDropDown = element(by.id('graphProviderSelector'));//Select Types of Graph Providers.
		var dcCrossDeviceRowsAddButton = element(by.css('.iconCell>a:nth-child(2)>img'));//This is (+) image button to add more rows in cross device.
		var dcGeneralNextBtn=element(by.css('.nextButton.default-button')); //general tab next button
		var marketNameInListItem = element(by.css('#records-list>tbody>tr>td:nth-of-type(6)')); // market list	
		//Flipper
		var dcClosedFlipper = element(by.css('.adv-flipper.closed')); //flipper button
		var dcDPSelectOption = element(by.xpath('//*[@id="dataProviderFilterSelectContainer"]/table/tbody/tr/td[1]/a'));
		var dcDataproviderTxtBox=element(by.xpath('//*[@id="dataProviderFilter"]/div/input[1]'));
		var dcSelectFilterLink=element(by.css('.selectFilterLink'));// Select market filter.
		var dcClearFilterLink = element(by.linkText("Clear all filters"));//clear all filter button.
		var dcClearAllFilter = element(by.linkText("Clear all"));
		var dcSelectOptionMarketFilter = element(by.css("#marketFilterSelectContainer a:nth-child(1)"));//select link for Market.
		var dcSelectMarketFilter = element(by.css(".dhx_combo_input"));//select market filter
		
		//Edit/View/Delete/Undo delete data contract
		var dcIconMenu = element(by.css('.icon-menu>img'));//Icon Menu data contract.
		var dcSearchTextField=element(by.id('search'));//Search Text box data contract.
		var dcEditLink=element(by.linkText('Edit'));//IconMenu->Edit link Data contract.
		var dcViewLink=element(by.linkText('View'));//IconMenu->View link data contract.
		var dcViewEditedtext=element(by.css('.title-text'));//View data contract text.
		var dcDeleteLink=element(by.linkText('Delete'));//IconMenu->Delete data contract link.
		var dcUndoDeleteLink=element(by.linkText('Undo Delete'));//IconMenu->Undo Delete data contract link.
		var dcFirstRecordListCheckBox=element(by.xpath("(.//input[@name='dataContractId'])[position()=1]"));//data contract list check box for first record.
		
		//Obtain pixel/View Audit Log
		var dcObtainPixels=element(by.partialLinkText('Obtain Pixel'));//Obtain pixel link.
		var dcViewAuditLog=element(by.linkText('View Audit Log'));//Audit log link.
		var dcObtainPixelsName=element(by.xpath("//*[@id='TB_window']/div/table/tbody/tr[1]/td/table/tbody/tr/td/h3/div")); // obtain pixel
		var dcObtainpixelclose = element(by.xpath('.//table[@class="wizardButtons"]/tbody/tr/td/input'));//obtain pixel frame close button.
		var dcSearchAuditLogTextBox = element(by.name('auditLogSearch'));//Search audit log of Data contract.
		var dcFieldNameLocation = element(by.css('.listView tr:nth-child(2) td:nth-child(4)'));//Field name location
		//Show/Hide/Expand all
		var dcShowDeletedLink=element(by.linkText('Show Deleted/Expired'));//Action->Show deleted Link of data Contract.
		var dcHideDeletedLink=element(by.linkText('Hide Deleted/Expired'));//Action->Hide deleted Link of data Contract.
		var dcExpandAllRowsLink=element(by.linkText('Expand All Rows'));//Action->Expand all rows Link of data Contract.
		var dcExpandDataContractconf=element(by.css('.expand-row-title>span'));//Expand->data contract configuration text.
		var dcExpandRateSchedules=element(by.css('.expanded-row:nth-child(1)>div:nth-child(1)'));//Expand->Rate schedule text.
		var dcExpandTaxonomy=element(by.css('.expanded-row:nth-child(2)>div:nth-child(1)'));//Expand->Taxonomytext.
		var dcExpandPixelOptions=element(by.css('.expanded-row:nth-child(3)>div:nth-child(1)'));//Expand->Pixel option text.
		var dcExpandEvents=	element(by.css('.expanded-row:nth-child(4)>div:nth-child(1)'));//Expand->Events text.
		var dcExpandCloseButton=element(by.css('.expand-close>img'));//Expand->Close button.	
		var dcCollapseAllRowsLink=element(by.linkText('Collapse All Rows'));//Action->Collapse all rows Link of data Contract.
		//Sorting
		var dataContractNameColumn =element(by.linkText('Name'));//column name for sorting Data contract list by name.
		var reverseDataContractLocation= element(by.css('.selected.reverse'));//reverse sorting of Data contarct.
		
		
	// Data type Tab(Standard) - (It shows when user selected data Provider has selected Parent Data Provider)
		//(Collection type -"Pixel")
		var dcDataTypeTab = element(by.xpath('.//div[@id="selectedContentNav"]/ul/li[2]/a'));//Click on Data type tab.
		var dcCollectiontypeDropdown = element(by.id('dataContractTypeId'));//select collection type
		var dcPixelTypeDropdown = element(by.id('pixelTypeSelect'));//select pixel type.It display when selected "Pixel" collection type.
		var dcIfaCheckedBtn = element(by.xpath('.//div[@id="deviceIdTypesDiv"]/div/div[1]/label/input'));
		var dcPlatformIdCheckedBtn = element(by.xpath('.//div[@id="deviceIdTypesDiv"]/div/div[2]/label/input'));
		var dcCategoriesCheckBtn = element(by.id('optionsCheckBox-categories'));//select categories check button.
		var dcKeywordsCheckBtn = element(by.id('typeKeywords'));//select Keywords check button.
		var dcKeysCheckBtn = element(by.id('optionsCheckBox-keyvalue'));//select Keys check button.
		var dcClickOnFrame = element(by.id('dataContractDataType'));//click on frame for refresh hidden value.
		var dcClearLink = element(by.linkText('Clear all'));
		
	
	// Data type Tab (Collection type -"Pixel" && Data Collection type - "Categories")
		var dcImportTaxonomyBtn =  element(by.id('taxonomyManageLink'));//Click on Import Taxonomy Button.
		var dcbrowserOnFrame = element(by.css('.textInput'));
		var dcapplyButton = element(by.name('next'));
	//--------------------
	
	// Data type Tab (Collection type -"Pixel" && Data Collection type - "Keys")	
		var dcKeyTxtBox =  element(by.id('keyValue0')); //Enter Key Value in text box.
	//---------------------
		
	//Data Type (FlexTag)	
		var dcFlexDataTypeHtmlTab=element(by.css('.html.tab'));
		var dcFlexDataTypeAddNewKeyBtn = element(by.css('.consoleButton[value="Add New Key"]'));
		var dcFlexDataTypeJavaScriptTab = element(by.css('.javascript.tab'));
		var dcclickonFlexagdataTypeFrame = element(by.id('selectedContentMain'));
	// Cross Device Duplication
		
			var dcFliexDataTypekeyElement=function(row){
			var webElement=element(by.xpath('//*[@id="transformationContainer"]/div[2]/div[1]/div/div[2]/div['+row+']/div[1]/input'));
			return webElement;
		};
		var dcFliexDataTypeEventName=function(row){
			var webElement=element(by.xpath('//*[@id="transformationContainer"]/div[2]/div[1]/div/div[2]/div['+row+']/div[2]/select'));
			return webElement;
		};
		var dcFliexDataTypeTransform=function(row){
			var webElement=element(by.xpath('//*[@id="transformationContainer"]/div[2]/div[1]/div/div[2]/div['+row+']/div[5]/select'));
			return webElement;
		};		
		var dcFliexDataTypeTransformValue=function(row){
			var webElement=element(by.xpath('//*[@id="transformationContainer"]/div[2]/div[1]/div/div[2]/div['+row+']/div[5]/input'));
			return webElement;
		};
		
		var dcFliexDataTypeTransformMinRange=function(row){
			var webElement=element(by.xpath('//*[@id="transformationContainer"]/div[2]/div[1]/div/div[2]/div['+row+']/div[5]/div[1]/input'));
			return webElement;
		};
		var dcFliexDataTypeTransformMaxRange=function(row){
			var webElement=element(by.xpath('//*[@id="transformationContainer"]/div[2]/div[1]/div/div[2]/div['+row+']/div[5]/div[2]/input'));
			return webElement;
		};
		var dcFliexDataTypeTransformValueSelect=function(row,value){
			var webElement=element(by.xpath("//*[@id='transformationContainer']/div[2]/div[1]/div/div[2]/div["+row+"]/div[5]/select/optgroup[*]/option[text()='"+value+"']"));
			return webElement;			
		};
		
		var dcFlexDataTypeAddTaxonomyBtn = element(by.css('.consoleButton.toggleTaxonomy'));
		var dcFlexDataTypeAddParentBtn = element(by.name('dataContractAddNode'));
		var dcFlexDataTypeClickOnParentBtnandOpenTxtBox = element(by.css('.intreeeditRow[type="text"]'));
		var dcFlexDataTypeAddChildBtn = element(by.css('.consoleButton.dataContractAddChildNode'));	
		var dcFlexDataTypeChildNode=element(by.xpath('//*[@id="dataContractTaxonomyView"]/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span'));
		var dcFlexDataTypeAddPiggyBackBtn = element(by.css('.btn.btn-link.pull-right.add-piggyback-button'));
		var dcFlexDataTypePiggybackType =element(by.xpath('.//div[@id="PiggybackModal"]/div/div/div[2]/div[1]/div/div/button'));
		var dcFlexDataTypePiggyBackNameTxtBox =element(by.name('piggybackName'));
		var dcFlexDataTypeHTMLCodeInjectBoard = element(by.xpath('//*[@id="PiggybackModal"]/div/div/div[2]/div[2]/div/ng-include/div[2]/div/div/div[2]/div'));
		var dcFlexDataTypeInsertPageEventDropDown = element(by.xpath('.//div[@id="PiggybackModal"]/div/div/div[2]/div[2]/div/ng-include/div[3]/div/div/button'));
		var dcFlexDataTypePiggyBackSaveBtn = element(by.css('.btn.btn-primary.save.ng-binding'));
		var dcFlexDataTypePiggyAdvertiser=element(by.name('piggybackDataAdvertiserId'));
		var dcFlexDataTypePiggyBeacon=element(by.name('piggybackDataBeaconId'));
		var dcFlexDataTypePiggyOrderId=element(by.name('piggybackDataOrderId'));
		var dcFlexDataTypePiggyOrderValue=element(by.name('piggybackDataOrderValue'));
		var dcFlexDataTypePiggyCurrency=element(by.name('piggybackDataCurrencyCode'));
		var dcflexDataTypePiggyPixcelId=element(by.name('piggybackDataPixelId'));
		var dcflexDataTypePiggyTaxonomyId=element(by.name('piggybackDataTaxonomyCategoryIds'));
		var dcflexDataTypePiggykeywords=element(by.name('piggybackDataKeywords'));
		var dcflexDataTypePiggykeywordPairs=element(by.name('piggybackDataKeyValuePairs'));
		var dcflexDataTypePiggyDPUserId=element(by.name('piggybackDataDataProviderUserId'));
		var dcflexDataTypePiggyOutPut=element(by.name('piggybackDataOptOut'));
		var dcflexDataTypePiggySubDomain=element(by.name('piggybackDataSubdomain'));
		var dcflexDataTypePiggyGroupTag=element(by.name('piggybackDataGroupTag'));
		var dcflexDataTypePiggyActivityTag=element(by.name('piggybackDataActivityTag'));
		var dcflexDataTypePiggyU=element(by.name('piggybackDatacustomVariable0Key'));
		var dcflexDataTypePiggyUValue=element(by.name('piggybackDatacustomVariable0Value'));
		var dcflexDataTypePiggyQuantity=element(by.name('piggybackDataQuantity'));
		var dcflexDataTypePiggyCost=element(by.name('piggybackDataCost'));
		var dcFlexDataTypeEventtypeRule = element(by.name('pageEvent_new1_0'));
		var dcFlexDataTypeTransformationRule= element(by.name('transformation_new1_0'));
		var dcFlexDataTypeRuleValueBox = element(by.id('pageEventValue_new1_0'));
		
		//New
		var dcNewDataContractPlusBtn=element(by.css(".tif-plus.btn-action-plus.pull-left"));
		var dcNewDataProviderDropDown=element(by.xpath("//div[@class='modal-dialog ng-scope']")).element(by.id('tapdataProviderName'));
		var dcNewElementList = element.all(by.xpath("(//td[@class='smart-table-data-cell medium column-name'])[position()=1]|//header[@class='title-text ng-binding']"));
		var dcNewPageHeaderLocation=element(by.css(".title-text.ng-binding,.titletext-wrapper>h2"));
		var dcNewListPageRecord=element(by.xpath("(//td[@ng-repeat='column in columns'])[position()=1]"));
		var dcNewsearchTxtBox=element(by.model("param"));
		var dcNewIconMenu=element(by.xpath("(//button[@type='button'])[position()=2]"));
		var dcViewConfigurationLink=element(by.linkText('View Configuration'));
		var dcNewExpandRateSchedules=element(by.css('.view-configuration-row:nth-child(1)>div:nth-child(1)'));//Expand->Rate schedule text.
		var dcNewExpandTaxonomy=element(by.css('.view-configuration-row:nth-child(2)>div:nth-child(1)'));//Expand->Taxonomytext.
		var dcNewExpandPixelOptions=element(by.css('.view-configuration-row:nth-child(3)>div:nth-child(1)'));//Expand->Pixel option text.
		var dcNewExpandEvents=	element(by.css('.view-configuration-row:nth-child(4)>div:nth-child(1)'));//Expand->Events text.
		var dcNewConfCloseButton=element(by.css('[ng-click="close()"]'));
		var dcNewConfPageHeaderText=element(by.xpath("//div[@id='viewConfigModal']/div[1]/h4"));
		var dcNewObtainPixelHeaderText= element(by.css('.modal-header>h4:nth-of-type(2)'));
		var dcNewActionDropDown=element(by.xpath("//button[@class='btn btn-dropdown dropdown-toggle pull-right ng-binding']"));
		var dcNewConfirmDeleteAlert=element(by.css('[ng-click="deleteConfirm()"]'));
		var dcNewNameColumnSorting=element(by.xpath("(//div[@class='relative ng-scope ng-binding'])[position()=2]"));
		var dcNewClearSearchIcon=element(by.css('.turn-clear'));
		var dcNewMarketNameInList=element(by.css(".smart-table-data-cell:nth-child(4)"));
		var dcNewDataProviderNameInList=element(by.css(".smart-table-data-cell:nth-child(5)"));
		var dcNewFilterButton=element(by.id('filter-dropdown'));
		var dcNewSelectFilterTypeMarket=element(by.css('li:nth-of-type(4)>[ng-click="addFilter(filterKey)"]'));
		var dcNewSelectMarket=element(by.id("tapMarketName"));
		var dcNewSelectFilterTypeDataProvider=element(by.css('li:nth-of-type(3)>[ng-click="addFilter(filterKey)"]'));
		var dcNewSelectDataProvider=element(by.id("tapdataProviderName"));
		var dcNewClearFilter=element(by.css('[ng-click="removeFilter(filterKey)"]'));
		var dcNewReverseDataContractName=element(by.xpath("//th[@class='smart-table-header-cell medium sort-descent']"));
		var dcNewDataProviderFrameNextButton=element(by.xpath("(//button[@class='btn btn-default ng-binding'])[position()=3]"));
		var dcNewAvailabilityFilter=element(by.id("availabilityId"));
		
		this.isMarketFilterPresent=function(){
			return dcNewSelectMarket.isPresent();
		};
		
		this.isDataProviderFilterPresent=function(){
			return dcNewSelectDataProvider.isPresent();
		};
		
		this.getConfigurationPageHeaderText=function(){
			utilityObj.browserWait(dcNewConfPageHeaderText,'dcNewConfPageHeaderText');
			return dcNewConfPageHeaderText.getText();
		};
		
		this.clickViewConfigurationLink=function(){
			utilityObj.browserWait(dcViewConfigurationLink,'dcViewConfigurationLink');
			dcViewConfigurationLink.click();
		};
		
		this.getViewConfRateScedulesText = function() {
			utilityObj.browserWait(dcNewExpandRateSchedules,'dcNewExpandRateSchedules');	
			return dcNewExpandRateSchedules.getText();
		};
		this.getViewConfTaxonomyText = function() {
			utilityObj.browserWait(dcNewExpandTaxonomy,'dcNewExpandTaxonomy');	
			return dcNewExpandTaxonomy.getText();
		};
		this.getViewConfPixelOptionsText = function() {
			utilityObj.browserWait(dcNewExpandPixelOptions,'dcNewExpandPixelOptions');	
			return dcNewExpandPixelOptions.getText();
		};
		this.getViewConfExpandEventsText = function() {
			utilityObj.browserWait(dcNewExpandEvents,'dcNewExpandEvents');	
			return dcNewExpandEvents.getText();
		};
		
		this.clickViewConfigurationCloseButton=function() {   
			utilityObj.browserWait(dcNewConfCloseButton,'dcNewConfCloseButton');
			return dcNewConfCloseButton.click();
		};
		
		this.isViewConfigurationCloseButtonDisplayed=function(){
			return dcNewConfCloseButton.isDisplayed();
		};
		
		 this.dataContractElementList=function()
		 {
			 return dcNewElementList;
		 };
		this.selectEventTypeRule = function(eventRule){
			utilityObj.browserWait(dcFlexDataTypeEventtypeRule, "dcFlexDataTypeEventtypeRule");
			dcFlexDataTypeEventtypeRule.sendKeys(eventRule);
		};
		
		this.selectTransformationeRule = function(tranformationRule){
			utilityObj.browserWait(dcFlexDataTypeTransformationRule, "dcFlexDataTypeTransformationRule");
			dcFlexDataTypeTransformationRule.sendKeys(tranformationRule);
			browser.waitForAngular();
		};
		
		this.selectRuleValue = function(ruleValue){
			utilityObj.browserWait(dcFlexDataTypeRuleValueBox, "dcFlexDataTypeRuleValueBox");
			dcFlexDataTypeRuleValueBox.sendKeys(ruleValue);
		};
		
		this.waitForFrame = function(){
			utilityObj.browserWait(dcSelectDataProviderFromFrame, "dcSelectDataProviderFromFrame");
		};
		
		this.isClearFilterPresent = function(){
			dcClearLink.isPresent().then(function(value){
				if(value){
				return dcClearLink.click();	
				}
				else
					{
					return false;
					}
			});
		};
			
	//	Rate Schedules Tab
		var dcRateSchedulesTabLink = element(by.linkText('3.   Rate Schedules'));
		var dcRateSchedulesTab = element(by.xpath('.//div[@id="selectedContentNav"]/ul/li[3]/a'));
		var dcAudineceDataCostDropDown = element(by.id('turnPayable'));
		var dcStartDateCalander=element(by.id('dataContractScheduleStartDate0')); //Start date
		var dcEndDateCalander = element(by.id('dataContractScheduleEndDate0'));
		var dcSelectRateTypeDropDown = element(by.id('dataContractPricingTypeId0'));
		var dcFlatRateTxtBox = element(by.id('ffRate0'));
		var dcCPMTxtBox = element(by.id('cpmRate0'));
		var dcCPUUTxtBox = element(by.id('cpuuRate0'));
		var dcCostPerStampTxtBox = element(by.id('cpsRate0'));
		var dcRevinueShareTxtBox=element(by.id('revSharePercent0'));
		var dcRateScheduleHeaderText = element(by.xpath('.//div[@id="dataContractRateSchedules"]/h3'));
	// Save Button
		var dcSaveBtn =element(by.name('save'));
		var dcHeaderLocation=element(by.css('.titletext-wrapper>h2'));//page header 
		
		this.clickConfirmDeleteAlert=function()
		{   utilityObj.browserWait(dcNewConfirmDeleteAlert,'dcNewConfirmDeleteAlert');
			return dcNewConfirmDeleteAlert.click();
		};
		
		this.getAuditLogPageHeader=function(){
			utilityObj.browserWait(dcPageHeaderLocation,"advPageHeaderLocation");
			return dcPageHeaderLocation.getText();
	    };
		
		this.getPageHeader = function() {
				utilityObj.browserWait(dcNewPageHeaderLocation,"dcNewPageHeaderLocation");
				return dcNewPageHeaderLocation.getText();
		};
		
		this.isSearchBoxPresent = function(){
				browser.waitForAngular();
				return dcNewsearchTxtBox.isPresent();
		};
		
		this.returnSearchElement = function(){
				utilityObj.browserWaitforseconds(2);
				utilityObj.browserWait(dcNewsearchTxtBox,"dcNewsearchTxtBox");
				return dcNewsearchTxtBox;
		};
		
		this.clickClearSearchIcon=function(){
			if(dcNewClearSearchIcon.isPresent()){
			return dcNewClearSearchIcon.click();
			}
		};
		
		this.enterDataProviderName = function(dataProvider){
			utilityObj.browserWait(dcDataproviderTxtBox,'dcDataproviderTxtBox');
			return dcDataproviderTxtBox.sendKeys(dataProvider);	
		};
		
		this.clickOnDataProviderFilter = function(datProvider){
			utilityObj.browserWait(element(by.xpath("//a[text()='"+datProvider +"']")),'Data Provider filter');
			return element(by.xpath("//a[text()='"+datProvider +"']")).click();
		};
		
		this.getDataContractPageHeaderText = function(){
				utilityObj.browserWait(dcNewPageHeaderLocation,"dcNewPageHeaderLocation");
				return dcNewPageHeaderLocation;
		};
		
		this.clickOnDataLink = function(){			
			utilityObj.browserWait(dcDataLink,'dcDataLink');
			return dcDataLink.click();
			
		};
		this.clickCSOnDataLink = function(){			
			utilityObj.browserWait(dcCSDataLink,'dcCSDataLink');
			return dcCSDataLink.click();
			
		};
		
		
		this.clickOnDataContractsSubLink = function(){
		utilityObj.browserWait(dcDataContractSubLink,'dcDataContractSubLink');
			return dcDataContractSubLink.click();
		};
		
		this.clickOnNewDataContractsButton = function(){
				utilityObj.browserWait(dcNewDataContractPlusBtn, 'dcNewDataContractPlusBtn');
				dcNewDataContractPlusBtn.click();
		};
		
		this.isDataContractNameTxtBoxPresent = function(){
			utilityObj.browserWait(dcNewDataContractNameTxtBox, 'dcNewDataContractNameTxtBox');
			 return dcNewDataContractNameTxtBox.isPresent().then(function(value){
				 return value;
			 });
		};
		
		this.checkAndClickNewDataContract=function(){
		return dcNewDataContractNameTxtBox.isPresent().then(function(present){
			if(present==false){
				return dcNewDataContractsButton.isPresent().then(function(value){
					if(value==true){
					dcNewDataContractsButton.click();
					browser.waitForAngular();
					}
				});
			}
		});
		
	};
		
		
		this.clickOnActionDropDownList = function(){
			utilityObj.browserWait(dcNewActionDropDown,"dcNewActionDropDown");
			return dcNewActionDropDown.click();
		};
		
		this.clickNewDataContractsLinkFromAction = function(){
			utilityObj.browserWait(dcNewDataContractsFromActionLink, 'dcNewDataContractsFromActionLink');
			return dcNewDataContractsFromActionLink.click();
		};
		
		this.switchToFrame = function(){
		return utilityObj.switchToFrame(dcSelectDataProviderFromFrame);
		};
		
		this.selectDataProviderfromDropDown = function(dataProvider){
			 utilityObj.browserWait(dcNewDataProviderDropDown, 'dcNewDataProviderDropDown');
			 dcNewDataProviderDropDown.sendKeys(dataProvider);
			 browser.waitForAngular();
			 utilityObj.browserWaitforseconds(1);
			return element(by.xpath("//li[contains(@title, '"+dataProvider+"')]")).click();
		};
		
		this.clickOnFrameNextButton = function(){
			dcNewDataProviderFrameNextButton.click();
		};

		this.enterNewDataContractName = function(name){
			utilityObj.browserWait(dcNewDataContractNameTxtBox, 'dcNewDataContractNameTxtBox');
			dcNewDataContractNameTxtBox.clear();
			return dcNewDataContractNameTxtBox.sendKeys(name);
		};
		
		this.selectParentDataContract = function(parentname){
			utilityObj.browserWait(dcParentDataContractDropDown, 'dcParentDataContractDropDown');
			return dcParentDataContractDropDown.sendKeys(parentname);
		};
		
		this.selectCurrencyType =  function(currency){
			utilityObj.browserWait(dcCurrencyDropDown, 'dcCurrencyDropDown');
			return dcCurrencyDropDown.sendKeys(currency);
		};
		
		this.clickAllAdvertiser =  function(){
			utilityObj.browserWait(dcAllAdvertiserRdBtn, 'dcAllAdvertiserRdBtn');
			return dcAllAdvertiserRdBtn.click();
		};
		
		this.clickSingleAdvertiser =  function(){
			utilityObj.browserWait(dcSingleAdvertiserRdBtn, 'dcSingleAdvertiserRdBtn');
			return dcSingleAdvertiserRdBtn.click();
		};
		
		this.selectSingleAdvertiser=function(singleadvertiser){
		utilityObj.browserWait(dcSingleAdvertiserDropDown, 'dcSingleAdvertiserDropDown');
			return dcSingleAdvertiserDropDown.sendKeys(singleadvertiser);
		};
		
		this.clickavailabilityAnalytics =  function(){
			utilityObj.browserWait(dcavailabilityAnalyticsRdBtn, 'dcavailabilityAnalyticsRdBtn');
			return dcavailabilityAnalyticsRdBtn.click();
		};
		
		this.clickEnableForCrossDeviceDuplication = function(){
			utilityObj.browserWait(dcEnableForCrossDeviceDuplicationCheckBtn, 'dcEnableForCrossDeviceDuplicationCheckBtn');
			return dcEnableForCrossDeviceDuplicationCheckBtn.click();
		};
		
		this.selectGraphProviderInCrossDevice = function(){
			utilityObj.browserWait(dcCrossDeviceGraphProviderDropDown, 'dcCrossDeviceGraphProviderDropDown');
			var rowElement = element.all(dcCrossDeviceGraphProviderDropDown);
			rowElement.then(function(elements){
			if(elements.length>0){	
				for(var i=0;i<elements.length;i++){
					var selectGraphProvider = element(by.xpath(".//select[@id='graphProviderSelector']/option["+ i +"]"));
					return selectGraphProvider.click();
				}
			}else{
				logger.info("Please Create at Least one Data Contracts to select Destination Contract Name");
				}
			});
		};
			
		this.clickGeneralTabNextButton=function(){
		return dcGeneralNextBtn.click();
		};
		
		
		this.clickOnDataTypeTab = function(){
			utilityObj.browserWait(dcDataTypeTab, 'dcDataTypeTab');
			return dcDataTypeTab.click();
		};
		
			
		this.selectCollectiontype = function(collection){
			utilityObj.browserWait(dcCollectiontypeDropdown, 'dcCollectiontypeDropdown');
			return dcCollectiontypeDropdown.sendKeys(collection);
		};
		
		this.selectPixelType = function(pixel){
			utilityObj.browserWait(dcPixelTypeDropdown, 'dcPixelTypeDropdown');
			return dcPixelTypeDropdown.sendKeys(pixel);
		};
		
		this.checkIFAInDeviceIdType = function(){
			utilityObj.browserWait(dcIfaCheckedBtn,'dcIfaCheckedBtn');
			return dcIfaCheckedBtn.click();
		};
		
		this.checkPlatformIdInDeviceIdType = function(){
			utilityObj.browserWait(dcPlatformIdCheckedBtn,'dcPlatformIdCheckedBtn');
			return dcPlatformIdCheckedBtn.click();
		};
		
		this.clickOnCategoriesCheckbutton = function(){
			return dcCategoriesCheckBtn.click();
		};
	
		this.clickOnFrame = function(){
			utilityObj.browserWait(dcClickOnFrame,'dcClickOnFrame');
			return	dcClickOnFrame.click();
		};

		this.ClickOnImportTaxonomy = function() {
			utilityObj.browserWait(dcImportTaxonomyBtn, 'dcImportTaxonomyBtn');
			return dcImportTaxonomyBtn.click();
		};
		
		this.clickOnBrowser = function() {
			utilityObj.browserWait(dcbrowserOnFrame, 'dcbrowserOnFrame', 20);
			return dcbrowserOnFrame.isPresent();
		};
		
		this.clickOnApplyButton = function() {
			utilityObj.browserWait(dcapplyButton, 'dcapplyButton');
			return dcapplyButton.click();
		};
	
		this.clickOnKeywordsCheckbutton = function(){
			return dcKeywordsCheckBtn.click();
		};
		
		
		this.clickOnKeysCheckbutton = function(){
			return dcKeysCheckBtn.click();
		};
		
		this.enterKeyInTextBox = function(key) {
			utilityObj.browserWait(dcKeyTxtBox, 'dcKeyTxtBox');
			return dcKeyTxtBox.sendKeys(key);
		};
		
		this.clickOnShowAdvancedOptions = function(){
			return dcShowAdvancedOptionsLink.click();
		};

		this.rateScheduleHeaderText = function(name){
			return (dcRateScheduleHeaderText.getText()).then(function(value){
			if(value== name){
					return true;
				}else{
					return false;
				};
			});
		};
		
		this.clickOnRateSchedulesTab = function(){
			utilityObj.browserWait(dcRateSchedulesTab, 'dcRateSchedulesTab',3);
			return dcRateSchedulesTab.click();
		};
		
		this.clickOnRateSchedulesTabLink = function(){
			utilityObj.browserWait(dcRateSchedulesTabLink, 'dcRateSchedulesTabLink');
			return dcRateSchedulesTabLink.click();
		};
		
		
		this.selectAudienceDataCost = function(dataCost){
			utilityObj.browserWait(dcAudineceDataCostDropDown, 'dcAudineceDataCostDropDown');
			return dcAudineceDataCostDropDown.sendKeys(dataCost);
		};
		
		this.selectStartDate = function(date){
			utilityObj.browserWait(dcEndDateCalander, 'dcEndDateCalander');
			dcStartDateCalander.clear();
			return dcStartDateCalander.sendKeys(date);
		};
		
		
		this.selectEndDate = function(date){
			utilityObj.browserWait(dcEndDateCalander, 'dcEndDateCalander');
			return dcEndDateCalander.sendKeys(date);
		};
		
		this.selectRateType = function(rate){
			utilityObj.browserWait(dcSelectRateTypeDropDown, 'dcSelectRateTypeDropDown');
			return dcSelectRateTypeDropDown.sendKeys(rate);
		};
		
		this.enterCPMValue = function(cost){
			utilityObj.browserWait(dcCPMTxtBox, 'dcCPMTxtBox');
			return dcCPMTxtBox.sendKeys(cost);
		};
		this.enterCPUUValue = function(cost){
			utilityObj.browserWait(dcCPUUTxtBox, 'dcCPUUTxtBox');
			return dcCPUUTxtBox.sendKeys(cost);
		};
		this.enterFlatfee = function(cost){
			utilityObj.browserWait(dcFlatRateTxtBox, 'dcFlatRateTxtBox');
			return dcFlatRateTxtBox.sendKeys(cost);
		};
		this.enterCostPerStamp = function(cost){
			utilityObj.browserWait(dcCostPerStampTxtBox, 'dcCostPerStampTxtBox');
			return dcCostPerStampTxtBox.sendKeys(cost);
		};
		this.enterRevenueShare = function(cost){
			utilityObj.browserWait(dcRevinueShareTxtBox, 'dcRevinueShareTxtBox');
			return dcRevinueShareTxtBox.sendKeys(cost);
		};
		
		this.clickonFlextagDataTypeFrame = function(){
			utilityObj.browserWait(dcclickonFlexagdataTypeFrame, 'dcclickonFlexagdataTypeFrame');
			return dcclickonFlexagdataTypeFrame.click();
		};
		
		this.clickOnFlexDataTypeAddNewKey = function(){
			utilityObj.browserWaitforseconds(3);
			utilityObj.browserWait(dcFlexDataTypeAddNewKeyBtn, 'dcFlexDataTypeAddNewKeyBtn');
			return dcFlexDataTypeAddNewKeyBtn.click();
		};
		
		this.clickOnFlexDataTypeJavaScript = function(){
			utilityObj.browserWait(FlexDataTypeJavaScriptTab, 'FlexDataTypeJavaScriptTab');
			return FlexDataTypeJavaScriptTab.click();
		};
			
		this.enterKeyValue = function(key){
			utilityObj.browserWait(dcFlexDataTypeKeyTxtbox, 'dcFlexDataTypeKeyTxtbox');
			return dcFlexDataTypeKeyTxtbox.sendKeys(key);
		};
		
		this.selectEventName = function(eventtype){
			utilityObj.browserWait(dcFlexDataTypeEventNameDropdDown, 'dcFlexDataTypeEventNameDropdDown');
			return dcFlexDataTypeEventNameDropdDown.sendKeys(eventtype);
		};
		
		this.selectTransformation = function(transformationtype){
			utilityObj.browserWait(dcFlexDataTypeTransformationDropDown, 'dcFlexDataTypeTransformationDropDown');
			return dcFlexDataTypeTransformationDropDown.sendKeys(transformationtype);
		};
		
		this.enterTransformationTypeValue = function(transformationValue){
			utilityObj.browserWait(dcFlexDataTypeTransformationTypeValueTxtBox, 'dcFlexDataTypeTransformationTypeValueTxtBox');
			return dcFlexDataTypeTransformationTypeValueTxtBox.sendKeys(transformationValue);
		};
		
		this.clickOnFlexDataTypeAddTaxonomy = function(){
			utilityObj.browserWait(dcFlexDataTypeAddTaxonomyBtn, 'dcFlexDataTypeAddTaxonomyBtn');
			return dcFlexDataTypeAddTaxonomyBtn.click();
		};
		
		this.clickOnFlexDataTypeAddParent = function(){
			utilityObj.browserWait(dcFlexDataTypeAddParentBtn, 'dcFlexDataTypeAddParentBtn');
			return dcFlexDataTypeAddParentBtn.click();
		};
		
		this.enterInFlexDataTypeParentTxtBox = function(){
			utilityObj.browserWait(FlexDataTypeClickOnParentBtnandOpenTxtBox,'FlexDataTypeClickOnParentBtnandOpenTxtBox');
			return FlexDataTypeClickOnParentBtnandOpenTxtBox.click();
		};
		
		this.selectFlexDataTypeParentTreeNode = function(){
			utilityObj.browserWait(dcFlexDataTypeSelectTreeNode, 'dcFlexDataTypeSelectTreeNode');
			return dcFlexDataTypeSelectTreeNode.click();
		};
		
		this.selectdcFlexDataTypeSelectUsersText = function(userText){
			utilityObj.browserWait(dcFlexDataTypeSelectUsersTextDropDown, 'dcFlexDataTypeSelectUsersTextDropDown');
			return dcFlexDataTypeSelectUsersTextDropDown.sendKeys(userText);
		}; 
		
		this.selectdcFlexDataTypeSelectContains = function(contains){
			utilityObj.browserWait(dcFlexDataTypeSelectContainsDropDown, 'dcFlexDataTypeSelectContainsDropDown');
			return dcFlexDataTypeSelectContainsDropDown.sendKeys(contains);
		}; 
		
		this.enterFlexDataTypeValueBox = function(value){
			utilityObj.browserWait(dcFlexDataTypeEnterValue, 'dcFlexDataTypeEnterValue');
			return dcFlexDataTypeEnterValue.sendKeys(value);
		};
		
			
		this.clickOnFlexDataTypeAddChildNode = function(){
			utilityObj.browserWait(dcFlexDataTypeAddChildNodeBtn, 'dcFlexDataTypeAddChildNodeBtn');
			return dcFlexDataTypeAddChildNodeBtn.click();
		};
		
		this.clickOnFlexDataTypeDeleteNode = function(){
			utilityObj.browserWait(dcFlexDataTypeDeleteNodeBtn, 'dcFlexDataTypeDeleteNodeBtn');
			return dcFlexDataTypeDeleteNodeBtn.click();
		};
		
		this.clickOnFlexDataTypeSitePersonalization = function(){
			utilityObj.browserWait(dcFlexDataTypeSitePersonalizationCheckBtn, 'dcFlexDataTypeSitePersonalizationCheckBtn');
			return dcFlexDataTypeSitePersonalizationCheckBtn.click();
		};
		
		this.clickOnFlexDataTypeAddPiggyBackBtn = function(){
			utilityObj.browserWait(dcFlexDataTypeAddPiggyBackBtn, 'dcFlexDataTypeAddPiggyBackBtn');
			return dcFlexDataTypeAddPiggyBackBtn.click();
		};
		
		this.clickOnFlexDataTypePaggyBackModal = function(){
			utilityObj.switchToFrame(dcFlexDataTypePaggyBackModal);
		};
		
		this.selectFlexDataTypepiggyBackType = function(customHTML){
			utilityObj.browserWait(dcFlexDataTypePiggybackType, 'dcFlexDataTypePiggybackType');
			dcFlexDataTypePiggybackType.click().then(function(){
				return element(by.linkText(customHTML)).click();
			});		
		};
		
		this.enterFlexDataTypePiggyBackName = function(piggyName){
		utilityObj.browserWait(dcFlexDataTypePiggyBackNameTxtBox, 'dcFlexDataTypePiggyBackNameTxtBox');
			return dcFlexDataTypePiggyBackNameTxtBox.sendKeys(piggyName);
		};
		
		this.enterFlexDataTypePiggyHtmlCodeInject = function(injectCode){
		utilityObj.browserWait(dcFlexDataTypeHTMLCodeInjectBoard, 'dcFlexDataTypeHTMLCodeInjectBoard');	
			return dcFlexDataTypeHTMLCodeInjectBoard.sendKeys(injectCode);
		};
		
		this.selectFlexDataTypeInsertPageEvent = function(insertPageEvent){
			utilityObj.browserWait(dcFlexDataTypeInsertPageEventDropDown, 'dcFlexDataTypeInsertPageEventDropDown');
			dcFlexDataTypeInsertPageEventDropDown.click().then(function(){
				utilityObj.browserWait(element(by.linkText(insertPageEvent)), 'insertPageEvent');		
			return element(by.linkText(insertPageEvent)).click();
			});
		};
		
		this.clickOnSaveButton = function(){
			utilityObj.browserWait(dcSaveBtn, 'dcSaveBtn');
			return dcSaveBtn.click();
		};
		
		
		this.checkFilterFlipper = function() {
			return dcClosedFlipper.isPresent();
		};
		
		this.clickFilterFlipper = function(){
			return dcClosedFlipper.isPresent().then(function(value){
				if(value){
					return dcClosedFlipper.click();
				}else{
					return false;
				}
			});
		};
		
		var clickOnClearFilterNew = function(){
			dcNewSelectMarket.isPresent().then(function(value)
					{if(value){
				browser.driver.executeScript('arguments[0].style.opacity=1',dcNewClearFilter.getWebElement());
				browser.waitForAngular();
				 browser.driver.executeScript('arguments[0].click()',dcNewClearFilter.getWebElement());
				 browser.waitForAngular();
				 }
				});			
			utilityObj.browserWaitforseconds(1);
			browser.waitForAngular();
			dcNewSelectDataProvider.isPresent().then(function(value)
					{if(value){browser.driver.executeScript('arguments[0].style.opacity=1',dcNewClearFilter.getWebElement());
					browser.waitForAngular();
				 browser.driver.executeScript('arguments[0].click()',dcNewClearFilter.getWebElement());
				 browser.waitForAngular();
				 }
				});
			dcNewAvailabilityFilter.isPresent().then(function(value){
				if(value){
						browser.driver.executeScript('arguments[0].style.opacity=1',dcNewClearFilter.getWebElement());
						browser.waitForAngular();
						 browser.driver.executeScript('arguments[0].click()',dcNewClearFilter.getWebElement());
						 browser.waitForAngular();
						 }
				});			
		};
			
		this.clickOnClearFilter = function(){
			clickOnClearFilterNew();
		};

		
		this.clickOnClearAllFilter = function(){
			clickOnClearFilterNew();
		};
		
		this.waitForSearchBox = function(){
			utilityObj.browserWait(dcSearchElement,'dcSearchElement');
		};

		this.clickOnSelectLink = function(){
			utilityObj.browserWait(dcDPSelectOption,'dcDPSelectOption');
			return dcDPSelectOption.click();
		};
		
		this.htmlTabClick=function(){
			utilityObj.browserWait(dcFlexDataTypeHtmlTab,'dcFlexDataTypeHtmlTab');
			return dcFlexDataTypeHtmlTab.click();
		};
		
		this.javaScriptTabClick=function(){
			utilityObj.browserWait(dcFlexDataTypeJavaScriptTab,'dcFlexDataTypeJavaScriptTab');
			return dcFlexDataTypeJavaScriptTab.click();
		};
			
		this.enterKey=function(row,value){
			var element=dcFliexDataTypekeyElement(row);
			utilityObj.browserWait(element,'Key'+row);
			return element.sendKeys(value);
		};
		
		this.enterEventName=function(row,value){
			var element=dcFliexDataTypeEventName(row);
			return element.sendKeys(value);
		};
		
		this.selectTransform=function(row,value){
			var element=dcFliexDataTypeTransform(row);
			element.click().then(function(){
			var dctransformElement=dcFliexDataTypeTransformValueSelect(row,value);
			return dctransformElement.click();
			});
		};
		
		this.eneterTransformValue=function(row,value){
			var element=dcFliexDataTypeTransformValue(row);
			return element.sendKeys(value);
		};
		
		this.eneterTransformMinRange=function(row,value){
			var element=dcFliexDataTypeTransformMinRange(row);
			utilityObj.browserWait(element,'TransformsMinValue');
			return element.sendKeys(value);
		};
		
		this.eneterTransformMaxRange=function(row,value){
			var element=dcFliexDataTypeTransformMaxRange(row);
			return element.sendKeys(value);
		};
		
		this.enterParenrtTaxonomy=function(parent){
			return dcFlexDataTypeClickOnParentBtnandOpenTxtBox.sendKeys(parent).then(function(){
				return dcFlexDataTypeClickOnParentBtnandOpenTxtBox.sendKeys(protractor.Key.ENTER);
			});	
		};
		
		this.clickChildTaxonomyButton=function(){
			utilityObj.browserWait(dcFlexDataTypeAddChildBtn,'dcFlexDataTypeAddChildBtn');
			return dcFlexDataTypeAddChildBtn.click();
		};
	
		this.clickOnChildNode=function(){
			return dcFlexDataTypeChildNode.click();
		};
		
		this.enterPiggyBackAdvertiser=function(advertiser){
			 utilityObj.browserWait(dcFlexDataTypePiggyAdvertiser,'dcFlexDataTypePiggyAdvertiser'); 
			 dcFlexDataTypePiggyAdvertiser.click();
			 utilityObj.browserWaitforseconds(2);
			 browser.waitForAngular();
			 dcFlexDataTypePiggyAdvertiser.sendKeys(advertiser);
			 return utilityObj.browserWaitforseconds(1);
		 };
	
		this.enterPiggyBackBeacon=function(beacon){
			 dcFlexDataTypePiggyBeacon.click().then(function(){
			 utilityObj.browserWaitforseconds(1);
			 browser.waitForAngular();
			 utilityObj.browserWait(dcFlexDataTypePiggyBeacon,'dcFlexDataTypePiggyBeacon');
			  dcFlexDataTypePiggyBeacon.sendKeys(beacon);
			  return utilityObj.browserWaitforseconds(1);
		   }); 
		 };
		
		 this.enterPiggyBackOrderId=function(orderid){
			utilityObj.browserWait(dcFlexDataTypePiggyOrderId,'dcFlexDataTypePiggyOrderId');
			return dcFlexDataTypePiggyOrderId.sendKeys(orderid);
		};
		
		this.enterPiggyBackOrderValue=function(orderValue){
			utilityObj.browserWait(dcFlexDataTypePiggyOrderValue,'dcFlexDataTypePiggyOrderValue');
			return dcFlexDataTypePiggyOrderValue.sendKeys(orderValue);
		};
		
		this.enterPiggyBackCurrency=function(currency){
			utilityObj.browserWait(dcFlexDataTypePiggyCurrency,'dcFlexDataTypePiggyCurrency');
			return dcFlexDataTypePiggyCurrency.sendKeys(currency);
		};
		
		this.clickSavePiggyBackButton=function(){
			utilityObj.browserWaitToEnabledElement(dcFlexDataTypePiggyBackSaveBtn,'dcFlexDataTypePiggyBackSaveBtn');
			return dcFlexDataTypePiggyBackSaveBtn.click();
		};
		
		this.enterPiggyBackPixelId=function(pixelId){
			utilityObj.browserWait(dcflexDataTypePiggyPixcelId,'dcflexDataTypePiggyPixcelId');
			return dcflexDataTypePiggyPixcelId.sendKeys(pixelId);
		};
		
		this.enterPiggyBackTaxonomyId=function(taxonomyId){
			utilityObj.browserWait(dcflexDataTypePiggyTaxonomyId,'dcflexDataTypePiggyTaxonomyId');
			return dcflexDataTypePiggyTaxonomyId.sendKeys(taxonomyId);
		};
		
		this.enterPiggyBackKeywords=function(keywords){
			utilityObj.browserWait(dcflexDataTypePiggykeywords,'dcflexDataTypePiggykeywords');
			return dcflexDataTypePiggykeywords.sendKeys(keywords);
		};
		
		this.enterPiggyBackKeywordPairs=function(keywordPairs){
			utilityObj.browserWait(dcflexDataTypePiggykeywordPairs,'dcflexDataTypePiggykeywordPairs');
			return dcflexDataTypePiggykeywordPairs.sendKeys(keywordPairs);
		};
		
		this.enterPiggyBackDPUserId=function(userId){
			utilityObj.browserWait(dcflexDataTypePiggyDPUserId,'dcflexDataTypePiggyDPUserId');
			return dcflexDataTypePiggyDPUserId.sendKeys(userId);
		};
		
		this.enterPiggyBackOutput=function(output){
			utilityObj.browserWait(dcflexDataTypePiggyOutPut,'dcflexDataTypePiggyOutPut');
			return dcflexDataTypePiggyOutPut.sendKeys(output);
		};
		
		this.enterPiggyBackSubdomain=function(subdomain){
			utilityObj.browserWait(dcflexDataTypePiggySubDomain,'dcflexDataTypePiggySubDomain');
			return dcflexDataTypePiggySubDomain.sendKeys(subdomain);
		};
		
		this.enterPiggyBackGroupTag=function(groupTag){
			utilityObj.browserWait(dcflexDataTypePiggyGroupTag,'dcflexDataTypePiggyGroupTag');
			return dcflexDataTypePiggyGroupTag.sendKeys(groupTag);
		};
		
		this.enterPiggyBackActivityTag=function(activityTag){
			utilityObj.browserWait(dcflexDataTypePiggyActivityTag,'dcflexDataTypePiggyActivityTag');
			return dcflexDataTypePiggyActivityTag.sendKeys(activityTag);
		};
		
		this.enterPiggyBackKeywordU=function(u){
			utilityObj.browserWait(dcflexDataTypePiggyU,'dcflexDataTypePiggyU');
			return dcflexDataTypePiggyU.sendKeys(u);
		};
		
		this.enterPiggyBackUValue=function(uValue){
			utilityObj.browserWait(dcflexDataTypePiggyUValue,'dcflexDataTypePiggyUValue');
			return dcflexDataTypePiggyUValue.sendKeys(uValue);
		};
		
		this.enterPiggyBackQuantity=function(quantity){
			utilityObj.browserWait(dcflexDataTypePiggyQuantity,'dcflexDataTypePiggyQuantity');
			return dcflexDataTypePiggyQuantity.sendKeys(quantity);
		};
		
		this.enterPiggyBackCost=function(cost){
			utilityObj.browserWait(dcflexDataTypePiggyCost,'dcflexDataTypePiggyCost');
			return dcflexDataTypePiggyCost.sendKeys(cost);
		};
		
		this.clickOnIconMenu=function(){
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
				dcNewListPageRecord.click().then(function(){
				utilityObj.browserWaitforseconds(1);
				 browser.driver.executeScript('arguments[0].click()',dcNewIconMenu.getWebElement());
				});
		};
		
		this.isIconMenuPresent=function(){  
			utilityObj.browserWaitforseconds(3);
			utilityObj.browserWait(dcNewListPageRecord,'dcNewListPageRecord');
			return dcNewListPageRecord.isPresent();
		};
		
		this.returnSearchTextField = function() {
				utilityObj.browserWait(dcNewsearchTxtBox,"dcNewsearchTxtBox");
				return dcNewsearchTxtBox;
		};
		
		this.clickOnEditLink=function(){
			utilityObj.browserWait(dcEditLink,'dcEditLink');
			dcEditLink.click();
			 browser.navigate().refresh();
			browser.waitForAngular();
		};
		
		this.clickOnViewLink=function(){
			utilityObj.browserWait(dcViewLink,'dcViewLink');
			return dcViewLink.click();
		};
		
		this.getExpectedDataContractNameViewPage = function() {
			utilityObj.browserWait(dcViewEditedtext, 'dcViewEditedtext');
			return dcViewEditedtext.getText();
		};
		
		this.clickOnDeleteLink=function(){
			utilityObj.browserWait(dcDeleteLink,'dcDeleteLink');
			return dcDeleteLink.click();
		};
		
		this.clickOnUndoDeleteLink=function()
		{
			utilityObj.browserWait(dcUndoDeleteLink,'dcUndoDeleteLink');
			return dcUndoDeleteLink.click();
		};
		
		this.checkFirstRecordCheckbox=function(){
				utilityObj.browserWait(dcNewListPageRecord,'dcNewListPageRecord');
				return dcNewListPageRecord.click();
		};
		
		this.clickOnObtainPixelsLink=function(){
			utilityObj.browserWait(dcObtainPixels,'dcObtainPixels');
			return dcObtainPixels.click();
		};
		
		this.getObtainPixelsName = function() {
			return dcNewObtainPixelHeaderText.getText();
		};
		
		this.clickCloseOnObtainPixels = function() {
			utilityObj.browserWait(dcNewConfCloseButton, 'dcNewConfCloseButton');
			return dcNewConfCloseButton.click();
		};
		
		this.clickOnViewAuditLink=function()
		{	utilityObj.browserWait(dcViewAuditLog,'dcViewAuditLog');
			return dcViewAuditLog.click();
		};
		this.isAuditLogSearchBoxPresent = function() {
			utilityObj.browserWait(dcSearchAuditLogTextBox, 'dcSearchAuditLogTextBox');
			return dcSearchAuditLogTextBox.isPresent();
		};
		
		this.searchAuditLogTextBox = function(fieldName) {
			dcSearchAuditLogTextBox.sendKeys(fieldName);
			dcSearchAuditLogTextBox.sendKeys(protractor.Key.ENTER);
		};
		
		this.returnAuditLogSearchElement = function() {
			return dcSearchAuditLogTextBox;
		};
		
		this.getFieldNameText = function() {
			utilityObj.browserWaitforseconds(2);
			utilityObj.browserWait(dcFieldNameLocation,'dcFieldNameLocation');
			return dcFieldNameLocation.getText();
		};
		
		this.clickOnShowDeletedLink=function()
		{	utilityObj.browserWait(dcShowDeletedLink,'dcShowDeletedLink');
			return dcShowDeletedLink.click();
		};
		
		this.clickOnHideDeletedLink=function()
		{	utilityObj.browserWait(dcHideDeletedLink,'dcHideDeletedLink');
			return dcHideDeletedLink.click();
		};
		
		this.checkActionDropdownPresent=function(){
			utilityObj.browserWait(dcNewActionDropDown, 'dcNewActionDropDown');
			return dcNewActionDropDown.isPresent();
		};
		
		this.clickDataContractNameColumn = function() {
			utilityObj.browserWait(dcNewNameColumnSorting,'dcNewNameColumnSorting');
			dcNewNameColumnSorting.click();
		};
		
		this.returnReverseDataContractName = function() {
				return dcNewReverseDataContractName.isPresent();	 
		};
		
		this.clickExpandDataContarctsLink=function(){
			utilityObj.browserWait(dcExpandAllRowsLink,'dcExpandAllRowsLink');
			return dcExpandAllRowsLink.click();
		};
		this.clickCollapseDataContarctsLink=function()
		{
			utilityObj.browserWait(dcCollapseAllRowsLink,'dcCollapseAllRowsLink');
			return dcCollapseAllRowsLink.click();
		};
		
		this.getExpandDatacontractConfText = function() {
			utilityObj.browserWait(dcExpandDataContractconf,'dcExpandDataContractconf');	
			return dcExpandDataContractconf.getText();
		};
		
		this.getExpandRateScedulesText = function() {
			utilityObj.browserWait(dcExpandRateSchedules,'dcExpandRateSchedules');	
			return dcExpandRateSchedules.getText();
		};
		
		this.getExpandTaxonomyText = function() {
			utilityObj.browserWait(dcExpandTaxonomy,'dcExpandTaxonomy');	
			return dcExpandTaxonomy.getText();
		};
		this.getExpandPixelOptionsText = function() {
			utilityObj.browserWait(dcExpandPixelOptions,'dcExpandPixelOptions');	
			return dcExpandPixelOptions.getText();
		};
		this.getExpandEventsText = function() {
			utilityObj.browserWait(dcExpandEvents,'dcExpandEvents');	
			return dcExpandEvents.getText();
		};
		
		this.clickExpandCloseButton=function() {   
			utilityObj.browserWait(dcExpandCloseButton,'dcExpandCloseButton');
			return dcExpandCloseButton.click();
		};
		
		this.isExpandCloseButtonDisplayed=function(){
			return dcExpandCloseButton.isDisplayed();
		};
		
		this.checkFilterFlipper = function() {
			return dcClosedFlipper.isPresent();
		};
		
		this.clickFilterFlipper = function() {
			return dcClosedFlipper.isPresent().then(function(value) {
				if (value) {
					return dcClosedFlipper.click();
				} else {
					return false;
				}
			});
		};
		
		this.clickSelectFilterLink = function() {
			utilityObj.browserWait(dcSelectFilterLink, 'dcSelectFilterLink');
			return dcSelectFilterLink.click();
		};
		this.clickSelectFilterLinkMarket = function() {
			utilityObj.browserWait(dcSelectOptionMarketFilter, 'dcSelectOptionMarketFilter');
			return dcSelectOptionMarketFilter.click();
		};
		
		this.clearFilterOption = function() {
			utilityObj.browserWait(clearfilterlink, 'clearfilterlink');
			return clearfilterlink.click();
		};
		
		this.enterMarketFilterName = function(marketName) {
			utilityObj.browserWait(dcSelectMarketFilter, 'dcSelectMarketFilter');
			dcSelectMarketFilter.clear();
			dcSelectMarketFilter.sendKeys(marketName);
		};
		
		this.clickOnMarketFilter = function(marketType) {
			return element(by.xpath("//div[1]/div[contains(text(),'" + marketType + "')]")).click();	
		};
		
		this.clearSearchBox = function() {
				 dcNewsearchTxtBox.clear();
				 return dcNewsearchTxtBox.sendKeys(protractor.Key.ENTER);
		};	
		
		this.getMarketNameFromColumn = function() {
                utilityObj.browserWait(dcNewMarketNameInList,'dcNewMarketNameInList');
			    utilityObj.browserWaitforseconds(2);
				return dcNewMarketNameInList.getText();
		};		

		this.getDataProviderNameFromColumn = function() {
		  utilityObj.browserWait(dcNewDataProviderNameInList,'dcNewDataProviderNameInList');
			    utilityObj.browserWaitforseconds(2);
				return dcNewDataProviderNameInList.getText();
		};
		
		this.clickNewFilterButton=function(){
			utilityObj.browserWait(dcNewFilterButton,'dcNewFilterButton');
			return dcNewFilterButton.click();
		};
		
		this.selectNewMarketTypeFilter=function(){
			utilityObj.browserWait(dcNewSelectFilterTypeMarket,'dcNewSelectFilterTypeMarket');
			return dcNewSelectFilterTypeMarket.click();
		};
		
		this.enterNewMarketNameFilter=function(marketName){
			utilityObj.browserWait(dcNewSelectMarket,'dcNewSelectMarket');
			dcNewSelectMarket.clear();
			return dcNewSelectMarket.sendKeys(marketName);
		};
		
		this.selectNewDataProviderTypeFilter=function(){
			utilityObj.browserWait(dcNewSelectFilterTypeDataProvider,'dcNewSelectFilterTypeDataProvider');
			return dcNewSelectFilterTypeDataProvider.click();
		};
		
		this.enterNewDataProviderNameFilter=function(dataprovider){
			utilityObj.browserWait(dcNewSelectDataProvider,'dcNewSelectDataProvider');
			dcNewSelectDataProvider.clear();
			dcNewSelectDataProvider.sendKeys(protractor.Key.ENTER);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			dcNewSelectDataProvider.sendKeys(dataprovider);
			utilityObj.browserWaitforseconds(5);
		};
		
		this.clickOnNewMarketFilter=function(marketName){
			utilityObj.browserWait(element(by.xpath("//a[text()='"+marketName+"']")),'filtertext');
			return element(by.xpath("//a[text()='"+marketName+"']")).click();
		};
		
		this.isLoginLinkPresent = function() {
			return dcLoginLink.isPresent();
		};
		
		this.clickLoginLink = function() {
			return dcLoginLink.click();
		};
		
		// Cross Device Duplication
		this.selectSameTypeOfGraphProvider = function(selectGraph,row){
			utilityObj.browserWaitforseconds(2);
			element(by.xpath("(.//select[@id='graphProviderSelector']/option[contains(text(),'"+selectGraph+"')])[position()="+row+"]")).click();
		};
		
		this.selectDiffGraphProvider = function(selectGraph,i){
			utilityObj.browserWaitforseconds(2);
			var graphElement = element(by.xpath("(//select[@id='graphProviderSelector']/option[contains(text(),'"+selectGraph+"')])[position()="+i+"]"));
			utilityObj.browserWait(graphElement, graphElement);
			graphElement.click();
		};
		
		this.returnGraph = function(selectGraph){
			var graphElement = element(by.xpath("//select[@id='preSelectedGraphProvderId']/option[contains(text(),'"+selectGraph+"')]"));
			utilityObj.browserWait(graphElement, 'graphElement');
			return graphElement.getText().then(function(value){
				return value;
			});
		};
		
		this.selectSameTypeOfDestinationDataContracts = function(selectDest,row){
			utilityObj.browserWaitforseconds(3);
			element(by.xpath("(.//select[@id='destinationContractSelector']/option[contains(text(),'"+selectDest+"')])[position()="+row+"]")).click();
		};
		
		this.returnDestinationDataContracts = function(dataContract){
			var destnationDC = element(by.xpath("//select[@id='preSelectedDestinationDataContractId']/option[contains(text(),'"+dataContract+"')]"));
			return destnationDC.getText().then(function(value){
				return value;
			});
		};
		
		this.selectDiffDataContract = function(selectDest,row){
			utilityObj.browserWaitforseconds(2);
			element(by.xpath("(.//select[@id='destinationContractSelector']/option[contains(text(),'"+selectDest+"')])[position()="+row+"]")).click();	
		};

		this.verifyErrMessage = function(errorMessage){
			var dcErrorMsg = element(by.xpath("(//div[@id='errorWrapper']/div/ul/li[contains(text(),'"+errorMessage+"')])[position()=1]"));
			utilityObj.browserWait(dcErrorMsg, 'dcErrorMsg');			
			return dcErrorMsg.getText().then(function(value){
				return	value;
			});
		};
			
		this.changeLastRowDestinationDC = function(selectDest){
			utilityObj.browserWaitforseconds(2);
			element(by.xpath("(.//select[@id='destinationContractSelector']/option[contains(text(),'"+selectDest+"')])[position()=2]")).click();
		};
		
		 this.clickOnGeneralTab = function(){
			utilityObj.browserWait(dcGeneralTab, 'dcGeneralTab');
			return dcGeneralTab.click();
		};
		
		this.clickRowAddBtn = function(){
			dcCrossDeviceRowsAddButton.click();	
		};
		
		var dcRemoveRows = element(by.xpath("//tr[@id='crossDeviceTableIndex0']/td/table/tbody/tr/td[2]/a[1]/img"));
		this.clickRemoveRowBtn = function(){
			utilityObj.browserWait(dcRemoveRows, 'dcRemoveRows');
			return dcRemoveRows.click();
		};
		
		var dcRemoveMapping = element(by.xpath("(//tr[@id='preSelectedCrossDeviceTable']/td/table/tbody/tr/td[2]/a[1]/img)[position()=1]"));
		this.clickRemoveRowBtnIfPresent = function(){
			return dcRemoveMapping.isPresent().then(function(value){
				if(value){
					return dcRemoveMapping.click();
				}
			});
		};
		
		this.clickRemoveMapping = function(){
			utilityObj.browserWait(dcRemoveMapping, 'dcRemoveMapping');
			return dcRemoveMapping.click();
		};
	};
	
	module.exports = new DataContractsPO();