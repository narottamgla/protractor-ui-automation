/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

var reportsPO = function(){
	var utility = require('../../lib/utility.js');
	var rcReportingTab = element(by.linkText('REPORTING'));
	var rcPivotReportTab = element(by.xpath('//a[contains(@href, "/pivotReport.htm")and not(contains(text(),"reporting"))and not(contains(text(),"Reporting"))]'));
	var rcInsightsReportTab = element(by.xpath('//a[contains(@href, "#/aiPage")or contains(@href,"insights-report") and not(contains(@href,"my-report"))]'));
	var myReportsTab = element(by.xpath('//a[contains(@href, "my-reports")]'));
    var rcPivotReortVelocityTab =element(by.xpath('//div/div[1]/div[2]/a[1]'));
	var rcLogoutLink = element(by.linkText('Logout'));	
	var loggerObj = require('../../lib/logger.js');
	var logger = loggerObj.loggers("reportsPO");
	
	var modalpopup = element(by.xpath("html/body/div[3]/div/div/div[5]/div"));
	
	this.clickonLogoutLink = function(){
		return rcLogoutLink.click();
	};
	this.clickOnReportingTab = function(){
		utility.browserWait(rcReportingTab,'rcReportingTab');
		return rcReportingTab.click();	
	};
	
	this.clickOnPivotReportTab = function(){
		utility.browserWait(rcPivotReportTab,'rcPivotReportTab');
		return rcPivotReportTab.click();	
	};
	
	this.clickOnPivotReportVelocityPageTab = function(){
		utility.browserWait(rcPivotReortVelocityTab,'rcPivotReortVelocityTab',3);
		return rcPivotReortVelocityTab.click();
	};
	
	this.checkOnPivotReportVelocityPagePresent = function(){
		return rcPivotReortVelocityTab.isPresent().then(function(value){
			if(value){
				return true;
			}else{
				return false;
				}
		});
	};
	
	this.clickOnInsightsReportTab = function(){
	  utility.browserWait(rcInsightsReportTab,'rcInsightsReportTab');
	  rcInsightsReportTab.click().then(function(){
		  modalpopup.isPresent().then(function(value){
				if(value){
					for(var i=1;i<=4;i++){
						utility.browserWaitforseconds(1);
						element(by.xpath("//div/div/div["+i+"]/div/div/button[3]")).click();
						}
					   	element(by.xpath("//div[3]/div/div/div[5]/div/div/button[4]")).click();
					   	utility.browserWaitforseconds(1);
					}
				else{
					logger.info("Modal Dialog is not available in Insights Reports tab.");
				};
			});
	  });
		
};
		
		 
	this.clickOnMyReportsTab = function(){
		utility.browserWait(myReportsTab,'myReportsTab',8);
		return myReportsTab.click();	
	};
	
	this.checkReportingtabPresent = function(){
		return rcReportingTab.isDisplayed();
		
	};
		
	this.checkPivotReportTabPresent = function() {
		utility.browserWait(rcPivotReportTab,'rcPivotReportTab');
		return rcPivotReportTab.isPresent();
	};
	
	this.checkInsightsReportTabPresent = function() {
	return rcInsightsReportTab.isPresent().then(function(value){
		if(value){
			return true;
			}else{
			return false;
			}
		});
	};
	
	this.checkMyReportsTabPresent = function() {
		return myReportsTab.isPresent().then(function(value){
			if(value){
				return true;
			}else{
				return false;
				}
			});
		};
};
module.exports = new reportsPO()