/**
 * Copyright (C) 2014 Turn, Inc. All Rights Reserved. Proprietary and
 * confidential.
 */

var AdvertiserPO = function() {

	var utility = require('../../lib/utility.js');

	var advertisersTabLink = element(by.linkText('ADVERTISERS')); // main menu link of advertisers.
	var advertisersLink = element(by.linkText('Advertisers')); // sub link of main menu of advertisers.
	var newAdvesrtisers = element(by.linkText('New Advertiser'));// Link for page which creates new advertiser.
	var market = element(by.id('marketId'));// id for market name from dropdown
	var advertiserNameTextbox = element(by.id('advertiserName'));// id for advertiserName textbox.
	var advSaveButton = element(by.name('next'));// name of save button.
	var advSearchText = element(by.id('search'));// Search textbox.
	var iconMenu = element(by.css('.icon-menu>img'));
	var advActionDropDown = element(by.css('.action-dropDown'));// location of action dropdown.
	var newAdvertiserActionLink = element(by.css('.options>li:nth-child(1)>a'));// Action dropdown Link for page which creates new advertiser.
	var advCancelButton = element(by.name('cancel'));// Cancel button of New Advertiser page.
	var advEditLink = element(by.linkText('Edit'));// Edit Advertiser link.
	var advViewLink = element(by.linkText('View'));// View Advertiser link.
	var advDeleteLink = element(by.linkText('Delete'));// Delete Advertiser link.
	var advEditMetaDataLink=element(by.linkText('Edit Metadata'));//Edit Meta Data link.
	var advEditMetaDataBrowseButton=element(by.name('importFile'));//Browse Meta data file button.
	var advEditMetaDataApplyButton=element(by.name('next'));//Edit Meta data apply button.
	var advEditMetaDataCancelButton=element(by.name('cancel'));//Edit Meta data cancel button.
	var advEditMetaDataFrame = element(by.id('TB_window'));//Edit Meta data frame.
	var advEditDataExportMappingLink=element(by.linkText('Export Mapping'));//Export mapping link.
	var advEditMetaEditButton=element(by.css('.default-button:nth-child(2)'));//Edit link of Meta Data.
	var advEditMetaDataIOPlusButton=element(by.xpath("//img[contains(@src,'/plus4.gif')]"));
	var advEditMetaDataPackagePlusButton=element(by.xpath("(//img[contains(@src,'/plus3.gif')])[position()=1]"));
	var advEditMetaDataLIPlusButton=element(by.xpath("(//img[contains(@src,'imgs/plus3.gif')])[position()=1]"));
	var advEditMetaDataCreativePlusButton=element(by.xpath("(//img[contains(@src,'/plus2.gif')])[position()=4]"));
	var advEditMetaDataPageTitle=element(by.css('div>.title-bar'));
	var advexpectedAdvertiserName = element(by.css('.name-text'));// Advertiser name location of View advertiser page.
	var advShowDeletedLink = element(by.linkText("Show Deleted"));// Show Deleted link of action dropdown.
	var advHideDeletedLink = element(by.linkText("Hide Deleted")); // Hide Deleted link of action dropdown.
	var advPageHeaderLocation = element(by.css('.titletext-wrapper>h2'));// Location of advertiser page header.
	var advFlipper = element(by.css('.adv-flipper.closed'));// Location to open filter.
	var advSelectFilterLink = element(by.css('.selectFilterLink'));// Select market filter.
	var advInputMarket = element(by.css('.dhx_combo_input'));// Text box of market filter.
	var advClearFilter = element(by.css('#clearFilters>a'));// Clear filter link.
	var advAdvertiserDetailsName = element(by.css('#insertionOrderDetailsWrapper>.titletext-wrapper>h2'));
	var auditLogLink = element(by.linkText('View Audit Log'));
	var searchAuditLogTextBox = element(by.name('auditLogSearch'));
	var fieldNameLocation = element(by.css('.listView tr:nth-child(2) td:nth-child(4)'));
	var marketNameInListItem= element(by.css('#records-list>tbody>tr>td:nth-of-type(5)'));
	var advNameColumn =element(by.linkText('Name'));
	var advReverseAdvLocation= element(by.css('.selected.reverse'));
	var advMediaProvider= element(by.id('mediaProviderId0'));
	var advMediaProviderId= element(by.id('id0'));
	
	var advNewsearchTxtBox=element(by.model("param"));
	var advNewActionDropDown=element(by.xpath("//button[@class='btn btn-dropdown dropdown-toggle pull-right ng-binding']"));
	var advActionNewAdvertiserLink=element(by.linkText('New Advertiser'));
	var advNewAdvertiserPlusBtn=element(by.xpath("//a[@class='tif-plus btn-action-plus pull-left' and not (contains(@style,'display: none;'))]"));
	var advNewPageHeaderLocation=element(by.xpath("//header[@class='title-text ng-binding']"));
	var advNewIconMenu=element(by.xpath("(//button[@type='button'])[position()=1]"));
	var advNewFilterButton=element(by.id('filter-dropdown'));
	var advNewSelectFilterType=element(by.css('[ng-click="addFilter(filterKey)"]'));
	var advNewSelectMarket=element(by.id("tapMarketName"));
	var advNewMarketNameInList=element(by.css(".smart-table-data-cell:nth-child(4)"));
	var advNewNameSorting=element(by.xpath("(//div[@class='relative ng-scope ng-binding'])[position()=2]"));
	var advNewUserbox=element(by.css('.user-wrapper'));
	var advNewElementList = element.all(by.xpath("//td[@ng-repeat='column in columns']/link-to-detail|//header[@class='title-text ng-binding']"));
	var advNewClearSearchIcon=element(by.css('.turn-clear'));
	var advNewConfirmDeleteAlert=element(by.css('[ng-click="deleteConfirm()"]'));
	var advNewClearFilter=element(by.css('[ng-click="removeFilter(filterKey)"]'));
	var advNewListPageRecord=element(by.xpath("(//td[@ng-repeat='column in columns'])[position()=1]"));
	var advNewEditMetaDataPopUp=element(by.id('content'));

	this.clickConfirmDeleteAlert=function()
	{   utility.browserWait(advNewConfirmDeleteAlert,'advNewConfirmDeleteAlert');
		return advNewConfirmDeleteAlert.click();
	};
	
	this.clickClearSearchIcon=function()
	{
		advNewClearSearchIcon.click();
	};
	 
	 this.AdvertiserelementList=function()
	 {
		 return advNewElementList;
	 };
	  
	this.newUserBox=function()
	{
		utility.browserWait(advNewUserbox,'advNewUserbox');
		return advNewUserbox.click();
	};
	
	this.clickOnNewMarketFilter=function(marketName)
	{
		utility.browserWait(element(by.xpath("//a[text()='"+marketName+"']")),'Filter element');
		element(by.xpath("//a[text()='"+marketName+"']")).click();
	};
	
	this.clickOnMarketFilter =function(marketType){
		return element(by.xpath("//div[1]/div[contains(text(),'"+marketType +"')]")).click();
		utility.browserWaitforseconds(3);
	};
	
	this.clickNewFilterButton=function()
	{
		utility.browserWait(advNewFilterButton,'advNewFilterButton');
		return advNewFilterButton.click();
	};
	
	this.selectNewMarketTypeFilter=function()
	{
		utility.browserWait(advNewSelectFilterType,'advNewSelectFilterType');
		return advNewSelectFilterType.click();
	};
	
	this.enterNewMarketNameFilter=function(marketName)
	{
		utility.browserWait(advNewSelectMarket,'advNewSelectMarket');
		advNewSelectMarket.sendKeys(protractor.Key.ENTER);
		browser.waitForAngular();
		return advNewSelectMarket.sendKeys(marketName);
	};
	
	this.selectMediaProvider = function(mediaProvider){
		utility.browserWait(advMediaProvider,'advMediaProvider');
		return advMediaProvider.sendKeys(mediaProvider);
	};
	
	this.enterMediaProviderId = function(mediaProviderId){
		advMediaProvider.sendKeys(protractor.Key.TAB);
		utility.browserWait(advMediaProviderId,'advMediaProviderId');
		return advMediaProviderId.sendKeys(mediaProviderId);
	};
	
	this.clickAdvertiserMenuLink = function() {
		utility.browserWait(advertisersTabLink, 'advertisersTabLink');
		return advertisersTabLink.click();
	};

	this.clickAdvertiserSubMenuLink = function() {
		utility.browserWait(advertisersLink, 'advertisersLink');
		return advertisersLink.click();
	};
	
	this.getAuditLogPageHeader=function(){
		utility.browserWait(advPageHeaderLocation,"advPageHeaderLocation");
		return advPageHeaderLocation.getText();
    };

	this.getPageHeader = function() {
			utility.browserWait(advNewPageHeaderLocation,"advNewPageHeaderLocation");
			return advNewPageHeaderLocation.getText();
	};
	
	this.clickActionDropDown = function() {
		utility.browserWait(advNewActionDropDown,"advNewActionDropDown");
		return advNewActionDropDown.click();		
	};
	
	this.clickNewAdvertiserActionMenu = function() {
		utility.browserWait(advActionNewAdvertiserLink,"advActionNewAdvertiserLink");
		return advActionNewAdvertiserLink.click();		
	};
	
	this.clickNewAdvertiserCancleButton = function() {
		return advCancelButton.click();
	};
	
	this.clickNewAdvertiserLink = function() {
		utility.browserWait(advNewAdvertiserPlusBtn,"advNewAdvertiserPlusBtn");
		return advNewAdvertiserPlusBtn.click();
	};
	
	this.selectMarket = function(marketName) {
		utility.browserWait(market,'market');
		return market.sendKeys(marketName);
	};
	
	this.enterAdvertiserName = function(advertiserName) {
		advertiserNameTextbox.clear();
		return advertiserNameTextbox.sendKeys(advertiserName);
	};
	
	this.clickSaveButton = function() {
		return advSaveButton.click();
	};
	
	this.clickIconMenu = function() {	
		 utility.browserWaitforseconds(2);
		 advNewListPageRecord.click().then(function(){
		 browser.waitForAngular();
		 utility.browserWaitforseconds(2);
		 browser.executeScript('arguments[0].click()',advNewIconMenu.getWebElement());	
		 utility.browserWaitforseconds(2);
		});
	
	};
	
	this.clickViewLink = function() {
		utility.browserWait(advViewLink,'advViewLink');
		return advViewLink.click();
	};

	this.clickEditLink = function() {
		utility.browserWait(advEditLink,'advertiseredit');
		return advEditLink.click();
	};
	
	this.getAdvertiserNameViewPage = function() {
		utility.browserWait(advAdvertiserDetailsName,'advAdvertiserDetailsName');
		return advexpectedAdvertiserName.getText();
	};
	this.clickDeleteLink = function() {
		utility.browserWait(advDeleteLink,'advDeleteLink');
		return advDeleteLink.click();
	};
	
	this.clickEditMetaDataLink=function(){
		utility.browserWait(advEditMetaDataLink,'advEditMetaDataLink');
		return advEditMetaDataLink.click();
	};
	
	this.returnMetaDatabrowsebutton=function()
	{   utility.browserWait(advEditMetaDataBrowseButton,'advEditMetaDataBrowseButton');
		return advEditMetaDataBrowseButton;
	};
	
	this.clickMetaDataApplyButton=function()
	{   utility.browserWait(advEditMetaDataApplyButton,'advEditMetaDataApplyButton');
		return advEditMetaDataApplyButton.click();;
	};
	
	
	this.clickEditDataExportMappingLink=function(){
		utility.browserWait(advEditDataExportMappingLink,'advEditDataExportMappingLink');
    	return advEditDataExportMappingLink.click();
	};
	
	this.isEditMetaDataEditButtonPresent=function()
	{
		return advEditMetaEditButton.isPresent();
	};
	
	this.clickEditMetaDataCancelButton=function()
	{	utility.browserWait(advEditMetaDataCancelButton,'advEditMetaDataCancelButton');
		return advEditMetaDataCancelButton.click();
	};
	
	this.clickIOPlusButton=function(){
		utility.browserWait(advEditMetaDataIOPlusButton,'advEditMetaDataIOPlusButton');
		return advEditMetaDataIOPlusButton.click();
	};
	
	this.clickPackagePlusButton=function(){
		utility.browserWait(advEditMetaDataPackagePlusButton,'advEditMetaDataPackagePlusButton');
		return advEditMetaDataPackagePlusButton.click();
	};
	
	this.clickEditMetaDataLIPlusButton =function(){
		utility.browserWait(advEditMetaDataLIPlusButton,'advEditMetaDataLIPlusButton');
	    return advEditMetaDataLIPlusButton.click();
	};
	
	this.clickEditMetaDataCreativePlusButton =function(){
		utility.browserWait(advEditMetaDataCreativePlusButton,'advEditMetaDataCreativePlusButton');
	    return advEditMetaDataCreativePlusButton.click();
	};
	
	this.getEditMetaDataPageTitleText=function(){  
		utility.browserWait(advEditMetaDataPageTitle,'advEditMetaDataPageTitle');
		return advEditMetaDataPageTitle.getText();
	};

	this.getEditMetaDataText=function(i){		
		return element(by.xpath("//div[@id='treeBox']/div/table/tbody/tr["+(i+1)+"]/td[2]/table/tbody/tr[1]/td[4]/span")).getText();	
	};
	
	this.getEditMetaDataSubText=function(i){	
	   return element(by.xpath("//div[@id='treeBox']/div/table/tbody/tr["+(i+1)+"]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span")).getText();	
	};
	this.returnSearchElement = function() {
		utility.browserWait(advNewsearchTxtBox,"advNewsearchTxtBox");
		return advNewsearchTxtBox;
	};
	
	this.clearSearchBox = function() {
		 advNewsearchTxtBox.clear();
		 return advNewsearchTxtBox.sendKeys(protractor.Key.ENTER);
	};
	
	this.checkActionDropdownPresent = function() {
		return advActionDropDown.isPresent();
	};
	
	this.clickShowDeletedLink = function() {
		utility.browserWait(advShowDeletedLink,'advShowDeletedLink');
		return advShowDeletedLink.click();
	};
		
	this.iconMenuPresent = function() {
		return iconMenu.isPresent();
	};
	
	this.clickAuditLogLink = function() {
		utility.browserWait(auditLogLink,'auditLogLink');
		return auditLogLink.click();
	};
	
	this.waitForHeaderText = function() {
		return utility.browserWait(pageHeaderText,'pageHeaderText');
	};
	
	this.waitForAuditLogSearchBox = function() {
		browser.waitForAngular();
		utility.browserWaitforseconds(3);
		return searchAuditLogTextBox.isPresent();
	};
	
	this.returnAuditLogSearchElement = function(searchField){
		searchAuditLogTextBox.sendKeys(searchField);
		searchAuditLogTextBox.sendKeys(protractor.Key.ENTER);
	};
	
	this.getFieldNameText = function(){
		return fieldNameLocation.getText();
	};
	
	this.clickHideDeletedLink = function(){
		return advHideDeletedLink.click();
	};
		
	this.checkFilterFlipper = function() {
		return advFlipper.isPresent();
	};
	
	this.clickFilterFlipper = function(){
		return advFlipper.click();
	};
	
	this.clickOnClearFilter = function(){
			return advNewSelectMarket.isPresent().then(function(value){
				if(value){
					browser.driver.executeScript('arguments[0].style.opacity="1"',advNewClearFilter.getWebElement()).then(function(){
					return browser.driver.executeScript("arguments[0].click()",advNewClearFilter.getWebElement());
					});
				}
				else{
					return false;
				}
			});
	};
	
	this.clickOnSelectLink = function(){
		utility.browserWait(advSelectFilterLink,'advSelectFilterLink');
		return advSelectFilterLink.click();
	};
	
	this.enterMarketName = function(marketName){
		utility.browserWait(advInputMarket,'advInputMarket');
		return advInputMarket.sendKeys(marketName);
	};
	this.getMarketNameFromColumn = function(){
			utility.browserWait(advNewMarketNameInList,'advNewMarketNameInList');
		    utility.browserWaitforseconds(2);
			return advNewMarketNameInList.getText();
	};
	
	this.clickAdvertiserNameColumn = function(){
			advNewNameSorting.click();
	};
	
	this.returnReverseAdvName =	function(){
		return advReverseAdvLocation.isPresent();
	};

};

module.exports = new AdvertiserPO();
