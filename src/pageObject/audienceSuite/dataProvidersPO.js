/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

var dataProvidersPO = function(){
	var utility = require('../../lib/utility.js');
	
	var dataTabLink = element(by.linkText('DATA')); // Main menu link.
	var dataProvidersLink = element(by.linkText('Data Providers')); // sub link of main menu of Data tab.
	var newDataProviderLink = element(by.linkText('New Data Provider')); // link of creating New Data Provider.
	var newDataProviderActionLink = element(by.css('.options>li:nth-child(1)>a'));
	var marketDropdown = element(by.id('marketId'));// id for market name from dropdown.
	var standardRadioButton = element(by.xpath("//input[@type='radio' and @value='1']")); //Standard Data Provider Radio Button.
	var flextagRadioButton = element(by.xpath("//input[@type='radio' and @value='2']")); //Flaxtag Data Provider Radio Button
	var parentDataProvider = element(by.id("parentDataProviderId"));
	var dataProviderName = element(by.id("dataProviderName"));
	var topLavelDomains = element(by.id("dataProviderTLDs"));
	var javascriptLink = element(by.css("#pageEventContainer div.expressionTypeTabs div.javascript"));
	var HTMLLink = element(by.css("#pageEventContainer div.expressionTypeTabs div.html"));
	var addManualEvent= element(by.xpath("//input[@value='Add Manual Event']"));
	var flextagViewerName = element(by.name("pageEventKey"));
	var htmlElementType = element(by.name("element"));
	var htmlIdentifier = element(by.name("identifier"));
	var htmlExpression = element(by.name("expression"));
	var htmlAttribute = element(by.name("attribute"));
	var variable = element(by.name("expression"));
	var eventCaptureLink = element(by.linkText("2.   Event Capture"));
	var saveButton = element(by.xpath("//input[@type='button' and @name='save']"));
	var searchTextField = element(by.id("search"));
	var editLink = element(by.linkText("Edit"));
	var iconMenu = element(by.css(".icon-menu>img"));
	var dataproviderPageHeaderLocation = element(by.css(".title-text.ng-binding,.titletext-wrapper>h2"));
	var deleteLink = element(by.linkText("Delete"));
	var dataProviderActionDropDown = element(by.css(".action-dropDown"));
	var showDeletedLink = element(by.linkText("Show Deleted"));
	var hideDeletedLink = element(by.linkText("Hide Deleted"));
	var viewLink = element(by.linkText("View"));
	var viewEditedText = element(by.className("name-text"));
	var closedFlipper = element(by.css(".adv-flipper.closed"));
	var clearFilterLink = element(by.linkText("Clear all filters"));
	var selectOption = element(by.linkText("Select..."));
	var selectMarket = element(by.css(".dhx_combo_input"));
	var selectMarketFileter = element(by.css(".dhx_combo_list.new-filter-dropdown-list>div[style='width: 100%; overflow: hidden; padding-right: 10px;']"));
	var auditLogLink = element(by.linkText('View Audit Log'));
	var searchAuditLogTextBox = element(by.name('auditLogSearch'));
	var fieldNameLocation = element(by.css('.listView tr:nth-child(2) td:nth-child(4)'));
	var marketNameInListItem = element(by.css('#records-list>tbody>tr>td:nth-of-type(6)'));
    var marketNameInListItemNew = element(by.css('td:nth-child(4)'));
	var dataProviderNameColumn =element(by.linkText('Name'));
	var reverseDataProviderLocation= element(by.css('.selected.reverse'));
	var logoBrowseButton = element(by.id('dataProviderImageFileName'));
	var notesTextArea = element(by.id('notes'));
	var obtainPixelLink = element(by.partialLinkText('Obtain Pixel'));
	var obtainPixelHeader = element(by.xpath('//table[@class="titleBar2"]/tbody/tr/td/h3/div'));
	var obtainPixelCloseButton = element(by.xpath('//*[@class="titleBar2"]/tbody/tr/td/a/img'));
	
	//New
	var dpNewSearchTextField=element(by.model('param'));
	var dpNewDPPlusBtn=element(by.xpath("//a[@class='tif-plus btn-action-plus pull-left' and not (contains(@style,'display: none;'))]"));
	var dpNewListPageRecord=element(by.xpath("(//td[@ng-repeat='column in columns'])[position()=1]"));
	var dpNewElementList = element.all(by.xpath("//a[@class='click-column-link ng-binding']|//header[@class='title-text ng-binding']"));
	var dpNewsearchTxtBox=element(by.model("param"));
	var dpNewIconMenu=element(by.xpath("(//button[@type='button'])[position()=1]"));
	var dpNewConfirmDeleteAlert=element(by.css('[ng-click="deleteConfirm()"]'));
	var dpNewActionDropDown=element(by.xpath("//button[@class='btn btn-dropdown dropdown-toggle pull-right ng-binding']"));
	var dpNewNameColumnSorting=element(by.xpath("(//div[@class='relative ng-scope ng-binding'])[position()=2]"));
	var dpNewReverseDataContractName=element(by.xpath("//th[@class='smart-table-header-cell medium sort-descent']"));
	var dpNewConfCloseButton=element(by.css('[ng-click="close()"]'));
	var dpNewObtainPixelHeaderText= element(by.css('.modal-header>h4:nth-of-type(2)'));
	var dpNewSelectMarket=element(by.id("tapMarketName"));
	var dpNewFilterButton=element(by.id('filter-dropdown'));
	var dpNewSelectFilterType=element(by.css('li:nth-of-type(1)>[ng-click="addFilter(filterKey)"]'));
	var dpNewCancelIcon=element(by.css('[ng-click="clear()"]'));
	var dpNewClearFilter=element(by.css('[ng-click="removeFilter(filterKey)"]'));
	
	this.clickCancelSearchIcon=function(){
		utility.browserWait(dpNewCancelIcon,'dpNewCancelIcon');
		return dpNewCancelIcon.click();
	};
	
	this.clickOnNewFilter=function(marketType) {
			utility.browserWait(element(by.xpath("//a[text()='"+marketType+"']")),'Market element filter');
			element(by.xpath("//a[text()='"+marketType+"']")).click();
			return utility.browserWaitforseconds(4);
	};
	
	this.enterNewMarketNameFilter=function(marketName) {
		utility.browserWait(dpNewSelectMarket,'dpNewSelectMarket');
		return dpNewSelectMarket.sendKeys(marketName);
	};

	this.clickNewFilterButton=function() {
		utility.browserWait(dpNewFilterButton,'dpNewFilterButton');
		return dpNewFilterButton.click();
	};
	
	this.selectNewMarketTypeFilter=function() {
		utility.browserWait(dpNewSelectFilterType,'dpNewSelectFilterType');
		return dpNewSelectFilterType.click();
	};
	
	this.isMarketFilterPresent=function(){
		return dpNewSelectMarket.isPresent();
	};
	
	this.getAuditLogPageHeader=function(){
		utility.browserWait(dataproviderPageHeaderLocation,"dataproviderPageHeaderLocation");
		return dataproviderPageHeaderLocation.getText();
    };
    
	this.dataProvidersElementList=function(){
		 return dpNewElementList;
	 };
	 
	 this.clickConfirmDeleteAlert=function(){  
		 utility.browserWait(dpNewConfirmDeleteAlert,'dpNewConfirmDeleteAlert');
			return dpNewConfirmDeleteAlert.click();
		};
	
	this.getMarketNameFromColumn = function(){
	utility.browserWait(marketNameInListItemNew,'marketNameInListItemNew');
		return marketNameInListItemNew.getText();
	};
	
	this.clickDataMenuLink = function(){
		utility.browserWait(dataTabLink,'dataTabLink');
		return dataTabLink.click();		
	};
	
	this.clickDataProviderSubLink = function(){
		utility.browserWait(dataProvidersLink,'dataProvidersLink');
		return dataProvidersLink.click();
	};
	
	this.isNewDataProviderLinkPresent = function(){
		return dpNewDPPlusBtn.isPresent();
	};
	
	this.clickOnNewDataProviderLink = function(){
			utility.browserWait(dpNewDPPlusBtn, 'dpNewDPPlusBtn');
			dpNewDPPlusBtn.click();
	};
	
	this.clickNewDataProviderActionLink = function(){
			utility.browserWait(newDataProviderLink, 'newDataProviderLink');
			newDataProviderLink.click();
	};
	
	this.selectMarket = function(marketName){
		utility.browserWaitforseconds(5);
		utility.browserWait(marketDropdown,'marketDropdown');
		return marketDropdown.sendKeys(marketName);
	};
	
	this.selectDataType = function(dataType){
		return standardRadioButton.isPresent().then(function(){
			if(dataType == "Standard"){
				return standardRadioButton.click();
			} else if(dataType == "Flextag"){
				return flextagRadioButton.click();
			}
		});
	};
	
	this.selectParentDataProvider = function(parentdataprovider){
		utility.browserWait(parentDataProvider,'parentDataProvider');
		return parentDataProvider.sendKeys(parentdataprovider);
	};
	
	this.enterDataProviderName = function(name){
		utility.browserWait(dataProviderName,'dataProviderName');
		dataProviderName.clear();
		return dataProviderName.sendKeys(name);
	};
	
	this.enterTopLevelDomains = function(topLavelDomainsName){
		return topLavelDomains.sendKeys(topLavelDomainsName);
	};
	
	this.clickEventCaptureTab = function(){
		return eventCaptureLink.click();
	};
	
	this.selectFlextagViewerEventsTab = function(tab){
		utility.browserWait(javascriptLink,'javascriptLink');
		javascriptLink.isPresent().then(function(){
			if (tab == "Javascript") {
				javascriptLink.click();
			} else if (tab == "HTML") {
				HTMLLink.click();
			}
		});		
	};
	
	this.clickAddManualEvent = function(){
		utility.browserWait(addManualEvent,'addManualEvent');
		return addManualEvent.click();
	};
	
	this.enterViewerName = function(flextagViewerEventName){
		return flextagViewerName.sendKeys(flextagViewerEventName);
	};
	
	this.enterVariable = function(variableName){
		return variable.sendKeys(variableName);
	};
	
	this.selectHtmlElementType = function(type){
		return htmlElementType.sendKeys(type);
	};
	
	this.selectHtmlIdentifier = function(identifier){
		return htmlIdentifier.sendKeys(identifier);
	};
	
	this.enterHtmlExpression = function(expression){
		return htmlExpression.sendKeys(expression);
	};
	
	this.selectHtmlAttribute = function(attribute){
		return htmlAttribute.sendKeys(attribute);
	};
	
	this.clickSave = function(){
		utility.browserWait(saveButton,'saveButton');
		return saveButton.isEnabled().then(function(){
			saveButton.click();
		});
	};
	
	this.clickIconMenu = function(){
			utility.browserWaitforseconds(3);
			dpNewListPageRecord.click();
			utility.browserWaitforseconds(3);
			browser.driver.executeScript('arguments[0].click()',dpNewIconMenu.getWebElement());
	};
	
	this.isIconMenuPresent = function(){
			utility.browserWaitforseconds(3);
			utility.browserWait(dpNewListPageRecord,'dpNewListPageRecord');
			return dpNewListPageRecord.isPresent();
	};
	
	this.clickEditLink = function(){
		utility.browserWait(editLink,'editLink');
		return editLink.click();
	};
	
	this.clickDeleteLink = function(){
		utility.browserWait(deleteLink,'deleteLink');
		return deleteLink.click();
	};
	
	this.clickActionDropDown = function(){
			utility.browserWait(dpNewActionDropDown,"dpNewActionDropDown");
			return dpNewActionDropDown.click();
	};
	
	this.isActionDropDownPresent = function(){
			utility.browserWait(dpNewActionDropDown, 'dpNewActionDropDown');
			return dpNewActionDropDown.isPresent();
	};
	
	this.clickHideDeleted = function(){
		utility.browserWait(hideDeletedLink,'hideDeletedLink');
		return hideDeletedLink.click();
	};
	
	this.clickShowDeleted = function(){
		utility.browserWait(showDeletedLink,'showDeletedLink');
		return showDeletedLink.click();
	};
	
	this.clickViewLink = function(){
		utility.browserWait(viewLink,'viewLink');
		return viewLink.click();
	};
	
	this.getExpectedDataProviderName = function(){
		utility.browserWait(viewEditedText,'viewEditedText');
		return viewEditedText.getText();
	};
	
	this.getDataProvideraPageHeaderText = function(){
		utility.browserWaitforseconds(2);
		utility.browserWait(dataproviderPageHeaderLocation,'dataproviderPageHeaderLocation');
		return dataproviderPageHeaderLocation.getText();
	};
	
	this.checkFilterFlipper = function() {
		return closedFlipper.isPresent();
	};
	
	this.clickFilterFlipper = function(){
		return closedFlipper.isPresent().then(function(value){
			if(value){
				return closedFlipper.click();
			}else{
				return false;
			}
		});
	};

	this.clickOnClearFilter = function(){
			browser.driver.executeScript('arguments[0].style.opacity=1',dpNewClearFilter.getWebElement());
			browser.driver.executeScript('arguments[0].click()',dpNewClearFilter.getWebElement());
	};
	
	this.clickOnSelectLink = function(){
		utility.browserWait(selectOption,'selectOption');
		return selectOption.click();
	};
	
	this.enterMarketName = function(marketName){
		utility.browserWait(selectMarket,'selectMarket');
		return selectMarket.sendKeys(marketName);
	};
	
	this.clickOnMarketFilter = function(marketType){
		return element(by.xpath("//div[1]/div[contains(text(),'"+marketType +"')]")).click();
	};
	
	this.returnSearchElement = function(){
			utility.browserWaitforseconds(2);
			utility.browserWait(dpNewsearchTxtBox,"dpNewsearchTxtBox");
			return dpNewsearchTxtBox;
	};
	
	this.clickAuditLogLink = function() {
		utility.browserWait(auditLogLink,'auditLogLink');
		return auditLogLink.click();
	};
	
	this.isAuditLogSearchBoxPresent = function() {
		utility.browserWait(searchAuditLogTextBox,'searchAuditLogTextBox');
		return searchAuditLogTextBox.isPresent();
	};
	
	this.getFieldNameText = function(){
		utility.browserWait(fieldNameLocation,'fieldNameLocation');
		return fieldNameLocation.getText();
	};
	
	this.searchAuditLog = function(name){
		searchAuditLogTextBox.sendKeys(name);
		return searchAuditLogTextBox.sendKeys(protractor.Key.ENTER);
	};
	
	this.clearSearchBox = function(){
			utility.browserWait(dpNewSearchTextField,'dpNewSearchTextField');
			dpNewSearchTextField.clear();
			return dpNewSearchTextField.sendKeys(protractor.Key.ENTER);
	};
	
	this.returnAuditLogSearchElement = function(){
		return searchAuditLogTextBox;
	};
	
	this.clickDataProviderNameColumn = function(){
			utility.browserWait(dpNewNameColumnSorting,'dpNewNameColumnSorting');
			dpNewNameColumnSorting.click();
	};
	
	this.returnReverseDataProviderName = function(){
			return dpNewReverseDataContractName.isPresent();	 
	};
	
	this.returnLogoBrowserButton = function(){
		return logoBrowseButton;
	};
	
	this.enterNotes = function(notes){
		return notesTextArea.sendKeys(notes);
	};
	
	this.clickObtainPixel = function(){
		return obtainPixelLink.click();
	};
	
	this.getObtainPixelHeaderText = function(){
			return dpNewObtainPixelHeaderText.getText();
	};
	
	this.clickObtainPixelCloseButton =function(){
			utility.browserWait(dpNewConfCloseButton, 'dpNewConfCloseButton');
			return dpNewConfCloseButton.click();
	};
};

module.exports = new dataProvidersPO();