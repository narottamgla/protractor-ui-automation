var cookieSpaceMediaProvidersPO = function(){
	var utility = require('../../lib/utility.js');
	
	var csmpMediaMenuLink = element(by.linkText('MEDIA')); // main menu link of Media tab.
    var csmpMediaProvidersSubLink= element(by.linkText('Cookie Space Media Providers')); // sub link of main menu of Cookie Space Media Provider.
    var csmpNewMediaProviderPlusLink= element(by.css(".tif-plus.btn-action-plus.pull-left")); // Plus Button to create Cookie Space Media Provider.

    // Link for page which creates new cookie space media provider.
    var csmpCookieSpaceDropDown= element(by.id('cookieSpaceId'));//Cookie Space dropdown.
    var csmpMediaProviderNameTxtBox= element(by.id('mediaProviderName'));//Media Provider textbox.
    var csmpLogoBtn=element(by.id('mediaProviderImageFileName'));// Logo Button.
    var csmpNotesTxtarea=element(by.id('notes'));	// Notes Text Area.
    var csmpSaveButton=element(by.name('next'));// Save Button.
    var csmpCancelButton=element(by.name('cancel'));// Cancel Button
    
    
    var csmpActionDropdown=element(by.css('.btn.btn-dropdown.dropdown-toggle.pull-right.ng-binding'));// location of action dropdown
    var csmpNewMediaProviderLink=element(by.linkText('New Media Provider'));// Action dropdown Link for page which creates cookie space media provider
    var csmpDeleteLink=element(by.linkText('Delete')); //Delete Link
    var csmpShowDeletedLink=element(by.linkText('Show Deleted')); //Show Deleted link of Action DrowDown.
    var csmpHideDeletedLink=element(by.linkText('Hide Deleted')); //Hide Deleted link of Action DropDown.
    var csmpSearchTxtBox=element(by.model('param')); //Search Textbox.
    var csmpPageHeaderText=element(by.css('.title-text.ng-binding,.titletext-wrapper>h2'));// Location of page header text.
    var csmpEditLink=element(by.linkText('Edit'));//Edit link.
    var csmpAuditLoglink=element(by.linkText('View Audit Log'));//View Audit Log Link.
    var csmpViewConfigurationLink=element(by.linkText('View Configuration'));//View Configuration Link.
    var csmpSearchAuditLogTextBox = element(by.name('auditLogSearch'));//Audit Log Search Textbox.
    var fieldNameLocation = element(by.css('.listView tr:nth-child(2) td:nth-child(4)'));
    var csmpNewIconMenu=element(by.xpath("(//button[@type='button'])[position()=1]")); //Grid Icon Menu.
    var csmpNewListPageRecord=element(by.xpath("//span[@class='ng-binding ng-scope']"));
    var csmpNewElementList = element.all(by.xpath("//td[@ng-repeat='column in columns']/span|//header[@class='title-text ng-binding']"));
    var csmpNewConfirmDeleteAlert=element(by.css('[ng-click="deleteConfirm()"]'));
    var csmpFieldNameLocation = element(by.css('.listView tr:nth-child(2) td:nth-child(4)'));
    var csmpObtainPixelLink=element(by.linkText('Obtain Pixel'));//obtain pixel link.
    var csmpObtainPixelName=element(by.xpath("//div[@id='obtainPixelModal']/div[1]/h4[1]")); // obtain pixel
    var csmpIDSyncText =element(by.xpath("//div[@id='obtainPixelModal']/div[2]/ng-include[1]/div/div[1]/h5"));
    var csmpNewCloseButton=element(by.css('[ng-click="close()"]'));
    var csmpViewConfigurationHeader = element(by.xpath("//div[@id='viewConfigModal']/div[1]/h4"));
    var csmpAdvertiserText = element(by.xpath("//div[@id='viewConfigModal']/div[2]/ng-include/div/div[1]/div[1]"));
    var csmpAdvertiserFeedBackText = element(by.xpath("//div[@id='viewConfigModal']/div[2]/ng-include/div/div[2]/div[1]"));
    var csmpViewConfigurationLink=element(by.linkText('View Configuration'));
    var csmpCloseBtn = element(by.css('.close'));
    var csmpNewNameSorting=element(by.xpath("(//div[@class='relative ng-scope ng-binding'])[position()=2]"));
    var csmpReverseMediaLocation= element(by.css('.selected.reverse'));
   
    this.returnReverseMediaProviderName =	function(){
		return csmpReverseMediaLocation.isPresent();
	};
    
    this.clickMediaProviderNameColumn = function(){
			csmpNewNameSorting.click();
	};
    
    this.clickViewConfigurationCloseButton = function(){
    	utility.browserWait(csmpNewCloseButton,'csmpCloseBtn');
    	csmpNewCloseButton.click();
    }
    
	this.isViewConfigurationCloseButtonDisplayed=function(){
		return csmpNewCloseButton.isDisplayed();
	};
    
    this.clickViewConfigurationLink=function(){
    	utility.browserWait(csmpViewConfigurationLink,'csmpViewConfigurationLink');
    	csmpViewConfigurationLink.click();
    };
    
    this.getConfigurationPageHeaderText=function(){
    	utility.browserWait(csmpViewConfigurationHeader,'csmpViewConfigurationHeader');
		return csmpViewConfigurationHeader.getText();
	};
	
	this.getViewConfAdvertiserText = function() {
	 	utility.browserWait(csmpAdvertiserText,'csmpAdvertiserText');	
		return csmpAdvertiserText.getText();
	};
	
	this.expandAdvertiserFeedbackText = function() {	
			return csmpAdvertiserFeedBackText.getText();
	};
    
    this.clickNewCloseBtn = function(){
    	utility.browserWait(csmpNewCloseButton, 'csmpNewCloseButton');
    	csmpNewCloseButton.click();
    }
    
    this.getObtainPixelsName = function() {
    	utility.browserWait(csmpObtainPixelName, 'csmpObtainPixelName');
		return csmpObtainPixelName.getText();
	};
	
	this.getIDSyncText = function() {
		utility.browserWait(csmpIDSyncText, 'csmpIDSyncText');
		return csmpIDSyncText.getText();
	}
    
    this.clickObtainPixelsLink = function() {
    	utility.browserWait(csmpObtainPixelLink, 'csmpObtainPixelLink');
		return csmpObtainPixelLink.click();
	};
    
    this.clickConfirmDeleteAlert=function()	{
    	utility.browserWait(csmpNewConfirmDeleteAlert,'csmpNewConfirmDeleteAlert');
		return csmpNewConfirmDeleteAlert.click();
	};
    
    this.clickMediaMenuLink = function() {
    	utility.browserWait(csmpMediaMenuLink,"csmpMediaMenuLink");
    	return csmpMediaMenuLink.click();
    };
    
    this.csmpElementList=function(){
    	return csmpNewElementList;
    };
    
    this.clickMediaProviderSubMenuLink = function() {
    	utility.browserWait(csmpMediaProvidersSubLink,"csmpMediaProvidersSubLink");
    	return csmpMediaProvidersSubLink.click();
    };
    
    this.clickNewMediaProviderPlusLink = function() {
    	utility.browserWait(csmpNewMediaProviderPlusLink,"csmpNewMediaProviderPlusLink");
    	return csmpNewMediaProviderPlusLink.click();
    };
    
    this.selectCookieSpace = function(cookieSpace) {
    	utility.browserWait(csmpCookieSpaceDropDown,"csmpCookieSpaceDropDown");
    	csmpCookieSpaceDropDown.sendKeys(cookieSpace);
    };
    
    this.enterMediaProviderName = function(name) {
    	csmpMediaProviderNameTxtBox.clear();
    	return csmpMediaProviderNameTxtBox.sendKeys(name);
    };
    
    this.selectLogo = function() {
    	return csmpLogoBtn;
    };
    
    this.enterNotes = function(notes) {
    	return csmpNotesTxtarea.sendKeys(notes);
    };
    
    this.clickSaveBtn = function() {
    	return csmpSaveButton.click();
    };
    
    this.clickCancelBtn = function() {
    	return csmpCancelButton.click();
    };
    
    this.clickActionDropDown = function() {
    	utility.browserWait(csmpActionDropdown,"csmpActionDropdown");
    	return csmpActionDropdown.click();
    };
    
    this.checkActionDropdownPresent=function() {
    	return csmpActionDropdown.isPresent();
    };
    
    this.clickNewMediaProviderLink = function() {
    	utility.browserWait(csmpNewMediaProviderLink,"csmpNewMediaProviderLink");
    	return csmpNewMediaProviderLink.click();
    };
    
    this.clickDeleteLink = function() {
    	utility.browserWait(csmpDeleteLink,"csmpDeleteLink");
    	return csmpDeleteLink.click();
    };
    
    this.clickShowDeletedLink = function() {
    	utility.browserWait(csmpShowDeletedLink,"csmpShowDeletedLink");
    	return csmpShowDeletedLink.click();
    };
    
    this.clickHideDeletedLink = function() {
    	utility.browserWait(csmpHideDeletedLink,"csmpHideDeletedLink");
    	return csmpHideDeletedLink.click();
    };
    
    this.returnSearchElement = function() {
    	utility.browserWait(csmpSearchTxtBox,"csmpSearchTxtBox");
    	return csmpSearchTxtBox;
    };
    
    this.returnPageHeader = function(){
    	utility.browserWait(csmpPageHeaderText,"csmpPageHeaderText");
    	return csmpPageHeaderText;
    };
    
    this.getPageHeaderText = function(){
    	utility.browserWait(csmpPageHeaderText,"csmpPageHeaderText");
    	return csmpPageHeaderText.getText();
    }
    
    this.clickEditLink = function() {
    	utility.browserWait(csmpEditLink,"csmpEditLink");
    	return csmpEditLink.click();
    }
    
    this.clickAuditLogLink = function() {
    	utility.browserWait(csmpAuditLoglink,"csmpAuditLoglink");
    	return csmpAuditLoglink.click();
    };
    
    this.clickViewConfigurationLink = function() {
    	utility.browserWait(csmpViewConfigurationLink,"csmpViewConfigurationLink");
    	return csmpViewConfigurationLink.click();
    };
    
    this.searchAuditLog = function(searchText) {
    	utility.browserWait(csmpSearchAuditLogTextBox,"csmpSearchAuditLogTextBox");
    	return csmpSearchAuditLogTextBox.sendKeys(searchText);
    };
    
    this.returnAuditLogSearchElement = function() {
		return csmpSearchAuditLogTextBox;
	};
	
	this.getFieldNameText = function() {
		return csmpFieldNameLocation.getText();
	};
	
	this.searchAuditLogTextBox = function(fieldName) {
		csmpSearchAuditLogTextBox.sendKeys(fieldName);
		csmpSearchAuditLogTextBox.sendKeys(protractor.Key.ENTER);
	};
    
    this.clickIconMenu = function() {
    	csmpNewListPageRecord.click();
    	browser.driver.executeScript('arguments[0].click()',csmpNewIconMenu.getWebElement());
    };
    
    this.isIconMenuPresent = function(){
    	return csmpNewIconMenu.isPresent();
    };
   
    this.isAuditLogSearchBoxPresent = function() {
    	utility.browserWait(csmpSearchAuditLogTextBox, 'csmpSearchAuditLogTextBox');
		return csmpSearchAuditLogTextBox.isPresent();
	};
};
module.exports = new cookieSpaceMediaProvidersPO()