var cookieSpaceDataContractsPO = function(){
	var utilityObj = require('../../lib/utility.js');
	
	var csdcFormHeaderLocation = element(by.xpath('//div[@id="dataContractGeneral"]/h3'));// Location of Data Contract create/edit general tab header.
	var csdcSearchElement = element(by.id('search'));
	var csdcDataLink = element(by.linkText('DATA')); //Main Menu Link of Data tab.
	var csdcCookieSpaceDataContractSubLink = element(by.linkText('Cookie Space Data Contracts')); //click on Cookie Space Data Contracts sub Menu Link of Data tab.
	var csdcNewCookieSpaceDataContractsButton = element(by.linkText('New Data Contract'));// Click on New Data Contract button.
	var csdcCookieSpaceDataContractsButton = element(by.xpath("//div[@id='leftNav']/div[1]/a[1]"));
	var csdcActionDropDownButton = element(by.css('.action-dropDown'));// location of action dropdown.
	var csdcNewDataContractsFromActionLink= element(by.css('.options>li:nth-child(1)>a'))//Select New Data Contracts from Action Drop down list.
	var csdcSelectDataProviderFromFrame = 'TB_window';//data Provider frame id.
	var csdcSelectDataProviderDropDown = element(by.id('data-provider-select'));//select data provider from frame drop down.
	var csdcFrameNextButton = element(by.id('modal-button-next'));//click on Next button after selecting data Provider.
	// General tab	
	var csdcShadowPage = element(by.xpath("//div[@id='shadow']"));
	var csdcNewCookieSpaceDataContractNameTxtBox = element(by.id('dataContractName'));//enter New Data Contract Name.
	var csdcCurrencyDropDown = element(by.id('localCurrencyId'));//Select Currency from drop down
	var csdcAllAdvertiserRdBtn = element(by.id('availabilityAll'));//Select All Advertiser Radio Button.
	var csdcSingleAdvertiserRdBtn = element(by.id('availabilitySingle'));//Select Single Advertiser Radio Button.
	var csdcSingleAdvertiserDropDown=element(by.id('singleAdvertiserId'));//Single advertiser drop down 
	var csdcavailabilityAnalyticsRdBtn = element(by.id('availabilityAnalytics'));//Select Analytics Only Radio Button.
	var csdcEnableForCrossDeviceDuplicationCheckBtn = element(by.id('optionalCheckbox-DupContract'));//Cross device duplication checkbox location.
	var csdcCrossDeviceGraphProviderDropDown = element(by.id('graphProviderSelector'));//Select Types of Graph Providers.
	var csdcCrossDeviceDestinationContractDropDown = element(by.id('destinationContractSelector'));//Select Types of Graph Providers.
	var csdcCrossDeviceRowsAddButton = element(by.css('.iconCell>a:nth-child(2)>img'));//This is (+) image button to add more rows in cross device.
	var csdcGeneralNextBtn=element(by.css('.nextButton.default-button')); //general tab next button
	var csdcclickonFlexagdataTypeFrame = element(by.id('selectedContentMain'));
	
	// Data type Tab(Standard) - (It shows when user selected data Provider has selected Parent Data Provider)
	//(Collection type -"Pixel")
	var csdcDataTypeTab = element(by.xpath('.//div[@id="selectedContentNav"]/ul/li[2]/a'));//Click on Data type tab.
	var csdcCollectiontypeDropdown = element(by.id('dataContractTypeId'));//select collection type
	var csdcPixelTypeDropdown = element(by.id('pixelTypeSelect'));//select pixel type.It display when selected "Pixel" collection type.
	var csdcIfaCheckedBtn = element(by.xpath('.//div[@id="deviceIdTypesDiv"]/div/div[1]/label/input'));
	var csdcPlatformIdCheckedBtn = element(by.xpath('.//div[@id="deviceIdTypesDiv"]/div/div[2]/label/input'));
	var csdcCategoriesCheckBtn = element(by.id('optionsCheckBox-categories'));//select categories check button.
	var csdcKeywordsCheckBtn = element(by.id('typeKeywords'));//select Keywords check button.
	var csdcKeysCheckBtn = element(by.id('optionsCheckBox-keyvalue'));//select Keys check button.
	var csdcClickOnFrame = element(by.id('dataContractDataType'));//click on frame for refresh hidden value.

	

	// Data type Tab (Collection type -"Pixel" && Data Collection type - "Categories")
	var csdcImportTaxonomyBtn =  element(by.id('taxonomyManageLink'));//Click on Import Taxonomy Button.
	var csdcbrowserOnFrame = element(by.css('.textInput'));
	var csdcapplyButton = element(by.name('next'));
	
	//--------------------	
	// Data type Tab (Collection type -"Pixel" && Data Collection type - "Keys")	
		var csdcKeyTxtBox =  element(by.id('keyValue0')); //Enter Key Value in text box.
	//---------------------
		
	//		Rate Schedules Tab
	var csdcRateSchedulesTabLink = element(by.linkText('3.   Rate Schedules'));
	var csdcRateSchedulesTab = element(by.xpath('.//div[@id="selectedContentNav"]/ul/li[3]/a'));
	var csdcAudineceDataCostDropDown = element(by.id('turnPayable'));
	var csdcStartDateCalander=element(by.id('dataContractScheduleStartDate0')); //Start date
	var csdcEndDateCalander = element(by.id('dataContractScheduleEndDate0'));
	var csdcSelectRateTypeDropDown = element(by.id('dataContractPricingTypeId0'));
	var csdcFlatRateTxtBox = element(by.id('ffRate0'));
	var csdcCPMTxtBox = element(by.id('cpmRate0'));
	var csdcCPUUTxtBox = element(by.id('cpuuRate0'));
	var csdcCostPerStampTxtBox = element(by.id('cpsRate0'));
	var csdcRevinueShareTxtBox=element(by.id('revSharePercent0'));
	var csdcRateScheduleHeaderText = element(by.xpath('.//div[@id="dataContractRateSchedules"]/h3'));
	// Save Button
	var csdcSaveBtn =element(by.css('.saveButton.save-button'));
	var csdcBackBtn = element(by.name('back'));
	
	//Edit/View/Delete/Undo delete data contract
	var csdcIconMenu = element(by.css('.icon-menu>img'));//Icon Menu data contract.
	var csdcSearchTextField=element(by.name('search'));//Search Text box data contract.
	var csdcEditLink=element(by.linkText('Edit'));//IconMenu->Edit link Data contract.
	var csdcViewLink=element(by.linkText('View'));//IconMenu->View link data contract.
	var csdcViewEditedtext=element(by.css('.title-text'));//View data contract text.
	var csdcDeleteLink=element(by.linkText('Delete'));//IconMenu->Delete data contract link.
	var csdcUndoDeleteLink=element(by.linkText('Undo Delete'));//IconMenu->Undo Delete data contract link.
	var csdcFirstRecordListCheckBox=element(by.xpath("(.//input[@name='dataContractId'])[position()=1]"));//data contract list check box for first record.
	var csdcUserNameLocation = element(by.id('userName'));
	var csdcHeaderLocation=element(by.css('.titletext-wrapper>h2,.title-text.ng-binding'));//page header 
	
	//Obtain pixel/View Audit Log
	var csdcObtainPixels=element(by.partialLinkText('Obtain Pixel'));//Obtain pixel link.
	var csdcViewAuditLog=element(by.linkText('View Audit Log'));//Audit log link.
	var csdcObtainPixelsName=element(by.xpath("//*[@id='TB_window']/div/table/tbody/tr[1]/td/table/tbody/tr/td/h3/div")); // obtain pixel
	var csdcObtainpixelclose = element(by.xpath('.//table[@class="wizardButtons"]/tbody/tr/td/input'));//obtain pixel frame close button.
	var csdcSearchAuditLogTextBox = element(by.name('auditLogSearch'));//Search audit log of Data contract.
	var csdcFieldNameLocation = element(by.css('.listView tr:nth-child(2) td:nth-child(4)'));//Field name location
	//Show/Hide/Expand all
	var csdcShowDeletedLink=element(by.linkText('Show Deleted/Expired'));//Action->Show deleted Link of data Contract.
	var csdcHideDeletedLink=element(by.linkText('Hide Deleted/Expired'));//Action->Hide deleted Link of data Contract.
	var csdcExpandAllRowsLink=element(by.linkText('Expand All Rows'));//Action->Expand all rows Link of data Contract.
	var csdcExpandDataContractconf=element(by.css('.expand-row-title>span'));//Expand->data contract configuration text.
	var csdcExpandRateSchedules=element(by.css('.expanded-row:nth-child(1)>div:nth-child(1)'));//Expand->Rate schedule text.
	var csdcExpandTaxonomy=element(by.css('.expanded-row:nth-child(2)>div:nth-child(1)'));//Expand->Taxonomytext.
	var csdcExpandPixelOptions=element(by.css('.expanded-row:nth-child(3)>div:nth-child(1)'));//Expand->Pixel option text.
	var csdcExpandEvents=	element(by.css('.expanded-row:nth-child(4)>div:nth-child(1)'));//Expand->Events text.
	var csdcExpandCloseButton=element(by.css('.expand-close>img'));//Expand->Close button.	
	var csdcCollapseAllRowsLink=element(by.linkText('Collapse All Rows'));//Action->Collapse all rows Link of data Contract.
	//Sorting
	var cookieSpaceDataContractNameColumn =element(by.linkText('Name'));//column name for sorting Data contract list by name.
	var reverseCookieSpaceDataContractLocation= element(by.css('.selected.reverse'));//reverse sorting of Data contarct.
	//New
	var consoleType='NEW';
	var csdcNewDPPlusBtn=element(by.css(".tif-plus.btn-action-plus.pull-left"));
	var csdcNewElementList = element.all(by.xpath("//td[@ng-repeat='column in columns']/span|//header[@class='title-text ng-binding']"));
	var csdcNewListPageRecord=element(by.xpath("(//td[@ng-repeat='column in columns'])[position()=1]"));
	var csdcNewsearchTxtBox=element(by.model("param"));
	var csdcNewIconMenu=element(by.xpath("(//button[@type='button'])[position()=2]"));
	var csdcNewConfirmDeleteAlert=element(by.css('[ng-click="deleteConfirm()"]'));
	var csdcNewActionDropDown=element(by.xpath("//button[@class='btn btn-dropdown dropdown-toggle pull-right ng-binding']"));
	var csdcNewNameColumnSorting=element(by.xpath("(//div[@class='relative ng-scope ng-binding'])[position()=2]"));
	var csdcNewReverseDataContractName=element(by.xpath("//th[@class='smart-table-header-cell medium sort-descent']"));
	var csdcNewDataProviderDropDown=element(by.xpath("//div[@class='modal-dialog ng-scope']")).element(by.id('tapdataProviderName'));
	var csdcNewDataProviderFrameNextButton=element(by.xpath("//button[@ng-click='next()']"));
	var csdcNewConfPageHeaderText=element(by.xpath("//div[@id='viewConfigModal']/div[1]/h4"));
	var csdcViewConfigurationLink=element(by.linkText('View Configuration'));
	var csdcNewExpandRateSchedules=element(by.css('.view-configuration-row:nth-child(1)>div:nth-child(1)'));//Expand->Rate schedule text.
	var csdcNewExpandTaxonomy=element(by.css('.view-configuration-row:nth-child(2)>div:nth-child(1)'));//Expand->Taxonomytext.
	var csdcNewExpandPixelOptions=element(by.css('.view-configuration-row:nth-child(3)>div:nth-child(1)'));//Expand->Pixel option text.
	var csdcNewExpandEvents=	element(by.css('.view-configuration-row:nth-child(4)>div:nth-child(1)'));//Expand->Events text.
	var csdcNewConfCloseButton=element(by.css('[ng-click="close()"]'));
	var csdcNewObtainPixelHeaderText= element(by.css('.modal-header>h4:nth-of-type(2)'));
	
	this.getConfigurationPageHeaderText=function(){
		utilityObj.browserWait(csdcNewConfPageHeaderText,'csdcNewConfPageHeaderText');
		return csdcNewConfPageHeaderText.getText();
	};
	

	this.clickViewConfigurationLink=function(){
		utilityObj.browserWait(csdcViewConfigurationLink,'csdcViewConfigurationLink');
		csdcViewConfigurationLink.click();
	};
	
	this.getViewConfRateScedulesText = function() {
		utilityObj.browserWait(csdcNewExpandRateSchedules,'csdcNewExpandRateSchedules');	
		return csdcNewExpandRateSchedules.getText();
	};
	this.getViewConfTaxonomyText = function() {
		utilityObj.browserWait(csdcNewExpandTaxonomy,'csdcNewExpandTaxonomy');	
		return csdcNewExpandTaxonomy.getText();
	};
	this.getViewConfPixelOptionsText = function() {
		utilityObj.browserWait(csdcNewExpandPixelOptions,'csdcNewExpandPixelOptions');	
		return csdcNewExpandPixelOptions.getText();
	};
	this.getViewConfExpandEventsText = function() {
		utilityObj.browserWait(csdcNewExpandEvents,'csdcNewExpandEvents');	
		return csdcNewExpandEvents.getText();
	};
	
	this.clickViewConfigurationCloseButton=function() {   
		utilityObj.browserWait(csdcNewConfCloseButton,'csdcNewConfCloseButton');
		return csdcNewConfCloseButton.click();
	};
	
	this.isViewConfigurationCloseButtonDisplayed=function(){
		return csdcNewConfCloseButton.isDisplayed();
	};
	
	
	this.clickViewConfigurationLink=function(){
		utilityObj.browserWait(csdcViewConfigurationLink,'csdcViewConfigurationLink');
		csdcViewConfigurationLink.click();
	};
	
	this.clickConfirmDeleteAlert=function(){  
		 utilityObj.browserWait(csdcNewConfirmDeleteAlert,'csdcNewConfirmDeleteAlert');
			return csdcNewConfirmDeleteAlert.click();
		};
	
	this.dataContractsElementList=function(){
		utilityObj.browserWaitforseconds(2);	
		 return csdcNewElementList;
	 };
	 
	this.getFormHeader = function() {
		return csdcFormHeaderLocation.isPresent().then(function(value){
			if(value){
				return csdcFormHeaderLocation.getText();
			}else{
				return null;
			}
		});		
	};
	
	this.clickOnClearSearch = function(){
			utilityObj.browserWait(csdcNewsearchTxtBox,"csdcNewsearchTxtBox");
			return csdcNewsearchTxtBox.clear();	
	};
	
	this.isSearchBoxPresent = function(){
			utilityObj.browserWaitforseconds(2);
			return csdcNewsearchTxtBox.isPresent();
	};
	
	this.returnSearchElement = function(){
			utilityObj.browserWaitforseconds(2);
			utilityObj.browserWait(csdcNewsearchTxtBox,"csdcNewsearchTxtBox");
			return csdcNewsearchTxtBox;
	};
		
	this.enterDataProviderName = function(dataProvider){
		utilityObj.browserWait(csdcDataproviderTxtBox,'csdcDataproviderTxtBox');
		return csdcDataproviderTxtBox.sendKeys(dataProvider);
		
	};
	
	this.clickBackBtn = function(){
		csdcBackBtn.click();
	};
	
	this.clickNewCookieSpaceDataContract = function(){
			return csdcNewDPPlusBtn.click();
	};
	
	this.waitForFrame = function(){
		utilityObj.browserWait(csdcSelectDataProviderFromFrame, "csdcSelectDataProviderFromFrame");
	};
	
	this.clickOnNewCookieSpaceDataContractsButton = function(){
			utilityObj.browserWait(csdcNewDPPlusBtn, 'csdcNewDPPlusBtn');
			csdcNewDPPlusBtn.click();
	};
	
	this.getCookieSpaceDataContractPageHeaderText = function(){
		 utilityObj.browserWaitforseconds(3);
		utilityObj.browserWait(csdcHeaderLocation,'csdcHeaderLocation');
		return csdcHeaderLocation.getText();
	};
	
	this.clickOnDataLink = function(){
		utilityObj.browserWait(csdcDataLink,'csdcDataLink');
		return csdcDataLink.click();
	};
	
	this.clickOnCookieSpaceDataContractsSubLink = function(){
		utilityObj.browserWait(csdcCookieSpaceDataContractSubLink,'csdcCookieSpaceDataContractSubLink');
		return csdcCookieSpaceDataContractSubLink.click();
	};
		
	this.checkAndClickNewDataContract=function(){
		return csdcNewDataContractNameTxtBox.isPresent().then(function(present){
			if(present==false){
				return csdcNewDataContractsButton.isPresent().then(function(value){
					if(value==true){
					csdcNewDataContractsButton.click();
					browser.waitForAngular();
					}
				});
			}
		});	
	};
	
	this.clickOnActionDropDownList = function(){
			utilityObj.browserWait(csdcNewActionDropDown,"csdcNewActionDropDown");
			return csdcNewActionDropDown.click();
	};
	
	this.clickNewDataContractsLinkFromAction = function(){
			utilityObj.browserWait(csdcNewCookieSpaceDataContractsButton, 'csdcNewCookieSpaceDataContractsButton');
			csdcNewCookieSpaceDataContractsButton.click();
	};
	
	this.switchToFrame = function(){
	return utilityObj.switchToFrame(csdcSelectDataProviderFromFrame);
	};
	
	this.selectDataProviderfromDropDown = function(dataProvider){
			 utilityObj.browserWait(csdcNewDataProviderDropDown, 'csdcNewDataProviderDropDown');
			 csdcNewDataProviderDropDown.sendKeys(dataProvider);
			 browser.waitForAngular();
			 utilityObj.browserWaitforseconds(3);
			return element(by.xpath("//li/a[contains(@title, '"+dataProvider+"')]")).click();
	};
	
	this.clickOnFrameNextButton = function(){
			csdcNewDataProviderFrameNextButton.click();
	};
// General tab	
	
	this.checkForShadowPage = function(){
		return csdcShadowPage.isPresent().then(function(value){
			if(!value){
				return csdcCookieSpaceDataContractsButton.click();
			}
		});
	};
	
	this.enterNewCookieSpaceDataContractName = function(name){
		utilityObj.browserWait(csdcNewCookieSpaceDataContractNameTxtBox, 'csdcNewCookieSpaceDataContractNameTxtBox');
		csdcNewCookieSpaceDataContractNameTxtBox.clear();
		return csdcNewCookieSpaceDataContractNameTxtBox.sendKeys(name);
	};
	
	this.selectCurrencyType =  function(currency){
		return csdcCurrencyDropDown.sendKeys(currency);
	};
	
	this.clickAllAdvertiser =  function(){
		return csdcAllAdvertiserRdBtn.click();
	};
	
	this.clickSingleAdvertiser =  function(){
		return csdcSingleAdvertiserRdBtn.click();
	};
	
	this.selectSingleAdvertiser=function(singleadvertiser){
	utilityObj.browserWait(csdcSingleAdvertiserDropDown, 'csdcSingleAdvertiserDropDown');
		return csdcSingleAdvertiserDropDown.sendKeys(singleadvertiser);
	};
	
	this.clickavailabilityAnalytics =  function(){
		return csdcavailabilityAnalyticsRdBtn.click();
	};
	
	this.clickEnableForCrossDeviceDuplication = function(){
		return csdcEnableForCrossDeviceDuplicationCheckBtn.click();
	};
	
	this.selectGraphProviderInCrossDevice = function(){
		utilityObj.browserWait(csdcCrossDeviceGraphProviderDropDown, 'csdcCrossDeviceGraphProviderDropDown');
		var rowElement = element.all(csdcCrossDeviceGraphProviderDropDown);
		rowElement.then(function(elements){
		if(elements.length>0){	
			for(var i=0;i<elements.length;i++){
				
				var selectGraphProvider = element(by.xpath(".//select[@id='graphProviderSelector']/option["+ i +"]"));
				return selectGraphProvider.click();
			}
		}else
			{
			
			logger.info("Please Create at Least one Data Contracts to select Destination Contract Name");
			}
		});
	};
	
	var selectDestinationContractInCrossDevice = function(){
		utilityObj.browserWait(csdcCrossDeviceDestinationContractDropDown, 'csdcCrossDeviceDestinationContractDropDown');
		var rowElement = element.all(csdcCrossDeviceDestinationContractDropDown);
		rowElement.then(function(elements){
		if(elements.length>0){	
			for(var i=0;i<elements.length;i++){
				
				var selectDestinationContracts = element(by.xpath(".//select[@id='destinationContractSelector']/option["+ i +"]"));
				return selectDestinationContracts.click();
			}
		}else
			{
			
			logger.info("Please Create at Least one Data Contracts to select Destination Contract Name ");
			}
		});
		
	};
	
		
	this.selectRowsAddButtonInCrossDevice = function(graphType){
		utilityObj.browserWait(csdcCrossDeviceRowsAddButton, 'csdcCrossDeviceRowsAddButton');
		return csdcCrossDeviceRowsAddButton.click();
	};
	
	this.clickGeneralTabNextButton=function(){
	return csdcGeneralNextBtn.click();
	};
	
	this.clickonFlextagDataTypeFrame = function(){
		utilityObj.browserWait(csdcclickonFlexagdataTypeFrame, 'csdcclickonFlexagdataTypeFrame');
		return csdcclickonFlexagdataTypeFrame.click();
	};
	
	// Data Type tab (Collection Type-Pixel)
	
	this.clickOnDataTypeTab = function(){
		return csdcDataTypeTab.click();
	};
	
		
	this.selectCollectiontype = function(collection){
		utilityObj.browserWait(csdcCollectiontypeDropdown, 'csdcCollectiontypeDropdown');
		return csdcCollectiontypeDropdown.sendKeys(collection);
	};
	
	this.selectPixelType = function(pixel){
		utilityObj.browserWait(csdcPixelTypeDropdown, 'csdcPixelTypeDropdown');
		return csdcPixelTypeDropdown.sendKeys(pixel);
	};
	
	this.checkIFAInDeviceIdType = function(){
		utilityObj.browserWait(csdcIfaCheckedBtn,'csdcIfaCheckedBtn');
		return csdcIfaCheckedBtn.click();
	};
	
	this.checkPlatformIdInDeviceIdType = function(){
		utilityObj.browserWait(csdcPlatformIdCheckedBtn,'csdcPlatformIdCheckedBtn');
		return csdcPlatformIdCheckedBtn.click();
	};
	
	this.clickOnCategoriesCheckbutton = function(){
		return csdcCategoriesCheckBtn.click();
	};

	this.clickOnFrame = function(){
		utilityObj.browserWait(csdcClickOnFrame,'csdcClickOnFrame');
		return	csdcClickOnFrame.click();
	};
// Data type Tab (Collection type -"Pixel" && Data Collection type - "Categories")	

	this.ClickOnImportTaxonomy = function() {
		utilityObj.browserWait(csdcImportTaxonomyBtn, 'csdcImportTaxonomyBtn');
		return csdcImportTaxonomyBtn.click();
	};
	
	this.clickOnBrowser = function() {
		utilityObj.browserWait(csdcbrowserOnFrame, 'csdcbrowserOnFrame', 2);
		return csdcbrowserOnFrame.isPresent();
	};
	
	this.clickOnApplyButton = function() {
		utilityObj.browserWait(csdcapplyButton, 'csdcapplyButton');
		return csdcapplyButton.click();
	};
//--------------------------	
	this.clickOnKeywordsCheckbutton = function(){
		return csdcKeywordsCheckBtn.click();
	};
	
	
	this.clickOnKeysCheckbutton = function(){
		return csdcKeysCheckBtn.click();
	};
	
// Data type Tab (Collection type -"Pixel" && Data Collection type - "Keys")	
	
	this.enterKeyInTextBox = function(key) {
		utilityObj.browserWait(csdcKeyTxtBox, 'csdcKeyTxtBox');
		return csdcKeyTxtBox.sendKeys(key);
	};
//-------------------------------
	
	this.clickOnShowAdvancedOptions = function(){
		return csdcShowAdvancedOptionsLink.click();
	};

//	Rate Schedules Tab
	
	this.rateScheduleHeaderText = function(name){
		return (csdcRateScheduleHeaderText.getText()).then(function(value){
		if(value== name){
				return true;
			}else{
				return false;
			};
		});
	};
	
	this.clickOnRateSchedulesTab = function(){
		utilityObj.browserWait(csdcRateSchedulesTab, 'csdcRateSchedulesTab',3);
		return csdcRateSchedulesTab.click();
	};
	
	this.clickOnRateSchedulesTabLink = function(){
		utilityObj.browserWait(csdcRateSchedulesTabLink, 'csdcRateSchedulesTabLink');
		return csdcRateSchedulesTabLink.click();
	};
	
	
	this.selectAudienceDataCost = function(dataCost){
		utilityObj.browserWait(csdcAudineceDataCostDropDown, 'csdcAudineceDataCostDropDown');
		return csdcAudineceDataCostDropDown.sendKeys(dataCost);
	};
	
	this.selectStartDate = function(date){
		utilityObj.browserWait(csdcEndDateCalander, 'csdcEndDateCalander');
		csdcStartDateCalander.clear();
		return csdcStartDateCalander.sendKeys(date);
	};
	
	
	this.selectEndDate = function(date){
		utilityObj.browserWait(csdcEndDateCalander, 'csdcEndDateCalander');
		return csdcEndDateCalander.sendKeys(date);
	};
	
	this.selectRateType = function(rate){
		utilityObj.browserWait(csdcSelectRateTypeDropDown, 'csdcSelectRateTypeDropDown');
		return csdcSelectRateTypeDropDown.sendKeys(rate);
	};
	
	this.enterCPMValue = function(cost){
		utilityObj.browserWait(csdcCPMTxtBox, 'csdcCPMTxtBox');
		return csdcCPMTxtBox.sendKeys(cost);
	};
	this.enterCPUUValue = function(cost){
		utilityObj.browserWait(csdcCPUUTxtBox, 'csdcCPUUTxtBox');
		return csdcCPUUTxtBox.sendKeys(cost);
	};
	this.enterFlatfee = function(cost){
		utilityObj.browserWait(csdcFlatRateTxtBox, 'csdcFlatRateTxtBox');
		return csdcFlatRateTxtBox.sendKeys(cost);
	};
	this.enterCostPerStamp = function(cost){
		utilityObj.browserWait(csdcCostPerStampTxtBox, 'csdcCostPerStampTxtBox');
		return csdcCostPerStampTxtBox.sendKeys(cost);
	};
	this.enterRevenueShare = function(cost){
		utilityObj.browserWait(csdcRevinueShareTxtBox, 'csdcRevinueShareTxtBox');
		return csdcRevinueShareTxtBox.sendKeys(cost);
	};
	
	// Common Save Button.
	
	this.clickOnSaveButton = function(){
		utilityObj.browserWait(csdcSaveBtn, 'csdcSaveBtn');
		return csdcSaveBtn.click();
	};
	
	//Edit/View/Delete/Undo delete data contarct
	
	this.clickOnIconMenu=function(){
	
		utilityObj.browserWaitforseconds(3);
		csdcNewListPageRecord.click();
		browser.driver.executeScript('arguments[0].click()',csdcNewIconMenu.getWebElement());
	};
	
	this.isIconMenuPresent=function(){
			utilityObj.browserWaitforseconds(3);
			utilityObj.browserWait(csdcNewListPageRecord,'csdcNewListPageRecord');
			return csdcNewListPageRecord.isPresent();
	};
	
	this.returnSearchTextField = function() {
		utilityObj.browserWait(csdcSearchTextField,'csdcSearchTextField');
		return csdcSearchTextField;
	};
	
	this.clickOnEditLink=function()
	{
		utilityObj.browserWait(csdcEditLink,'csdcEditLink');
		return csdcEditLink.click();
	};
	
	this.clickOnViewLink=function()
	{
		utilityObj.browserWait(csdcViewLink,'csdcViewLink');
		return csdcViewLink.click();
	};
	
	this.getExpectedDataContractNameViewPage = function() {
		utilityObj.browserWait(csdcViewEditedtext, 'csdcViewEditedtext');
		return csdcViewEditedtext.getText();
	};
	
	this.clickOnDeleteLink=function()
	{
		utilityObj.browserWait(csdcDeleteLink,'csdcDeleteLink');
		return csdcDeleteLink.click();
	};
	
	this.clickOnUndoDeleteLink=function()
	{
		utilityObj.browserWait(csdcUndoDeleteLink,'csdcUndoDeleteLink');
		return csdcUndoDeleteLink.click();
	};
	
	this.checkFirstRecordCheckbox=function(){
			utilityObj.browserWait(csdcNewListPageRecord,'csdcNewListPageRecord');
			return csdcNewListPageRecord.click();
	};
	
	this.clickOnObtainPixelsLink=function(){
		utilityObj.browserWait(csdcObtainPixels,'csdcObtainPixels');
		return csdcObtainPixels.click();
	};
	
	this.getObtainPixelsName = function() {
			return csdcNewObtainPixelHeaderText.getText();
	};
	
	this.clickCloseOnObtainPixels = function() {
			utilityObj.browserWait(csdcNewConfCloseButton, 'csdcNewConfCloseButton');
			return csdcNewConfCloseButton.click();
	};
	
	this.clickOnViewAuditLink=function()
	{	utilityObj.browserWait(csdcViewAuditLog,'csdcViewAuditLog');
		return csdcViewAuditLog.click();
	};
	this.isAuditLogSearchBoxPresent = function() {
		utilityObj.browserWait(csdcSearchAuditLogTextBox, 'csdcSearchAuditLogTextBox');
		return csdcSearchAuditLogTextBox.isPresent();
	};
	
	this.searchAuditLogTextBox = function(fieldName) {
		csdcSearchAuditLogTextBox.sendKeys(fieldName);
		csdcSearchAuditLogTextBox.sendKeys(protractor.Key.ENTER);
	};
	
	this.returnAuditLogSearchElement = function() {
		return csdcSearchAuditLogTextBox;
	};
	
	this.getFieldNameText = function() {
		return csdcFieldNameLocation.getText();
	};
	
	this.clickOnShowDeletedLink=function()
	{	utilityObj.browserWait(csdcShowDeletedLink,'csdcShowDeletedLink');
		return csdcShowDeletedLink.click();
	};
	
	this.clickOnHideDeletedLink=function()
	{	utilityObj.browserWait(csdcHideDeletedLink,'csdcHideDeletedLink');
		return csdcHideDeletedLink.click();
	};
	
	this.checkActionDropdownPresent=function(){  
			browser.waitForAngular();
			return csdcNewActionDropDown.isPresent();
	};
	
	this.clickCookieSpaceDataContractNameColumn = function() {
			utilityObj.browserWait(csdcNewNameColumnSorting,'csdcNewNameColumnSorting');
			csdcNewNameColumnSorting.click();
	};
	
	this.returnReverseCookieSpaceDataContractName = function() {
			return csdcNewReverseDataContractName.isPresent();	 
	};
	
	this.clickExpandDataContarctsLink=function()
	{
		utilityObj.browserWait(csdcExpandAllRowsLink,'csdcExpandAllRowsLink');
		return csdcExpandAllRowsLink.click();
	};
	this.clickCollapseDataContarctsLink=function()
	{
		utilityObj.browserWait(csdcCollapseAllRowsLink,'csdcCollapseAllRowsLink');
		return csdcCollapseAllRowsLink.click();
	};
	
	this.getExpandDatacontractConfText = function() {
		utilityObj.browserWait(csdcExpandDataContractconf,'csdcExpandDataContractconf');	
		return csdcExpandDataContractconf.getText();
	};
	
	this.getExpandRateScedulesText = function() {
		utilityObj.browserWait(csdcExpandRateSchedules,'csdcExpandRateSchedules');	
		return csdcExpandRateSchedules.getText();
	};
	
	this.getExpandTaxonomyText = function() {
		utilityObj.browserWait(csdcExpandTaxonomy,'csdcExpandTaxonomy');	
		return csdcExpandTaxonomy.getText();
	};
	
	this.getExpandPixelOptionsText = function() {
		utilityObj.browserWait(csdcExpandPixelOptions,'csdcExpandPixelOptions');	
		return csdcExpandPixelOptions.getText();
	};
	
	this.getExpandEventsText = function() {
		utilityObj.browserWait(csdcExpandEvents,'csdcExpandEvents');	
		return csdcExpandEvents.getText();
	};
	
	this.clickExpandCloseButton=function() {   
		utilityObj.browserWait(csdcExpandCloseButton,'csdcExpandCloseButton');
		return csdcExpandCloseButton.click();
	};
	
	this.isExpandCloseButtonDisplayed=function()
	{
		return csdcExpandCloseButton.isDisplayed();
	};
	

	this.clearSearchBox = function() {
		 csdcSearchTextField.clear();
		return csdcSearchTextField.sendKeys(protractor.Key.ENTER);
	};
	
	this.getMarketNameFromColumn = function() {
		utilityObj.browserWait(marketNameInListItem, 'marketNameInListItem');
		return marketNameInListItem.getText();
	};
		
	this.isUserNamePresent = function(){
		return csdcUserNameLocation.isPresent();
	};

	this.clickOnUserName = function(){
		utilityObj.browserWait(csdcUserNameLocation, 'csdcUserNameLocation');
		csdcUserNameLocation.click();
	}
	
	
};
module.exports = new cookieSpaceDataContractsPO();