/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

var cookieSpaceDataProvidersPO = function(){
var utility = require('../../lib/utility.js');
	
	var csdpDataTabLink = element(by.linkText('DATA')); // Main menu link.
	var csdpLink = element(by.linkText('Cookie Space Data Providers')); // sub link of main menu of Data tab.
	var csdpNewDataProviderLink = element(by.linkText('New Data Provider')); // link of creating New Data Provider.
	var csdpNewDataProviderActionLink = element(by.css('.options>li:nth-child(1)>a'));
	var csdpCookieSpaceId = element(by.id("cookieSpaceId"));
	var csdpName = element(by.id("dataProviderName"));
	var csdpLogoBrowseButton = element(by.id('dataProviderImageFileName'));
	var csdpNotesTextArea = element(by.id('notes'));
	var csdpSaveButton = element(by.xpath("//input[@type='button' and @name='save']"));
	var csdpCancelButton = element(by.name("cancel"));
	var csdpSearchTextField = element(by.id("search"));
	var csdpEditLink = element(by.linkText("Edit"));
	var csdpIconMenu = element(by.css(".icon-menu>img"));
	var csdpPageHeaderLocation = element(by.css(".titletext-wrapper>h2,.title-text.ng-binding"));
	var csdpDeleteLink = element(by.linkText("Delete"));
	var csdpActionDropDown = element(by.css(".action-dropDown"));
	var csdpShowDeletedLink = element(by.linkText("Show Deleted"));
	var csdpHideDeletedLink = element(by.linkText("Hide Deleted"));
	var csdpViewLink = element(by.linkText("View"));
	var csdpViewEditedText = element(by.className("name-text"));
	var csdpAuditLogLink = element(by.linkText('View Audit Log'));
	var csdpSearchAuditLogTextBox = element(by.name('auditLogSearch'));
	var csdpFieldNameLocation = element(by.css('.listView tr:nth-child(2) td:nth-child(4)'));
	var csdpDataProviderNameColumn =element(by.linkText('Name'));
	var csdpReverseDataProviderLocation= element(by.css('.selected.reverse'));
	//New
	var csdpNewDPPlusBtn=element(by.css(".tif-plus.btn-action-plus.pull-left"));
	var csdpNewElementList = element.all(by.xpath("//a[@class='click-column-link ng-binding']|//header[@class='title-text ng-binding']"));
	var csdpNewListPageRecord=element(by.xpath("(//td[@ng-repeat='column in columns'])[position()=1]"));
	var csdpNewsearchTxtBox=element(by.model("param"));
	var csdpNewIconMenu=element(by.xpath("(//button[@type='button'])[position()=1]"));
	var csdpNewConfirmDeleteAlert=element(by.css('[ng-click="deleteConfirm()"]'));
	var csdpNewActionDropDown=element(by.xpath("//button[@class='btn btn-dropdown dropdown-toggle pull-right ng-binding']"));
	var csdpNewNameColumnSorting=element(by.xpath("(//div[@class='relative ng-scope ng-binding'])[position()=2]"));
	var csdpNewReverseDataContractName=element(by.xpath("//th[@class='smart-table-header-cell medium sort-descent']"));
	
	this.clickConfirmDeleteAlert=function(){  
		 utility.browserWait(csdpNewConfirmDeleteAlert,'csdpNewConfirmDeleteAlert');
			return csdpNewConfirmDeleteAlert.click();
		};
	
	this.dataProvidersElementList=function(){
		 return csdpNewElementList;
	 };

	this.clickDataMenuLink = function(){
		utility.browserWait(csdpDataTabLink,'csdpDataTabLink');
		return csdpDataTabLink.click();		
	};
	
	this.clickCookieSpaceDataProviderSubLink = function(){
		utility.browserWait(csdpLink,'csdpLink');
		return csdpLink.click();
	};
	
	this.isNewDataProviderLinkPresent = function(){
			return csdpNewDPPlusBtn.isPresent();
	};
	
	this.clickOnNewDataProviderLink = function(){
			return csdpNewDPPlusBtn.click();
	};
	
	this.clickNewDataProviderActionLink = function(){
			utility.browserWait(csdpNewDataProviderLink, 'csdpNewDataProviderLink');
			csdpNewDataProviderLink.click();
	};
	
	this.clickCancelButton = function(){
		utility.browserWait(csdpCancelButton,'csdpCancelButton');
		return csdpCancelButton.click();
	};
	
	this.selectCookieSpace = function(cookieSpace){
		utility.browserWait(csdpCookieSpaceId,'csdpCookieSpaceId');
		return csdpCookieSpaceId.sendKeys(cookieSpace);
	};
	
	this.enterCookieSpaceDataProviderName = function(name){
		utility.browserWait(csdpName,'csdpName');
		csdpName.clear();
		return csdpName.sendKeys(name);
	};
	
	this.clickSave = function(){
		utility.browserWait(csdpSaveButton,'csdpSaveButton');
		return csdpSaveButton.isEnabled().then(function(){
			csdpSaveButton.click();
		});
	};
	
	this.clickIconMenu = function(){
			utility.browserWaitforseconds(3);
			csdpNewListPageRecord.click();
			browser.driver.executeScript('arguments[0].click()',csdpNewIconMenu.getWebElement());
	};
	
	this.isIconMenuPresent = function(){
			utility.browserWaitforseconds(3);
			utility.browserWait(csdpNewListPageRecord,'csdpNewListPageRecord');
			return csdpNewListPageRecord.isPresent();
	};
	
	this.clickEditLink = function(){
		utility.browserWait(csdpEditLink,'csdpEditLink');
		return csdpEditLink.click();
	};
	
	this.getCookieSpaceDataProviderHeaderText = function(){
		utility.browserWait(csdpPageHeaderLocation,'csdpPageHeaderLocation');
		return csdpPageHeaderLocation.getText();
	};
	
	this.clickDeleteLink = function(){
		utility.browserWait(csdpDeleteLink,'csdpDeleteLink');
		return csdpDeleteLink.click();
	};
	
	this.clickActionDropDown = function(){
			utility.browserWait(csdpNewActionDropDown,"csdpNewActionDropDown");
			return csdpNewActionDropDown.click();
	};
	
	this.isActionDropDownPresent = function(){
			utility.browserWait(csdpNewActionDropDown, 'csdpNewActionDropDown');
			return csdpNewActionDropDown.isPresent();
	};
	
	this.clickHideDeleted = function(){
		utility.browserWait(csdpHideDeletedLink,'csdpHideDeletedLink');
		return csdpHideDeletedLink.click();
	};
	
	this.clickShowDeleted = function(){
		utility.browserWait(csdpShowDeletedLink,'csdpShowDeletedLink');
		return csdpShowDeletedLink.click();
	};
	
	this.clickViewLink = function(){
		utility.browserWait(csdpViewLink,'csdpViewLink');
		return csdpViewLink.click();
	};
	
	this.getExpectedDataProviderName = function(){
		utility.browserWait(csdpViewEditedText,'csdpViewEditedText');
		return csdpViewEditedText.getText();
	};
	
	this.returnSearchElement = function(){
			utility.browserWaitforseconds(2);
			utility.browserWait(csdpNewsearchTxtBox,"csdpNewsearchTxtBox");
			return csdpNewsearchTxtBox;
	};
	
	this.clickAuditLogLink = function() {
		utility.browserWait(csdpAuditLogLink,'csdpAuditLogLink');
		return csdpAuditLogLink.click();
	};
	
	this.isAuditLogSearchBoxPresent = function() {
		utility.browserWait(csdpSearchAuditLogTextBox,'csdpSearchAuditLogTextBox');
		return csdpSearchAuditLogTextBox.isPresent();
	};
	
	this.getFieldNameText = function(){
		utility.browserWait(csdpFieldNameLocation,'csdpFieldNameLocation');
		return csdpFieldNameLocation.getText();
	};
	
	this.searchAuditLog = function(name){
		csdpSearchAuditLogTextBox.sendKeys(name);
		return csdpSearchAuditLogTextBox.sendKeys(protractor.Key.ENTER);
	};
	
	this.clearSearchBox = function(){
		return csdpSearchTextField.clear();
		return csdpSearchTextField.sendKeys(protractor.Key.ENTER);
	};
	
	this.returnAuditLogSearchElement = function(){
		return csdpSearchAuditLogTextBox;
	};
	
	this.clickDataProviderNameColumn = function(){
			utility.browserWait(csdpNewNameColumnSorting,'csdpNewNameColumnSorting');
			csdpNewNameColumnSorting.click();
	};
	
	this.returnReverseDataProviderName = function(){
			return csdpNewReverseDataContractName.isPresent();	 
	};
	
	this.returnLogoBrowserButton = function(){
		return csdpLogoBrowseButton;
	};
	
	this.enterNotes = function(notes){
		csdpNotesTextArea.clear();
		return csdpNotesTextArea.sendKeys(notes);
	};
	
};
module.exports = new cookieSpaceDataProvidersPO();