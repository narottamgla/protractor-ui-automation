/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

var segmentPO = function(){
	var utility = require('../../lib/utility.js');
	var loggerObj = require('../../lib/logger.js');
	var logger = loggerObj.loggers("segmentPO");
	
	var sgmtSegmentLink = element(by.linkText("SEGMENTS"));
	var sgmtNewSegmentBtn = element(by.linkText("New Segment"));
	var sgmtActionDropDown = element(by.css('.action-dropDown,.btn.btn-dropdown.dropdown-toggle.pull-right.ng-binding'));
	var sgmtNewSegmentActionLink = element(by.css('.options>li:nth-child(1)>a'));
	var sgmtShowDeletedLink = element(by.linkText('Show Deleted'));
	var sgmtHideDeletedLink = element(by.linkText('Hide Deleted'));
	var sgmtIconMenu = element(by.css('.icon-menu>img'));
	var sgmtEditLink = element(by.linkText('Edit'));
	var sgmtCopyLink = element(by.linkText('Copy'));
	var sgmtSharedDataLink = element(by.linkText('DATA'));
	var sgmtSharedSegmentLink = element(by.linkText('Segments'));
	var sgmtDeleteLink = element(by.linkText('Delete'));
	var sgmtViewAuditLogLink = element(by.linkText('View Audit Log'));
	var sgmtSegmentNameTxtBox = element(by.id('segmentName'));
	var sgmtAllAdvertiserRdb = element(by.id('availabilityAll'));
	var sgmtSingleAdvertiserRdb = element(by.id('availabilitySingle'));
	var sgmtAnalyticsOnlyRdb = element(by.id('availabilityAnalytics'));
	var sgmtSingleAdvertiserDropDown = element(by.id('advertiserId'));
	var sgmtDisplayMPTab = element(by.css('.media-type-list>li:nth-child(1)>a'));
	var sgmtVideoMPTab = element(by.css('.media-type-list>li:nth-child(2)>a'));
	var sgmtMobileMPTab = element(by.css('.media-type-list>li:nth-child(3)>a'));
	var sgmtSocialMPTab = element(by.css('.media-type-list>li:nth-child(4)>a'));
	var sgmtSitePersonalizationMPTab = element(by.css('.media-type-list>li:nth-child(5)'));
	var sgmtMediaProviderChkBox = element(by.css('.rows>tr:nth-child(2)>td:nth-child(1)>input'));
	var sgmtPixelIdTxtBox = element(by.css('.rows>tr:nth-child(2)>td:nth-child(4)>input'));
	var sgmtSegmentFilterLabel = element(by.css('#targetingGeographicSummary>font'));
	var sgmtTargetContinentsRdb = element(by.xpath("//input[@value='geo']"));
	var sgmtTargetPostalCodesRdb= element(by.xpath("//input[@value='zip']"));
	var sgmtSelectGeoChkBox = element(by.css('.containerTableStyle>table>tbody>tr:nth-child(4)>td>table>tbody>tr>td:nth-child(2)>img'));
	var sgmtCountryDropDown = element(by.id('zipTargetGeoCountry'));
	var sgmtEntriesTxtArea = element(by.id('lineItemTargetZips'));
	var sgmtApplyBtn= element(by.css('.save-button'));
	var sgmtSaveBtn = element(by.id('modal-button-save'));
	var sgmtCancelBtn= element(by.css('.default-button'));
	var sgmtNagtedDropDown = element(by.id('isNegated1'));
	var sgmtDataContractDropDown = element(by.id('dataContractId1'));
	var sgmtSegmentRuleTypeDropDown = element(by.css('#span1_2>select'));
	var sgmtFlexKeyDropDown = element(by.css('#span1_3>select'));
	var sgmtCategoriesBtn = element(by.xpath('//span[@id="categorySelection1"]/input'));
	var sgmtFlexOptionsDropDown = element(by.css('#span1_31>select'));
	var sgmtFlexKeyValueTxtArea = element(by.id('flexKeyValue1'));
	var sgmtKeywordTxtArea = element(by.id('keywords1'));
	var sgmtNextBtn = element(by.css('#modal-button-next'));
	var segmentPageHeaderLocation = element(by.css(".title-text.ng-binding,.titletext-wrapper>h2"));
	var segmExpandSegmentFilters = element(by.xpath('//div[@id="segmentRulesScreen"]/div[4]/div/div'));
	var segmDPMExpandSegmentFilters = element(by.xpath('//div[contains(@class, "filtersContainer")]/div[1]/div'));
	var segmSearchElement = element(by.id('search'));
	var segmCategoriesChkBox = element(by.xpath('//div[@id="searchResultsTree"]/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/img'));
	var segmBuildRulesBtn = element(by.id('buildRulesButtons'));
	var segmFlexTagExpressionMinValueTxtBox = element(by.id('flexTagExpressionMinValue1'));
	var segmFlexTagExpressionMaxValueTxtBox = element(by.id('flexTagExpressionMaxValue1'));
	var segmFlexTagExpressionValueTxtBox = element(by.id('flexTagExpressionValue1'));
	var segmFlexTagTargetObjectBtn = element(by.id('targetObjectButton1'));
	var segmFrequenceDropDown = element(by.id('isTypeFrequency1'));
	var segmFrequenceTxtBox = element(by.id('frequency1'));
	var segmPeriodDropDown = element(by.id('periodPrefix1'));
	var segmAddOptions = element(by.xpath('//table[@id="targetObjectTable"]/tbody/tr/td[2]/a[@class="add"]'));
	var segmMediaProviderTable = element(by.xpath('//table[@id="mediaProviderSegmentsTable"]/tbody[2]/tr'));
	var segmRuleTable = element(by.css('.estimation-div'));
	var segmMediaProviderTab = element(by.css('.media-type-list'));
	var segmSearchAuditLogTextBox = element(by.name('auditLogSearch'));
	var segmFieldNameLocation = element(by.css('.listView tr:nth-child(2) td:nth-child(4)'));
	var segmClosedFlipper = element(by.css(".adv-flipper.closed"));
	var segmClearFilterLink = element(by.linkText("Clear all filters"));
	var segmSelectOption = element(by.linkText("Select..."));
	var segmSelectMarket = element(by.css(".dhx_combo_input"));
	var segmSelectMarketFileter = element(by.css(".dhx_combo_list.new-filter-dropdown-list>div[style='width: 100%; overflow: hidden; padding-right: 10px;']"));
	var segmMarketNameColumn = element(by.css('#records-list>tbody>tr>td:nth-of-type(5)'));
	var segmNameColumn =element(by.linkText('Name'));
	var segmReverseSegmentLocation= element(by.css('.selected.reverse'));
	var segmInsertionorderFrameId='TB_window';
	var segSearchButtion=element(by.id('submitSearch'));
	var segmNewSegmentBtn=element(by.xpath("//a[@class='tif-plus btn-action-plus pull-left' and not (contains(@style,'display: none;'))]"));
	var segmNewSearchElement=element(by.model('param'));
	var segmNewElementList=element.all(by.xpath("(//td[@ng-repeat='column in columns'])[position()=2]|//header[@class='title-text ng-binding']"));
	var sgmtNewIconMenu=element(by.xpath("(//button[@type='button'])[position()=1]"));
	var segmNewListRecord=element(by.xpath("(//td[@ng-repeat='column in columns'])[position()=1]"));
	var dpNewConfirmDeleteAlert=element(by.css('[ng-click="deleteConfirm()"]'));
	var segmNewReverseSegmentLocation=element(by.xpath("//th[@class='smart-table-header-cell medium sort-descent']"));
	var segmNewNameColumn =element(by.xpath("(//div[@class='relative ng-scope ng-binding'])[position()=2]"));
	var segmNewFilterMarket=element(by.id('tapMarketName'));
	var segmNewFilterClose=element(by.css('.tif-delete.remove-filter'));
	var segNewFilterDropdown=element(by.id('filter-dropdown'));
	var segmMarketLink=element(by.linkText('Market'));
	var segmNewMarketColoumn=element(by.xpath('//tbody/tr[1]/td[4]'));
	var segmSearchCancel=element(by.css('[ng-click="clear()"]'));
	
	 var campaignsuiteWarningclose=element(by.css(".hideModal"));
	 var campaignSuitePopupWindow=element(by.id("dialog-70"));
	    
	    this.warningPresent=function(){
	    	return campaignSuitePopupWindow.isPresent();
	    };
	    
	    this.warningClose=function(){
	    	return campaignsuiteWarningclose.click();
	    };
	/**
	 * This method is use to search Turn Media Provider from Segment Media Provider table and 
	 * check on Turn Media Provider.
	 * 
	 * @author hemins
	 */
	this.checkTurnMediaProviderCheckBox = function(){
		segmMediaProviderTable.isPresent().then(function(){
			//element.all is use to get all tr element of mediaProviderSegmentsTable
			var rowElement =element.all(by.xpath('//table[@id="mediaProviderSegmentsTable"]/tbody[2]/tr[@style="display: table-row;"]'));
			rowElement.then(function(elements){
				var k=0;
				var flag=null;
				//element.length is used to get count of tr of mediaProviderSegmentsTable.
				for(var j=1;j<=elements.length;j++){	
					//Get all element of media provider column by passing j value.
					var mediaProviderColumn =element(by.xpath("(//table[@id='mediaProviderSegmentsTable']/tbody[2]/tr[@style='display: table-row;'])[position()="+ j +"]/td[3]/span[@class='mp-name']"));
					mediaProviderColumn.getText().then(function(value){
						k=k+1;
						if(value == "Turn"){
							flag=true;
							var checkBoxElement = element(by.xpath("(//table[@id='mediaProviderSegmentsTable']/tbody[2]/tr[@style='display: table-row;'])[position()="+ k +"]/td[1]/input"));
							checkBoxElement.click();	
						}else{
							flag=false;
						}
					});				
				}
				if(flag == false){
					logger.info("'Turn' Media Provider is not found.");
				}
			});
		});		
	};
	
	this.clickSearchCancel=function(){  
		if(segmSearchCancel.isPresent()){
			return segmSearchCancel.click();
		 }
	};	
	this.clickConfirmDeleteAlert=function(){  
		 utility.browserWait(dpNewConfirmDeleteAlert,'dpNewConfirmDeleteAlert');
			return dpNewConfirmDeleteAlert.click();
		};
		
	this.clickSearchButtion = function(){
		utility.browserWait(segSearchButtion,'segSearchButtion');
		return segSearchButtion.click();
	};
	this.switchToFrame=function(){
	utility.browserWaitforseconds(2);
	return browser.switchTo().frame(segmInsertionorderFrameId);
	};
	this.clickAddOption = function(){
		utility.browserWait(segmAddOptions,'segmAddOptions');
		return segmAddOptions.click();
	};
	
	this.selectFrequenceDropDown = function(value){
		utility.browserWait(segmFrequenceDropDown,'segmFrequenceDropDown');
		segmFrequenceDropDown.sendKeys(value);
		return segmRuleTable.click();
	};
	
	this.enterFrequenceDropDown = function(value){
		segmFrequenceTxtBox.clear();
		return segmFrequenceTxtBox.sendKeys(value);
		
	};
	
	this.selectPeriodDropDropDown = function(value){
		utility.browserWait(segmPeriodDropDown,'segmPeriodDropDown');
		segmPeriodDropDown.sendKeys(value);
		return segmRuleTable.click();
	};
	
	this.clickTargetObjectBtn = function(){
		return segmFlexTagTargetObjectBtn.click();
	};
	
	this.enterMinValue = function(minValue){
		return segmFlexTagExpressionMinValueTxtBox.sendKeys(minValue);
	};
	
	this.enterMaxValue = function(maxValue){
		return segmFlexTagExpressionMaxValueTxtBox.sendKeys(maxValue);
	};
	
	this.enterExpressionValue = function(value){
		return segmFlexTagExpressionValueTxtBox.sendKeys(value);
	};
	
	this.clickCategories = function(){
		utility.browserWait(sgmtCategoriesBtn,'sgmtCategoriesBtn');
		return sgmtCategoriesBtn.click();
	};
	
	this.selectCategories = function(){
		utility.browserWaitforseconds(2);
		utility.browserWait(segmCategoriesChkBox,'segmCategoriesChkBox');
		return segmCategoriesChkBox.click();
	};
	
	this.clickBuildRuleBtn = function(){
		return segmBuildRulesBtn.click();
	};
		
	this.clickExpandSegmentFilter = function(){
		if((browser.user).toUpperCase() == 'CS' ){
			utility.browserWait(segmExpandSegmentFilters,'segmExpandSegmentFilters');
			return segmExpandSegmentFilters.click();
		}else{
			utility.browserWait(segmDPMExpandSegmentFilters,'segmDPMExpandSegmentFilters');
			return segmDPMExpandSegmentFilters.click();
		}
		
	};
	
	this.clearSearchBoxText = function(){
			utility.browserWait(segmNewSearchElement,"segmNewSearchElement");
			utility.browserWaitforseconds(2);
			return segmNewSearchElement.clear();		
	};
	
	this.returnSearchElement = function(){
			utility.browserWait(segmNewSearchElement,"segmNewSearchElement");
			utility.browserWaitforseconds(2);
			return segmNewSearchElement;
	};
	
	this.clickSegmentLink = function(){
		utility.browserWait(sgmtSegmentLink,'sgmtSegmentLink');
		return sgmtSegmentLink.click();
	};
	this.clickDataLink = function(){
		utility.browserWait(sgmtSharedDataLink,'sgmtSharedDataLink');
		utility.browserWaitforseconds(2);
		return sgmtSharedDataLink.click();
	};
	this.clickSharedSegmentLink = function(){
		utility.browserWait(sgmtSharedSegmentLink,'sgmtSharedSegmentLink');
		utility.browserWaitforseconds(2);
		return sgmtSharedSegmentLink.click();
	};
	
	this.clickNewSegmentBtn = function(){
			return segmNewSegmentBtn.click();
	};
	
	this.clickActionDropDown = function(){
		return sgmtActionDropDown.click();
	};
	
	this.isActionDropDownPresent = function(){
		return sgmtActionDropDown.isPresent();
	};
	
	this.clickNewSegmentActionLink = function(){
		utility.browserWait(sgmtNewSegmentActionLink,'sgmtNewSegmentActionLink');
		return sgmtNewSegmentActionLink.click();
	};
	
	this.clickShowDeletedLink = function(){
		utility.browserWait(sgmtShowDeletedLink,'sgmtShowDeletedLink');
		return sgmtShowDeletedLink.click();
	};
	
	this.clickHideDeletedLink = function(){
		utility.browserWait(sgmtHideDeletedLink,'sgmtHideDeletedLink');
		return sgmtHideDeletedLink.click();
	};
	
	this.clickIconMenu = function(){
			browser.waitForAngular();
			utility.browserWaitforseconds(1);
			segmNewListRecord.click().then(function(){
			browser.waitForAngular();
			utility.browserWaitforseconds(1);
				browser.driver.executeScript('arguments[0].click()',sgmtNewIconMenu.getWebElement());
			});
	};
	
	this.isIconMenuPresent = function(){
			utility.browserWait(segmNewListRecord,'segmNewListRecord');		
			segmNewListRecord.isPresent();	
	};
	
	this.clickEditLink = function(){
		utility.browserWait(sgmtEditLink,'sgmtEditLink');
		return sgmtEditLink.click();
	};
	
	this.clickCopyLink = function(){
		utility.browserWait(sgmtCopyLink,'sgmtCopyLink');
		return sgmtCopyLink.click();
	};
	
	this.clickDeleteLink = function(){
		utility.browserWait(sgmtDeleteLink,'sgmtDeleteLink');
		return sgmtDeleteLink.click();
	};
	
	this.clickAuditLogLink = function(){
		utility.browserWait(sgmtViewAuditLogLink,'sgmtViewAuditLogLink');
		return sgmtViewAuditLogLink.click();
	};
	
	this.isAuditLogSearchBoxPresent = function() {
		utility.browserWait(segmSearchAuditLogTextBox,'searchAuditLogTextBox');
		return segmSearchAuditLogTextBox.isPresent();
	};
	
	this.getFieldNameText = function(){
		utility.browserWait(segmFieldNameLocation,'fieldNameLocation');
		return segmFieldNameLocation.getText();
	};
	
	this.searchAuditLog = function(name){
		segmSearchAuditLogTextBox.sendKeys(name);
		return segmSearchAuditLogTextBox.sendKeys(protractor.Key.ENTER);
	};
	
	this.selectMarketDropDown = function(market){
		if(market.toUpperCase() == "TURN"){
			var sgmtMarketDropDown =browser.findElement(protractor.By.id('marketId'));
			sgmtMarketDropDown.sendKeys(market);
			utility.browserWait(segmMediaProviderTab,'segmMediaProviderTab');
			segmMediaProviderTab.click();
		}else{
			var sgmtMarketDropDown =browser.findElement(protractor.By.id('marketId'));
			utility.selectDropdownByText(sgmtMarketDropDown,market,3);
			utility.browserWait(segmMediaProviderTab,'segmMediaProviderTab');
			segmMediaProviderTab.click();
		}		
	};
	
	this.enterSagmentName = function(name){
		utility.browserWait(sgmtSegmentNameTxtBox,'sgmtSegmentNameTxtBox');
		utility.browserWaitforseconds(1);
		sgmtSegmentNameTxtBox.clear();
		return sgmtSegmentNameTxtBox.sendKeys(name);
	};
	
	this.clickAllAdvertiserRbt = function(){
		return sgmtAllAdvertiserRdb.click();
	};
	
	this.clickSingleAdvertiserRbt = function(){
		utility.browserWait(sgmtSingleAdvertiserRdb,'sgmtSingleAdvertiserRdb');
		return sgmtSingleAdvertiserRdb.click();
	};
	
	this.clickAnalyticsRbt = function(){
		utility.browserWait(sgmtAnalyticsOnlyRdb,'sgmtAnalyticsOnlyRdb');
		return sgmtAnalyticsOnlyRdb.click();
	};
	
	this.selectSingleAdvertiser = function(advertiser){
		utility.browserWait(sgmtSingleAdvertiserDropDown,'sgmtSingleAdvertiserDropDown');
		sgmtSingleAdvertiserDropDown.sendKeys(advertiser);
		utility.browserWait(segmMediaProviderTab,'segmMediaProviderTab');
		return segmMediaProviderTab.click();
	};
	
	this.clickDisplayMPTab = function(){
		utility.browserWait(sgmtDisplayMPTab,'sgmtDisplayMPTab');
		utility.browserWaitforseconds(1);
		return sgmtDisplayMPTab.click();
	};
	
	this.clickVideoMPTab = function(){
		utility.browserWait(sgmtVideoMPTab,'sgmtVideoMPTab');
		utility.browserWaitforseconds(1);
		return sgmtVideoMPTab.click();
	};
	
	this.clickMobileMPTab = function(){
		utility.browserWait(sgmtMobileMPTab,'sgmtMobileMPTab');
		utility.browserWaitforseconds(1);
		return sgmtMobileMPTab.click();
	};
	
	this.clickSocialMPTab = function(){
		utility.browserWait(sgmtSocialMPTab,'sgmtSocialMPTab');
		utility.browserWaitforseconds(1);
		return sgmtSocialMPTab.click();
	};
	
	this.clickSitePersonalizationMPTab = function(){
		utility.browserWait(sgmtSitePersonalizationMPTab,'sgmtSitePersonalizationMPTab');
		return sgmtSitePersonalizationMPTab.click();
	};
	
	this.checkMediaProvider = function(){
		utility.browserWait(sgmtMediaProviderChkBox,'sgmtMediaProviderChkBox');
		return sgmtMediaProviderChkBox.click();
	};
	
	this.clickSegmentFilterLabel = function(){
		utility.browserWait(sgmtSegmentFilterLabel,'sgmtSegmentFilterLabel');
		return sgmtSegmentFilterLabel.click();
	};
	
	this.clickTargetContinentsRdb = function(){
		utility.browserWait(sgmtTargetContinentsRdb,'sgmtTargetContinentsRdb');
		return sgmtTargetContinentsRdb.click();
	};
	
	this.clickTargetPostalCodesRdb = function(){
	utility.browserWait(sgmtTargetPostalCodesRdb,'sgmtTargetPostalCodesRdb');
		return sgmtTargetPostalCodesRdb.click();
	};
	
	this.selectGeoChkBox = function(){
		utility.browserWaitforseconds(2);
		utility.browserWait(sgmtSelectGeoChkBox,'sgmtSelectGeoChkBox');
		return sgmtSelectGeoChkBox.click();
	};
	
	this.enterPixelId = function(pixelId){
		return sgmtPixelIdTxtBox.sendKeys(pixelId);
		
	};
	
	this.isPixcelIdPresent=function(){
		return sgmtPixelIdTxtBox.isPresent();
	};
	this.selectCountryDropDown = function(country){
		return sgmtCountryDropDown.sendKeys(country);
	};
	
	this.enterEntries = function(zip){
		return sgmtEntriesTxtArea.sendKeys(zip);
	};
	this.clickApplyBtn = function(){
		utility.browserWait(sgmtApplyBtn,'sgmtApplyBtn');
		return sgmtApplyBtn.click();
	};
	
	this.clickSaveBtn = function(){
		utility.browserWait(sgmtSaveBtn,'sgmtSaveBtn');
		return sgmtSaveBtn.click();
	};
	
	this.clickCancelBtn = function(){
		return sgmtCancelBtn.click();
	};
	
	this.selectNagtedDropDown = function(value){
		utility.browserWait(sgmtNagtedDropDown,'sgmtNagtedDropDown');
		return sgmtNagtedDropDown.sendKeys(value);
	};
	
	this.selectDataContractDropDown = function(dataContract){
		sgmtDataContractDropDown.sendKeys(dataContract);
		return segmRuleTable.click();	
	};
	
	this.selectSegmentRuleTypeDropDown = function(segmentRuleType){
		utility.browserWait(sgmtSegmentRuleTypeDropDown,'sgmtSegmentRuleTypeDropDown');
		sgmtSegmentRuleTypeDropDown.sendKeys(segmentRuleType);
		return segmRuleTable.click();
	};
	
	this.selectFlexKeyDropDown = function(flexKey){
		sgmtFlexKeyDropDown.sendKeys(flexKey);
		return segmRuleTable.click();
	};
	
	this.selectFlexOptionsDropDown = function(flexOptions){
		utility.browserWait(sgmtFlexOptionsDropDown,'sgmtFlexOptionsDropDown');
		sgmtFlexOptionsDropDown.sendKeys(flexOptions);
		return segmRuleTable.click();
	};
	
	this.enterFlexKeyValueTxtArea = function(flexKeyValue){
		return sgmtFlexKeyValueTxtArea.sendKeys(flexKeyValue);
	};
	
	this.enterKeywordTxtArea = function(keyword){
		return sgmtKeywordTxtArea.sendKeys(keyword);
	};
	
	this.clickNextBtn = function(){
		return sgmtNextBtn.click();
	};
	
	this.getSegmentPageHeaderText = function(){
		utility.browserWait(segmentPageHeaderLocation,'segmentPageHeaderLocation');
		utility.browserWaitforseconds(2);
		return segmentPageHeaderLocation.getText().then(function(value){
			return value;		
		});
	};
	
	this.checkFilterFlipper = function() {
			return segmNewFilterMarket.isPresent();
	};
	
	this.clickFilterFlipper = function(){
		return segmClosedFlipper.isPresent().then(function(value){
			if(value){
				return segmClosedFlipper.click();
			}else{
				return false;
			}
		});
	};

	this.clickOnClearFilter = function(){
			return segmNewFilterMarket.isPresent().then(function(value){
				if(value){
					browser.driver.executeScript('arguments[0].style.opacity="1"',segmNewFilterClose.getWebElement()).then(function(){
						browser.driver.executeScript("arguments[0].click()",segmNewFilterClose.getWebElement());
					});
					
				}else{
					return false;
				}
				
			});	
	};
	
	this.clickOnSelectLink = function(){
			utility.browserWait(segNewFilterDropdown,'segNewFilterDropdown');
			return segNewFilterDropdown.click().then(function(){
				return segmMarketLink.click();
			});
	};
	
	this.enterMarketName = function(marketName){
			if(segmNewFilterMarket.isPresent()){
			return segmNewFilterMarket.sendKeys(marketName);
			}
	};
	
	
	this.clickOnMarketFilter = function(marketType){
			utility.browserWait(element(by.xpath("//a[contains(text(),'"+marketType+"')]")),'Market element filter');
			element(by.xpath("//a[contains(text(),'"+marketType+"')]")).click();
			return utility.browserWaitforseconds(4);
	};
	
	this.isNewSegmentBtnPresent = function(){
			return segmNewSegmentBtn.isPresent();
	};
	
	this.getMarketNameFromColumn = function(){
			browser.waitForAngular();
			utility.browserWait(segmNewMarketColoumn,'segmNewMarketColoumn');
			return segmNewMarketColoumn.getText();
	};
	
	this.clickSegmentNameColumn = function(){
			utility.browserWait(segmNewNameColumn,'segmNewNameColumn');
			return segmNewNameColumn.click();
	};
	
	this.returnReverseSegmentName = function(){
			return segmNewReverseSegmentLocation.isPresent();
	};
	
	this.segmentElementList=function(){
		return segmNewElementList;
	};
};

module.exports = new segmentPO();