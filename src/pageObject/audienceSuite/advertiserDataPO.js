/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

var AdvertiserDataPO = function() {

	var utilityObj = require('../../lib/utility.js');
	
    var advertisersTabLink = element(by.linkText('ADVERTISERS')); // main menu link of advertisers.
	var advertisersLink = element(by.linkText('Advertisers')); // sub link of main menu of advertisers.
	var newAdvesrtisers = element(by.linkText('New Advertiser'));// Link for page which creates new advertiser.
	var advertiserDataSubLink = element(by.linkText('Advertiser Data'));//sub link of main menu of advertiser data.
	var market = element(by.id('marketId'));// id for market name from dropdown
	var advertiserNameTextbox = element(by.id('advertiserName'));// id for advertiserName textbox.
	var advSearchText = element(by.id('search'));// Search textbox.
	var iconMenuAdvData = element(by.css('.img-container>img'));
	var iconMenuAdv = element(by.css('.icon-menu>img'));
	var advActionDropDown = element(by.css('.action-dropDown'));// location of action dropdown.
	var newAdvertiserActionLink = element(by.css('.options>li:nth-child(1)>a'));// Action dropdown Link for page which creates new advertiser.
	var advCancelButton = element(by.name('cancel'));// Cancel button of New Advertiser page.
	var advEditLink = element(by.linkText('Edit'));// Edit Advertiser link.
	var advViewLink = element(by.linkText('View'));// View Advertiser link.
	var advDeleteLink = element(by.linkText('Delete'));// Delete Advertiser link.
	var advexpectedAdvertiserDataName = element(by.css('.title-text'));// Advertiser name location of View advertiser page.
	var advShowDeletedLink = element(by.linkText('Show Deleted'));// Show Deleted link of action dropdown.
	var advPageHeaderLocation = element(by.css(".title-text.ng-binding,.titletext-wrapper>h2"));// Location of advertiser page header.
	var advHideDeletedLink = element(by.linkText('Hide Deleted')); // Hide Deleted link of action dropdown.
	var advFlipper = element(by.css('.adv-flipper'));// Location to open filter.
	var advSelectFilterLink = element(by.css('.selectFilterLink'));// Select market filter.
	var advInputMarket = element(by.css('.dhx_combo_input'));// Text box of market filter.
	var advClearFilter = element(by.css('#clearFilters>a'));// Clear filter link.
	var advMediaProvider = element(by.id('mediaProviderId0'));// Media Provider Dropdown.
	var advMediaProviderId = element(by.id('id0'));// Media provider Id Textbox
	var auditLogLink = element(by.linkText('View Audit Log'));
	var searchAuditLogTextBox = element(by.name('auditLogSearch'));
	var fieldNameLocation = element(by.css('.listView tr:nth-child(2) td:nth-child(4)'));
	var marketNameInListItem= element(by.css('#records-list>tbody>tr>td:nth-of-type(5)'));
	var advNameColumn =element(by.linkText('Name'));
	var advertiserDataLink =element(by.css('.order'));
	var advertiserDataButton =element(by.linkText('New Advertiser Data'));
	var advertiserDataName = element(by.id('advertiserDatumName'));
	var advertiserDataType = element(by.id('advertiserDatumTypeId'));
	var advDataPixelType = element(by.css('.select-dropdown.dataPixelType'));
	var advertriserCategory = element(by.id('optionsCheckBox-categories'));
	var importTaxonomy =  element(by.id('taxonomyManageLink'));
	var OnFrame = 'TB_window';
	var browserOnFrame = element(by.css('.textInput'));
	var applyButton = element(by.name('next'));
	var adveriserDataSaveButton =element (by.id('saveButton'));
	var advertriserKeyword = element(by.id('typeKeywords'));
	var obtainPixelsName = element(by.xpath('.//table[@class="titleBar2"]/tbody/tr/td/h3/div'));
	var advObtainLink = element(by.partialLinkText('Obtain Pixel'));
	var advObtainpixelclose = element(by.xpath('.//table[@class="wizardButtons"]/tbody/tr/td/input'));
	var advReverseAdvLocation= element(by.css('.selected.reverse'));
	var advAdvertiserSelectlink = element(by.xpath("//li[@id='advertiserFilterSelectContainer']/table/tbody/tr/td[1]/a"));
	var advDataClientPiggybacks = element(by.css('.form-nav-list>li:nth-child(2)>a'));
	var advDataCreatePiggybackButton = element(by.id('addAdditional'));
	var advDataCreateserverPiggybackButton = element(by.id('addAdditionalSS'));
	var severPiggybacks = element(by.css('.form-nav-list>li:nth-child(3)>a'));
	var advDataMediaProvider = element(by.name('piggybackMediaProvider1'));
	var advDataServerMediaProvider =element(by.xpath('.//div[@id="ssPiggyback1"]/div[1]/div[2]/select'));
	var advDataClientDataType = element(by.xpath('.//div[@id="piggyback1"]/div[3]/div[2]/select'));
	var advDataUrlTextBox = element(by.css('.piggybackDataUrl.form-text-box'));
	var advDataHTMLTextBox = element(by.name('piggybackDataHtml1'));
	var advDataSelectBeacon = element(by.xpath('.//div[@id="ssPiggyback1"]/div[3]/div[2]/select'));
	var advDataBeaconFrame = element(by.id('TB_window'));
	var advDataBeaconType = element(by.id('beaconType'));
	var advDataFrameApplyButton = element(by.xpath('.//div[@id="fieldContainer"]/div[3]/div[1]/input[2]'));
	var advDataAdvancedOptionLink = element(by.id('optionsLink-pixel'));
	var advDataOrderIdCheckButton = element(by.id('typeOrderId'));
	var advDataOptOutCheckButton = element(by.id('typeOptOut'));
	
	//New List Page
	
	var advDataNewsearchTxtBox=element(by.model("param"));
	var advDataNewActionDropDown=element(by.css(".btn.btn-dropdown.dropdown-toggle.pull-right.ng-binding"));
	var advDataActionNewAdvertiserDataLink=element(by.linkText('New Advertiser Data'));
	var advDataNewAdvertiserDataPlusBtn=element(by.css(".tif-plus.btn-action-plus.pull-left"));
	var advDataNewIconMenu=element(by.xpath("(//button[@type='button'])[position()=1]"));
	var advDataNewFilterButton=element(by.id('filter-dropdown'));
	var advDataSelectNewAdvertiserFilter = element(by.css('li:nth-of-type(1)>[ng-click="addFilter(filterKey)"]'));
	var advDataNewSelectFilterType=element(by.css('li:nth-of-type(2)>[ng-click="addFilter(filterKey)"]'));
	var advDataNewSelectMarket=element(by.id("tapMarketName"));
	var advDataNewSelectAdvertiser= element(by.id("tapAdvertiserName"));
	var advDataNewMarketNameInList=element(by.css(".smart-table-data-cell:nth-child(4)"));
	var advDataNewNameSorting=element(by.xpath("//tr[@class='smart-table-header-row']/th[2]"));
	var advDataReverseLocation = element(by.xpath("//th[@class='smart-table-header-cell sort-descent']"));
	var advDataNewUserbox=element(by.css('.user-wrapper'));
	var advDataNewElementList = element.all(by.xpath("//td[@ng-repeat='column in columns']/span|//header[@class='title-text ng-binding']"));
	var advNewElementList = element.all(by.xpath("//a[@class='click-column-link ng-binding']|//header[@class='title-text ng-binding']"));
	var advDataNewClearSearchIcon=element(by.css('.turn-clear'));
	var advDataNewConfirmDeleteAlert=element(by.css('[ng-click="deleteConfirm()"]'));
	var advDataNewClearFilter=element(by.css('.filter-element.ng-scope:nth-of-type(1)>div'));
	var advDataNewListPageRecord=element(by.xpath("(//td[@ng-repeat='column in columns'])[position()=1]"));
	var advNewListPageRecord=element(by.xpath("(//td[@ng-repeat='column in columns'])[position()=1]"));
	var advDataObtainPixelClose = element(by.xpath("//div[@id='obtainPixelModal']/div[3]/button"));
	var advDataNewObtainPixelName=element(by.xpath("//div[@id='obtainPixelModal']/div[1]/h4[2]"));
	var advNewClearFilter=element(by.css('[ng-click="removeFilter(filterKey)"]'));
	
	 this.AdvertiserElementList=function(){
		 return advNewElementList;
	 };
	 
	this.clickConfirmDeleteAlert=function() {
		utilityObj.browserWait(advDataNewConfirmDeleteAlert,'advDataNewConfirmDeleteAlert');
		return advDataNewConfirmDeleteAlert.click();
	};
	
	this.clickOnNewFilter=function(name) {
		utilityObj.browserWait(element(by.xpath("//a[text()='"+name+"']")),'filter page object');
		return element(by.xpath("//a[text()='"+name+"']")).click();
	};
	
	this.clickClearSearchIcon=function() {
		advDataNewClearSearchIcon.click();
	};
	
	 this.AdvertiserDataElementList=function() {
		 return advDataNewElementList;
	 };
	
	this.newUserBox=function() {
		utilityObj.browserWait(advDataNewUserbox,'advNewUserbox');
		return advDataNewUserbox.click();
	};
	
	this.selectMediaProvider = function(mediaProvider) {
		utilityObj.browserWait(advMediaProvider, 'advMediaProvider');
		return advMediaProvider.sendKeys(mediaProvider);
	};

	this.enterMediaProviderId = function(mediaProviderId) {
		utilityObj.browserWait(advMediaProviderId, 'advMediaProviderId');
		return advMediaProviderId.sendKeys(mediaProviderId);
	};

	this.clickAdvertiserMenuLink = function() {
		utilityObj.browserWait(advertisersTabLink, 'advertisersTabLink');
		return advertisersTabLink.click();
	};

	this.clickAdvertiserSubMenuLink = function() {
		utilityObj.browserWait(advertisersLink, 'advertisersLink');
		return advertisersLink.click();
	};
	
	this.clickAdvertiserDataSubLink = function() {
		utilityObj.browserWait(advertiserDataSubLink, 'advertiserDataSubLink');
		return advertiserDataSubLink.click();
	};

	this.clickNewFilterButton=function() {
		utilityObj.browserWait(advDataNewFilterButton,'advDataNewFilterButton');
		return advDataNewFilterButton.click();
	};
	
	this.selectNewMarketTypeFilter=function() {
		utilityObj.browserWait(advDataNewSelectFilterType,'advDataNewSelectFilterType');
		return advDataNewSelectFilterType.click();
	};
	
	this.selectNewAdvertiserTypeFilter=function() {
		utilityObj.browserWait(advDataSelectNewAdvertiserFilter,'advDataSelectNewAdvertiserFilter');
		return advDataSelectNewAdvertiserFilter.click();
	};
	
	this.isMarketFilterPresent=function(){
		return advDataNewSelectMarket.isPresent();
	};
	
	this.isAdvertiserFilterPresent=function(){
		return advDataNewSelectAdvertiser.isPresent();
	};
	
	this.enterNewMarketNameFilter=function(marketName) {
		utilityObj.browserWait(advDataNewSelectMarket,'advDataNewSelectMarket');
		advDataNewSelectMarket.clear();
		return advDataNewSelectMarket.sendKeys(marketName);
	};
	
	this.enterNewAdvetiserNameFilter=function(advertiserName) {
		utilityObj.browserWait(advDataNewSelectAdvertiser,'advDataNewSelectAdvertiser');
		return advDataNewSelectAdvertiser.sendKeys(advertiserName);
	};
	
	this.getAuditLogPageHeader=function(){
		utilityObj.browserWait(advPageHeaderLocation,"advPageHeaderLocation");
		return advPageHeaderLocation.getText();
    };
	
	this.getPageHeader = function() {
			utilityObj.browserWait(advPageHeaderLocation, "advPageHeaderLocation");
			return advPageHeaderLocation.getText();	
	};

	this.clickActionDropDown = function() {
			utilityObj.browserWait(advDataNewActionDropDown, "advDataNewActionDropDown");
			return advDataNewActionDropDown.click();		
	};

	this.clickNewAdvertiserActionMenu = function() {
			utilityObj.browserWait(advDataActionNewAdvertiserDataLink, "advDataActionNewAdvertiserDataLink");
			return advDataActionNewAdvertiserDataLink.click();
	};

	this.clickNewAdvertiserCancleButton = function() {
		utilityObj.browserWait(advCancelButton, "advCancelButton");
		return advCancelButton.click();
	};

	this.clickNewAdvertiserLink = function() {
			utilityObj.browserWait(advDataNewAdvertiserDataPlusBtn, 'advDataNewAdvertiserDataPlusBtn');
			return advDataNewAdvertiserDataPlusBtn.click();
	};

	this.selectMarket = function(marketName) {
		utilityObj.browserWait(market, 'market');
		return market.sendKeys(marketName);
	};

	this.enterAdvertiserName = function(advertiserName) {
		advertiserNameTextbox.clear();
		return advertiserNameTextbox.sendKeys(advertiserName);
	};

	this.clickSaveButton = function() {
		return advSaveButton.click();
	};

	this.clickIconMenuAdvData = function() {
			utilityObj.browserWait(advDataNewListPageRecord, 'advDataNewListPageRecord');
			utilityObj.browserWaitforseconds(2);
			advDataNewListPageRecord.click().then(function(){
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
			return browser.driver.executeScript('arguments[0].click()',advDataNewIconMenu.getWebElement());
			});
	};
	
	this.clickIconMenuAdv = function() {
			utilityObj.browserWait(advNewListPageRecord, 'advNewListPageRecord');
			utilityObj.browserWaitforseconds(2);
			advNewListPageRecord.click().then(function(){
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			browser.driver.executeScript('arguments[0].click()',advDataNewIconMenu.getWebElement());
			});
	};
	
	this.clickViewLink = function() {
		utilityObj.browserWait(advViewLink, 'advViewLink');
		return advViewLink.click();
	};

	this.clickEditLink = function() {
		utilityObj.browserWait(advEditLink, 'advertiseredit');
		return advEditLink.click();
	};

	this.getAdvertiserDataNameViewPage = function() {
		utilityObj.browserWait(advexpectedAdvertiserDataName,
				'advexpectedAdvertiserDataName');
		return advexpectedAdvertiserDataName.getText();
	};

	this.clickDeleteLink = function() {
		utilityObj.browserWait(advDeleteLink, 'advDeleteLink');
		return advDeleteLink.click();
	};

	this.returnSearchElement = function() {
			utilityObj.browserWait(advDataNewsearchTxtBox,"advDataNewsearchTxtBox");
			return advDataNewsearchTxtBox;
	};

	this.clearSearchBox = function() {
			utilityObj.browserWait(advDataNewsearchTxtBox,"advDataNewsearchTxtBox");
			advDataNewsearchTxtBox.clear();
			return advDataNewsearchTxtBox.sendKeys(protractor.Key.ENTER);
	};

	this.checkActionDropdownPresent = function() {
		return advActionDropDown.isPresent();
	};

	this.clickShowDeletedLink = function() {
		utilityObj.browserWait(advShowDeletedLink, 'advShowDeletedLink');
		return advShowDeletedLink.click();
	};

	this.iconMenuPresentAdv = function() {
		return iconMenuAdv.isPresent();
	};
	this.iconMenuPresentAdvData = function() {
		return iconMenuAdvData.isPresent();
	};

	this.clickAuditLogLink = function() {
		utilityObj.browserWait(auditLogLink, 'auditLogLink');
		return auditLogLink.click();
	};

	this.waitForHeaderText = function() {
		return utilityObj.browserWait(pageHeaderText, 'pageHeaderText');
	};

	this.waitForAuditLogSearchBox = function() {
		utilityObj.browserWaitforseconds(2);
		return searchAuditLogTextBox.isPresent();
	};

	this.returnAuditLogSearchElement = function(searchField) {
		searchAuditLogTextBox.sendKeys(searchField);
		searchAuditLogTextBox.sendKeys(protractor.Key.ENTER);
		
	};

	this.getFieldNameText = function() {
		return fieldNameLocation.getText();
	};

	this.clickHideDeletedLink = function() {
		utilityObj.browserWait(advHideDeletedLink, 'advHideDeletedLink');
		return advHideDeletedLink.click();
	};

	this.checkFilterFlipper = function() {
		return advFlipper.isPresent();
	};

	this.clickFilterFlipper = function() {
		return advFlipper.click();
	};

	this.clickOnClearFilter = function() {
			 browser.driver.executeScript('arguments[0].style.opacity=1',advNewClearFilter.getWebElement());
			 browser.driver.executeScript('arguments[0].click()',advNewClearFilter.getWebElement());		 
	};

	this.clickOnSelectLink = function() {
		utilityObj.browserWait(advSelectFilterLink, 'advSelectFilterLink');
		return advSelectFilterLink.click();
	};

	this.getMarketNameFromColumn = function() {
			utilityObj.browserWait(advDataNewMarketNameInList,'advDataNewMarketNameInList');
		    utilityObj.browserWaitforseconds(2);
			return advDataNewMarketNameInList.getText();
	};

	this.clickAdvertiserNameColumn = function() {
			utilityObj.browserWait(advDataNewNameSorting, 'advDataNewNameSorting');
			return advDataNewNameSorting.click();
	};

	this.returnReverseAdvName = function() {
			return advDataReverseLocation.isPresent();
	};

	this.clickAdvertiserLink = function() {
		utilityObj.browserWait(advertiserDataLink, 'advertiserDataLink');
		return advertiserDataLink.click();
	};

	this.clickAdvertiserDataButton = function() {
			utilityObj.browserWait(advDataNewAdvertiserDataPlusBtn, 'advDataNewAdvertiserDataPlusBtn');
			return advDataNewAdvertiserDataPlusBtn.click();
	};

	this.enterAdvertiserDataName = function(advertiser) {
		advertiserDataName.clear();
		return advertiserDataName.sendKeys(advertiser);
	};

	this.selectDataType = function(dataType) {
		utilityObj.browserWait(advertiserDataType, 'advertiserDataType', 2);
		return advertiserDataType.sendKeys(dataType);

	};

	this.selectCategory = function() {
		advertiserDataType.sendKeys(protractor.Key.TAB);
		utilityObj.browserWait(advertriserCategory, 'advertriserCategory');
		return advertriserCategory.click();
	};

	this.selectKeyword = function() {
		advertiserDataType.sendKeys(protractor.Key.TAB);
		utilityObj.browserWait(advertriserKeyword, 'advertriserKeyword');
		return advertriserKeyword.click();

	};
	this.importTaxonomy = function() {
		utilityObj.browserWait(importTaxonomy, 'importTaxonomy');
		return importTaxonomy.click();
	};

	this.switchToFrame = function() {
		utilityObj.switchToFrame(OnFrame);
	};

	this.clickOnBrowser = function() {
		//utilityObj.browserWait(browserOnFrame, 'browserOnFrame',30);
		browser.waitForAngular();
		return browserOnFrame.isPresent();
	};

	this.clickOnApplyButton = function() {
		utilityObj.browserWait(applyButton, 'applyButton');
		return applyButton.click();
	};

	this.clickAdvertiserDataSaveButton = function() {
		utilityObj.browserWait(adveriserDataSaveButton, 'adveriserDataSaveButton');
		return adveriserDataSaveButton.click();
	};
	
	this.getObtainPixelsName = function(){
			utilityObj.browserWait(advDataNewObtainPixelName,'advDataNewObtainPixelName');
			return advDataNewObtainPixelName.getText();
	};
	
	this.clickObtainPixels = function(){
		return advObtainLink.click();
	};
		
	this.clickCloseOnObtainPixels = function(){
			utilityObj.browserWait(advDataObtainPixelClose,'advDataObtainPixelClose');
			return advDataObtainPixelClose.click();
	};
	
	this.selectDataPixelType = function(dataPixel) {
	return advDataPixelType.sendKeys(dataPixel);

	};
	
	this.isFlipperPresent = function(){
		browser.waitForAngular();
		return advFlipper.isPresent();
	};
	
	this.clickOnSelectMarketLink = function(){
		utilityObj.browserWait(advSelectFilterLink,'advSelectFilterLink');
		return advSelectFilterLink.click();
	};
	
	this.enterFilterData = function(marketName){
		utilityObj.browserWait(advInputMarket,'advInputMarket');
		advInputMarket.sendKeys(marketName);
		return advInputMarket.sendKeys(protractor.Key.ENTER);
	};
	
	this.selectFilterData =function(marketType){
		utilityObj.browserWaitforseconds(3);
		element(by.xpath("//div[1]/div[contains(text(),'"+marketType +"')]")).click();
		utilityObj.browserWaitforseconds(3);
	};
	
	this.clickOnSelectAdvertiserLink = function(){
		utilityObj.browserWait(advAdvertiserSelectlink,'advAdvertiserSelectlink');
		return advAdvertiserSelectlink.click();
	};
	
	this.clickOnClientPiggybacksTab = function(){
		utilityObj.browserWait(advDataClientPiggybacks,'advDataClientPiggybacks');
		return advDataClientPiggybacks.click();
	};
	
	
	this.clickOnCreatePiggybackButton = function(){
		utilityObj.browserWait(advDataCreatePiggybackButton,'advDataCreatePiggybackButton');
		return advDataCreatePiggybackButton.click();
	};
	
	this.clickOnCreatePiggybackserverButton = function(){
		utilityObj.browserWait(advDataCreateserverPiggybackButton,'advDataCreateserverPiggybackButton');
		return advDataCreateserverPiggybackButton.click();
	};
	
	this.clickOnSeverPiggybacksTab = function(){
		utilityObj.browserWait(severPiggybacks,'severPiggybacks');
		return severPiggybacks.click();
	};
	
	this.selectMediaProvider = function(mediaProviderType){
		utilityObj.browserWait(advDataMediaProvider,'advDataMediaProvider');
		return advDataMediaProvider.sendKeys(mediaProviderType);
	};
	
	this.selectServerMediaProvider = function(mediaProviderType){
		utilityObj.browserWait(advDataServerMediaProvider,'advDataServerMediaProvider');
		return advDataServerMediaProvider.sendKeys(mediaProviderType);
	};
	
	this.selectClientDataType = function(clientDataType){
		utilityObj.browserWait(advDataClientDataType,'advDataClientDataType');
		return advDataClientDataType.sendKeys(clientDataType);
	};
	
	this.enterUrl = function(url){
		utilityObj.browserWait(advDataUrlTextBox,'advDataUrlTextBox');
		return advDataUrlTextBox.sendKeys(url);
	};
	
	this.enterHTML = function(htmlValue){
		utilityObj.browserWait(advDataHTMLTextBox,'advDataHTMLTextBox');
		return advDataHTMLTextBox.sendKeys(htmlValue);
	};
	
	this.selectBeacon = function(beacon){
		utilityObj.browserWait(advDataSelectBeacon,'advDataSelectBeacon');
		return advDataSelectBeacon.sendKeys(beacon);
	};
	
	this.switchOnBeaconFrame = function(){	
		return utilityObj.switchToFrame(advDataBeaconFrame);
	};
	
	this.selectBeaconTypeOnFrame = function(beaconType){
		utilityObj.browserWait(advDataBeaconType,'advDataBeaconType');
		return advDataBeaconType.sendKeys(beaconType);
	};
	this.clickOnFrameApplyButton = function(){
		utilityObj.browserWait(advDataFrameApplyButton,'advDataFrameApplyButton');
		advDataFrameApplyButton.click();
	};
		
	this.clickOnAdvanceOptionLink = function(){
		utilityObj.browserWait(advDataAdvancedOptionLink,'advDataAdvancedOptionLink');
		advDataAdvancedOptionLink.click();
	};
	
	this.clickOrderIdCheckButton = function(){
		utilityObj.browserWait(advDataOrderIdCheckButton,'advDataOrderIdCheckButton');
		advDataOrderIdCheckButton.click();
	};
	
	this.clickOptOutCheckButton = function(){
		utilityObj.browserWait(advDataOptOutCheckButton,'advDataOptOutCheckButton');
		advDataOptOutCheckButton.click();
	};
};

module.exports = new AdvertiserDataPO();
