/** Copyright (C) 2012 Turn, Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */

var LoginPO = function() {
	
	var utilityObj=require('../../lib/utility.js');
	
    var userNameTextBox = element(by.id('accountName'));
    var passwordTextBox = element(by.id('accountPassword'));
    var loginbutton = element(by.id('btnlogin'));   
	var audienceSuitProduct=element(by.xpath("//span[@class='suite box-sizing-fix dropdown-toggle ng-binding'][@title='Audience Suite']|//*[id='switch']"));
	var campaignSuitProduct=element(by.css('.label.selected[title="Campaign Suite"]'));
	var audienceSuiteLink=element(by.css('.options>li[data-value="2"]'));
	var campaignSuiteLink=element(by.linkText('Campaign Suite'));
    var logoutUserBoxAS=element(by.css(".user,.user-name,span[class='user dropdown-toggle ng-binding']"));
    var logoutUserBoxCS=element(by.css(".user,.user-name,span[class='user dropdown-toggle ng-binding']"));
    var logoutLink=element(by.linkText('Logout'));
    var loginLink = element(by.linkText('Login'));// login link text
    var campaignSuitProductCSPage=element(by.css('.selected[title="Campaign Suite"]'));
   
    //New list Page
    var logoutUserBoxNew=element(by.css(".user,.user-name,span[class='user dropdown-toggle ng-binding']"));
    var oldConsoleLink=element(by.css(".old-console-link:nth-child(1)"));
	var newConsoleLink=element(by.css(".turnbox>img"));
	var toggleOKButton=element(by.css(".consolelink"));
	var newConsoleToggleOkButton=element(by.xpath("//a[@class='btn btn-primary console-link ng-binding']"));
    var campaignsuiteWarningclose=element(by.css(".hideModal"));
    var campaignSuitePopupWindow=element(by.id("dialog-70"));
    
    this.warningPresent=function(){
    	return campaignSuitePopupWindow.isPresent();
    };
    
    this.warningClose=function(){
    	return campaignsuiteWarningclose.click();
    };
    
	this.isOldConsoleLinkDisplayed=function(){
		return oldConsoleLink.isPresent();
	};
	
	this.isNewConsoleLinkDisplayed=function(){
		return newConsoleLink.isPresent();
	};
	
	this.clickOldConsoleLink=function(){
		utilityObj.browserWait(oldConsoleLink,'oldConsoleLink');
		return oldConsoleLink.click();
	};
	
	this.clickNewConsoleLink=function()
	{
		utilityObj.browserWait(newConsoleLink,'newConsoleLink');
		return newConsoleLink.click();
	};
	
	this.clickToggleOkButton=function()
	{
		utilityObj.browserWait(toggleOKButton,'toggleOKButton');
		return toggleOKButton.click();
	};
	
	this.clickNewPageToggleOkButton=function()
	{
		utilityObj.browserWait(newConsoleToggleOkButton,'newConsoleToggleOkButton');
		return newConsoleToggleOkButton.click();
	};
	
    
    this.clickLoginLink=function() {
    	browser.waitForAngular();
    	if(loginLink.isPresent()){
		return loginLink.click();
    	}
    };
    
    this.isLoginLinkPresent=function() {
    	utilityObj.browserWait(loginLink,'loginLink');
		return loginLink.isPresent();
    };
    
    this.isLogoutLinkPresent=function() {    	
		return logoutLink.isPresent();
    };
    
    this.clickLogoutLink=function() {
    	return logoutLink.click();
    };
	
    /**
	 * This method is use to check logout UserBox  is present or not on Audience Suite/Campaign page.
	 * 
	 * @author narottamc
	 * @param projectName
	 * @returns boolean
	 */
    
	this.isLogoutUserBoxPresent=function(projectName){
		if(typeof projectName=='undefined'){
		projectName='AS';
		}
		if((projectName.toUpperCase()=='CS')||(projectName.toUpperCase()=='CAMPAIGN SUITE')||(projectName.toUpperCase()=='CAMPAIGNSUITE')){	
		return logoutUserBoxCS.isPresent();
	    }else{
		return logoutUserBoxAS.isPresent();
		}		
	};
	
	
	  /**
	 * This method is use to check logout UserBox  is present or not on Audience Suite/Campaign page with New list pages.
	 * 
	 * @author narottamc
	 * @param projectName
	 * @returns boolean
	 */
    
	this.isLogoutUserBoxPresentNew=function(projectName){
		if(typeof projectName=='undefined'){
		projectName='AS';
		}
		if((projectName.toUpperCase()=='CS')||(projectName.toUpperCase()=='CAMPAIGN SUITE')||(projectName.toUpperCase()=='CAMPAIGNSUITE'))
		{	
		utilityObj.browserWait(logoutUserBoxCS,'logoutUserBoxCS');
		return logoutUserBoxCS.isPresent();
		}
		else{
			return logoutUserBoxNew.isPresent();
		}
	};
	
	/**
	 * This method is use to logout from Audience Suite/Campaign Suite Project.
	 * 
	 * @author narottamc
	 * @param  projectName
	 */
	this.clickLogoutUserBox=function(projectName){
		if(typeof projectName=='undefined'){
			projectName='AS';
			}
		
		if((projectName.toUpperCase()=='CS')||(projectName.toUpperCase()=='CAMPAIGN SUITE')||(projectName.toUpperCase()=='CAMPAIGNSUITE'))
		{	
		utilityObj.browserWait(logoutUserBoxCS,'logoutUserBoxCS');
		return logoutUserBoxCS.click();
		}
		else{
			utilityObj.browserWait(logoutUserBoxNew,'logoutUserBoxNew');
			return logoutUserBoxNew.click();
		}
	};
	
	
	this.clickLogoutUserBoxCS=function(){
		utilityObj.browserWait(logoutUserBox,'logoutUserBox');
		return logoutUserBoxCS.click();
	};
	
	this.enterUserName = function(userName) {
		utilityObj.browserWait(userNameTextBox,'userNameTextBox');
		return userNameTextBox.sendKeys(userName);
	};
	
	this.enterPassword = function(password) {
		return passwordTextBox.sendKeys(password);
	};
	
	this.clickLoginButton = function() {
		return loginbutton.click();
	};
	
	this.returnAudienceSuitProduct = function() {
		return audienceSuitProduct;
	};
	
	this.returnCampaignSuitProduct = function() {
		return campaignSuitProduct;
	};
	
	this.returnAudienceSuiteLink = function() {
		return audienceSuiteLink;
	};
	this.returnCampaignSuiteLink=function()
	{
		return campaignSuiteLink;
	};
	
	this.isAudienceSuiteLinkPresent = function() {
		browser.waitForAngular();
		return audienceSuitProduct.isPresent();
	};
	
	this.isCampaignSuiteLinkPresent=function(){
		browser.waitForAngular();
		utilityObj.browserWait(campaignSuitProductCSPage,'campaignSuitProductCSPage');
		return campaignSuitProductCSPage.isPresent();
	};
	
	this.isUserNameTextBoxPresent = function() {
		utilityObj.browserWait(userNameTextBox,'userNameTextBox',30);
		return userNameTextBox.isPresent();
	};
};

module.exports = new LoginPO();