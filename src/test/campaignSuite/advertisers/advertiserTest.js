describe('Turn Campaign suite', function() {
	 var Utility= require('../../../lib/utility.js');
	 
	
	beforeEach(function() {
		  isAngularSite(false);
		 protract=protractor.getInstance();
		  browser.waitForAngular();
	 	});
	
	 
var loginbase = require('../../../base/campaignSuite/loginBase.js');
var advbase = require('../../../base/campaignSuite/advertiserBase.js');
var turnDataFilter=require('../../../lib/turnDataProvider.js');
var loginDataPath='./src/testData/campaignSuite/login.json';
var advertiserDataPath='./src/testData/campaignSuite/advertiser.json';

  it('Turn login page', function() {
	  browser.ignoreSynchronization = true;
	 var data = turnDataFilter.getDataProvider(loginDataPath,'User',browser.user);	  
	 
	
	 Utility.geturl(data.url);
	
	 browser.waitForAngular();
	 loginbase.login(data);	
	 browser.waitForAngular();
	// Utility.uploadFile("test","../testData/campaignSuite/login-2.json","test2");
	// Utility.uploadFile("test","..\\testData\\campaignSuite\\login-2.json","test2");
	 	
  });
  it('redirection',function(){
	  browser.ignoreSynchronization = true;
	  advbase.gotoAdvertiser();
	  
  });
  it('redirect to create advertiser tab',function(){
	  browser.ignoreSynchronization = true;
	 
	    console.log("Redirection completed");
	    advbase.redirectToNewadvertiser();
	   
    
  });
  
  
  it('create advertiser',function(){
	  browser.ignoreSynchronization = false;
	
	    console.log("In Create Advertiser tab ");
	   advbase.createNewadvertiser(advertiserDataPath,browser.language,'createadvertiser');
	   
	   
    
  });
  
  it('serach advertiser',function(){
	  browser.ignoreSynchronization = true;
	 var advData = turnDataFilter.getDataProvider(advertiserDataPath,browser.language,'createadvertiser');
	    console.log("In Create Advertiser tab ");
	   advbase.searchAdvertiser(advData);
    
  });
  

  
});
 
 
 