/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

	/**
	 * This test suite is use to check advertiser permission for segment tab.
	 * 
	 * @author hemins
	 */
	describe("Advertiser Permission for segment Tab:"+browser.user,function(){
		
		var pathObj  =require( '../../../testData/audienceSuite/path.js');
		var utilityObj = require('../../../lib/utility.js');
		var loginBaseObj = require('../../../base/audienceSuite/loginBase.js');
		var segmentBaseObj = require('../../../base/audienceSuite/segmentBase.js');
		var turnDataFilterObj = require('../../../lib/turnDataProvider.js');
		var assertsObj = require('../../../lib/assert.js');
		var loggerObj = require('../../../lib/logger.js');
		var logger=loggerObj.loggers("segmentTest");
		var segmentPath=pathObj.getSegmentTestDataFilePath();
		
		beforeEach(function() {
			isAngularSite(false);
			actualValue='';
			expectedValue='';
		});
		browser.ignoreSynchronization = true;		
	if((browser.user).toUpperCase()=='CS'){
		/**
		 * This method is use to login with whitelisted user and verify advertiser permission for segment tab.
		 * 
		 * @author hemins
		 */
		it("Check Advertiser Permission for segment tab by MMW user Segments tab",function(){
			logger.info("Checking Advertiser Permission for MMW user in Segments tab.");
			var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language,'MMW');
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			loginBaseObj.login(loginTestdata);
			browser.waitForAngular();
			loginBaseObj.isLogoutUserBoxPresent("MMW");
			browser.waitForAngular();
			var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(segmentPath,browser.env, 'MMW', 'advertiserPermission');
			loginBaseObj.redirectToAudienceSuite();
			browser.waitForAngular();
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
	    	utilityObj.browserWaitforseconds(3);
			segmentBaseObj.redirectToSegmentTab();
			browser.waitForAngular();	
					utilityObj.findList(segmentBaseObj.returnSearchElement(),advertiserPermissionTestData.whitelistedAdvertiser);
					segmentBaseObj.returnElementList().map(function (elm) {
					   return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListContain(result,advertiserPermissionTestData.whitelistedAdvertiser,"White listed advertiser's segment("+advertiserPermissionTestData.whitelistedAdvertiser+") is not found in search list for MMW user.");
					});
					utilityObj.findList(segmentBaseObj.returnSearchElement(),advertiserPermissionTestData.blacklistedAdvertiser);
					segmentBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
				assertsObj.assertListNotContain(result,advertiserPermissionTestData.blacklistedAdvertiser,"Black listed advertiser's segment("+advertiserPermissionTestData.blacklistedAdvertiser+") is found in search list for MMB user.");
				});
			loginBaseObj.logout('MMW');
		});
		
		/**
		 * This method is use to login with blacklisted user and verify advertiser permission for segment tab.
		 * 
		 * @author hemins
		 */
		it("Check Advertiser Permission for segment tab by MMB user  Segments tab",function(){
			logger.info("Checking Advertiser Permission for MMB user in Segments tab.");
			var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env,browser.language, 'MMB');
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			loginBaseObj.login(loginTestdata);
			browser.waitForAngular();
			loginBaseObj.isLogoutUserBoxPresent("MMB");
			browser.waitForAngular();
			var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(segmentPath, browser.env,'MMB', 'advertiserPermission');
			loginBaseObj.redirectToAudienceSuite();
			browser.waitForAngular();
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
	    	utilityObj.browserWaitforseconds(3);
			segmentBaseObj.redirectToSegmentTab();
			browser.waitForAngular();
					utilityObj.findList(segmentBaseObj.returnSearchElement(),advertiserPermissionTestData.whitelistedAdvertiser);
					segmentBaseObj.returnElementList().map(function (elm) {
					   return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListContain(result,advertiserPermissionTestData.whitelistedAdvertiser,"White listed advertiser's segment("+advertiserPermissionTestData.whitelistedAdvertiser+") is not found in search list for MMB user.");
					});
					utilityObj.findList(segmentBaseObj.returnSearchElement(),advertiserPermissionTestData.blacklistedAdvertiser);
					segmentBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
				assertsObj.assertListNotContain(result,advertiserPermissionTestData.blacklistedAdvertiser,"Black listed advertiser's segment("+advertiserPermissionTestData.blacklistedAdvertiser+") is found in search list for MMB user.");
				});
			loginBaseObj.logout('MMB');
		});
		
		/**
		 * This method is use to login with All Advertiser user and verify advertiser permission for segment tab.
		 * 
		 * @author hemins
		 */
		it("Check Advertiser Permission for segment tab by MMA user  Segments tab",function(){
			logger.info("Checking Advertiser Permission for MMA user in Segments tab.");
			var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, 'MMA');
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			loginBaseObj.login(loginTestdata);
			browser.waitForAngular();
			loginBaseObj.isLogoutUserBoxPresent("MMA");
			browser.waitForAngular();
			var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(segmentPath, browser.env,'MMA', 'advertiserPermission');
			loginBaseObj.redirectToAudienceSuite();
			browser.waitForAngular();
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
	    	utilityObj.browserWaitforseconds(3);
			segmentBaseObj.redirectToSegmentTab();
			browser.waitForAngular();
			var advertisers=(advertiserPermissionTestData.whitelistedAdvertiser).split(";");
			var j=0;
			for(var i=0;i<advertisers.length;i++){
						utilityObj.findList(segmentBaseObj.returnSearchElement(),advertisers[i]);
						segmentBaseObj.returnElementList().map(function (elm) {
						   return elm.getText();
						    }).then(function(result){
						    	console.log(result);
						    	assertsObj.assertListContain(result,advertisers[j],"White listed advertiser's segment("+advertisers[j]+") is not found in search list for All Advertiser Permission user.");
						    	j++;
						    });
					} 
			loginBaseObj.logout('MMA');
		});
		
		/**
		 * This method is use to login with Cadmin user and verify advertiser permission for segment tab.
		 * 
		 * @author hemins
		 */			
		it("Check Advertiser Permission for segment tab by CAdmin user  Segments tab",function(){
			logger.info("Checking Advertiser Permission for CAdmin user in Segments tab.");
			var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, 'CADMIN');
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			loginBaseObj.login(loginTestdata);
			browser.waitForAngular();
			loginBaseObj.isLogoutUserBoxPresent("Cadmin");
			browser.waitForAngular();
			var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(segmentPath,browser.env, 'CADMIN', 'advertiserPermission');
			loginBaseObj.redirectToAudienceSuite();
			browser.waitForAngular();
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
	    	utilityObj.browserWaitforseconds(3);
			segmentBaseObj.redirectToSegmentTab();
			browser.waitForAngular();
			var advertisers=(advertiserPermissionTestData.whitelistedAdvertiser).split(";");
			var j=0;
			for(var i=0;i<advertisers.length;i++){
					 browser.waitForAngular();
						utilityObj.findList(segmentBaseObj.returnSearchElement(),advertisers[i]);
						utilityObj.browserWaitforseconds(1);
						segmentBaseObj.returnElementList().map(function (elm) {
						return elm.getText();
						    }).then(function(result){
						    	assertsObj.assertListContain(result,advertisers[j],"White listed advertiser's segment("+advertisers[j]+") is not found in search list for Cadmin user.");
						    	j++;
						    });
					} 
			loginBaseObj.logout('CAdmin');
		});		
	}
	else{
		logger.info("Advertiser Permission TestCase : Skipped,because UserType is Non 'CS'.");
	};
	});