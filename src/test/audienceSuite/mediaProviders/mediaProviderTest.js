/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

describe('Turn audience suite Media Provider Tab:'+browser.user,function(){
	var pathObj  =require( '../../../testData/audienceSuite/path.js');
	var utilityObj = require('../../../lib/utility.js');
	var loginBaseObj = require('../../../base/audienceSuite/loginBase.js');
	var mediaProviderBaseObj = require('../../../base/audienceSuite/mediaProviderBase.js');
	var turnDataFilterObj = require('../../../lib/turnDataProvider.js');
	var assertsObj = require('../../../lib/assert.js');
	var loggerObj = require('../../../lib/logger.js');
	var logger=loggerObj.loggers("mediaProviderTest");
	var actualValue ;
	var expectedValue;
	
	beforeEach(function() {
		isAngularSite(false);
		actualValue='';
		expectedValue='';
	});
	browser.ignoreSynchronization = true;			
	
	/**
	 * This method used to enter Url and Login details. 
	 * 
	 * @author narottamc
	 */
	it('Turn login page media provider tab:'+browser.user, function() {
		var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env,browser.language,browser.user);
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		loginBaseObj.login(loginTestdata);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(4);
		assertsObj.assertTrue(loginBaseObj.isLogoutUserBoxPresent(browser.user),'Logout UserBox is not available.');		
	});
	
	/**
	 * This method used for redirect to Audience suite Project. 
	 * 
	 * @author narottamc
	 */
	it('Redirect to Audience Suite Project media provider tab:'+browser.user, function() {
		loginBaseObj.redirectToAudienceSuite();
		browser.waitForAngular();
		var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env,browser.language,browser.user);
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
    	utilityObj.browserWaitforseconds(3);
		assertsObj.assertTrue(loginBaseObj.checkAudienceSuite(),"Not redirect to Audience Suite");
	});
	
	/**
	 * This method used to call createNewMediaProvider & deleteCreatedMediaProvider method of
	 * mediaProviderBase JS File.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	it('Create Media Provider with Non Turn market and Delete the created media provider:'+browser.user, function() {
		var mediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaprovidernonturnmarket');
		mediaProviderBaseObj.gotoMediaProviderTab();
		browser.waitForAngular();
		mediaProviderBaseObj.newMediaProviderWithCancelButton();
		browser.waitForAngular();
		var mediaProviderName = mediaProviderTestData.mediaProviderName + utilityObj.getTimeStamp();
		browser.waitForAngular();
		mediaProviderBaseObj.createNewMediaProvider(mediaProviderTestData,mediaProviderName, "Non Turn Market");
		browser.waitForAngular();
			utilityObj.findList(mediaProviderBaseObj.returnSearchElement(),mediaProviderName);
			browser.waitForAngular();
	    	utilityObj.browserWaitforseconds(1);
			mediaProviderBaseObj.returnElementList().map(function(elm){
				return elm.getText();
			    	}).then(function(result){
			    		assertsObj.assertListContain(result,mediaProviderName,'Non Turn Markets type Media Provider is not found in search list.');
			    	});
			mediaProviderBaseObj.deleteCreatedMediaProvider();
		
		});
	
	/**
	 * This method used to call createNewMediaProvider method of
	 * mediaProviderBase JS File.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	it('Create Media Provider with Turn market:'+browser.user, function() {
		var mediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
			mediaProviderBaseObj.gotoMediaProviderTab();
				var mediaProviderName = mediaProviderTestData.mediaProviderName + utilityObj.getTimeStamp();
				mediaProviderBaseObj.createNewMediaProvider(mediaProviderTestData,mediaProviderName,"Turn Market");
			utilityObj.findList(mediaProviderBaseObj.returnSearchElement(),mediaProviderName);
			browser.waitForAngular();
	    	utilityObj.browserWaitforseconds(1);	
			mediaProviderBaseObj.returnElementList().map(function(elm){
					return elm.getText();
				}).then(function(result){
					assertsObj.assertListContain(result,mediaProviderName,' Turn Markets type Media Provider is not found in search list.');
				});
		});
	
	/**
	 * This method used to call editCreatedMediaProvider method of
	 * mediaProviderBase JS File.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	it('Edit Created Media provider:'+browser.user, function() {
		var mediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
		mediaProviderBaseObj.gotoMediaProviderTab();
		utilityObj.findList(mediaProviderBaseObj.returnSearchElement(),mediaProviderTestData.createdMediaProviderName);
		var updatedMediaProviderName=mediaProviderTestData.createdMediaProviderName+'_Edit';
		mediaProviderBaseObj.editCreatedMediaProvider(mediaProviderTestData,updatedMediaProviderName);
				utilityObj.findList(mediaProviderBaseObj.returnSearchElement(),updatedMediaProviderName);
				browser.waitForAngular();
		    	utilityObj.browserWaitforseconds(1);
				mediaProviderBaseObj.returnElementList().map(function (elm) {
					return elm.getText();
				    	}).then(function(result){
				    		assertsObj.assertListContain(result,updatedMediaProviderName,"Edited Media Provider name:"+updatedMediaProviderName+" is not found in search list.");
				    	});
			});
	
	/**
	 * This method used to call viewCreatedMediaProvider method of
	 * mediaProviderBase JS File.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	it('View Created Media provider:'+browser.user, function(){
		var mediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
		mediaProviderBaseObj.gotoMediaProviderTab();
		utilityObj.findList(mediaProviderBaseObj.returnSearchElement(),mediaProviderTestData.updatedMediaProviderName);
		mediaProviderBaseObj.gotoViewMediaProvider();
		actualValue=mediaProviderBaseObj.viewCreatedMediaProvider(mediaProviderTestData.updatedMediaProviderName);
		expectedValue=mediaProviderTestData.updatedMediaProviderName;
		assertsObj.assertEqual(actualValue,expectedValue,
		" Actual Media Provider name:"+actualValue+" is not matching with Expected Media Provider name"+expectedValue+" in view Media Provider page.");
		utilityObj.browserWaitforseconds(1);
		mediaProviderBaseObj.gotoMediaProviderTab();
	});
		      
    /**
	 * This method is used to call expandmediaproviders method of mediaProviderBase JS file.
	 * 
	 * @author vishalbha
	 * @updated vishalbha
	 */
   it('Expand/Collapse/View Configuration  functionality Media Provider:'+browser.user,function () {
     var mediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
     mediaProviderBaseObj.gotoMediaProviderTab();
     browser.waitForAngular();
     utilityObj.findList(mediaProviderBaseObj.returnSearchElement(),mediaProviderTestData.updatedMediaProviderName);
     utilityObj.browserWaitforseconds(1);
    	 mediaProviderBaseObj.viewConfigurationMediaProvider(mediaProviderTestData);
     	 assertsObj.assertTrue(mediaProviderBaseObj.checkViewConfigurationCloseButton(),'Close button not found on Data contract->\'View Configuraion\' page');
     	mediaProviderBaseObj.clickMediaProviderViewConfigurationCloseButton();
   	});
   
  	/**
	 * This method used to call deleteCreatedMediaProvider method of
	 * mediaProviderBase JS File.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	it('Delete Created Media provider:'+browser.user, function() {
		var mediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
		mediaProviderBaseObj.gotoMediaProviderTab();
		utilityObj.findList(mediaProviderBaseObj.returnSearchElement(),mediaProviderTestData.updatedMediaProviderName);
		mediaProviderBaseObj.deleteCreatedMediaProvider();
		browser.waitForAngular();
				utilityObj.findList(mediaProviderBaseObj.returnSearchElement(),mediaProviderTestData.updatedMediaProviderName);
				browser.waitForAngular();
		    	mediaProviderBaseObj.returnElementList().map(function (elm){
						return elm.getText();
				    	}).then(function(result){
				    		assertsObj.assertListNotContain(result,mediaProviderTestData.updatedMediaProviderName,"Deleted Media Provider name:"+mediaProviderTestData.updatedMediaProviderName+" is found in search list.");	
				    	});
		browser.waitForAngular();
	 });
	
	 /**
	 * This method used to call clickShowDeletedMediaProvider method of
	 * mediaProviderBase JS File.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
    it('Show Deleted Media Provider:'+browser.user, function(){
    	var mediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
		mediaProviderBaseObj.gotoMediaProviderTab();	
    	mediaProviderBaseObj.clickShowDeletedMediaProvider();
       	browser.waitForAngular();
		utilityObj.findList(mediaProviderBaseObj.returnSearchElement(),mediaProviderTestData.updatedMediaProviderName);
		browser.waitForAngular();
    	utilityObj.browserWaitforseconds(1);	
		mediaProviderBaseObj.returnElementList().map(function (elm) {
				return elm.getText();
				}).then(function(result){
					assertsObj.assertListContain(result,mediaProviderTestData.updatedMediaProviderName,'Show deleted:Deleted Media Provider Name:"+mediaProviderTestData.updatedMediaProviderName+" is not found in list.');
				});
		browser.waitForAngular();
    	});
    
    /**
	 * This method used to call gotoMediaProviderViewAuditLog method of
	 * mediaProviderBase JS File.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	it('View Media Provider Audit Log:'+browser.user, function() {
		var mediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
		utilityObj.findList(mediaProviderBaseObj.returnSearchElement(),mediaProviderTestData.updatedMediaProviderName);
		mediaProviderBaseObj.gotoMediaProviderViewAuditLog();
		isAngularSite(false);	
		mediaProviderBaseObj.searchMediaProviderAuditLog(mediaProviderTestData.auditLogSearch);
		browser.waitForAngular();
    	utilityObj.browserWaitforseconds(1);
		actualValue=mediaProviderBaseObj.getFieldNameValue();
		expectedValue=mediaProviderTestData.auditLogSearch;
		assertsObj.assertEqual(actualValue,expectedValue,"Media Provider Actual Field name:"+actualValue+" is not matching with expected:"+expectedValue+"  for audit log"); 		
		utilityObj.switchToMainWindow();
	});
	
	/**
	 * This method used to call gotoMediaProviderObtainPixels & returnMediaProviderObtainPixelHeaderName method of
	 * mediaProviderBase JS File.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	 it('Obtain Pixel Media Provider:'+browser.user, function() {	
	    	var mediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
	    	utilityObj.findList(mediaProviderBaseObj.returnSearchElement(),mediaProviderTestData.updatedMediaProviderName);
	    	mediaProviderBaseObj.gotoMediaProviderObtainPixels();
	    	actualValue=mediaProviderBaseObj.returnMediaProviderObtainPixelHeaderName(mediaProviderTestData);
	    	expectedValue=mediaProviderTestData.obtainPixelsName;
	    	assertsObj.assertEqual(actualValue,expectedValue,"Media Provider Actual Obtain pixel :"+ actualValue+ "is not matching with"+expectedValue+" on obtain pixel page.");			
		});
	
	 /**
	 * This method used to call clickHideDeletedMediaProvider method of
	 * mediaProviderBase JS File.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
    it('Hide Deleted Media Provider:'+browser.user, function() {
    	var mediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
    	mediaProviderBaseObj.clickHideDeletedMediaProvider();
  				utilityObj.findList(mediaProviderBaseObj.returnSearchElement(),mediaProviderTestData.updatedMediaProviderName);
				browser.waitForAngular();
		    	utilityObj.browserWaitforseconds(1);
				mediaProviderBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	console.info("result :"+ result);
				    	console.info("mediaProviderTestData.updatedMediaProviderName :"+ mediaProviderTestData.updatedMediaProviderName);
			assertsObj.assertListNotContain(result,mediaProviderTestData.updatedMediaProviderName,'Hide deleted:Deleted Media Provider Name:"+mediaProviderTestData.updatedMediaProviderName+" is not found in list.');
				    });
		});
   
    
    /**
	 * This method used to call sortByMediaProviderName & assertMediaProviderNameColumn method of
	 * mediaProviderBase JS File.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
    it('Sorting functionality of Media Provider:'+browser.user, function() {
    	mediaProviderBaseObj.gotoMediaProviderTab();
    	browser.waitForAngular();
    	utilityObj.browserWaitforseconds(3);
    	mediaProviderBaseObj.clearSearchMediaProvider();
    	browser.waitForAngular();
    	utilityObj.browserWaitforseconds(3);
    	mediaProviderBaseObj.sortByMediaProviderName();
    	browser.waitForAngular();
    	utilityObj.browserWaitforseconds(3);
		assertsObj.assertNotNull(mediaProviderBaseObj.assertMediaProviderNameColumn(),"Sorting on Media Provider name column is not perform.");	
    });
    
    
    /**
	 * This method used to call selectMarketFilterMediaProvider & getMarketNameMediaProvider method of
	 * mediaProviderBase JS File.
	 * 
	 * @author narottamc
	 */
    it('Filter Operation of Media Provider:'+browser.user, function() {
    	mediaProviderBaseObj.gotoMediaProviderTab();
    	var userType = browser.user;
    	if(userType.toUpperCase()=='CS'){
    	browser.waitForAngular();
    	utilityObj.browserWaitforseconds(3);
    	var mediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
    		mediaProviderBaseObj.selectMarketFilterMediaProvider(mediaProviderTestData.market);
			actualValue=mediaProviderBaseObj.getMarketNameMediaProvider();
			expectedValue=mediaProviderTestData.market;	
			assertsObj.assertEqual(actualValue,expectedValue,
					"Acutal Market filter:'"+actualValue+"'is not matching with expected filter:'"+expectedValue+"'in Media Provider");
			}
			else{
				logger.info(userType + " user doesnot contain market filter.");
			};  	
    	});
    
    /**
	 * This method is used to call logout method of
	 * mediaProviderBase JS File.
	 * 
	 * @author narottamc
	 * @updated vishalbha
	 */
	it(' Turn Logout media provider tab:'+browser.user,function(){
		loginBaseObj.logout(browser.user);
		  isAngularSite(false);
		assertsObj.assertTrue(loginBaseObj.isUserNamePresent(),"Username Textbox is not found for login page.");
	});
});
