/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

describe('Turn audience suite Cookie Space Media Providers Tab:'+browser.user,function(){
	var pathObj  =require( '../../../testData/audienceSuite/path.js');
	var utilityObj = require('../../../lib/utility.js');
	var loginBaseObj = require('../../../base/audienceSuite/loginBase.js');
	var cookieSpaceMediaProvidersBaseObj = require('../../../base/audienceSuite/cookieSpaceMediaProvidersBase.js');
	var turnDataFilterObj = require('../../../lib/turnDataProvider.js');
	var assertsObj = require('../../../lib/assert.js');
	var loggerObj = require('../../../lib/logger.js');
	var logger=loggerObj.loggers("CookieSpacemediaProviderTest");
	
	var actualValue ;
	var expectedValue;
	beforeEach(function() {
		isAngularSite(false);		
		actualValue='';
		expectedValue='';
	});
	browser.ignoreSynchronization = true;			
	
	if(browser.user.toUpperCase()=="CS"){
		/**
		 * This method used to enter Url and Login details. 
		 * 
		 * @author narottamc
		 */
		it('Turn login page CookieSpaceMediaProvider tab:'+browser.user, function() {
			var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env,browser.language,browser.user);
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			loginBaseObj.login(loginTestdata);
			utilityObj.browserWaitforseconds(5);			
			assertsObj.assertTrue(loginBaseObj.isLogoutUserBoxPresent(browser.user),'Logout UserBox is not available.');
		});
		
		/**
		 * This method used for redirect to Audience suite Project. 
		 * 
		 * @author hemins
		 */
		it('Redirect to Audience Suite Project CookieSpaceMediaProvider tab:'+browser.user, function() {
			loginBaseObj.redirectToAudienceSuite();
			browser.waitForAngular();
			var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env,browser.language,browser.user);
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			assertsObj.assertTrue(loginBaseObj.checkAudienceSuite(),"Not redirect to Audience Suite");
		});
		
		/**
		 * This method used to call createNewCookieSpaceMediaProviders method of
		 * cookieSpaceMediaProviderBase JS File.
		 * 
		 * @author hemins
		 */
		it('Create Cookie Space Media Provider with Turn market: '+browser.user, function() {
			var cookieSpaceMediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getCookieSpaceMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
			cookieSpaceMediaProvidersBaseObj.redirectToCookieSpaceMediaProvidersTab();
			cookieSpaceMediaProvidersBaseObj.newMediaProviderWithCancelButton();
			var cookieSpaceMediaProviderName = cookieSpaceMediaProviderTestData.mediaProviderName + utilityObj.getTimeStamp();
			cookieSpaceMediaProvidersBaseObj.createNewCookieSpaceMediaProviders(cookieSpaceMediaProviderTestData,cookieSpaceMediaProviderName,'createmediaproviderturnmarket');
			utilityObj.findList(cookieSpaceMediaProvidersBaseObj.returnSearchElement(),cookieSpaceMediaProviderName);
			cookieSpaceMediaProvidersBaseObj.returnElementList().map(function (elm) {
			    return elm.getText();
			    }).then(function(result){
			    	assertsObj.assertListContain(result,cookieSpaceMediaProviderName,"Created TURN Market Cookie Space Media Provider name:"+cookieSpaceMediaProviderName+" is not found in search list.");
			});
		});
		
		/**
		 * This method used to call editCookieSpaceMediaProvider method of
		 * cookieSpaceMediaProviderBase JS File.
		 * 
		 * @author hemins
		 */
		it('Edit Cookie Space Media provider: '+browser.user, function() {
			var cookieSpaceMediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getCookieSpaceMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
			cookieSpaceMediaProvidersBaseObj.redirectToCookieSpaceMediaProvidersTab();
			utilityObj.findList(cookieSpaceMediaProvidersBaseObj.returnSearchElement(),cookieSpaceMediaProviderTestData.createdMediaProviderName);
			updatedCookieSpaceMediaProviderName=cookieSpaceMediaProviderTestData.createdMediaProviderName+'_Edit';
			cookieSpaceMediaProvidersBaseObj.editCookieSpaceMediaProvider(cookieSpaceMediaProviderTestData,updatedCookieSpaceMediaProviderName,'createmediaproviderturnmarket');
			utilityObj.findList(cookieSpaceMediaProvidersBaseObj.returnSearchElement(),updatedCookieSpaceMediaProviderName);
			cookieSpaceMediaProvidersBaseObj.returnElementList().map(function (elm) {
			    return elm.getText();
			    }).then(function(result){
			    	assertsObj.assertListContain(result,updatedCookieSpaceMediaProviderName,"Edited TURN Market Cookie Space Media Provider name:"+updatedCookieSpaceMediaProviderName+" is not found in search list.");
			});
		});
		
		 /**
		 * This method is used to call viewConfigurationMediaProvider method of mediaProviderBase JS file.
		 * 
		 * @author hemins
		 */
	    it('View Configuration  functionality Cookie Space Media Provider:'+browser.user,function () {
		     var cookieSpaceMediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getCookieSpaceMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
		     cookieSpaceMediaProvidersBaseObj.redirectToCookieSpaceMediaProvidersTab();
		     browser.waitForAngular();
		     utilityObj.findList(cookieSpaceMediaProvidersBaseObj.returnSearchElement(),cookieSpaceMediaProviderTestData.updatedMediaProviderName);
		     utilityObj.browserWaitforseconds(1);     
		     cookieSpaceMediaProvidersBaseObj.viewConfigurationMediaProvider(cookieSpaceMediaProviderTestData);
		     assertsObj.assertTrue(cookieSpaceMediaProvidersBaseObj.checkViewConfigurationCloseButton(),'Close button not found on Cookie Space Media Provider->\'View Configuraion\' page');
		     cookieSpaceMediaProvidersBaseObj.clickMediaProviderViewConfigurationCloseButton();
	   	});
		
		/**
		 * This method used to call deleteCookieSpaceMediaProvider method of
		 * cookieSpaceMediaProviderBase JS File.
		 * 
		 * @author hemins
		 */
		it('Delete Cookie Space Media provider:'+browser.user, function() {
			var cookieSpaceMediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getCookieSpaceMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
			cookieSpaceMediaProvidersBaseObj.redirectToCookieSpaceMediaProvidersTab();
			utilityObj.findList(cookieSpaceMediaProvidersBaseObj.returnSearchElement(),cookieSpaceMediaProviderTestData.updatedMediaProviderName);
			cookieSpaceMediaProvidersBaseObj.deleteCookieSpaceMediaProvider(cookieSpaceMediaProviderTestData);
			utilityObj.findList(cookieSpaceMediaProvidersBaseObj.returnSearchElement(),cookieSpaceMediaProviderTestData.updatedMediaProviderName);
			cookieSpaceMediaProvidersBaseObj.returnElementList().map(function (elm) {
			    return elm.getText();
			    }).then(function(result){
			    	assertsObj.assertListNotContain(result,cookieSpaceMediaProviderTestData.updatedMediaProviderName,"Deleted Cookie Space Media Provider name:"+cookieSpaceMediaProviderTestData.updatedMediaProviderName+" is found in search list.");
			});
		});
		
		 /**
		 * This method used to call clickShowDeletedCookieSpaceMediaProvider method of
		 * cookieSpaceMediaProviderBase JS File.
		 * 
		 * @author hemins
		 */
	    it('Show Deleted Cookie Space Media Provider:'+browser.user, function() {
	    	var cookieSpaceMediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getCookieSpaceMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
	    	cookieSpaceMediaProvidersBaseObj.redirectToCookieSpaceMediaProvidersTab();	
	    	cookieSpaceMediaProvidersBaseObj.clickShowDeletedCookieSpaceMediaProvider();	    	
	    	utilityObj.findList(cookieSpaceMediaProvidersBaseObj.returnSearchElement(),cookieSpaceMediaProviderTestData.updatedMediaProviderName);
			cookieSpaceMediaProvidersBaseObj.returnElementList().map(function (elm) {
			    return elm.getText();
			    }).then(function(result){
			    	assertsObj.assertListContain(result,cookieSpaceMediaProviderTestData.updatedMediaProviderName,"Show deleted:Deleted Cookie Space Media Provider Name:"+cookieSpaceMediaProviderTestData.updatedMediaProviderName+" is not found in list.");
			});
	    });
	    
	    /**
		 * This method used to call gotoCookieSpaceMediaProviderViewAuditLog method of
		 * cookieSpaceMediaProviderBase JS File.
		 * 
		 * @author hemins
		 */
		it('View Cookie Space Media Provider Audit Log:'+browser.user, function() {
			var cookieSpaceMediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getCookieSpaceMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
			utilityObj.findList(cookieSpaceMediaProvidersBaseObj.returnSearchElement(),cookieSpaceMediaProviderTestData.updatedMediaProviderName);
			cookieSpaceMediaProvidersBaseObj.gotoCookieSpaceMediaProviderViewAuditLog();
			cookieSpaceMediaProvidersBaseObj.searchCookieSpaceMediaProviderAuditLog(cookieSpaceMediaProviderTestData.auditLogSearch);
			actualValue=cookieSpaceMediaProvidersBaseObj.getFieldNameValue();
			expectedValue=cookieSpaceMediaProviderTestData.auditLogSearch;
			assertsObj.assertEqual(actualValue,expectedValue,"Cookie Space Media Provider Actual Field name:"+actualValue+" is not matching with expected:"+expectedValue+"  for audit log"); 		
			utilityObj.switchToMainWindow();
		});
		
		/**
		 * This method used to call gotoCookieSpaceMediaProviderObtainPixels & returnCookieSpaceMediaProviderObtainPixelHeaderName method of
		 * cookieSpaceMediaProviderBase JS File.
		 * 
		 * @author hemins
		 */
		it('Obtain Pixel of Cookie Space Media Provider :'+browser.user, function() {	
		    	var cookieSpaceMediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getCookieSpaceMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
		    	utilityObj.findList(cookieSpaceMediaProvidersBaseObj.returnSearchElement(),cookieSpaceMediaProviderTestData.updatedMediaProviderName);
		    	cookieSpaceMediaProvidersBaseObj.gotoCookieSpaceMediaProviderObtainPixels();
		    	actualValue=cookieSpaceMediaProvidersBaseObj.returnMediaProviderObtainPixelHeaderName(cookieSpaceMediaProviderTestData);
		    	expectedValue=cookieSpaceMediaProviderTestData.obtainPixelsName;
				assertsObj.assertEqual(actualValue,expectedValue,"Cookie Space Media Provider Actual Obtain pixel :"+ actualValue+ "is not matching with"+expectedValue+" on obtain pixel page.");
			});
		
		 /**
		 * This method used to call clickHideDeletedMediaProvider method of
		 * cookieSpaceMediaProviderBase JS File.
		 * 
		 * @author hemins
		 */
	    it('Hide Deleted cookie space Media Provider:'+browser.user, function() {
	    	var cookieSpaceMediaProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getCookieSpaceMediaProviderTestDataFilePath(),browser.env, browser.language,'createmediaproviderturnmarket');
	    	cookieSpaceMediaProvidersBaseObj.clickHideDeletedMediaProvider();
	    	assertsObj.assertNull(utilityObj.findList(cookieSpaceMediaProvidersBaseObj.returnSearchElement(),cookieSpaceMediaProviderTestData.updatedMediaProviderName),"Hide Deleted:Deleted Cookie Space Media Provider Name:"+cookieSpaceMediaProviderTestData.updatedMediaProviderName+" is found in list.");	
		});
	    
		/**
		 * This method is use to call sort by Media Provider name method of cookieSpaceMediaProviderBase
		 * JS File and assert that records are sort by name or not.
		 * 
		 * @author hemins
		 */
		it('sorting functionality CookieSpaceMediaProvider:'+browser.user, function(){
			cookieSpaceMediaProvidersBaseObj.sortByMediaProviderName();
			assertsObj.assertNotNull(cookieSpaceMediaProvidersBaseObj.assertNameColumn(),"Sorting on name column is not perform.");
		});
		
		/**
		 * This Method is used to log out from turn page.
		 * @author omp
		 */
		it(' Turn Logout CookieSpaceMediaProvider tab :'+browser.user,function(){
			loginBaseObj.logout(browser.user);
			  isAngularSite(false);
			assertsObj.assertTrue(loginBaseObj.isUserNamePresent(),"Username Textbox is not found for login page.");
		});

	}else{
		logger.info("Test Cases of Cookie Space Media Provider tab is avialable for New Console Only.");
	}
	
});