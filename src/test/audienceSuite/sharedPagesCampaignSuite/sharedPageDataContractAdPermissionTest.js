/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

   /**
    * This test suite is use to check advertiser permission for data Contracts tab.
    * 
    * @author narottamc
    */
  describe("Shared Pages Advertiser Permission of Data Contracts Tab:"+browser.user,function(){
	  
	  var pathObj  =require( '../../../testData/audienceSuite/path.js');
		var utilityObj = require('../../../lib/utility.js');
		var loginBaseObj = require('../../../base/audienceSuite/loginBase.js');
		var dataContractBaseObj = require('../../../base/audienceSuite/dataContractBase.js');
		var turnDataFilterObj = require('../../../lib/turnDataProvider.js');
		var assertsObj = require('../../../lib/assert.js');
		var loggerObj = require('../../../lib/logger.js');
		var logger=loggerObj.loggers("Shared DataContract Permission Test");
		var testDataPath=pathObj.getSharedPageDataContractTestDataFilePath();
	
		beforeEach(function() {
			isAngularSite(false);		  
		});
		browser.ignoreSynchronization = true;		
		
if((browser.user).toUpperCase()=='CS'){
   /**
	 * This method is use to login with whitelisted user and verify advertiser permission.
	 * 
	 * @author narottamc
	 */
   it("Check Advertiser Permission for MMW user Shared Data Contract tab",function(){
	   logger.info("Check Advertiser Permission for MMW user in shared Pages.");
	   var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language,'MMW');
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		loginBaseObj.login(loginTestdata);
		utilityObj.browserWaitforseconds(2);
		browser.waitForAngular();
		loginBaseObj.isLogoutUserBoxPresent("MMW",'CS');
		utilityObj.browserWaitforseconds(2);
		loginBaseObj.redirectToCampaignSuite();
		var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, 'MMW', 'advertiserPermission');
		dataContractBaseObj.redirectToDataContractsTab();
		utilityObj.browserWaitforseconds(4);
		browser.waitForAngular();	
		utilityObj.findList(dataContractBaseObj.returnSearchElement(),advertiserPermissionTestData.whitelistedAdvertiser);
 			 utilityObj.browserWaitforseconds(4);
 			dataContractBaseObj.returnElementList().map(function (elm) {
 			    return elm.getText();
 			    }).then(function(result){
 			    	assertsObj.assertListContain(result,advertiserPermissionTestData.whitelistedAdvertiser,'White listed data contracts is not found in search list for MMW use');
 			});
 	 			utilityObj.findList(dataContractBaseObj.returnSearchElement(),advertiserPermissionTestData.blacklistedAdvertiser);
 			 utilityObj.browserWaitforseconds(4);
 			dataContractBaseObj.returnElementList().map(function (elm) {
 			    return elm.getText();
 			    }).then(function(result){
 			    	assertsObj.assertListNotContain(result,advertiserPermissionTestData.blacklistedAdvertiser,'Black listed data contracts is found in search list for MMW user.');
 			});
		loginBaseObj.logout("MMW",'CS');
	});
	
   /**
	 * This method is use to login with blacklisted user and verify advertiser permission.
	 * 
	 * @author narottamc
	 */
	it("Check Advertiser Permission for MMB user Shared Data Contract tab",function(){
		logger.info("Check Advertiser Permission for MMB user in shared Pages.");
		var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language,'MMB');
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		loginBaseObj.login(loginTestdata);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(2);
		loginBaseObj.isLogoutUserBoxPresent("MMB",'CS');
		utilityObj.browserWaitforseconds(2);
		loginBaseObj.redirectToCampaignSuite();
		var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, 'MMB', 'advertiserPermission');
		dataContractBaseObj.redirectToDataContractsTab();
		utilityObj.browserWaitforseconds(4);
		utilityObj.findList(dataContractBaseObj.returnSearchElement(),advertiserPermissionTestData.whitelistedAdvertiser);
  			 utilityObj.browserWaitforseconds(4);
  			dataContractBaseObj.returnElementList().map(function (elm) {
  			    return elm.getText();
  			    }).then(function(result){
  			    	assertsObj.assertListContain(result,advertiserPermissionTestData.whitelistedAdvertiser,'White listed data contracts is not found in search list for MMB user');
  			});
  			utilityObj.findList(dataContractBaseObj.returnSearchElement(),advertiserPermissionTestData.blacklistedAdvertiser);
  			utilityObj.browserWaitforseconds(4);
  			dataContractBaseObj.returnElementList().map(function (elm) {
  			    return elm.getText();
  			    }).then(function(result){
  			    	assertsObj.assertListNotContain(result,advertiserPermissionTestData.blacklistedAdvertiser,'Black listed data contracts is found in search list for MMB user.');
  			});
		loginBaseObj.logout("MMB",'CS');
	});
	
	/**
	 * This method is use to login with All Advertiser user and verify advertiser permission.
	 * 
	 * @author narottamc
	 */
	it("Check Advertiser Permission for MMA user Shared Data Contract tab",function(){
		 logger.info("Check Advertiser Permission for MMA user in shared Pages.");
		var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, 'MMA');
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		loginBaseObj.login(loginTestdata);
		browser.waitForAngular();
		loginBaseObj.redirectToCampaignSuite();
		utilityObj.browserWaitforseconds(2);
		loginBaseObj.isLogoutUserBoxPresent("MMA",'CS');
		utilityObj.browserWaitforseconds(2);
		loginBaseObj.redirectToCampaignSuite();
		var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(testDataPath, browser.env,'MMA', 'advertiserPermission');
		dataContractBaseObj.redirectToDataContractsTab();
		 utilityObj.browserWaitforseconds(4);
		browser.waitForAngular();
		var dataContracts=(advertiserPermissionTestData.whitelistedAdvertiser).split(";");
		var j=0;
		for(var i=0;i<dataContracts.length;i++){
			    utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContracts[i]);
	  			utilityObj.browserWaitforseconds(4);
	  			dataContractBaseObj.returnElementList().map(function (elm) {
	  			    return elm.getText();
	  			    }).then(function(result){
	  			    	assertsObj.assertListContain(result,dataContracts[j],'White listed advertiser is not found in search list for All Advertiser Permission user.');
	  			    	j++;
	  			});
		}
		loginBaseObj.logout("MMA",'CS');
	});
	
	/**
	 * This method is use to login with Cadmin user and verify advertiser permission.
	 * 
	 * @author narottamc
	 */			
	it("Check Advertiser Permission for CAdmin user Shared Data Contract tab",function(){
		 logger.info("Check Advertiser Permission for CAdmin user in shared Pages.");
		var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, 'CADMIN');
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		loginBaseObj.login(loginTestdata);	
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(2);
		loginBaseObj.isLogoutUserBoxPresent("CAdmin",'CS');
		utilityObj.browserWaitforseconds(2);
		loginBaseObj.redirectToCampaignSuite();
		var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, 'CADMIN', 'advertiserPermission');
		dataContractBaseObj.redirectToDataContractsTab();
		 utilityObj.browserWaitforseconds(4);
		browser.waitForAngular();
		var dataContracts=(advertiserPermissionTestData.whitelistedAdvertiser).split(";");
		var j=0;
		for(var i=0;i<dataContracts.length;i++){
	  			utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContracts[i]);
	  			utilityObj.browserWaitforseconds(4);
	  			dataContractBaseObj.returnElementList().map(function (elm) {
	  			    return elm.getText();
	  			    }).then(function(result){
	  			    	assertsObj.assertListContain(result,dataContracts[j],'White listed advertiser is not found in search list for Cadmin user.');
	  			    	j++;
	  			});
			}
		loginBaseObj.logout("CAdmin",'CS');
		});		  
	}else{
		logger.info("Advertiser Permission TestCase : Skipped,because UserType is Non 'CS'.");
	};
	});
  