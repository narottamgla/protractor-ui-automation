/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

describe("Turn Shared Pages Audience Suite Data Provider Tab:"+browser.user,function(){
	
	var pathObj  =require( '../../../testData/audienceSuite/path.js');
	var utilityObj = require('../../../lib/utility.js');
	var loginBaseObj = require('../../../base/audienceSuite/loginBase.js');
	var dataProviderBaseObj = require('../../../base/audienceSuite/dataProvidersBase.js');
	var turnDataFilterObj = require('../../../lib/turnDataProvider.js');
	var assertsObj = require('../../../lib/assert.js');
	var loggerObj = require('../../../lib/logger.js');
	var logger=loggerObj.loggers("Shared DataProvider Test");
	var testDataPath=pathObj.getSharedPageDataProviderTestDataFilePath();
	
	beforeEach(function() {
		isAngularSite(false);
	});
	browser.ignoreSynchronization = true;			
	if((browser.user.toUpperCase()=='CS')|| (browser.user.toUpperCase()=='DPMMM')){
		
	/**
	 * This method used to call login method of loginBase file.
	 * 
	 * @author hemins
	 */
	it('Turn login page Shared DataProvider tab:'+browser.user, function() {
		var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, browser.user);
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		loginBaseObj.login(loginTestdata);
		utilityObj.browserWaitforseconds(3);
		browser.waitForAngular();
		assertsObj.assertTrue(loginBaseObj.isLogoutUserBoxPresent(browser.user),'Logout UserBox is not available.');
	});
	
	/**
	 * This method is used to redirect campaign suite project.
	 * 
	 * @author hemins
	 */
	it('Redirection to Campaign Suite Shared DataProvider tab:'+browser.user, function() {
		utilityObj.browserWaitforseconds(3);
		loginBaseObj.redirectToCampaignSuite();
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		assertsObj.assertTrue(loginBaseObj.checkCampaignSuite(),"Not redirect to Campaign Suite");
	});
	
	describe("Shared Page Campaign Suite: Operations for Standard type of data Provider",function(){
		
		/**
		 * This method used to call create standard type of DataProvider method of dataProviderBase file
		 * and assert that newly created data provider is available in search list or not.
		 * 
		 * @author narottamc
		 */
		it('Create new Standard type Shared Data Provider:'+browser.user,function(){
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath, browser.env, browser.language,'createStandardDataProviderdata');
			dataProviderBaseObj.redirectToDataProvidersTab();
			browser.waitForAngular();    
				var dataProviderName = dataProviderTestData.dataProviderName + utilityObj.getTimeStamp();
				dataProviderBaseObj.createDataProvider(testDataPath,dataProviderTestData,dataProviderName,'createStandardDataProviderdata');  
					utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderName);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(3);
					dataProviderBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListContain(result,dataProviderName,'Newly Created Standard Data Provider Name is not found in search list.');
					});
		});
		
		/**
		 * This method used to call standard type of editDataProvider method of dataProviderBase file
		 * and assert that edited data provider is available in search list or not.
		 * 
		 * @author narottamc
		 */
		it('Edit newly created Standard type of Shared Data Provider:'+browser.user, function() {
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataProviderdata');
			dataProviderBaseObj.redirectToDataProvidersTab();
			utilityObj.browserWaitforseconds(3);
			utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderTestData.createdDataProvider);
			var updatedDataProviderName = dataProviderTestData.createdDataProvider + "_Edit";
			utilityObj.browserWaitforseconds(6);
			dataProviderBaseObj.editDataProvider(testDataPath,dataProviderTestData, updatedDataProviderName,'createStandardDataProviderdata');
					utilityObj.findList(dataProviderBaseObj.returnSearchElement(),updatedDataProviderName);
					utilityObj.browserWaitforseconds(3);
					dataProviderBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListContain(result,updatedDataProviderName,'Updated Standard Data Provider Name is not found in search list.');
					});
		        });
		
		/**
		 * This method used to call redirect to viewDataProvider method of dataProvidersBase
		 * file and assert that viewed data provider match with actual data provider or not.
		 * 
		 * @author narottamc
		 */
		it('View standard type of Shared data provider:'+browser.user,function() {
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataProviderdata');
			dataProviderBaseObj.redirectToDataProvidersTab();
			utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderTestData.updatedDataProvider);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			dataProviderBaseObj.redirectToViewDataProviderPage();
			assertsObj.assertEqual(dataProviderBaseObj.returnDataProviderNameOfViewPage(dataProviderTestData.updatedDataProvider,dataProviderTestData.dataType),dataProviderTestData.updatedDataProvider,
			  			"Standard Data Provider name is not match with actual Data Provider in view Data Proider page.");
		});
		
		/**
		 * This method used to call delete standard type of Data Provider method of dataProvidersBase
		 * file and assert deleted data provider is available in search list or not.
		 * 
		 * @author narottamc
		 */
		it('Delete standard type of Shared data provider:'+browser.user,function() {
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataProviderdata');
			dataProviderBaseObj.redirectToDataProvidersTab();
			utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderTestData.updatedDataProvider);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			dataProviderBaseObj.deleteDataProvider(dataProviderTestData);
				utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderTestData.updatedDataProvider);
				browser.waitForAngular();
				dataProviderBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListNotContain(result,dataProviderTestData.updatedDataProvider,'Updated Standard Data Provider Name is not found in search list.');
				});
	        });
		
		/**
		  * This method is use to call goto data provider page and click Show deleted data provider method 
		  * of dataProviderBase and assert that deleted Standard Data Provider is available in search list or not.
		  * 
		  * @author narottamc
		  */
		it('Show Deleted standard type of Shared data provider:'+browser.user, function() {
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataProviderdata');
			dataProviderBaseObj.redirectToDataProvidersTab();
			dataProviderBaseObj.clickShowDeletedDataProvider(dataProviderTestData.dataType);
			utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderTestData.updatedDataProvider);
				dataProviderBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,dataProviderTestData.updatedDataProvider,'Deleted Standard Data provider Name is not found in list of show deleted Data Provider.');
				});
		});
		
		/**
		  * This method is use to call goto view audit log page and search audit log field name method 
		  * of dataProviderBase and assert that edited field name is available or not.
		  * 
		  * @author narottamc
		  */
		it('View standard type of Audit Log Shared DataProvider :'+browser.user,function(){
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language, 'createStandardDataProviderdata');
			dataProviderBaseObj.gotoViewAuditLog();
			isAngularSite(false);
			utilityObj.browserWaitforseconds(3);
			dataProviderBaseObj.searchAuditLog(dataProviderTestData.fieldNameValue);
			assertsObj.assertEqual(dataProviderBaseObj.getFieldNameValue(),dataProviderTestData.fieldNameValue,"Standard Data Provider Field name is not found for audit log.");
			
		});
		
		/**
		  * This method is use to call click Hide deleted Standard data provider method 
		  * of dataProviderBase and assert that deleted data provider is available in search list or not.
		  * 
		  * @author narottamc
		  */
		it('Hide Deleted standard type of Shared data provider:'+browser.user,function() {
			utilityObj.switchToMainWindow();
			utilityObj.browserWaitforseconds(3);
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language, 'createStandardDataProviderdata');
			dataProviderBaseObj.clickHideDeletedDataProvider(dataProviderTestData.dataType);
			utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderTestData.updatedDataProvider);
				dataProviderBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListNotContain(result,dataProviderTestData.updatedDataProvider,'Deleted Standard Data provider Name is found in list of show deleted Data Provider.');
				});
		});
});

	
	describe("Shared Page Campaign Suite : Operations for Flaxtag type of data Provider:"+browser.user,function(){
		

		/**
		 * This method used to call create flextag type of DataProvider(Javascript event) method of dataProviderBase file
		 * and assert that newly created data provider is available in search list or not.
		 * 
		 * @author narottamc
		 */
		it('Create new Flextag type Shared Data Provider with javascript event:'+browser.user,function(){
			utilityObj.browserWaitforseconds(3);
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath, browser.env, browser.language,'createFlextagJavascriptDataProviderdata');
			dataProviderBaseObj.redirectToDataProvidersTab();
			var dataProviderName = dataProviderTestData.dataProviderName + utilityObj.getTimeStamp();
			dataProviderBaseObj.createDataProvider(testDataPath,dataProviderTestData,dataProviderName,'createFlextagJavascriptDataProviderdata');
			 		utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderName);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					dataProviderBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListContain(result,dataProviderName,'Newly Created Flaxtag Data Provider Name (Javascript Event) is not found in search list.');
					});
		        });
		
		/**
		 * This method used to call create flextag type of DataProvider(HTML event) method and delete method of dataProviderBase file
		 * and assert that newly created data provider is available in search list or not
		 * after delete asset that it is not available in search list.
		 * 
		 * @author narottamc
		 */
		it('Create new Flextag type Shared Data Provider with HTML event:'+browser.user,function(){
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath, browser.env, browser.language,'createFlextagHTMLDataProviderdata');
			dataProviderBaseObj.redirectToDataProvidersTab();
			var dataProviderName = dataProviderTestData.dataProviderName + utilityObj.getTimeStamp();
			dataProviderBaseObj.createDataProvider(testDataPath,dataProviderTestData,dataProviderName,'createFlextagHTMLDataProviderdata');
					utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderName);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					dataProviderBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListContain(result,dataProviderName,'Newly Created Flextag Data Provider Name(HTML Event) is not found in search list.');
					});
			 dataProviderBaseObj.deleteDataProvider(dataProviderTestData);
					utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderName);
					utilityObj.browserWaitforseconds(2);
					dataProviderBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListNotContain(result,dataProviderName,'Newly Created Flextag Data Provider Name(HTML Event) is found in search list.');
					});
		        });
		
		/**
		 * This method used to call  gotoObtainPixel method of dataProviderBase file
		 * and assert obtain Pixel header name.
		 * 
		 * @author narottamc
		 */
		it('Obtain Pixels for Flextag type Shared data provider:'+browser.user,function(){
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath, browser.env, browser.language,'createFlextagJavascriptDataProviderdata');
			utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderTestData.createdDataProvider);
			browser.waitForAngular();
			dataProviderBaseObj.gotoObtainPixel();
			utilityObj.browserWaitforseconds(2);
			assertsObj.assertEqual(dataProviderBaseObj.returnObtainPixelHeaderName(),dataProviderTestData.obtainPixel,"Flextag Data Provider: Obtain Pixel header is not found.");
		});

		
		/**
		 * This method used to call Flextag type of editDataProvider method of dataProviderBase file
		 * and assert that edited data provider is available in search list or not.
		 * 
		 * @author narottamc
		 */
		it('Edit newly created Flextag type of Shared Data Provider with javascript:'+browser.user, function() {
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createFlextagJavascriptDataProviderdata');
			dataProviderBaseObj.redirectToDataProvidersTab();
			utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderTestData.createdDataProvider);
			var updatedDataProviderName = dataProviderTestData.createdDataProvider + "_Edit";
			dataProviderBaseObj.editDataProvider(testDataPath,dataProviderTestData, updatedDataProviderName,'createFlextagJavascriptDataProviderdata');
					utilityObj.findList(dataProviderBaseObj.returnSearchElement(),updatedDataProviderName);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					dataProviderBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListContain(result,updatedDataProviderName,'Updated Flextag Data Provider Name with Javascript event is not found in search list.');
					});
		});
		
		/**
		 * This method used to call redirect to viewDataProvider method of dataProvidersBase
		 * file and assert that viewed data provider match with actual data provider or not.
		 * 
		 * @author narottamc
		 */
		it('View Flextag type of Shared data provider:'+browser.user,function() {
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createFlextagJavascriptDataProviderdata');
			dataProviderBaseObj.redirectToDataProvidersTab();
			utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderTestData.updatedDataProvider);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			dataProviderBaseObj.redirectToViewDataProviderPage();
			assertsObj.assertEqual(dataProviderBaseObj.returnDataProviderNameOfViewPage(dataProviderTestData.updatedDataProvider,dataProviderTestData.dataType),dataProviderTestData.updatedDataProvider,
			  			"Flextag Data Provider name with javascript event is not match with actual Data Provider in view Data Proider page.");
		});
		
		/**
		 * This method used to call delete flextag type of Data Provider with javascript event method of dataProvidersBase
		 * file and assert deleted data provider is available in search list or not.
		 * 
		 * @author narottamc
		 */
		it('Delete Flextag type of Shared data provider:'+browser.user,function() {
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createFlextagJavascriptDataProviderdata');
			dataProviderBaseObj.redirectToDataProvidersTab();
			utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderTestData.updatedDataProvider);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			dataProviderBaseObj.deleteDataProvider(dataProviderTestData);
				utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderTestData.updatedDataProvider);
				dataProviderBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListNotContain(result,dataProviderTestData.updatedDataProvider,'Deleted flaxtag Data Provider Name with javascript event is found in search list.');
				});
		});
		
		/**
		  * This method is use to call goto data provider page and click Show deleted Flaxtag data provider method 
		  * of dataProviderBase and assert that deleted Standard Data Provider is available in search list or not.
		  * 
		  * @author narottamc
		  */
		it('Show Deleted Flextag type of Shared data provider:'+browser.user, function() {
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createFlextagJavascriptDataProviderdata');
			dataProviderBaseObj.redirectToDataProvidersTab();
			dataProviderBaseObj.clickShowDeletedDataProvider(dataProviderTestData.dataType);
						utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderTestData.updatedDataProvider);
						browser.waitForAngular();
						utilityObj.browserWaitforseconds(1);
						dataProviderBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,dataProviderTestData.updatedDataProvider,'Deleted Flaxtag Data provider Name with javascript event is not found in list of show deleted Data Provider..');
				});
		});
		
		/**
		  * This method is use to call goto view audit log page and search audit log field name method 
		  * of dataProviderBase and assert that edited field name is available or not.
		  * 
		  * @author narottamc
		  */
		it('View Flextag type of Audit Log Shared DataProvider:'+browser.user,function(){
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language, 'createFlextagJavascriptDataProviderdata');
			dataProviderBaseObj.gotoViewAuditLog();
			isAngularSite(false);
			utilityObj.browserWaitforseconds(3);
			dataProviderBaseObj.searchAuditLog(dataProviderTestData.fieldNameValue);
			assertsObj.assertEqual(dataProviderBaseObj.getFieldNameValue(),dataProviderTestData.fieldNameValue,"Flaxtag Data Provider with javascript event Field name is not found for audit log.");
			utilityObj.switchToMainWindow();
		});
		
		/**
		  * This method is use to call click Hide deleted Flaxtag data provider method 
		  * of dataProviderBase and assert that deleted data provider is available in search list or not.
		  * 
		  * @author narottamc
		  */
		it('Hide Deleted Flextag type of Shared data provider:'+browser.user,function() {
			var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language, 'createFlextagJavascriptDataProviderdata');
			dataProviderBaseObj.clickHideDeletedDataProvider(dataProviderTestData.dataType);
				utilityObj.findList(dataProviderBaseObj.returnSearchElement(),dataProviderTestData.updatedDataProvider);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				dataProviderBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListNotContain(result,dataProviderTestData.updatedDataProvider,'Deleted Flaxtag Data provider Name with javascript event is found in list of hide deleted data provider.');
				});
		});
	});
	
	/**
	 * This method used to call select Market Filter of data provider base class
	 * and assert that compare selected market and list shows market.
	 * 
	 * @author narottamc
	 */
	it('Filter Market Shared DataProvider tab :'+browser.user, function(){
		var dataProviderTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataProviderdata');
		var userType = browser.user;
		if(userType.toUpperCase()=='CS'){
			dataProviderBaseObj.clearSearch();
				dataProviderBaseObj.clearNewFilter();
				utilityObj.browserWaitforseconds(2);
				dataProviderBaseObj.selectMarketFilterNew(dataProviderTestData.market);
				utilityObj.browserWaitforseconds(2);
				assertsObj.assertEqual(dataProviderBaseObj.getMarketName(),dataProviderTestData.market,"Total filter records are not match with expected total records.");
			}
		else{
			logger.info(userType + " user doesnot contain market filter.");
		}
	});
			
	/**
	 * This method is use to call sort by data provider name method of data provider
	 * base class and assert that records are sort by name or not.
	 * 
	 * @author narottamc
	 */
	
	it('Sorting functionality Shared DataProvider tab :'+browser.user, function(){
		dataProviderBaseObj.sortByDataProviderName();
		assertsObj.assertNotNull(dataProviderBaseObj.assertNameColumn(),"Sorting on data provider name column is not perform.");
	});
	
	/**
	 * This method is used to call logout method of
	 * dataProviderBase JS File.
	 * 
	 * @author narottamc
	 */
	it('Logout Shared DataProvider tab :'+browser.user,function(){
		loginBaseObj.logout(browser.user,'CS');
		isAngularSite(false);
		assertsObj.assertTrue(loginBaseObj.isUserNamePresent(),"Username Textbox is not found for login page.");
	});
	}else{
		  logger.info(browser.user+" user does not have Data provider Shared pages rights.");
		  }
		
});