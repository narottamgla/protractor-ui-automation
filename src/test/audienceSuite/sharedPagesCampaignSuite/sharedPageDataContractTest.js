/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

describe("Turn shared pages Data Contarct :"+browser.user,function(){

	var pathObj  =require( '../../../testData/audienceSuite/path.js');
	var utilityObj = require('../../../lib/utility.js');
	var loginBaseObj = require('../../../base/audienceSuite/loginBase.js');
	var dataContractBaseObj = require('../../../base/audienceSuite/dataContractBase.js');
	var turnDataFilterObj = require('../../../lib/turnDataProvider.js');
	var assertsObj = require('../../../lib/assert.js');
	var loggerObj = require('../../../lib/logger.js');
	var logger=loggerObj.loggers("Shared DataContractTest");
	var testDataPath=pathObj.getSharedPageDataContractTestDataFilePath();

	var actualValue;
	var expectedValue;
	beforeEach(function() {
		isAngularSite(false);
	    actualValue='';
		expectedValue='';
	});
	browser.ignoreSynchronization = true;		
	if((browser.user.toUpperCase()=='CS')|| (browser.user.toUpperCase()=='DPMMM')){
	
	/**
	 * This method used to call login method of loginBase file.
	 * 
	 * @author narottamc
	 */
	it('Turn login page Shared Data Contract tab:'+browser.user, function() {
		var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, browser.user);
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		loginBaseObj.login(loginTestdata);
		utilityObj.browserWaitforseconds(3);
		browser.waitForAngular();
		assertsObj.assertTrue(loginBaseObj.isLogoutUserBoxPresent(browser.user),'Logout UserBox is not available.');
	});
	
	/**
	 * This method is used to redirect campaign suite project.
	 * 
	 * @author narottamc
	 */
	it('Redirection to Campaign Suite Shared Data Contract tab:'+browser.user, function() {
		loginBaseObj.redirectToCampaignSuite();
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);		
		assertsObj.assertTrue(loginBaseObj.checkCampaignSuite(),"Not redirect to Campaign Suite");	
		dataContractBaseObj.redirectToCSDataContractsTab();
		browser.waitForAngular();		
	});
	
		/**
		 * This test case used to create All Advertisers with collection type API.
		 * 
		 * @author narottamc
		 */
		it('Creation of Flex tag Shared datacontact with Single advertiser and collection type File:'+browser.user,function () {
			browser.navigate().refresh();
			browser.waitForAngular();	
			var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createFlexTagDataContractSingle');			
	        if((dataContractTestData.crossDevice.toUpperCase()=='YES') &&((browser.user).toUpperCase()=='CS')){	
	            dataContractBaseObj.removeMappingSourceDataContract(dataContractTestData);
	    		}
	        dataContractBaseObj.selectDPFromfilter(dataContractTestData.dPName);
	        utilityObj.browserWaitforseconds(3);
	        dataContractBaseObj.clickNewDataContractsButton();
			 utilityObj.browserWaitforseconds(3);
	        browser.waitForAngular();
	        var dataContractName = dataContractTestData.dcName + utilityObj.getTimeStamp();
	        dataContractBaseObj.createDataContracts(testDataPath,dataContractTestData,dataContractName,pathObj.getAdvertiserDataTexonomyDataFilePath(),'createFlexTagDataContractSingle');
				utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractName);
				utilityObj.browserWaitforseconds(3);
				dataContractBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,dataContractName,'DataContract Name is not found in search list.');
				});
	        dataContractBaseObj.deleteCreatedDataContract(dataContractName);
	        utilityObj.browserWaitforseconds(3);
					utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractTestData.updatedDC);
					utilityObj.browserWaitforseconds(3);
					dataContractBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
				assertsObj.assertListNotContain(result,dataContractName,'Deleted Data Contract Name:' + dataContractName + ' is  found in list.');
				});
			utilityObj.browserWaitforseconds(5);
	        dataContractBaseObj.removeFilter();
	      });

	      /**
			 * This test case used to create Single Advertisers with collection type File.
			 * 
			 * @author vishalbha
			 */
	      xit('Creation of Shared datacontact with Analytics Only Type advertiser and collection type Pixel:'+browser.user,function () {
	        var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createFlexTagDataContractAnalytics');
	        dataContractBaseObj.redirectToCSDataContractsTab();
	        dataContractBaseObj.selectDPFromfilter(dataContractTestData.dPName);
	        utilityObj.browserWaitforseconds(3);
	        dataContractBaseObj.clickNewDataContractsButton();
	        var dataContractName = dataContractTestData.dcName + utilityObj.getTimeStamp();
	        dataContractBaseObj.createDataContracts(testDataPath,dataContractTestData,dataContractName,pathObj.getAdvertiserDataTexonomyDataFilePath(),'createFlexTagDataContractAnalytics');
				utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractName);
				dataContractBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,dataContractName,'DataContract Name is not found in search list.');
				});
	        dataContractBaseObj.deleteCreatedDataContract(dataContractName);
					utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractTestData.updatedDC);
					dataContractBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
				assertsObj.assertListNotContain(result,dataContractName,'Deleted Data Contract Name:' + dataContractName + ' is  found in list.');
				});
	        dataContractBaseObj.removeFilter();
	      });

	  	/**
		 * This test case used to create data contract with all advertiser with collection type Pixel.
		 * 
		 * @author narottamc
		 */
	      xit('Creation of Shared data Contract with all advertisers and collection type API:'+browser.user,function () { 
	        var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createFlexTagDataContractAll');
	        dataContractBaseObj.redirectToCSDataContractsTab();
	        browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			  runs(function() {
	        dataContractBaseObj.selectDataContractFromNewButton(dataContractTestData);
	        var dataContractName = dataContractTestData.dcName + utilityObj.getTimeStamp();
	        dataContractBaseObj.createDataContracts(testDataPath,dataContractTestData,dataContractName,pathObj.getAdvertiserDataTexonomyDataFilePath(),'createFlexTagDataContractAll');
				utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractName);
				dataContractBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,dataContractName,'DataContract Name is not found in search list.');
				});
	        dataContractBaseObj.deleteCreatedDataContract(dataContractName);
					utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractTestData.updatedDC);
					dataContractBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
				assertsObj.assertListNotContain(result,dataContractName,'Deleted Data Contract Name:' + dataContractName + ' is  found in list.');
				});
	      });
			  browser.waitForAngular();
		  });

    	/**
  		 * This test case used to create Single Advertisers with collection type File.
  		 * 
  		 * @author narottamc
  		 */
        it('Creation of standard Shared datacontact with Single advertiser and collection type File :'+browser.user,function () {
        var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
			browser.navigate().refresh();
			browser.waitForAngular();
        utilityObj.browserWaitforseconds(3);
          dataContractBaseObj.removeFilter();
          utilityObj.browserWaitforseconds(3);
          browser.waitForAngular();
          dataContractBaseObj.selectDPFromfilter(dataContractTestData.dPName);
          utilityObj.browserWaitforseconds(3);
          dataContractBaseObj.clickNewDataContractsButton();
          browser.waitForAngular();
          var dataContractName = dataContractTestData.dcName + utilityObj.getTimeStamp();
          dataContractBaseObj.createDataContracts(testDataPath,dataContractTestData,dataContractName,pathObj.getAdvertiserDataTexonomyDataFilePath(),'createStandardDataContractSingleAdvertiser'); 
  			utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractName);
			browser.waitForAngular();
  			dataContractBaseObj.returnElementList().map(function (elm) {
  			    return elm.getText();
  			    }).then(function(result){
  			    	assertsObj.assertListContain(result,dataContractName,'DataContract Name is not found in search list.');
  			});
          browser.waitForAngular();
        });

        
      

      /**
		 * This method is used to call editCreatedDataContract method of dataContractBase JS file.
		 * 
		 * @author narottamc
		 */

      it('Edit Created Shared data contract with cross device functionality:'+browser.user,function () {
          var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
          dataContractBaseObj.redirectToCSDataContractsTab();
          utilityObj.browserWaitforseconds(2);
          browser.waitForAngular();
          utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractTestData.createdDC);
          browser.waitForAngular();
          var updatedDataContractName = dataContractTestData.createdDC + '_Edit';
          dataContractBaseObj.editCreatedDataContract(testDataPath,dataContractTestData,'createStandardDataContractSingleAdvertiser',updatedDataContractName);
          browser.waitForAngular();
			utilityObj.findList(dataContractBaseObj.returnSearchElement(),updatedDataContractName);
  			browser.waitForAngular();
  			utilityObj.browserWaitforseconds(2);
  			dataContractBaseObj.returnElementList().map(function (elm) {
  			    return elm.getText();
  			    }).then(function(result){
  			    	assertsObj.assertListContain(result,updatedDataContractName,'Updated DataContract Name is not found in search list.');
  			});
          browser.waitForAngular();
      });

      /**
		 * This method is used to call gotoViewDataContracts method of dataContractBase JS file.
		 * 
		 * @author narottamc
		 */
      it('View Created Shared data contract with cross device functionality:'+browser.user,function () {
          var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
          dataContractBaseObj.redirectToCSDataContractsTab();
          browser.waitForAngular();
          utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractTestData.updatedDC);
          browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
          dataContractBaseObj.gotoViewDataContracts();
          actualValue = dataContractBaseObj.viewCreatedDataContract(dataContractTestData);
          expectedValue = dataContractTestData.updatedDC;
          assertsObj.assertEqual(actualValue,expectedValue,'Actual Data Contract Name:\'' + actualValue + '\' is not match with Expected Data contract name:\'' + expectedValue + '\' in view Data Contract page.');
      });

      /**
		 * This method is used to call expandDataContracts method of dataContractBase JS file.
		 * 
		 * @author narottamc
		 */
      it('Expand/Collapse/View Configuration  functionality Shared Data  contract:'+browser.user,function () {
          var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
          dataContractBaseObj.redirectToCSDataContractsTab();
          browser.waitForAngular();
          utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractTestData.updatedDC);
		    browser.waitForAngular();
          utilityObj.browserWaitforseconds(1);
          	dataContractBaseObj.viewConfigurationDataContracts(dataContractTestData);
          	 assertsObj.assertTrue(dataContractBaseObj.checkViewConfigurationCloseButton(),'Close button not found on Data contract->\'View Configuraion\' page');
          	 dataContractBaseObj.clickingDataContractViewConfigurationCloseButton();
        });
      
      /**
		 * This method is used to call gotoDataContractObtainPixels method of dataContractBase JS file.
		 * 
		 * @author narottamc
		 */
      it('Obtain Pixel Shared Data contract:'+browser.user,function () {
          var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
          dataContractBaseObj.redirectToCSDataContractsTab();
          utilityObj.browserWaitforseconds(3);
          browser.waitForAngular();
          utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractTestData.updatedDC);
          browser.waitForAngular();
          dataContractBaseObj.gotoDataContractObtainPixels();
          utilityObj.browserWaitforseconds(3);
          browser.waitForAngular();
          actualValue = dataContractBaseObj.returnDataContractObtainPixelHeaderName();
          expectedValue = dataContractTestData.obtainPixelsName;
          assertsObj.assertEqual(actualValue,expectedValue,'Data Contract Actual Obtain pixel:' + actualValue + ' is not matching with Expected:' + expectedValue + 'on obtain pixel page.');
        });

        /**
  		 * This method is used to call gotoDataContractViewAuditLog method of dataContractBase JS file.
  		 * 
  		 * @author narottamc
  		 */
        it('View Audit Log Shared Data contract:'+browser.user,function () {
          var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
          dataContractBaseObj.redirectToCSDataContractsTab();
          browser.waitForAngular();
          utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractTestData.updatedDC);
          browser.waitForAngular();
          utilityObj.browserWaitforseconds(2);
          dataContractBaseObj.gotoDataContractViewAuditLog();
          utilityObj.browserWaitforseconds(3);
          dataContractBaseObj.searchDataContractAuditLog(dataContractTestData.auditLogSearch);
          actualValue = dataContractBaseObj.getFieldNameValue();
          expectedValue = dataContractTestData.auditLogSearch;
          assertsObj.assertEqual(actualValue,expectedValue,'Actual Field name:' + actualValue + ' is not matching with Expected Field name:\'' + expectedValue + '\' for audit log ');
          
        });

        /**
  		 * This method is used to call deleteCreatedDataContract method of dataContractBase JS file.
  		 * 
  		 * @author narottamc
  		 */  
        it('Delete mapping from source & dastination data contract and finally delete Shared data contract:'+browser.user,function () { 
        	utilityObj.switchToMainWindow();
        	var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
            dataContractBaseObj.redirectToCSDataContractsTab();
            utilityObj.browserWaitforseconds(3);
            browser.waitForAngular();
            if((dataContractTestData.crossDevice.toUpperCase()=='YES') &&((browser.user).toUpperCase()=='CS')){	
            dataContractBaseObj.deleteCrossDeviceMapping((dataContractTestData.selectDest[2]).value, 'Source');
			  browser.waitForAngular();
            utilityObj.browserWaitforseconds(3);
            dataContractBaseObj.deleteCrossDeviceMapping(dataContractTestData.updatedDC, 'Destination');
            }
            dataContractBaseObj.deleteCreatedDataContract(dataContractTestData.updatedDC);
            browser.waitForAngular();
            utilityObj.browserWaitforseconds(5);            
    				utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractTestData.updatedDC);
					  browser.waitForAngular();
    				 utilityObj.browserWaitforseconds(4);
    				dataContractBaseObj.returnElementList().map(function (elm) {
    				    return elm.getText();
    				    }).then(function(result){
    			assertsObj.assertListNotContain(result,dataContractTestData.updatedDC,'Deleted Data Contract Name:' + dataContractTestData.updatedDC + ' is  found in list.');
    			});
            browser.waitForAngular();
          });
        
        /**
  		 * This method is used to call undoDeleteDataContract method of dataContractBase JS file.
  		 * 
  		 * @author narottamc
  		 */

        it('Undo delete Shared Data contract:'+browser.user,function () {
          var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
          dataContractBaseObj.redirectToCSDataContractsTab();
          utilityObj.browserWaitforseconds(4);
          browser.waitForAngular();
          dataContractBaseObj.clickShowDeletedDataContract();
          browser.waitForAngular();
          utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractTestData.updatedDC);
          browser.waitForAngular();
          dataContractBaseObj.selectDeletedDataContract();
          dataContractBaseObj.undoDeleteDataContract();
		    browser.waitForAngular();
          utilityObj.browserWaitforseconds(4);
          dataContractBaseObj.redirectToCSDataContractsTab();
          browser.waitForAngular();
  				utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractTestData.updatedDC);
				  browser.waitForAngular();
  				 utilityObj.browserWaitforseconds(3);
  				dataContractBaseObj.returnElementList().map(function (elm) {
  				    return elm.getText();
  				    }).then(function(result){
  			assertsObj.assertListContain(result,dataContractTestData.updatedDC,'Undo Deleted Data Contract Name:' + dataContractTestData.updatedDC + ' is not found in list.');
  			});
          dataContractBaseObj.deleteCreatedDataContract(dataContractTestData.updatedDC);
          utilityObj.browserWaitforseconds(4);
          browser.waitForAngular();
        });

        /**
  		 * This method is used to call clickShowDeletedDataContract method of dataContractBase JS file.
  		 * 
  		 * @author narottamc
  		 */

        it('Show Deleted Shared data contract:'+browser.user,function () {
          var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
          dataContractBaseObj.redirectToCSDataContractsTab();
          utilityObj.browserWaitforseconds(2);
          browser.waitForAngular();
          dataContractBaseObj.clickShowDeletedDataContract();
          browser.waitForAngular(); 
  				utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractTestData.updatedDC);
				  browser.waitForAngular();
  				 utilityObj.browserWaitforseconds(4);
  				dataContractBaseObj.returnElementList().map(function (elm) {
  				    return elm.getText();
  				    }).then(function(result){
  			assertsObj.assertListContain(result,dataContractTestData.updatedDC,'Deleted Data Contract Name:' + dataContractTestData.updatedDC + ' is not found in list.');
  			});
        });

        /**
  		 * This method is used to call clickHideDeletedDataContract method of dataContractBase JS file.
  		 * 
  		 * @author narottamc
  		 */
        it('Hide Deleted Shared data contract:'+browser.user,function () {
          var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
          dataContractBaseObj.clickHideDeletedDataContract();
          browser.waitForAngular();
  				utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractTestData.updatedDC);
				  browser.waitForAngular();
  				utilityObj.browserWaitforseconds(4);
  				dataContractBaseObj.returnElementList().map(function (elm) {
  				    return elm.getText();
  				    }).then(function(result){
  			assertsObj.assertListNotContain(result,dataContractTestData.updatedDC,'Deleted Data Contract Name:' + dataContractTestData.updatedDC + ' is  found in list.');
  			});
        });

        /**
  		 * This method is used to call sortByDataContractName method of dataContractBase JS file.
  		 * 
  		 * @author narottamc
  		 */
        it('Sorting functionality Shared Data contract:'+browser.user,function () {
          dataContractBaseObj.redirectToCSDataContractsTab();
          browser.waitForAngular();
          dataContractBaseObj.sortByDataContractName();
		    browser.waitForAngular();
          utilityObj.browserWaitforseconds(2);
          assertsObj.assertNotNull(dataContractBaseObj.assertDataContractNameColumn(),'Sorting on Data Contract name column is not perform.');
        });

        /**
  		 * This method is used to call selectMarketFilterDataContract method of dataContractBase JS file.
  		 * 
  		 * @author narottamc
  		 */
        
        it('Market Filter functionality Shared Data contract:'+browser.user,function () {  
          dataContractBaseObj.redirectToCSDataContractsTab();
          browser.waitForAngular(); 
          var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
         // utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractTestData.updatedDC);
          var userType = browser.user;
          if (userType.toUpperCase() == 'CS') {
          	 dataContractBaseObj.removeFilter();
          	 browser.waitForAngular(); 
             utilityObj.browserWaitforseconds(2);
            dataContractBaseObj.selectMarketFilterDataContract(dataContractTestData.marketFilter);
            browser.waitForAngular();
            utilityObj.browserWaitforseconds(2);
            dataContractBaseObj.clearSearchDataContract();
            browser.waitForAngular();
            utilityObj.browserWaitforseconds(4);
            actualValue = dataContractBaseObj.getMarketNameDataContract();
            expectedValue = dataContractTestData.marketFilter;
            assertsObj.assertEqual(actualValue,expectedValue,
              'Acutal Market filter:\'' + actualValue + '\'is not matching with Expected Market filter:\'' + expectedValue + '\'in Data contract.');
            utilityObj.browserWaitforseconds(5);
            dataContractBaseObj.removeFilter();
            utilityObj.browserWaitforseconds(3);
          }
          else{
            logger.info(userType + ' user doesnot contain market filter.');
          }
        });
        
        
        /**
  		 * This method is used to call selectDPFromfilter method of dataContractBase JS file.
  		 * 
  		 * @author narottamc
  		 */
      
      it('Data Provider Filter functionality Shared Data contract:'+browser.user,function () {
       var userType = browser.user;
       if (userType.toUpperCase() == 'CS') {
        dataContractBaseObj.redirectToCSDataContractsTab();
        utilityObj.browserWaitforseconds(5);
        browser.waitForAngular(); 
        var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
      	 dataContractBaseObj.removeFilter();
       	 utilityObj.browserWaitforseconds(2);
          dataContractBaseObj.selectDPFromfilter(dataContractTestData.dPName);
          browser.waitForAngular(); 
          utilityObj.browserWaitforseconds(5);
          actualValue = dataContractBaseObj.getDataProviderNameDataContract();
          expectedValue = dataContractTestData.dPName;
          assertsObj.assertEqual(actualValue,expectedValue,
            'Acutal Data provider filter:\'' + actualValue + '\'is not matching with Expected data provider filter:\'' + expectedValue + '\'in Data contract.');
          utilityObj.browserWaitforseconds(5);
        }
        else{
          logger.info(userType + ' user doesnot contain market filter.');
        }
      });

        
        /**
  		 * This test case used to create Analytics Only Advertisers with collection type Pixel.
  		 * 
  		 * @author narottamc
  		 */
        xit('Creation of Shared datacontact with Analytics Only Type advertiser and collection type Pixel:'+browser.user,function () {
          var dataContractTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractAnalytics');
          dataContractBaseObj.redirectToCSDataContractsTab();
          dataContractBaseObj.selectDPFromfilter(dataContractTestData.dPName);
          utilityObj.browserWaitforseconds(3);
          dataContractBaseObj.clickNewDataContractsButton();
          var dataContractName = dataContractTestData.dcName + utilityObj.getTimeStamp();
          dataContractBaseObj.createDataContracts(testDataPath,dataContractTestData,dataContractName,pathObj.getAdvertiserDataTexonomyDataFilePath(),'createStandardDataContractAnalytics');
  			utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractName);
  			dataContractBaseObj.returnElementList().map(function (elm) {
  			    return elm.getText();
  			    }).then(function(result){
  			    	assertsObj.assertListContain(result,dataContractName,'DataContract Name is not found in search list.');
  			});
          dataContractBaseObj.deleteCreatedDataContract(dataContractName);
  				utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContractTestData.updatedDC);
  				dataContractBaseObj.returnElementList().map(function (elm) {
  				    return elm.getText();
  				    }).then(function(result){
  			assertsObj.assertListNotContain(result,dataContractName,'Deleted Data Contract Name:' + dataContractName + ' is  found in list.');
  			});
        }); 
        	/**
		     * This method is used to call logout method of
		     * dataContractBase JS File.
		     * 
		     * @author narottamc
		    */
	    it(' Turn Logout Shared Data Contract tab:'+browser.user,function () {
		  utilityObj.browserWaitforseconds(3);
	      loginBaseObj.logout(browser.user);
	      assertsObj.assertTrue(loginBaseObj.isUserNamePresent(),'Username Textbox is not found for login page.');
	    });
  }else{
	  logger.info(browser.user+" user does not have Data contract Shared pages rights.");
	  }
});

