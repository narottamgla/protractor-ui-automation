/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

describe("Turn  Shared pages Audience Suite Shared Segment Tab:"+browser.user,function(){
	
	var pathObj  =require( '../../../testData/audienceSuite/path.js');
	var utilityObj = require('../../../lib/utility.js');
	var loginBaseObj = require('../../../base/audienceSuite/loginBase.js');
	var segmentBaseObj = require('../../../base/audienceSuite/segmentBase.js');
	var turnDataFilterObj = require('../../../lib/turnDataProvider.js');
	var assertsObj = require('../../../lib/assert.js');
	var loggerObj = require('../../../lib/logger.js');
	var logger=loggerObj.loggers("Shared segmentTest");
	var actualValue ;
	var expectedValue;
	var segmentPath=pathObj.getSharedPageSegmentTestDataFilePath();
	
	beforeEach(function() {
		isAngularSite(false);
		actualValue='';
		expectedValue='';
	});
	browser.ignoreSynchronization = true;			
	

	describe("Operations for Shared Segment Tab:"+browser.user,function(){
		/**
		 * This method used to call login method of loginBase file.
		 * 
		 * @author omp
		 */
		it('Turn login page Shared segment tab:'+browser.user, function() {
			var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, browser.user);
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			loginBaseObj.login(loginTestdata);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			assertsObj.assertTrue(loginBaseObj.isLogoutUserBoxPresent(browser.user),'Logout UserBox is not available.');
		});
		
		/**
		 * This method is used to redirect Campaign suite project.
		 * 
		 * @author omp
		 */
		it('Redirection to Campaign Suite Shared segment tab:'+browser.user, function() {
			loginBaseObj.redirectToCampaignSuite();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			assertsObj.assertTrue(loginBaseObj.checkCampaignSuite(),"Not redirect to Campaign Suite");
		});
		
		/**
		 * This method is used to call create segment (with All Advertiser availability and key/value segment rules) 
		 * method of segmentBase file.
		 * 
		 * @author omp
		 */
		it('Create Segment with All Advertisers in shared page:'+browser.user, function() {
			var segmentTestData = turnDataFilterObj.getDataProvider(segmentPath, browser.env, browser.language,'createAllAdvertiserSegment');
			segmentBaseObj.redirectToSharedSegmentTab();
			browser.waitForAngular();			  
			var segmentName = segmentTestData.segmentName + utilityObj.getFullDateString();
			segmentBaseObj.createSegment(segmentPath,segmentTestData,segmentName,'createAllAdvertiserSegment');
				utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentName);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					segmentBaseObj.returnElementList().map(function (elm) {
					   return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListContain(result,segmentName,'Segment Name is not found in search list.');
					});
		});
		
		/**
		 * This method is used to call create segment (with Single Advertiser availability and Taxonomy segment rules) 
		 * method of segmentBase file.
		 * 
		 * @author omp
		 */
		it('Create Segment with Single advertiser with Taxonomy segment rules in shared page:'+browser.user, function() {
			var segmentTestData = turnDataFilterObj.getDataProvider(segmentPath, browser.env, browser.language,'createSingleAdvertiserSegmentWithTexonomy');
			segmentBaseObj.redirectToSharedSegmentTab();
			var segmentName = segmentTestData.segmentName + utilityObj.getFullDateString();
			segmentBaseObj.createSegment(segmentPath,segmentTestData,segmentName,'createSingleAdvertiserSegmentWithTexonomy');
				utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentName);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					segmentBaseObj.returnElementList().map(function (elm) {
					   return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListContain(result,segmentName,'Segment Name is not found in search list.');
					});
				segmentBaseObj.deleteSegment(segmentTestData);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
					utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentName);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(1);
					segmentBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
				assertsObj.assertListNotContain(result,segmentName,'Deleted Segment Name:' + segmentName + ' is  found in list.');
				});
		});
		
		/**
		 * This method is used to call create segment (with Single Advertiser availability and Individual Pages segment rules) 
		 *  method of segmentBase  file.
		 * 
		 * @author omp
		 */
		it('Create Segment with Single advertiser with Individual Pages segment rules in shared page:'+browser.user, function() {
			var segmentTestData = turnDataFilterObj.getDataProvider(segmentPath, browser.env, browser.language,'createSingleAdvertiserSegmentWithIndividualPages');
			segmentBaseObj.redirectToSharedSegmentTab();
			var segmentName = segmentTestData.segmentName + utilityObj.getFullDateString();
			segmentBaseObj.createSegment(segmentPath,segmentTestData,segmentName,'createSingleAdvertiserSegmentWithIndividualPages');
					utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentName);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					segmentBaseObj.returnElementList().map(function (elm) {
					   return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListContain(result,segmentName,'Segment Name is not found in search list.');
					});
				segmentBaseObj.deleteSegment(segmentTestData);				
				utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentName);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				segmentBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
			assertsObj.assertListNotContain(result,segmentName,'Deleted Segment Name:' + segmentName + ' is  found in list.');
			});
		});
		
		/**
		 * This method is used to call create segment (with Single Advertiser availability and Impression Sharing segment rules) 
		 *  method of segmentBase file.
		 * 
		 * @author omp
		 */
		it('Create Segment with Single advertiser with Impression Sharing in shared page segment:'+browser.user, function() {
			var segmentTestData = turnDataFilterObj.getDataProvider(segmentPath, browser.env, browser.language,'createSingleAdvertiserSegmentWithImpressionSharing');
			segmentBaseObj.redirectToSharedSegmentTab();
			var segmentName = segmentTestData.segmentName + utilityObj.getFullDateString();
			segmentBaseObj.createSegment(segmentPath,segmentTestData,segmentName,'createSingleAdvertiserSegmentWithImpressionSharing');
					utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentName);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					segmentBaseObj.returnElementList().map(function (elm) {
					   return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListContain(result,segmentName,'Segment Name is not found in search list.');
					});
				segmentBaseObj.deleteSegment(segmentTestData);
					utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentName);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(1);
					segmentBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
				assertsObj.assertListNotContain(result,segmentName,'Deleted Segment Name:' + segmentName + ' is  found in list.');
				});
		});
		
		/**
		 * This method is used to call create segment (with Analytics Only availability and keyword segment rules) 
		 * method of segmentBase file.
		 * 
		 * @author omp
		 */
		xit('Create Segment with Analytics Only in shared page segment :'+browser.user, function() {
			var segmentTestData = turnDataFilterObj.getDataProvider(segmentPath, browser.env, browser.language,'createAnalyticsOnlySegment');
			segmentBaseObj.redirectToSharedSegmentTab();
			var segmentName = segmentTestData.segmentName + utilityObj.getFullDateString();
			segmentBaseObj.createSegment(segmentPath,segmentTestData,segmentName,'createAnalyticsOnlySegment');
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			assertsObj.assertNotNull(utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentName),"Newly Created Segment Name("+segmentName+") is not found in search list.");
			segmentBaseObj.deleteSegment(segmentTestData);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			assertsObj.assertNull(utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentName),"Deleted Newly Created Segment Name("+segmentName+") is found in search list.");
		});
		
		/**
		 * This method used to call editSegment method of segmentBase file (For All Advertiser availability Segment)
		 * and assert that edited segment is available in search list or not.
		 * 
		 * @author omp
		 */
		it('Edit newly created All Advertiser avalibality shared page Segment:'+browser.user, function() {
			var segmentTestData = turnDataFilterObj.getDataProvider(segmentPath,browser.env, browser.language,'createAllAdvertiserSegment');
			segmentBaseObj.redirectToSharedSegmentTab();
			segmentBaseObj.clearAllFilter();
			utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentTestData.createdSegment);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			var updatedSegmentName = segmentTestData.createdSegment + "_Edit";
			segmentBaseObj.editSegment(segmentPath,segmentTestData, updatedSegmentName,'createAllAdvertiserSegment');
					utilityObj.findList(segmentBaseObj.returnSearchElement(),updatedSegmentName);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(4);
					segmentBaseObj.returnElementList().map(function (elm) {
					   return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListContain(result,updatedSegmentName,"Updated All Advertiser availabality Segment("+updatedSegmentName+") is not found in search list.");
					});	
		});
		
		/**
		 * This method used to call copySegment method of segmentBase file (For All Advertiser availability Segment)
		 * and assert that copied segment is available in search list or not.
		 * 
		 * @author omp
		 */
		it('Copy newly created All Advertiser avalibality shared page Segment:'+browser.user, function() {
			var segmentTestData = turnDataFilterObj.getDataProvider(segmentPath,browser.env, browser.language,'createAllAdvertiserSegment');
			segmentBaseObj.redirectToSharedSegmentTab();
			utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentTestData.updatedSegment);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			segmentBaseObj.copySegment(segmentTestData);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
				utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentTestData.updatedSegment+" - copy");
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2);
				segmentBaseObj.returnElementList().map(function (elm) {
				   return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,segmentTestData.updatedSegment+" - copy","Copied All Advertiser availabality Segment Name("+segmentTestData.updatedSegment+" - copy) is not found in search list.");
				});
				segmentBaseObj.deleteSegment(segmentTestData);
				utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentTestData.updatedSegment+" - copy");
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				segmentBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
			assertsObj.assertListNotContain(result,segmentTestData.updatedSegment+" - copy","Deleted Copied Segment with All Advertiser availabality Name("+segmentTestData.updatedSegment+" - copy) is found in search list.");
			});
		});
		
		/**
		 * This method used to call deleteSegment method of segmentBase file (For All Advertiser availability Segment)
		 * and assert that deleted segment is available in search list or not.
		 * 
		 * @author omp
		 */
		it('Delete newly created All Advertiser avalibality shared page Segment:'+browser.user, function() {
			var segmentTestData = turnDataFilterObj.getDataProvider(segmentPath,browser.env, browser.language,'createAllAdvertiserSegment');
			segmentBaseObj.redirectToSharedSegmentTab();
			segmentBaseObj.clearAllFilter();
			utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentTestData.updatedSegment);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			segmentBaseObj.deleteSegment(segmentTestData);
					utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentTestData.updatedSegment);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(1);
					segmentBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
				assertsObj.assertListNotContain(result,segmentTestData.updatedSegment,"Deleted All Advertiser availability Segment Name("+segmentTestData.updatedSegment+") is found in search list.");
				});
		});
		
		/**
		  * This method is use to call goto data segment and click Show deleted segment method 
		  * of segmentBase and assert that deleted All Advertiser availability segment is available in search list or not.
		  * 
		  * @author omp
		  */
		it('Show Deleted All Advertiser availability shared page segment:'+browser.user, function() {
			var segmentTestData = turnDataFilterObj.getDataProvider(segmentPath,browser.env, browser.language,'createAllAdvertiserSegment');
			segmentBaseObj.redirectToSharedSegmentTab();
			segmentBaseObj.clickShowDeletedSegment(segmentTestData);
					utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentTestData.updatedSegment);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(1);
					segmentBaseObj.returnElementList().map(function (elm) {
					   return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListContain(result,segmentTestData.updatedSegment,"Deleted All Advertiser availability Segment Name("+segmentTestData.updatedSegment+") is not found in list of show deleted Segment.");
					});
		});
		
		/**
		  * This method is use to call goto view audit log page and search audit log field name method 
		  * of segmentBase and assert that edited field name is available or not.
		  * 
		  * @author omp
		  */
		it('View Audit Log for All Advertiser Availability shared page Segment:'+browser.user,function(){
			var segmentTestData = turnDataFilterObj.getDataProvider(segmentPath,browser.env, browser.language, 'createAllAdvertiserSegment');
			browser.waitForAngular();
			segmentBaseObj.gotoViewAuditLog();
			utilityObj.browserWaitforseconds(3);
			segmentBaseObj.searchAuditLog(segmentTestData.fieldNameValue);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			actualValue = segmentBaseObj.getFieldNameValue();
			expectedValue = segmentTestData.fieldNameValue;
			assertsObj.assertEqual(actualValue,expectedValue,"All Advertiser Availability Actual Segment's Field name("+actualValue+") is not matching with("+expectedValue+") expected Segment field name for audit log.");
			utilityObj.switchToMainWindow();
		});
		
		/**
		  * This method is use to call click Hide deleted segment method (for All Advertiser availability segment) 
		  * of dataProviderBase and assert that deleted segment is available in search list or not.
		  * 
		  * @author omp
		  */
		it('Hide Deleted All Advertiser avalibality shared page Segment:'+browser.user,function() {
			var segmentTestData = turnDataFilterObj.getDataProvider(segmentPath,browser.env, browser.language, 'createAllAdvertiserSegment');
			segmentBaseObj.clickHideDeletedSegment(segmentTestData);
					utilityObj.findList(segmentBaseObj.returnSearchElement(),segmentTestData.updatedSegment);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(1);
					segmentBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
				assertsObj.assertListNotContain(result,segmentTestData.updatedSegment,"Deleted All Advertiser Availability Segment Name("+segmentTestData.updatedSegment+") is found in list of hide deleted Segment.");
				});
		});
		
		/**
		 * This method used to call select Market Filter of segmentBase class
		 * and assert that compare selected market and market name in Segment list page.
		 * 
		 * @author omp
		 */
		it('Filter Market for Shared Segment Tab:'+browser.user, function(){
			var segmentTestData = turnDataFilterObj.getDataProvider(segmentPath,browser.env, browser.language,'createAllAdvertiserSegment');
			segmentBaseObj.clearSearchBoxText();
			var userType = browser.user;
			if(userType.toUpperCase()=='CS'){
				segmentBaseObj.selectMarketFilter(segmentTestData.market);
				actualValue = segmentBaseObj.getMarketName();
				expectedValue = segmentTestData.market;
				assertsObj.assertEqual(actualValue,expectedValue,
			"Actual Market filter("+actualValue+") is not matching with expected filter("+expectedValue+") in Shared Segment.");
			}
			else{
				logger.info(userType + " user doesnot contain market filter.");
			}
		});
		
		/**
		 * This method is use to call sort by segment name method of segment
		 * base class and assert that records are sort by name or not.
		 * 
		 * @author omp
		 */
		
		it('Sorting functionality for Shared Segment Tab:'+browser.user, function(){
			segmentBaseObj.sortBySegmentName();
			assertsObj.assertNotNull(segmentBaseObj.assertNameColumn(),"Sorting on segment name column is not perform.");
		});
		
		/**
		 * This method is used to call logout method of
		 * segmentBase JS File.
		 * 
		 * @author omp
		 */
		it('Logout Shared segment tab:'+browser.user,function(){
			loginBaseObj.logout(browser.user,"CS");
			assertsObj.assertTrue(loginBaseObj.isUserNamePresent(),"Username Textbox is not found for login page.");
		});
	});	
	

	
	
});