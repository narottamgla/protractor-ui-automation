/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

describe("Turn Audience Suite Cookie Space Data Provider Tab:"+browser.user,function(){
	
	var pathObj  =require( '../../../testData/audienceSuite/path.js');
	var utilityObj = require('../../../lib/utility.js');
	var loginBaseObj = require('../../../base/audienceSuite/loginBase.js');
	var cookieSpaceDataProviderBaseObj = require('../../../base/audienceSuite/cookieSpaceDataProvidersBase.js');
	var turnDataFilterObj = require('../../../lib/turnDataProvider.js');
	var assertsObj = require('../../../lib/assert.js');
	var loggerObj = require('../../../lib/logger.js');
	var logger=loggerObj.loggers("cookieSpaceDataProvidersBase");
	var actualValue ;
	var expectedValue;
	
	beforeEach(function() {
		isAngularSite(false);		
		actualValue='';
		expectedValue='';
	});
	browser.ignoreSynchronization = true;			
	if((browser.user).toUpperCase()=='CS'){
		/**
		 * This method used to call login method of loginBase file.
		 * 
		 * @author narottamc
		 */
		it('Turn login page cookieSpaceDataProviders tab', function() {
				var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, browser.user);
				utilityObj.geturl(loginTestdata.url);
				browser.waitForAngular();
				loginBaseObj.login(loginTestdata);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(4);
				browser.waitForAngular();
	     		assertsObj.assertTrue(loginBaseObj.isLogoutUserBoxPresent(browser.user),'Logout UserBox is not available.');
		});
		
		/**
		 * This method is used to redirect Audience suite project.
		 * 
		 * @author narottamc
		 */
		it('Redirection to Audience Suite cookieSpaceDataProviders tab', function() {
			loginBaseObj.redirectToAudienceSuite();
			browser.waitForAngular();
			var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, browser.user);
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			assertsObj.assertTrue(loginBaseObj.checkAudienceSuite(),"Not redirect to Audience Suite");
		});
		
		/**
		 * This method used to call create cookieSpaceDataProvider method of cookieSpaceDataProviderBase file
		 * and assert that newly created Cookie Space Data Provider is available in search list or not.
		 * 
		 * @author narottamc
		 */
		it('Create Cookie Space Data Provider',function(){
			var cookieSpaceDataProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getCookieSpaceDataProviderTestDataFilePath(), browser.env, browser.language,'createCookieSpaceDataProvider');
			cookieSpaceDataProviderBaseObj.redirectToCookieSpaceDataProvidersTab();
			browser.waitForAngular();			
			utilityObj.browserWaitforseconds(3);			
			cookieSpaceDataProviderBaseObj.newCookieSpaceDataProviderWithCancelButton();
			var cookieSpaceDataProviderName = cookieSpaceDataProviderTestData.cookieSpaceDataProviderName + utilityObj.getTimeStamp();
			cookieSpaceDataProviderBaseObj.createCookieSpaceDataProvider(cookieSpaceDataProviderTestData,cookieSpaceDataProviderName,'createCookieSpaceDataProvider');
				utilityObj.findList(cookieSpaceDataProviderBaseObj.returnSearchElement(),cookieSpaceDataProviderName);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
				cookieSpaceDataProviderBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,cookieSpaceDataProviderName,'Newly Created Cookie Space Data Provider Name("+cookieSpaceDataProviderName+") is not found in search list.');
				});
		});
		
		/**
		 * This method used to call editCookieSpaceDataProvider method of cookieSpaceDataProviderBase file
		 * and assert that edited Cookie Space Data Provider is available in search list or not.
		 * 
		 * @author narottamc
		 */
		it('Edit newly created Cookie Space Data Provider', function() {
			utilityObj.browserWaitforseconds(3);
			var cookieSpaceDataProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getCookieSpaceDataProviderTestDataFilePath(),browser.env, browser.language,'createCookieSpaceDataProvider');
			cookieSpaceDataProviderBaseObj.redirectToCookieSpaceDataProvidersTab();
			utilityObj.findList(cookieSpaceDataProviderBaseObj.returnSearchElement(),cookieSpaceDataProviderTestData.createdCookieSpaceDataProvider);
			var updatedCookieSpaceDataProviderName = cookieSpaceDataProviderTestData.createdCookieSpaceDataProvider + "_Edit";
			cookieSpaceDataProviderBaseObj.editCookieSpaceDataProvider(cookieSpaceDataProviderTestData, updatedCookieSpaceDataProviderName,'createCookieSpaceDataProvider');			
				utilityObj.findList(cookieSpaceDataProviderBaseObj.returnSearchElement(),updatedCookieSpaceDataProviderName);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
				cookieSpaceDataProviderBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,updatedCookieSpaceDataProviderName,'Updated Cookie Space Data Provider Name("+updatedCookieSpaceDataProviderName+") is not found in search list.');
				});
		});
		
		/**
		 * This method used to call redirect to viewCookieSpaceDataProvider method of cookieSpaceDataProviderBase
		 * file and assert that viewed cookie space data provider match with actual cookie space data provider or not.
		 * 
		 * @author narottamc
		 */
		it('View Cookie Space Data Provider',function() {
			var cookieSpaceDataProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getCookieSpaceDataProviderTestDataFilePath(),browser.env, browser.language,'createCookieSpaceDataProvider');
			cookieSpaceDataProviderBaseObj.redirectToCookieSpaceDataProvidersTab();
			utilityObj.findList(cookieSpaceDataProviderBaseObj.returnSearchElement(),cookieSpaceDataProviderTestData.updatedCookieSpaceDataProvider);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			cookieSpaceDataProviderBaseObj.redirectToViewCookieSpaceDataProviderPage();
			actualValue=cookieSpaceDataProviderBaseObj.returnCookieSpaceDataProviderNameOfViewPage(cookieSpaceDataProviderTestData.updatedCookieSpaceDataProvider);
			expectedValue = cookieSpaceDataProviderTestData.updatedCookieSpaceDataProvider;
			assertsObj.assertEqual(actualValue,expectedValue,
			 			"Cookie Space Data Provider name("+actualValue+") is not match with expected Cookie Space Data Provider Name"+expectedValue+" in view Cookie Space Data Proider page.");
		});
		
		/**
		 * This method used to call delete Cookie Space Data Provider method of cookieSpaceDataProviderBase
		 * file and assert deleted cookie space data provider is available in search list or not.
		 * 
		 * @author narottamc
		 */
		it('Delete Cookie Space Data Provider',function() {
			var cookieSpaceDataProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getCookieSpaceDataProviderTestDataFilePath(),browser.env, browser.language,'createCookieSpaceDataProvider');
			cookieSpaceDataProviderBaseObj.redirectToCookieSpaceDataProvidersTab();
			utilityObj.findList(cookieSpaceDataProviderBaseObj.returnSearchElement(),cookieSpaceDataProviderTestData.updatedCookieSpaceDataProvider);
			cookieSpaceDataProviderBaseObj.deleteCookieSpaceDataProvider(cookieSpaceDataProviderTestData);
				utilityObj.findList(cookieSpaceDataProviderBaseObj.returnSearchElement(),cookieSpaceDataProviderTestData.updatedCookieSpaceDataProvider);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				cookieSpaceDataProviderBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListNotContain(result,cookieSpaceDataProviderTestData.updatedCookieSpaceDataProvider,'Deleted Cookie Space Data Provider Name("+cookieSpaceDataProviderTestData.updatedCookieSpaceDataProvider+") is found in search list.');
				});
		});
		
		/**
		  * This method is use to call goto data provider page and click Show deleted cookie space data provider method 
		  * of cookieSpaceDataProviderBase and assert that deleted Cookie Space Data Provider is available in search list or not.
		  * 
		  * @author narottamc
		  */
		it('Show Deleted Cookie Space Data Provider', function() {
			var cookieSpaceDataProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getCookieSpaceDataProviderTestDataFilePath(),browser.env, browser.language,'createCookieSpaceDataProvider');
			cookieSpaceDataProviderBaseObj.redirectToCookieSpaceDataProvidersTab();
			cookieSpaceDataProviderBaseObj.clickShowCookieSpaceDeletedDataProvider();
				utilityObj.findList(cookieSpaceDataProviderBaseObj.returnSearchElement(),cookieSpaceDataProviderTestData.updatedCookieSpaceDataProvider);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				cookieSpaceDataProviderBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,cookieSpaceDataProviderTestData.updatedCookieSpaceDataProvider,"Deleted Cookie Space Data provider Name("+cookieSpaceDataProviderTestData.updatedCookieSpaceDataProvider+") is not found in list of show deleted Cookie Space Data Provider.");
				});
		});
		
		/**
		  * This method is use to call goto view audit log page and search audit log field name method 
		  * of cookieSpaceDataProviderBase and assert that edited field name is available or not.
		  * 
		  * @author narottamc
		  */
		it('View Audit Log for Cookie Space Data Provider',function(){
			var cookieSpaceDataProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getCookieSpaceDataProviderTestDataFilePath(),browser.env, browser.language, 'createCookieSpaceDataProvider');
			utilityObj.findList(cookieSpaceDataProviderBaseObj.returnSearchElement(),cookieSpaceDataProviderTestData.updatedCookieSpaceDataProvider);
			cookieSpaceDataProviderBaseObj.gotoViewAuditLog();
			isAngularSite(false);
			utilityObj.browserWaitforseconds(3);
			cookieSpaceDataProviderBaseObj.searchAuditLog(cookieSpaceDataProviderTestData.fieldNameValue);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			actualValue=cookieSpaceDataProviderBaseObj.getFieldNameValue();
			expectedValue=cookieSpaceDataProviderTestData.fieldNameValue;
			assertsObj.assertEqual(actualValue,expectedValue,"Cookie Space Data Provider Field name ("+actualValue+")is not found for audit log.");
			utilityObj.switchToMainWindow();
		});
		
		/**
		  * This method is use to call click Hide deleted cookie space data provider method 
		  * of cookieSpaceDataProviderBase and assert that deleted cookie space data provider is available in search list or not.
		  * 
		  * @author narottamc
		  */
		it('Hide Deleted Cookie Space Data Provider',function() {
			utilityObj.browserWaitforseconds(3);
			var cookieSpaceDataProviderTestData = turnDataFilterObj.getDataProvider(pathObj.getCookieSpaceDataProviderTestDataFilePath(),browser.env, browser.language,'createCookieSpaceDataProvider');
			cookieSpaceDataProviderBaseObj.clickHideDeletedCookieSpaceDataProvider();
				utilityObj.findList(cookieSpaceDataProviderBaseObj.returnSearchElement(),cookieSpaceDataProviderTestData.updatedCookieSpaceDataProvider);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				cookieSpaceDataProviderBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListNotContain(result,cookieSpaceDataProviderTestData.updatedCookieSpaceDataProvider,"Deleted Cookie Space Data Provider Name("+cookieSpaceDataProviderTestData.updatedCookieSpaceDataProvider+") is found in list of hide deleted Cookie Space Data Provider.");
				});
		});
		
		/**
		 * This method is use to call sort by cookie space data provider name method of cookieSpaceDataProviderBase
		 *  and assert that records are sort by name or not.
		 * 
		 * @author narottamc
		 */
		
		it('Sorting functionality for Cookie Space Data Provider', function(){
			cookieSpaceDataProviderBaseObj.redirectToCookieSpaceDataProvidersTab();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			cookieSpaceDataProviderBaseObj.sortByCookieSpaceDataProviderName();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			assertsObj.assertNotNull(cookieSpaceDataProviderBaseObj.assertNameColumn(),"Sorting on data provider name column is not perform.");
		});
		
		/**
		 * This method is used to call logout method of
		 * cookieSpaceDataProviderBase JS File.
		 * 
		 * @author narottamc
		 */
		it('Logout Cokkie space data Provider tab:',function(){
			loginBaseObj.logout(browser.user);
			isAngularSite(false);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			assertsObj.assertTrue(loginBaseObj.isUserNamePresent(),"Username Textbox is not found for login page.");
		});
	}else{
		console.info(browser.user+" user does not have Cookie Space Data Provider Tab.");
	}
});