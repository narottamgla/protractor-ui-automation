/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/


describe("Turn Audience Suite Cookie Space Data Contract Tab:"+browser.user,function(){

	var pathObj  =require( '../../../testData/audienceSuite/path.js');
	var utilityObj = require('../../../lib/utility.js');
	var loginBaseObj = require('../../../base/audienceSuite/loginBase.js');
	var cookieSpaceDataContractBaseObj = require('../../../base/audienceSuite/cookieSpaceDataContractsBase.js');
	var turnDataFilterObj = require('../../../lib/turnDataProvider.js');
	var assertsObj = require('../../../lib/assert.js');
	var loggerObj = require('../../../lib/logger.js');
	var logger=loggerObj.loggers("cookieSpaceDataContractsTest");
	var testDataPath = pathObj.getCookieSpaceDataContractsTestDataFilePath();
	var actualValue;
	var expectedValue;
	beforeEach(function() {
		isAngularSite(false);
		actualValue='';
		expectedValue='';
	});
	browser.ignoreSynchronization = true;		
	
	if((browser.user).toUpperCase()=='CS'){	
		/**
		 * This method used to call login method of loginBase file.
		 * 
		 * @author hemins
		 */
		it('Turn login page cookieSpaceDataContracts tab', function() {
			var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, browser.user);
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			loginBaseObj.login(loginTestdata);
			utilityObj.browserWaitforseconds(3);
			browser.waitForAngular();
			assertsObj.assertTrue(loginBaseObj.isLogoutUserBoxPresent(browser.user),'Logout UserBox is not available');
		});
		
		
		/**
		 * This method is used to redirect Audience suite project.
		 * 
		 * @author hemins
		 */
		it('Redirection to Audience Suite cookieSpaceDataContracts tab', function() {
			loginBaseObj.redirectToAudienceSuite();
			browser.waitForAngular();
			var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, browser.user);
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			assertsObj.assertTrue(loginBaseObj.checkAudienceSuite(),"Not redirect to Audience Suite");
		});
		
		/**
		 * This test case used to create Cookie Space Data Provider with All Advertisers and API collection type .
		 * 
		 * @author hemins
		 */
		it('Creation of Cookie Space Data Contact with all advertisers and API collection type ',function(){
			var cookieSpaceDataContractsTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createCookieSpaceDataContractAllAdvertiser');
			cookieSpaceDataContractBaseObj.redirectToCookieSpaceDataContractsTab();
			browser.waitForAngular();			
			utilityObj.browserWaitforseconds(3);
			cookieSpaceDataContractBaseObj.selectCookieSpaceDataContractFromNewButton(cookieSpaceDataContractsTestData);
			browser.waitForAngular();			
			utilityObj.browserWaitforseconds(3);
			var dataContractName=cookieSpaceDataContractsTestData.dcName+utilityObj.getTimeStamp();
			cookieSpaceDataContractBaseObj.createCookieSpaceDataContracts(cookieSpaceDataContractsTestData,dataContractName,pathObj.getAdvertiserDataTexonomyDataFilePath(),'createCookieSpaceDataContractAllAdvertiser');
				utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),dataContractName);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
				cookieSpaceDataContractBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,dataContractName,'Newly Created Cookie Space Data Contracts('+dataContractName +') is not found in search list..');
				});
		});
		
		/**
		 * This test case used to create Cookie Space Data Provider with Single Advertisers and File collection type.
		 * 
		 * @author hemins
		 */ 
	   it('Creation of Cookie Space Data Contract with Single advertiser and File collection type',function(){
			var cookieSpaceDataContractsTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createCookieSpaceDataContractSingleAdvertiser');
			cookieSpaceDataContractBaseObj.redirectToCookieSpaceDataContractsTab();
			cookieSpaceDataContractBaseObj.selectCookieSpaceDataContractFromNewButton(cookieSpaceDataContractsTestData);
			var dataContractName=cookieSpaceDataContractsTestData.dcName+utilityObj.getTimeStamp();
			isAngularSite(false);
			cookieSpaceDataContractBaseObj.createCookieSpaceDataContracts(cookieSpaceDataContractsTestData,dataContractName,pathObj.getAdvertiserDataTexonomyDataFilePath(),'createCookieSpaceDataContractSingleAdvertiser');	
			browser.waitForAngular();
				utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),dataContractName);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
				cookieSpaceDataContractBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,dataContractName,'Newly Created Cookie Space Data Contracts('+dataContractName +') is not found in search list..');
				});
			cookieSpaceDataContractBaseObj.deleteCreatedCookieSpaceDataContract(dataContractName);  
				utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),dataContractName);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				cookieSpaceDataContractBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListNotContain(result,dataContractName,"Deleted Cookie Space Data Contract name("+cookieSpaceDataContractsTestData.updatedDC+") is found in search list.");
				});
	   });
	   
	   /**
		 * This test case used to create Cookie Space Data Provider with Analytics Only Advertisers and Pixel collection type Pixel.
		 * 
		 * @author hemins
		 */	
		xit('Creation of Cookie Space Data Contract with Analytics Only Type advertiser and Pixel collection type',function(){
			var cookieSpaceDataContractsTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createCookieSpaceDataContractAnalytics');
			cookieSpaceDataContractBaseObj.redirectToCookieSpaceDataContractsTab();
			cookieSpaceDataContractBaseObj.selectCookieSpaceDataContractFromNewButton(cookieSpaceDataContractsTestData);
			var dataContractName=cookieSpaceDataContractsTestData.dcName+utilityObj.getTimeStamp();
			cookieSpaceDataContractBaseObj.createCookieSpaceDataContracts(cookieSpaceDataContractsTestData,dataContractName,pathObj.getAdvertiserDataTexonomyDataFilePath(),'createCookieSpaceDataContractAnalytics');
				utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),dataContractName);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				cookieSpaceDataContractBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,dataContractName,'Newly Created Cookie Space Data Contracts('+dataContractName +') is not found in search list..');
				});
			cookieSpaceDataContractBaseObj.deleteCreatedCookieSpaceDataContract(dataContractName);  
				utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),dataContractName);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				cookieSpaceDataContractBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListNotContain(result,dataContractName,"Deleted Cookie Space Data Contract name("+cookieSpaceDataContractsTestData.updatedDC+") is found in search list.");
				});
		});
		
		/**
		 * This method is used to call editCreatedCookieSpaceDataContract method of cookieSpaceDataContracrtsBase JS file.
		 * 
		 * @author hemins
		 */
	  it('Edit Created cookie spce data contract',function(){
		    var cookieSpaceDataContractsTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createCookieSpaceDataContractAllAdvertiser');
		    cookieSpaceDataContractBaseObj.redirectToCookieSpaceDataContractsTab();
		    utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),cookieSpaceDataContractsTestData.createdDC);
		    var updatedDataContractName=cookieSpaceDataContractsTestData.createdDC+'_Edit';
		    utilityObj.browserWaitforseconds(3);
		    cookieSpaceDataContractBaseObj.editCreatedCookieSpaceDataContract('createCookieSpaceDataContractAllAdvertiser',updatedDataContractName);
				utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),updatedDataContractName);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				cookieSpaceDataContractBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,updatedDataContractName,"Edited Cookie Space Data Contract name:"+updatedDataContractName+" is not found in search list.");
				});
		});
		
	    /**
		 * This method is used to call gotoViewCookieSpaceDataContracts method of cookieSpaceDataContracrtsBase JS file.
		 * 
		 * @author hemins
		 */
	  it('View Created cookie space data contract',function(){
		    var cookieSpaceDataContractsTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createCookieSpaceDataContractAllAdvertiser');
			    cookieSpaceDataContractBaseObj.redirectToCookieSpaceDataContractsTab();
			    utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),cookieSpaceDataContractsTestData.updatedDC);
			    cookieSpaceDataContractBaseObj.gotoViewCookieSpaceDataContracts();
			    browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
			    actualValue=cookieSpaceDataContractBaseObj.viewCreatedCookieSpaceDataContract(cookieSpaceDataContractsTestData.updatedDC);
			    expectedValue=cookieSpaceDataContractsTestData.updatedDC;
			    assertsObj.assertEqual(actualValue,expectedValue,"Actual Cookie Space Data Contract Name:'"+actualValue+"' is not match with Expected Cookie Space Data contract name:'"+expectedValue+"' in view Cookie Space Data Contract page.");	
		});
	    
	    /**
		 * This method is used to call expandCookieSpaceDataContracts method of cookieSpaceDataContracrtsBase JS file.
		 * 
		 * @author hemins
		 */
	    it('Expand/Collapse/View configurataion functionality cookie space data contract',function(){
		    var cookieSpaceDataContractsTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createCookieSpaceDataContractAllAdvertiser');
	 	    cookieSpaceDataContractBaseObj.redirectToCookieSpaceDataContractsTab();
	        utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),cookieSpaceDataContractsTestData.updatedDC);
	        utilityObj.browserWaitforseconds(1);
	        	cookieSpaceDataContractBaseObj.viewConfigurationDataContracts(cookieSpaceDataContractsTestData);
	        	browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
	        	assertsObj.assertTrue(cookieSpaceDataContractBaseObj.checkViewConfigurationCloseButton(),'Close button not found on Cookie Space Data contract->\'View Configuraion\' page');
	        	cookieSpaceDataContractBaseObj.clickingDataContractViewConfigurationCloseButton();	
	      });
	    
	    /**
		 * This method is used to call gotoCookieSpaceDataContractObtainPixels method of cookieSpaceDataContracrtsBase JS file.
		 * 
		 * @author hemins
		 */
	   it('Obtain Pixel of cookie space data contract',function(){
	        var cookieSpaceDataContractsTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createCookieSpaceDataContractAllAdvertiser');
	  	    cookieSpaceDataContractBaseObj.redirectToCookieSpaceDataContractsTab();
	  	    utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),cookieSpaceDataContractsTestData.updatedDC);   
	  	    cookieSpaceDataContractBaseObj.gotoCookieSpaceDataContractObtainPixels();
	  	    browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
	  	    actualValue=cookieSpaceDataContractBaseObj.returnCookieSpaceDataContractObtainPixelHeaderName();
	  	    expectedValue=cookieSpaceDataContractsTestData.obtainPixelsName;	    	
	  	    assertsObj.assertEqual(actualValue,expectedValue,"Cookie Space Data Contract Actual Obtain pixel:"+actualValue+" is not matching with Expected:"+expectedValue+"on obtain pixel page."); 
	 	 });
	    
	    /**
		 * This method is used to call gotoCookieSpaceDataContractViewAuditLog method of cookieSpaceDataContracrtsBase JS file.
		 * 
		 * @author hemins
		 */
	    it('View Audit Log of cookie space data contract',function(){
	 	   var cookieSpaceDataContractsTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createCookieSpaceDataContractAllAdvertiser');
	  	   cookieSpaceDataContractBaseObj.redirectToCookieSpaceDataContractsTab();
	  	   utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),cookieSpaceDataContractsTestData.updatedDC);   
	  	   cookieSpaceDataContractBaseObj.gotoCookieSpaceDataContractViewAuditLog();
	  	   isAngularSite(false);
	  	   utilityObj.browserWaitforseconds(2);
	  	   cookieSpaceDataContractBaseObj.searchCookieSpaceDataContractAuditLog(cookieSpaceDataContractsTestData.auditLogSearch);
	  	   browser.waitForAngular();
		   actualValue=cookieSpaceDataContractBaseObj.getFieldNameValue();
		   expectedValue=cookieSpaceDataContractsTestData.auditLogSearch;
	  	   assertsObj.assertEqual(actualValue,expectedValue,"Actual Field name:"+actualValue+" is not matching with Expected Field name:'"+expectedValue+"' for audit log "); 		
			   utilityObj.switchToMainWindow();
		   });
	    
	    /**
		 * This method is used to call deleteCreatedCookieSpaceDataContract method of cookieSpaceDataContracrtsBase JS file.
		 * 
		 * @author hemins
		 */
	    it('Delete Created cookie space data contract',function(){
		    var cookieSpaceDataContractsTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createCookieSpaceDataContractAllAdvertiser');
			    cookieSpaceDataContractBaseObj.redirectToCookieSpaceDataContractsTab();
			    utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),cookieSpaceDataContractsTestData.updatedDC);
			    cookieSpaceDataContractBaseObj.deleteCreatedCookieSpaceDataContract(cookieSpaceDataContractsTestData.updatedDC);   
					utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),cookieSpaceDataContractsTestData.updatedDC);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(1);
					cookieSpaceDataContractBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListNotContain(result,cookieSpaceDataContractsTestData.updatedDC,"Deleted Cookie Space Data Contract name("+cookieSpaceDataContractsTestData.updatedDC+") is found in search list.");
					});
		});
	    
	    /**
		 * This method is used to call undoDeleteCookieSpaceDataContract method of cookieSpaceDataContracrtsBase JS file.
		 * 
		 * @author hemins
		 */
	    it('Undo delete cookie space data contract',function(){
	    	var cookieSpaceDataContractsTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createCookieSpaceDataContractAllAdvertiser');
	    	cookieSpaceDataContractBaseObj.redirectToCookieSpaceDataContractsTab();
	    	cookieSpaceDataContractBaseObj.clickShowDeletedCookieSpaceDataContract();
	    	browser.waitForAngular();
	    	utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),cookieSpaceDataContractsTestData.updatedDC);
	    	cookieSpaceDataContractBaseObj.selectDeletedDataContract();
	    	browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
	    	cookieSpaceDataContractBaseObj.undoDeleteCookieSpaceDataContract();
	    	browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
	    	cookieSpaceDataContractBaseObj.redirectToCookieSpaceDataContractsTab();
					utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),cookieSpaceDataContractsTestData.updatedDC);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(1);
					cookieSpaceDataContractBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListContain(result,cookieSpaceDataContractsTestData.updatedDC,"Undo Deleted Cookie Space Data Contract name("+cookieSpaceDataContractsTestData.updatedDC+") is not found in search list..");
					});
	    	cookieSpaceDataContractBaseObj.deleteCreatedCookieSpaceDataContract(cookieSpaceDataContractsTestData.updatedDC);  
	    });
	
	    /**
		 * This method is used to call clickShowDeletedCookieSpaceDataContract method of cookieSpaceDataContracrtsBase JS file.
		 * 
		 * @author hemins
		 */
	   it('Show Deleted cookie space data contract',function(){
		   var cookieSpaceDataContractsTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createCookieSpaceDataContractAllAdvertiser');
	 	   cookieSpaceDataContractBaseObj.redirectToCookieSpaceDataContractsTab();
	 	   cookieSpaceDataContractBaseObj.clickShowDeletedCookieSpaceDataContract();
	 	  browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
				utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),cookieSpaceDataContractsTestData.updatedDC);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				cookieSpaceDataContractBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,cookieSpaceDataContractsTestData.updatedDC,"Deleted Cookie Space Data Contract name("+cookieSpaceDataContractsTestData.updatedDC+") is not found in search list.");
				});
	   });
	   
	   /**
		 * This method is used to call clickHideDeletedCookieSpaceDataContract method of cookieSpaceDataContracrtsBase JS file.
		 * 
		 * @author hemins
		 */
	   it('Hide Deleted cookie space data contract',function(){
		   var cookieSpaceDataContractsTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createCookieSpaceDataContractAllAdvertiser');
	 	   cookieSpaceDataContractBaseObj.clickHideDeletedCookieSpaceDataContract();
				utilityObj.findList(cookieSpaceDataContractBaseObj.returnSearchElement(),cookieSpaceDataContractsTestData.updatedDC);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				cookieSpaceDataContractBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListNotContain(result,cookieSpaceDataContractsTestData.updatedDC,"Deleted Cookie Space Data Contract name("+cookieSpaceDataContractsTestData.updatedDC+") is found in search list.");
				});
	   });
	   
	   /**
		 * This method is used to call assertCookieSpaceDataContractNameColumn method of cookieSpaceDataContracrtsBase JS file.
		 * 
		 * @author hemins
		 */
	   it('Sorting functionality cookie space data contract',function(){
		   cookieSpaceDataContractBaseObj.redirectToCookieSpaceDataContractsTab(); 
		   cookieSpaceDataContractBaseObj.sortByCookieSpaceDataContractName();
		   assertsObj.assertNotNull(cookieSpaceDataContractBaseObj.assertCookieSpaceDataContractNameColumn(),"Sorting on Cookie Space Data Contract name column is not perform.");	
	   });
	   
	   /**
	    * This method is used to call logout method of
	    * loginBase JS File.
	    * 
	    * @author hemins
	   */
	   it('Turn Logout Cookie space data Contract tab:',function(){
		   browser.waitForAngular();
		   utilityObj.browserWaitforseconds(1);
		   loginBaseObj.logout(browser.user);
		   isAngularSite(false);
		   browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
		   assertsObj.assertTrue(loginBaseObj.isUserNamePresent(),"Username Textbox is not found for login page.");
	   });
	}else{
		console.info(browser.user+" user does not have Cookie Space Data Contracts Tab.");
	}
});