/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/


  /**
    * This test suite is use to check advertiser permission for data Contracts tab.
    * 
    * @author vishalbha
    */
   describe("Advertiser Permission of Data Contracts Tab:"+browser.user,function(){
	   
		var pathObj  =require( '../../../testData/audienceSuite/path.js');
		var utilityObj = require('../../../lib/utility.js');
		var loginBaseObj = require('../../../base/audienceSuite/loginBase.js');
		var dataContractBaseObj = require('../../../base/audienceSuite/dataContractBase.js');
		var turnDataFilterObj = require('../../../lib/turnDataProvider.js');
		var assertsObj = require('../../../lib/assert.js');
		var loggerObj = require('../../../lib/logger.js');
		var logger=loggerObj.loggers("dataContract Permission Test");
		var testDataPath=pathObj.getDataContractsTestDataFilePath();

	
		beforeEach(function() {
			isAngularSite(false);			
		    actualValue='';
			expectedValue='';
		});
		browser.ignoreSynchronization = true;				
		
	if((browser.user).toUpperCase()=='CS'){
	   /**
		 * This method is use to login with whitelisted user and verify advertiser permission.
		 * 
		 * @author vishalbha
		 */
	   it("Check Advertiser Permission for MMW user dataContract tab",function(){
		   logger.info("Checking Advertiser Permission for MMW user in Data Contracts tab.");
		   var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language,'MMW');
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			loginBaseObj.login(loginTestdata);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			loginBaseObj.isLogoutUserBoxPresent("MMW");
			browser.waitForAngular();
			var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, 'MMW', 'advertiserPermission');
			loginBaseObj.redirectToAudienceSuite();
			browser.waitForAngular();
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			dataContractBaseObj.redirectToDataContractsTab();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);		
	  			utilityObj.findList(dataContractBaseObj.returnSearchElement(),advertiserPermissionTestData.whitelistedAdvertiser);
	  			browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
	  			dataContractBaseObj.returnElementList().map(function (elm) {
	  			    return elm.getText();
	  			    }).then(function(result){
	  			    	assertsObj.assertListContain(result,advertiserPermissionTestData.whitelistedAdvertiser,'White listed data contracts is not found in search list for MMW use');
	  			});
	  			utilityObj.findList(dataContractBaseObj.returnSearchElement(),advertiserPermissionTestData.blacklistedAdvertiser);
	  			browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
	  			dataContractBaseObj.returnElementList().map(function (elm) {
	  			    return elm.getText();
	  			    }).then(function(result){
	  			    	assertsObj.assertListNotContain(result,advertiserPermissionTestData.blacklistedAdvertiser,'Black listed data contracts is found in search list for MMW user.');
	  			});
			loginBaseObj.logout("MMW");
		});
		
	   /**
		 * This method is use to login with blacklisted user and verify advertiser permission.
		 * 
		 * @author vishalbha
		 */
		it("Check Advertiser Permission for MMB user dataContract tab",function(){
			 logger.info("Checking Advertiser Permission for MMB user in Data Contracts tab.");
			var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language,'MMB');
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			loginBaseObj.login(loginTestdata);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			loginBaseObj.isLogoutUserBoxPresent("MMB");
			browser.waitForAngular();
			var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, 'MMB', 'advertiserPermission');
			loginBaseObj.redirectToAudienceSuite();
			browser.waitForAngular();
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			dataContractBaseObj.redirectToDataContractsTab();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
  			utilityObj.findList(dataContractBaseObj.returnSearchElement(),advertiserPermissionTestData.whitelistedAdvertiser);
	  			browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
	  			dataContractBaseObj.returnElementList().map(function (elm) {
	  			    return elm.getText();
	  			    }).then(function(result){
	  			    	assertsObj.assertListContain(result,advertiserPermissionTestData.whitelistedAdvertiser,'White listed data contracts is not found in search list for MMB user');
	  			});
	  			utilityObj.findList(dataContractBaseObj.returnSearchElement(),advertiserPermissionTestData.blacklistedAdvertiser);
	  			browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
	  			dataContractBaseObj.returnElementList().map(function (elm) {
	  			    return elm.getText();
	  			    }).then(function(result){
	  			    	assertsObj.assertListNotContain(result,advertiserPermissionTestData.blacklistedAdvertiser,'Black listed data contracts is found in search list for MMB user.');
	  			});
			loginBaseObj.logout("MMB");
		});
		
		/**
		 * This method is use to login with All Advertiser user and verify advertiser permission.
		 * 
		 * @author vishalbha
		 */
		it("Check Advertiser Permission for MMA user dataContract tab",function(){
			 logger.info("Checking Advertiser Permission for MMA user in Data Contracts tab.");
			var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, 'MMA');
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			loginBaseObj.login(loginTestdata);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			loginBaseObj.isLogoutUserBoxPresent("MMA");
			browser.waitForAngular();
			var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(testDataPath, browser.env,'MMA', 'advertiserPermission');
			loginBaseObj.redirectToAudienceSuite();
			browser.waitForAngular();
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			dataContractBaseObj.redirectToDataContractsTab();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			var dataContracts=(advertiserPermissionTestData.whitelistedAdvertiser).split(";");
			var j=0;
			for(var i=0;i<dataContracts.length;i++){
					  			utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContracts[i]);
		  			browser.waitForAngular();
		  			utilityObj.browserWaitforseconds(3);
		  			dataContractBaseObj.returnElementList().map(function (elm) {
		  			    return elm.getText();
		  			    }).then(function(result){
		  			    	assertsObj.assertListContain(result,dataContracts[j],'White listed advertiser is not found in search list for All Advertiser Permission user.');
		  			    	j++;
		  			});
			}
			loginBaseObj.logout("MMA");
		});
		
		/**
		 * This method is use to login with Cadmin user and verify advertiser permission.
		 * 
		 * @author hemins
		 */			
		it("Check Advertiser Permission for CAdmin user dataContract tab",function(){
			 logger.info("Checking Advertiser Permission for CAdmin user in Data Contracts tab.");
			var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, 'CADMIN');
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			loginBaseObj.login(loginTestdata);
			utilityObj.browserWaitforseconds(3);
			loginBaseObj.isLogoutUserBoxPresent("CAdmin");
			browser.waitForAngular();
			var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(testDataPath,browser.env, 'CADMIN', 'advertiserPermission');
			loginBaseObj.redirectToAudienceSuite();
			browser.waitForAngular();
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			dataContractBaseObj.redirectToDataContractsTab();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			browser.waitForAngular();
			var dataContracts=(advertiserPermissionTestData.whitelistedAdvertiser).split(";");
			var j=0;
			for(var i=0;i<dataContracts.length;i++){
					utilityObj.findList(dataContractBaseObj.returnSearchElement(),dataContracts[i]);
		  			browser.waitForAngular();
		  			utilityObj.browserWaitforseconds(3);
		  			dataContractBaseObj.returnElementList().map(function (elm) {
		  			    return elm.getText();
		  			    }).then(function(result){
		  			    	assertsObj.assertListContain(result,dataContracts[j],'White listed advertiser is not found in search list for Cadmin user.');
		  			    	j++;
		  			});
				}
			loginBaseObj.logout("CAdmin");
			});
		}
		else{
			logger.info("Advertiser Permission TestCase : Skipped,because UserType is Non 'CS'.");
		};
   	});