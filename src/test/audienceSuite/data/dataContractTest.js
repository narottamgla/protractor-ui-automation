

describe(' Audience Suite Data Contract Tab:'+browser.user,function () {

  var pathObj  = require('../../../testData/audienceSuite/path.js');
  var utilityObj = require('../../../lib/utility.js');
  var loginBaseObj = require('../../../base/audienceSuite/loginBase.js');
  var dataContractBaseObj = require('../../../base/audienceSuite/dataContractBase.js');
  var DataFilterObj = require('../../../lib/DataProvider.js');
  var assertsObj = require('../../../lib/assert.js');
  var loggerObj = require('../../../lib/logger.js');
  var logger = loggerObj.loggers('dataContractTest');
  var testDataPath = pathObj.getDataContractsTestDataFilePath();

  var actualValue;
  var expectedValue;
  beforeEach(function () {
    isAngularSite(false);   
    actualValue = '';
    expectedValue = '';
  });
  browser.ignoreSynchronization = true;

  /**
	 * This test Suite used to create data contracts.
	 * 
	 * @author narottamc
	 */
  describe('Operations for data Contract:'+browser.user,function () {

    /**
		 * This method used to call login method of loginBase file.
		 * 
		 * @author narottamc
		 */
    it(' login page dataContract tab:'+browser.user, function () {
      var loginTestdata = DataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, browser.user);
      utilityObj.geturl(loginTestdata.url);
      browser.waitForAngular();
      loginBaseObj.login(loginTestdata);
      utilityObj.browserWaitforseconds(3);
      browser.waitForAngular();
      assertsObj.assertTrue(loginBaseObj.isLogoutUserBoxPresent(browser.user),'Logout Link is not available');
    });

    /**
		 * This method is used to redirect Audience suite project.
		 * 
		 * @author narottamc
		 */
    it('Redirection to Audience Suite dataContract tab:'+browser.user, function () {
      loginBaseObj.redirectToAudienceSuite();
      browser.waitForAngular();
      var loginTestdata = DataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, browser.user);
      utilityObj.geturl(loginTestdata.url);
      browser.waitForAngular();
      utilityObj.browserWaitforseconds(3);
      assertsObj.assertTrue(loginBaseObj.checkAudienceSuite(),'Not redirect to Audience Suite');
    });

    /**
		 * This test Suite used to create Standard type data contracts.
		 * 
		 * @author narottamc
		 */
    describe('Operations for standard type data Contract:'+browser.user,function () {

       
    	 /**
		 * This test case used to create Single Advertisers with collection type File.
		 * 
		 * @author narottamc
		 */
      it('Creation of standared datacontact with Single advertiser and collection type File:'+browser.user,function () {
        var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
        browser.waitForAngular();
        dataContractBaseObj.redirectToDataContractsTab();
        utilityObj.browserWaitforseconds(3);
        browser.waitForAngular();
        dataContractBaseObj.selectDPFromfilter(dataContractTestData.dPName);
        utilityObj.browserWaitforseconds(3);
        dataContractBaseObj.clickNewDataContractsButton();
        browser.waitForAngular();
        var dataContractName = dataContractTestData.dcName + utilityObj.getTimeStamp();
        dataContractBaseObj.createDataContracts(testDataPath,dataContractTestData,dataContractName,pathObj.getAdvertiserDataTexonomyDataFilePath(),'createStandardDataContractSingleAdvertiser'); 
  			utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractName);
			dataContractBaseObj.reElementList().map(function (elm) {
			    re elm.getText();
			    }).then(function(result){
			    	assertsObj.assertListContain(result,dataContractName,'DataContract Name is not found in search list.');
			});        
      });

    	

        /**
  		 * This method is used to call editCreatedDataContract method of dataContractBase JS file.
  		 * 
  		 * @author narottamc
  		 */

        it('Edit standared Created data contract functionality:'+browser.user,function () {
            var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
            dataContractBaseObj.redirectToDataContractsTab();
            utilityObj.browserWaitforseconds(2);
            browser.waitForAngular();
            dataContractBaseObj.removeFilter();
            browser.waitForAngular();
            utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.createdDC);
            browser.waitForAngular();
            var updatedDataContractName = dataContractTestData.createdDC + '_Edit';
            dataContractBaseObj.editCreatedDataContract(testDataPath,dataContractTestData,'createStandardDataContractSingleAdvertiser',updatedDataContractName);
            browser.waitForAngular();
    			utilityObj.findList(dataContractBaseObj.reSearchElement(),updatedDataContractName);
    			 browser.waitForAngular();
    		      utilityObj.browserWaitforseconds(3);
    			dataContractBaseObj.reElementList().map(function (elm) {
    			    re elm.getText();
    			    }).then(function(result){
    			    	assertsObj.assertListContain(result,updatedDataContractName,'Updated DataContract Name is not found in search list.');
    			});
            browser.waitForAngular();
        });

        /**
  		 * This method is used to call gotoViewDataContracts method of dataContractBase JS file.
  		 * 
  		 * @author narottamc
  		 */
        it('View Created standared data contract  functionality:'+browser.user,function () {
            var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
            dataContractBaseObj.redirectToDataContractsTab();
            browser.waitForAngular();
            utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.updatedDC);
            browser.waitForAngular();
            utilityObj.browserWaitforseconds(2);
            dataContractBaseObj.gotoViewDataContracts();
            actualValue = dataContractBaseObj.viewCreatedDataContract(dataContractTestData);
            expectedValue = dataContractTestData.updatedDC;
            assertsObj.assertEqual(actualValue,expectedValue,'Actual Data Contract Name:\'' + actualValue + '\' is not match with Expected Data contract name:\'' + expectedValue + '\' in view Data Contract page.');
        });

        /**
  		 * This method is used to call expandDataContracts method of dataContractBase JS file.
  		 * 
  		 * @author narottamc
  		 */
        it('Expand/Collapse/View Configuration  functionality standared Data  contract:'+browser.user,function () {
            var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
            dataContractBaseObj.redirectToDataContractsTab();
            browser.waitForAngular();
            utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.updatedDC);
            browser.waitForAngular();
            utilityObj.browserWaitforseconds(1);
             	dataContractBaseObj.viewConfigurationDataContracts(dataContractTestData);
             	 browser.waitForAngular();
                 utilityObj.browserWaitforseconds(1);
            	 assertsObj.assertTrue(dataContractBaseObj.checkViewConfigurationCloseButton(),'Close button not found on Data contract->\'View Configuraion\' page');
            	 browser.waitForAngular();
                 utilityObj.browserWaitforseconds(2);
            	 dataContractBaseObj.clickingDataContractViewConfigurationCloseButton();
          });
        
        /**
  		 * This method is used to call gotoDataContractObtainPixels method of dataContractBase JS file.
  		 * 
  		 * @author narottamc
  		 */
        it('Obtain Pixel standared Data contract:'+browser.user,function () {
            var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
            dataContractBaseObj.redirectToDataContractsTab();
            utilityObj.browserWaitforseconds(3);
            browser.waitForAngular();
            utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.updatedDC);
            browser.waitForAngular();
            dataContractBaseObj.gotoDataContractObtainPixels();
            utilityObj.browserWaitforseconds(3);
            browser.waitForAngular();
            actualValue = dataContractBaseObj.reDataContractObtainPixelHeaderName();
            expectedValue = dataContractTestData.obtainPixelsName;
            assertsObj.assertEqual(actualValue,expectedValue,'Data Contract Actual Obtain pixel:' + actualValue + ' is not matching with Expected:' + expectedValue + 'on obtain pixel page.');
          });

          /**
    		 * This method is used to call gotoDataContractViewAuditLog method of dataContractBase JS file.
    		 * 
    		 * @author narottamc
    		 */
          it('View Audit Log Data contract:'+browser.user,function () {
            var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
            dataContractBaseObj.redirectToDataContractsTab();
            browser.waitForAngular();
            utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.updatedDC);
            browser.waitForAngular();
            dataContractBaseObj.gotoDataContractViewAuditLog();
            utilityObj.browserWaitforseconds(3);
            dataContractBaseObj.searchDataContractAuditLog(dataContractTestData.auditLogSearch);
            actualValue = dataContractBaseObj.getFieldNameValue();
            expectedValue = dataContractTestData.auditLogSearch;
            assertsObj.assertEqual(actualValue,expectedValue,'Actual Field name:' + actualValue + ' is not matching with Expected Field name:\'' + expectedValue + '\' for audit log ');
            
          });

          /**
    		 * This method is used to call deleteCreatedDataContract method of dataContractBase JS file.
    		 * 
    		 * @author narottamc
    		 */  
          it('Delete mapping from source & dastination data contract and finally delete data contract:'+browser.user,function () { 
        	  utilityObj.switchToMainWindow();
        	  var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
              dataContractBaseObj.redirectToDataContractsTab();
              utilityObj.browserWaitforseconds(3);
              browser.waitForAngular();
              if((dataContractTestData.crossDevice.toUpperCase()=='YES') &&((browser.user).toUpperCase()=='CS')){	
              dataContractBaseObj.deleteCrossDeviceMapping((dataContractTestData.selectDest[2]).value, 'Source');
              utilityObj.browserWaitforseconds(3);
              dataContractBaseObj.deleteCrossDeviceMapping(dataContractTestData.updatedDC, 'Destination');
              }
              dataContractBaseObj.deleteCreatedDataContract(dataContractTestData.updatedDC);
              utilityObj.browserWaitforseconds(3);
              browser.waitForAngular();
      				utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.updatedDC);
      				browser.waitForAngular(); 
      				utilityObj.browserWaitforseconds(3);
      				dataContractBaseObj.reElementList().map(function (elm) {
      				    re elm.getText();
      				    }).then(function(result){
      			assertsObj.assertListNotContain(result,dataContractTestData.updatedDC,'Deleted Data Contract Name:' + dataContractTestData.updatedDC + ' is  found in list.');
      			});
              browser.waitForAngular();
            });
          
          /**
    		 * This method is used to call undoDeleteDataContract method of dataContractBase JS file.
    		 * 
    		 * @author narottamc
    		 */

          it('Undo delete Data contract:'+browser.user,function () {
            var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
            dataContractBaseObj.redirectToDataContractsTab();
            utilityObj.browserWaitforseconds(4);
            browser.waitForAngular();
            dataContractBaseObj.clickShowDeletedDataContract();
            browser.waitForAngular();
            utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.updatedDC);
            browser.waitForAngular();
            dataContractBaseObj.selectDeletedDataContract();
            browser.waitForAngular();
            utilityObj.browserWaitforseconds(2);
            dataContractBaseObj.undoDeleteDataContract();
            browser.waitForAngular();
            utilityObj.browserWaitforseconds(3);
            dataContractBaseObj.redirectToDataContractsTab();
            browser.waitForAngular();
    				utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.updatedDC);
    				 browser.waitForAngular();
    			      utilityObj.browserWaitforseconds(3);
    				dataContractBaseObj.reElementList().map(function (elm) {
    				    re elm.getText();
    				    }).then(function(result){
    			assertsObj.assertListContain(result,dataContractTestData.updatedDC,'Undo Deleted Data Contract Name:' + dataContractTestData.updatedDC + ' is not found in list.');
    			});
            dataContractBaseObj.deleteCreatedDataContract(dataContractTestData.updatedDC);
            browser.waitForAngular();
            utilityObj.browserWaitforseconds(3);
          });

          /**
    		 * This method is used to call clickShowDeletedDataContract method of dataContractBase JS file.
    		 * 
    		 * @author narottamc
    		 */

          it('Show Deleted data contract:'+browser.user,function () {
            var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
            dataContractBaseObj.redirectToDataContractsTab();
            utilityObj.browserWaitforseconds(2);
            browser.waitForAngular();
            dataContractBaseObj.clickShowDeletedDataContract();
            browser.waitForAngular(); 
    				utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.updatedDC);
    				 browser.waitForAngular();
    			      utilityObj.browserWaitforseconds(3);
    				dataContractBaseObj.reElementList().map(function (elm) {
    				    re elm.getText();
    				    }).then(function(result){
    			assertsObj.assertListContain(result,dataContractTestData.updatedDC,'Deleted Data Contract Name:' + dataContractTestData.updatedDC + ' is not found in list.');
    			});
          });

          /**
    		 * This method is used to call clickHideDeletedDataContract method of dataContractBase JS file.
    		 * 
    		 * @author narottamc
    		 */
          it('Hide Deleted data contract:'+browser.user,function () {
            var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
            dataContractBaseObj.clickHideDeletedDataContract();
            browser.waitForAngular();
    				utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.updatedDC);
    				 browser.waitForAngular();
    			      utilityObj.browserWaitforseconds(4);
    				dataContractBaseObj.reElementList().map(function (elm) {
    				    re elm.getText();
    				    }).then(function(result){
    			assertsObj.assertListNotContain(result,dataContractTestData.updatedDC,'Deleted Data Contract Name:' + dataContractTestData.updatedDC + ' is  found in list.');
    			});
          });

          /**
    		 * This method is used to call sortByDataContractName method of dataContractBase JS file.
    		 * 
    		 * @author narottamc
    		 */
          it('Sorting functionality Data contract:'+browser.user,function () {
            dataContractBaseObj.redirectToDataContractsTab();
            browser.waitForAngular();
            dataContractBaseObj.sortByDataContractName();
            utilityObj.browserWaitforseconds(2);
            assertsObj.assertNotNull(dataContractBaseObj.assertDataContractNameColumn(),'Sorting on Data Contract name column is not perform.');
          });

          /**
    		 * This method is used to call selectMarketFilterDataContract method of dataContractBase JS file.
    		 * 
    		 * @author narottamc
    		 */
          
          it('Market Filter functionality Data contract:'+browser.user,function () {  
            dataContractBaseObj.redirectToDataContractsTab();
            browser.waitForAngular(); 
            var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
            utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.updatedDC);
            var userType = browser.user;
            if (userType.toUpperCase() == 'CS') {
            	 dataContractBaseObj.removeFilter();
            	 browser.waitForAngular(); 
            	 utilityObj.browserWaitforseconds(2);
              dataContractBaseObj.selectMarketFilterDataContract(dataContractTestData.marketFilter);
              browser.waitForAngular();
              utilityObj.browserWaitforseconds(2);
              dataContractBaseObj.clearSearchDataContract();
              browser.waitForAngular();
              utilityObj.browserWaitforseconds(4);
              actualValue = dataContractBaseObj.getMarketNameDataContract();
              expectedValue = dataContractTestData.marketFilter;
              assertsObj.assertEqual(actualValue,expectedValue,
                'Acutal Market filter:\'' + actualValue + '\'is not matching with Expected Market filter:\'' + expectedValue + '\'in Data contract.');
              utilityObj.browserWaitforseconds(3);
              dataContractBaseObj.removeFilter();
              utilityObj.browserWaitforseconds(3);
            }
            else{
              logger.info(userType + ' user doesnot contain market filter.');
            }
          });
          
          
          /**
    		 * This method is used to call selectDPFromfilter method of dataContractBase JS file.
    		 * 
    		 * @author narottamc
    		 */
        
        it('Data Provider Filter functionality Data contract:'+browser.user,function () {
         var userType = browser.user;
         if (userType.toUpperCase() == 'CS') {
          dataContractBaseObj.redirectToDataContractsTab();
          utilityObj.browserWaitforseconds(3);
          browser.waitForAngular(); 
          var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractSingleAdvertiser');
        	 dataContractBaseObj.removeFilter();
         	 utilityObj.browserWaitforseconds(2);
            dataContractBaseObj.selectDPFromfilter(dataContractTestData.dPName);
            browser.waitForAngular(); 
            utilityObj.browserWaitforseconds(3);
            actualValue = dataContractBaseObj.getDataProviderNameDataContract();
            expectedValue = dataContractTestData.dPName;
            assertsObj.assertEqual(actualValue,expectedValue,
              'Acutal Data provider filter:\'' + actualValue + '\'is not matching with Expected data provider filter:\'' + expectedValue + '\'in Data contract.');
            utilityObj.browserWaitforseconds(3);
          }
          else{
            logger.info(userType + ' user doesnot contain market filter.');
          }
        });

         
          /**
    		 * This test case used to create Analytics Only Advertisers with collection type Pixel.
    		 * 
    		 * @author narottamc
    		 */
          xit('Creation of datacontact with Analytics Only Type advertiser and collection type Pixel:'+browser.user,function () {
            var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractAnalytics');
            dataContractBaseObj.redirectToDataContractsTab();
            dataContractBaseObj.selectDPFromfilter(dataContractTestData.dPName);
            utilityObj.browserWaitforseconds(3);
            dataContractBaseObj.clickNewDataContractsButton();
            var dataContractName = dataContractTestData.dcName + utilityObj.getTimeStamp();
            dataContractBaseObj.createDataContracts(testDataPath,dataContractTestData,dataContractName,pathObj.getAdvertiserDataTexonomyDataFilePath(),'createStandardDataContractAnalytics');
    			utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractName);
    			dataContractBaseObj.reElementList().map(function (elm) {
    			    re elm.getText();
    			    }).then(function(result){
    			    	assertsObj.assertListContain(result,dataContractName,'DataContract Name is not found in search list.');
    			});

            dataContractBaseObj.deleteCreatedDataContract(dataContractName);
     				utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.updatedDC);
    				dataContractBaseObj.reElementList().map(function (elm) {
    				    re elm.getText();
    				    }).then(function(result){
    			assertsObj.assertListNotContain(result,dataContractName,'Deleted Data Contract Name:' + dataContractName + ' is  found in list.');
    			});
          });
          
          /**
    		 * This test case used to call remove mapping from source data contract and then createDataContracts method of dataContractBae JS file.
    		 * 
    		 * @author narottamc
    		 */

          it('Creation of standared datacontact with all advertisers, collection type API and cross device functionality:'+browser.user,function () {
        	  var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createStandardDataContractAllAdvertiser');
              browser.waitForAngular();
              dataContractBaseObj.redirectToDataContractsTab();
              utilityObj.browserWaitforseconds(3);
              browser.waitForAngular();
               	if((dataContractTestData.crossDevice.toUpperCase()=='YES') &&((browser.user).toUpperCase()=='CS')){	
              	dataContractBaseObj.removeMappingSourceDataContract(dataContractTestData);
              	browser.waitForAngular();
              	}
                dataContractBaseObj.removeFilter();
                browser.waitForAngular();
          		dataContractBaseObj.selectDPFromfilter(dataContractTestData.dPName);
          		utilityObj.browserWaitforseconds(3);
          		dataContractBaseObj.clickNewDataContractsButton();
          	  var dataContractName = dataContractTestData.dcName + utilityObj.getTimeStamp();
          	  dataContractBaseObj.createDataContracts(testDataPath,dataContractTestData,dataContractName,pathObj.getAdvertiserDataTexonomyDataFilePath(),'createStandardDataContractAllAdvertiser');
       				utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractName);   
      				 browser.waitForAngular();
      			     utilityObj.browserWaitforseconds(3);    			    
      				dataContractBaseObj.reElementList().map(function (elm) {
      				    re elm.getText();
      				    }).then(function(result){
      				    	assertsObj.assertListContain(result,dataContractName,'DataContract Name is not found in search list.');
      				});
                dataContractBaseObj.deleteCreatedDataContract(dataContractName);
                utilityObj.browserWaitforseconds(4);
        				utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.updatedDC);
        				utilityObj.browserWaitforseconds(3);
        				dataContractBaseObj.reElementList().map(function (elm) {
        				    re elm.getText();
        				    }).then(function(result){
        			assertsObj.assertListNotContain(result,dataContractName,'Deleted Data Contract Name:' + dataContractName + ' is  found in list.');
        			});
                browser.waitForAngular();
          });
        });

    /**
	 * This test suite used to create Flextag type data contracts..
	 * 
	 * @author narottamc
	 */
	describe('Operations for FlexTag type dataContract:'+browser.user,function(){
		/**
		 * This test case used to create All Advertisers with collection type API.
		 * 
		 * @author vishalbha
		 */
      it('Creation of Flex tag datacontact with Single advertiser and collection type File:'+browser.user,function () {
    	var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createFlexTagDataContractSingle');
        if((dataContractTestData.crossDevice.toUpperCase()=='YES') &&((browser.user).toUpperCase()=='CS')){	
            dataContractBaseObj.removeMappingSourceDataContract(dataContractTestData);
    		}
		dataContractBaseObj.redirectToDataContractsTab();
        dataContractBaseObj.selectDPFromfilter(dataContractTestData.dPName);
        browser.waitForAngular();
		utilityObj.browserWaitforseconds(2);
        dataContractBaseObj.clickNewDataContractsButton();
        browser.waitForAngular();
        var dataContractName = dataContractTestData.dcName + utilityObj.getTimeStamp();
        dataContractBaseObj.createDataContracts(testDataPath,dataContractTestData,dataContractName,pathObj.getAdvertiserDataTexonomyDataFilePath(),'createFlexTagDataContractSingle');
			utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractName);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			dataContractBaseObj.reElementList().map(function (elm) {
			    re elm.getText();
			    }).then(function(result){
			    	assertsObj.assertListContain(result,dataContractName,'DataContract Name is not found in search list.');
			});
        dataContractBaseObj.deleteCreatedDataContract(dataContractName);
        browser.waitForAngular();
		utilityObj.browserWaitforseconds(2);
				utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.updatedDC);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2); 
				dataContractBaseObj.reElementList().map(function (elm) {
				    re elm.getText();
				    }).then(function(result){
			assertsObj.assertListNotContain(result,dataContractName,'Deleted Data Contract Name:' + dataContractName + ' is  found in list.');
			});
        dataContractBaseObj.removeFilter();
      });

      /**
		 * This test case used to create Single Advertisers with collection type File.
		 * 
		 * @author vishalbha
		 */

      xit('Creation of Flex tag datacontact with Analytics Only Type advertiser and collection type Pixel:'+browser.user,function () {
        var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createFlexTagDataContractAnalytics');
        dataContractBaseObj.redirectToDataContractsTab();
        dataContractBaseObj.selectDPFromfilter(dataContractTestData.dPName);
        utilityObj.browserWaitforseconds(3);
        dataContractBaseObj.clickNewDataContractsButton();
        var dataContractName = dataContractTestData.dcName + utilityObj.getTimeStamp();
        dataContractBaseObj.createDataContracts(testDataPath,dataContractTestData,dataContractName,pathObj.getAdvertiserDataTexonomyDataFilePath(),'createFlexTagDataContractAnalytics');
 			utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractName);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			dataContractBaseObj.reElementList().map(function (elm) {
			    re elm.getText();
			    }).then(function(result){
			    	assertsObj.assertListContain(result,dataContractName,'DataContract Name is not found in search list.');
			});
        dataContractBaseObj.deleteCreatedDataContract(dataContractName);
				utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.updatedDC);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2);
				dataContractBaseObj.reElementList().map(function (elm) {
				    re elm.getText();
				    }).then(function(result){
			assertsObj.assertListNotContain(result,dataContractName,'Deleted Data Contract Name:' + dataContractName + ' is  found in list.');
			});
        dataContractBaseObj.removeFilter();
      });

  	/**
	 * This test case used to create data contract with all advertiser with collection type Pixel.
	 * 
	 * @author narottamc
	 */
      xit('Creation of flex tag data Contract with all advertisers and collection type API:'+browser.user,function () {
        var dataContractTestData = DataFilterObj.getDataProvider(testDataPath,browser.env, browser.language,'createFlexTagDataContractAll');
        dataContractBaseObj.redirectToDataContractsTab();
        browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		  runs(function() {
        dataContractBaseObj.selectDataContractFromNewButton(dataContractTestData);
        var dataContractName = dataContractTestData.dcName + utilityObj.getTimeStamp();
        dataContractBaseObj.createDataContracts(testDataPath,dataContractTestData,dataContractName,pathObj.getAdvertiserDataTexonomyDataFilePath(),'createFlexTagDataContractAll');
 			utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractName);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			dataContractBaseObj.reElementList().map(function (elm) {
			    re elm.getText();
			    }).then(function(result){
			    	assertsObj.assertListContain(result,dataContractName,'DataContract Name is not found in search list.');
			});
        dataContractBaseObj.deleteCreatedDataContract(dataContractName);
				utilityObj.findList(dataContractBaseObj.reSearchElement(),dataContractTestData.updatedDC);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(1);
				dataContractBaseObj.reElementList().map(function (elm) {
				    re elm.getText();
				    }).then(function(result){
			assertsObj.assertListNotContain(result,dataContractName,'Deleted Data Contract Name:' + dataContractName + ' is  found in list.');
			});
      });
		  browser.waitForAngular();
	  });
    });

    /**
	     * This method is used to call logout method of
	     * dataContractBase JS File.
	     * 
	     * @author narottamc
	    */
    it('  Logout Data Contract tab:'+browser.user,function () {
		utilityObj.browserWaitforseconds(2);
      loginBaseObj.logout(browser.user);
      isAngularSite(false);
      browser.waitForAngular();
      assertsObj.assertTrue(loginBaseObj.isUserNamePresent(),'Username Textbox is not found for login page.');
    });
  });
});;

