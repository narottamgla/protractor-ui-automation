/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

/**
 * This test suite is use to check advertiser permission for advertiser data tab.
 * 
 * @author vishalbha
 */
describe("Advertiser Permission of Advertiser data Tab:"+browser.user,function(){
	
	var pathObj  =require( '../../../testData/audienceSuite/path.js');
	var utilityObj = require('../../../lib/utility.js');
	var loginBaseObj = require('../../../base/audienceSuite/loginBase.js');
	var advertiserDataBaseObj = require('../../../base/audienceSuite/advertiserDataBase.js');
	var turnDataFilterObj = require('../../../lib/turnDataProvider.js');
	var assertsObj = require('../../../lib/assert.js');
	var loggerObj = require('../../../lib/logger.js');
	var logger = loggerObj.loggers("advertiserData Permission Test");
	
	beforeEach(function() {
		isAngularSite(false);
	});
	browser.ignoreSynchronization = true;	
if((browser.user).toUpperCase()=='CS'){
	/**
	 * This method is use to login with whitelisted user and verify advertiser permission.
	 * 
	 * @author vishalbha
	 */
	it("Check Advertiser Permission for MMW user Advertiser Data tab:"+browser.user,function(){
		logger.info("Checking Advertiser Permission for MMW user in Advertiser Data tab.");
		var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language,'MMW');
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		loginBaseObj.login(loginTestdata);
		loginBaseObj.isLogoutUserBoxPresent("MMW");
		browser.waitForAngular();
		loginBaseObj.redirectToAudienceSuite();
		browser.waitForAngular();
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		advertiserDataBaseObj.gotoAdvertiserData();
		utilityObj.browserWaitforseconds(2);
		 var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(),browser.env, 'MMW', 'advertiserPermission');
				utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserPermissionTestData.whitelistedAdvertiser);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2);
				advertiserDataBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,advertiserPermissionTestData.whitelistedAdvertiser,"White listed advertiser data("+advertiserPermissionTestData.whitelistedAdvertiser+") is not found in search list for MMW user.");
				});
		 browser.waitForAngular();	
		utilityObj.browserWaitforseconds(2);
				utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserPermissionTestData.blacklistedAdvertiser);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2);
				advertiserDataBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				}).then(function(result){
				    	assertsObj.assertListNotContain(result,advertiserPermissionTestData.blacklistedAdvertiser,"Black listed advertiser data ("+advertiserPermissionTestData.blacklistedAdvertiser+") is found in search list for MMW user.");
				});
		 loginBaseObj.logout("MMW");
	});
	
	/**
	 * This method is use to login with blacklisted user and verify advertiser permission.
	 * 
	 * @author hemins
	 */
	it("Check Advertiser Permission for MMB user Advertiser Data tab:"+browser.user,function(){
		logger.info("Checking Advertiser Permission for MMB user in Advertiser Data tab.");
		var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env,browser.language, 'MMB');
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		loginBaseObj.login(loginTestdata);
		loginBaseObj.isLogoutUserBoxPresent("MMB");
		browser.waitForAngular();
		var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(), browser.env,'MMB', 'advertiserPermission');
		loginBaseObj.redirectToAudienceSuite();
		browser.waitForAngular();
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		advertiserDataBaseObj.gotoAdvertiserData();
		utilityObj.browserWaitforseconds(2);
				utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserPermissionTestData.whitelistedAdvertiser);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2);
				advertiserDataBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,advertiserPermissionTestData.whitelistedAdvertiser,"White listed advertiser data is not found in search list for MMB user.");
				});
				utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserPermissionTestData.blacklistedAdvertiser);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2);
				advertiserDataBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				}).then(function(result){
				    	assertsObj.assertListNotContain(result,advertiserPermissionTestData.blacklistedAdvertiser,"Black listed advertiser is found in search list for MMB user.");
				});
		loginBaseObj.logout("MMB");
	});
	
	/**
	 * This method is use to login with All Advertiser user and verify advertiser permission.
	 * 
	 * @author hemins
	 */
	it("Check Advertiser Permission for MMA user Advertiser Data tab:"+browser.user,function(){
		logger.info("Checking Advertiser Permission for MMA user in Advertiser Data tab.");
		var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, 'MMA');
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		loginBaseObj.login(loginTestdata);
		loginBaseObj.isLogoutUserBoxPresent("MMA");
		browser.waitForAngular();
		var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(), browser.env,'MMA', 'advertiserPermission');
		loginBaseObj.redirectToAudienceSuite();
		browser.waitForAngular();
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		advertiserDataBaseObj.gotoAdvertiserData();
		utilityObj.browserWaitforseconds(2);
		var advertisers=(advertiserPermissionTestData.whitelistedAdvertiser).split(";");
		var j=0;
		for(var i=0;i<advertisers.length;i++){
					utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertisers[i]);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					advertiserDataBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
					    	assertsObj.assertListContain(result,advertisers[j],"White listed advertiser ("+advertisers[j]+")is not found in search list for All Advertiser Permission user.");
					    	j++;
					});
		}
		loginBaseObj.logout("MMA");
	});
	
	/**
	 * This method is use to login with Cadmin user and verify advertiser permission.
	 * 
	 * @author hemins
	 */			
	it("Check Advertiser Permission for CAdmin user Advertiser Data tab:"+browser.user,function(){
		logger.info("Checking Advertiser Permission for CAdmin user in Advertiser Data tab.");
		var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, 'CADMIN');
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		loginBaseObj.login(loginTestdata);
		loginBaseObj.isLogoutUserBoxPresent("CAdmin");
		browser.waitForAngular();
		var advertiserPermissionTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(),browser.env, 'CADMIN', 'advertiserPermission');
		loginBaseObj.redirectToAudienceSuite();
		browser.waitForAngular();
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		advertiserDataBaseObj.gotoAdvertiserData();
		utilityObj.browserWaitforseconds(2);
		var advertisers=(advertiserPermissionTestData.whitelistedAdvertiser).split(";");
		var j=0;
		for(var i=0;i<advertisers.length;i++){
				utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertisers[i]);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2);
				advertiserDataBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				}).then(function(result){
				    	assertsObj.assertListContain(result,advertisers[j],"White listed advertiser ("+advertisers[j]+")is not found in search list for Cadmin user.");
				    	j++;
				});
			}
			loginBaseObj.logout("CAdmin");
		});
	}
	else{
		logger.info("Advertiser Permission TestCase : Skipped,because UserType is Non 'CS'.");
	};
});		
		
