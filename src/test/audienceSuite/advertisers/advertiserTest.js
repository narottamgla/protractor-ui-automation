/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

describe('Turn audience suite  Advertiser Tab:'+browser.user,function(){
	
		var pathObj  =require( '../../../testData/audienceSuite/path.js');
		var utilityObj = require('../../../lib/utility.js');
		var loginBaseObj = require('../../../base/audienceSuite/loginBase.js');
		var advertiserBaseObj = require('../../../base/audienceSuite/advertiserBase.js');
		var turnDataFilterObj = require('../../../lib/turnDataProvider.js');
		var assertsObj = require('../../../lib/assert.js');
		var loggerObj = require('../../../lib/logger.js');
		var logger=loggerObj.loggers("advertiserTest");
		var checkAdvertiserType;
		var advertiserTestData;
		beforeEach(function() {
			isAngularSite(false);
			advertiserTestData=null;
		});
		browser.ignoreSynchronization = true;			
		
		/**
		 * This method used to enter Url and Login details. 
		 * 
		 * @author vishalbha
		 */
		it('Turn login page Advertiser tab:'+browser.user, function() {
			var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, browser.user);
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			loginBaseObj.login(loginTestdata);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			assertsObj.assertTrue(loginBaseObj.isLogoutUserBoxPresent(browser.user),'Logout UserBox is not available.');
		});
		
		/**
		 * This method is used to redirect Audience suite project.
		 * 
		 * @author vishalbha
		 */
		it('Redirection  Advertiser tab:'+browser.user, function() {
			loginBaseObj.redirectToAudienceSuite();
			browser.waitForAngular();
			var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env, browser.language, browser.user);
			utilityObj.geturl(loginTestdata.url);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			assertsObj.assertTrue(loginBaseObj.checkAudienceSuite(),"Not redirect to Audience Suite");
		});

		/**
		 * This method used to call createAdvertiser method of
		 * advertiserBase class.
		 * 
		 * @author vishalbha
		 */
		it('create advertiser  Advertiser tab:'+browser.user, function() {

			advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserTestDataFilePath(), browser.env, browser.language,'createadvertiser');
			checkAdvertiserType="AdvertiserWithoutMediaProvider";
			advertiserBaseObj.gotoAdvertiser();
			browser.waitForAngular();			
		    utilityObj.browserWaitforseconds(3);
			advertiserBaseObj.newAdvertiserWithCancelButton();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			advertiserName = advertiserTestData.advertiserName + utilityObj.getTimeStamp();
			advertiserBaseObj.createNewAdvertiser(advertiserTestData, advertiserName,checkAdvertiserType);
				utilityObj.findList(advertiserBaseObj.returnSearchElement(),advertiserName);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2);
				advertiserBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				    }).then(function(result){
				    	assertsObj.assertListContain(result,advertiserName,'Advertiser is not found in search list.');
				});
		});
		
		/**
		 * This method used to call editAdvertiser method of advertiserBase class.
		 * 
		 * @author narottamc
		 */
		it('edit created advertiser:'+browser.user, function() {
			advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserTestDataFilePath(),browser.env, browser.language,'createadvertiser');
			advertiserBaseObj.gotoAdvertiser();
			browser.waitForAngular();
			utilityObj.findList(advertiserBaseObj.returnSearchElement(),advertiserTestData.createdAdvertiser);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			var updatedAdvertiserName = advertiserTestData.createdAdvertiser + "_Edit";
			advertiserBaseObj.editAdvertiser('createadvertiser', updatedAdvertiserName);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
					utilityObj.findList(advertiserBaseObj.returnSearchElement(),updatedAdvertiserName);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(1);
					advertiserBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
					    assertsObj.assertListContain(result,updatedAdvertiserName,'Updated advertiser is not found in search list.');
					});
	
		});
		
		/**
		 * This method used to call viewAdvertiser method of advertiserBase
		 * class.
		 * 
		 * @author vishalbha
		 */

		it('view advertiser:'+browser.user,function() {
			advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserTestDataFilePath(),browser.env, browser.language,'createadvertiser');
			advertiserBaseObj.gotoAdvertiser();
			browser.waitForAngular();
			utilityObj.findList(advertiserBaseObj.returnSearchElement(),advertiserTestData.updatedAdvertiser);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			advertiserBaseObj.gotoViewAdvertiser();
			assertsObj.assertEqual(advertiserBaseObj.viewAdvertiserName(advertiserTestData.updatedAdvertiser),advertiserTestData.updatedAdvertiser,
			  			"Advertiser name is not match with actual advertiser in view advertiser page.");
			utilityObj.browserWaitforseconds(1);
			advertiserBaseObj.gotoAdvertiser();
		});

		/**
		 * This method used to call deleteAdvertiser method of
		 * advertiserBase class.
		 * 
		 * @author vishalbha
		 */
		it('delete advertiser:'+browser.user, function() {
			advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserTestDataFilePath(),browser.env, browser.language,'createadvertiser');
			advertiserBaseObj.gotoAdvertiser();
			browser.waitForAngular();
			utilityObj.findList(advertiserBaseObj.returnSearchElement(),advertiserTestData.updatedAdvertiser);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			advertiserBaseObj.deleteAdvertiser();
					utilityObj.findList(advertiserBaseObj.returnSearchElement(),advertiserTestData.updatedAdvertiser);
					advertiserBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
					    assertsObj.assertListNotContain(result,advertiserTestData.updatedAdvertiser,'Deleted Advertiser is found in search list.');
					});
				browser.waitForAngular();
		});
		
		/**
		  * This method is use to call goto advertiser page and click Show deleted advertiser method 
		  * of Advertiser base and assert that deleted advertiser is available in search list or not.
		  * 
		  * @author hemins
		  */
		it('Show Deleted Advertiser:'+browser.user, function() {
			advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserTestDataFilePath(),browser.env, browser.language,'createadvertiser');
			advertiserBaseObj.gotoAdvertiser();
			advertiserBaseObj.clickShowDeletedAdvertiser();
			browser.waitForAngular();
					utilityObj.findList(advertiserBaseObj.returnSearchElement(),advertiserTestData.updatedAdvertiser);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					advertiserBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
				assertsObj.assertListContain(result,advertiserTestData.updatedAdvertiser,'Deleted Advertiser Name is not found in list.');
				});
		});
		
		 /**
		  * This method is use to call goto view audit log page and search audit log field name method 
		  * of Advertiser base and assert that edited field name is available or not.
		  * 
		  * @author hemins
		  */
		it('View Advertiser Audit Log:'+browser.user,function(){
			advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserTestDataFilePath(),browser.env, browser.language, 'createadvertiser');
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(1);
			advertiserBaseObj.gotoViewAuditLog();
			isAngularSite(false);	
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			advertiserBaseObj.searchAuditLog(advertiserTestData.fieldNameValue);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			assertsObj.assertEqual(advertiserBaseObj.getFieldNameValue(),advertiserTestData.fieldNameValue,"Advertiser: Field name is not found for audit log.");
			utilityObj.switchToMainWindow();
		});
		
		/**
		  * This method is use to call click Hide deleted advertiser method 
		  * of Advertiser base and assert that deleted advertiser is available in search list or not.
		  * 
		  * @author hemins
		  */
		it('Hide Deleted Advertiser:'+browser.user,function() {
			advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserTestDataFilePath(),browser.env, browser.language, 'createadvertiser');
			advertiserBaseObj.clickHideDeletedAdvertiser();
					utilityObj.findList(advertiserBaseObj.returnSearchElement(),advertiserTestData.updatedAdvertiser);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					advertiserBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
				assertsObj.assertListNotContain(result,advertiserTestData.updatedAdvertiser,'Deleted Advertiser Name is found in list.');
				});	    
		});
		
		/**
		 * This method used to call select Market Filter of advertiser base class
		 * and assert that compare selected market and list shows market.
		 * 
		 * @author vishalbha
		 */
		it('Filter Market Advertiser tab:'+browser.user, function(){
			advertiserBaseObj.gotoAdvertiser();
			var advertiserFilterData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserTestDataFilePath(),browser.env, browser.language,'createadvertiser');
			var userType = browser.user;
			if(userType=='CS'){
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			advertiserBaseObj.clearSearch();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(4);
			browser.navigate().refresh();
			browser.waitForAngular();	
			utilityObj.browserWaitforseconds(4);
			advertiserBaseObj.selectMarketFilter(advertiserFilterData.market);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(5);
			assertsObj.assertEqual(advertiserBaseObj.marketNameCount(),advertiserFilterData.market,
			"Total filter records are not match with expected total records.");
			}
			else{
			logger.info(userType + " user doesnot contain market filter.");
			}
		});
				
		/**
		 * This method is use to call sort by advertiser name method of advertiser
		 * base class and assert that records are sort by name or not.
		 * 
		 * @author vishalbha
		 */
		
		it('sorting functionality advertiser tab:'+browser.user, function(){
			advertiserBaseObj.gotoAdvertiser();
			browser.waitForAngular();	
			advertiserBaseObj.clearSearch();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			advertiserBaseObj.sortByAdvertiserName();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			assertsObj.assertNotNull(advertiserBaseObj.assertNameColumn(),"Sorting on name column is not perform.");
		});
		
		/**
		 * This method used to call createAdvertiser method of
		 * advertiserBase class.
		 * 
		 * @author vishalbha
		 */		

		it('create advertiser with Media Provider:'+browser.user, function(){
			browser.waitForAngular();
			advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserTestDataFilePath(),browser.env, browser.language,'createadvertiser');
			checkAdvertiserType="AdvertiserWithMediaProvider";
			advertiserBaseObj.gotoAdvertiser();
			advertiserName = advertiserTestData.advertiserNameWithMediaProvider + utilityObj.getTimeStamp();
			advertiserBaseObj.createNewAdvertiser(advertiserTestData, advertiserName,checkAdvertiserType);
			utilityObj.browserWaitforseconds(2);
					utilityObj.findList(advertiserBaseObj.returnSearchElement(),advertiserName);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					advertiserBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
				assertsObj.assertListContain(result,advertiserName,'Advertiser is not found in search list.');
				 });
		});
		
		/**
		 This method used to call deleteAdvertiser method of
		 * advertiserBase class.
		 * 
		 * @author vishalbha
		 */
		it('delete advertiser with Media Provider:'+browser.user, function(){
			var advertiserTestDeleteData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserTestDataFilePath(),browser.env, browser.language,'createadvertiser');
			utilityObj.findList(advertiserBaseObj.returnSearchElement(),advertiserTestDeleteData.createadvertiserwithmediaprovider);
			advertiserBaseObj.deleteAdvertiser();
			browser.waitForAngular();
					utilityObj.findList(advertiserBaseObj.returnSearchElement(),advertiserTestDeleteData.createadvertiserwithmediaprovider);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					advertiserBaseObj.returnElementList().map(function (elm) {
					    return elm.getText();
					    }).then(function(result){
					    assertsObj.assertListNotContain(result,advertiserTestDeleteData.createadvertiserwithmediaprovider,'Advertiser with Media Provider is found in search list.');	
					});
		});
		
		if((browser.env).toUpperCase()=='STG1'){
			
		/**
		 * This method used to call browseAdvertiserMetaDataFile method of
		 * advertiserBase class.
		 * 
		 * @author narottamc
		 */

		xit('Edit Advertiser MetaData Upload Metadata file:'+browser.user, function(){
			var advertiserTestDeleteData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserTestDataFilePath(),browser.env, browser.language,'createadvertiser');
			advertiserBaseObj.gotoAdvertiser();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			advertiserBaseObj.clearExistingFilter();
			browser.waitForAngular();
			utilityObj.findList(advertiserBaseObj.returnSearchElement(),advertiserTestDeleteData.editMetaDataAdvertiser);
				browser.waitForAngular();			
				utilityObj.browserWaitforseconds(2);
				advertiserBaseObj.clickEditAdvertiserMetaDataLink();
					isAngularSite(false);
					browser.waitForAngular();
					utilityObj.browserWaitforseconds(2);
					advertiserBaseObj.browseAdvertiserMetaDataFile(advertiserTestDeleteData.editMetaDataxlsFilePath);
					browser.waitForAngular();
					assertsObj.assertTrue(advertiserBaseObj.assertEditMetaDataEditButton(),'Edit MetaData .xls File not uplaoded properly.');
					browser.waitForAngular();
					advertiserBaseObj.clickEditMetaDataCancelButton();				
			browser.waitForAngular();
		});
		
		xit('Edit Advertiser MetaData Export Meta data file:'+browser.user, function(){
			//To-DO
		});
		
		/**
		 * This method used to call verifyAdvertiserEditedMetaData method of
		 * advertiserBase class.
		 * 
		 * @author narottamc
		 */

		xit('Verify Edited Advertiser Metadata:'+browser.user, function(){
			var advertiserTestDeleteData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserTestDataFilePath(),browser.env, browser.language,'createadvertiser');
			advertiserBaseObj.clickEditAdvertiserMetaDataLink();
			isAngularSite(false);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
			advertiserBaseObj.verifyAdvertiserEditedMetaData(advertiserTestDeleteData);
			assertsObj.verifyEqual(advertiserBaseObj.assertEditMetaDataPageTitle(),advertiserTestDeleteData.editMetaDataPageTitle);
			advertiserBaseObj.clickEditMetaDataCancelButton();
			browser.waitForAngular();
		});
		}
		
		/**
		 * This method is used to call logout method of
		 * advertiserBase JS File.
		 * 
		 * @author hemins
		 */

			it('Logout advertiser tab:'+browser.user,function(){
			loginBaseObj.logout(browser.user);
			isAngularSite(false);
			assertsObj.assertTrue(loginBaseObj.isUserNamePresent(),"Username Textbox is not found for login page.");
		});
		
		
	});



