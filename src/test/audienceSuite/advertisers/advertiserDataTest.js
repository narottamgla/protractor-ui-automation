/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

describe('Turn audience suite Advertiser Data Tab:'+browser.user,function(){
	var pathObj  =require( '../../../testData/audienceSuite/path.js');
	var utilityObj = require('../../../lib/utility.js');
	var loginBaseObj = require('../../../base/audienceSuite/loginBase.js');
	var advertiserDataBaseObj = require('../../../base/audienceSuite/advertiserDataBase.js');
	var turnDataFilterObj = require('../../../lib/turnDataProvider.js');
	var assertsObj = require('../../../lib/assert.js');
	var loggerObj = require('../../../lib/logger.js');
	var logger = loggerObj.loggers("advertiserDataTest");
	
	beforeEach(function() {
		isAngularSite(false);		
	});
	browser.ignoreSynchronization = true;			
	
	/**
	 * This method used to enter Url and Login details. 
	 * 
	 * @author vishalbha
	 */
	it('Turn login page Advertiser Data tab:'+browser.user, function() {
		var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env,browser.language,browser.user);
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		loginBaseObj.login(loginTestdata);
		utilityObj.browserWaitforseconds(2);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(4);		
		assertsObj.assertTrue(loginBaseObj.isLogoutUserBoxPresent(browser.user),'Logout UserBox is not available.');
	});
	
	/**
	 * This method used for redirect to Audience suite Project. 
	 * 
	 * @author vishalbha
	 */
	it('Redirect to Audience Suite Project Advertiser Data tab:'+browser.user, function() {
		loginBaseObj.redirectToAudienceSuite();
		browser.waitForAngular();
		var loginTestdata = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env,browser.language,browser.user);
		utilityObj.geturl(loginTestdata.url);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		assertsObj.assertTrue(loginBaseObj.checkAudienceSuite(),"Not redirect to Audience Suite");
	});
	
	/**
	 * This method used to call createAdvertiser method of
	 * advertiserDataBase class.
	 * 
	 * @author vishalbha
	 */
	it('Create Advertiser Advertiser Data tab:'+browser.user, function() {
		var advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(),browser.env, browser.language,'createadvertiserdata');
		advertiserDataBaseObj.gotoAdvertiser();		
		var advertiserName = advertiserTestData.advertiserName + utilityObj.getTimeStamp();
		advertiserDataBaseObj.createNewAdvertiser(advertiserTestData, advertiserName,"createadvertiserdata");
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(1);
			utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserName);
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(3);
			advertiserDataBaseObj.returnElementListAdv().map(function (elm) {
			    return elm.getText();
			    }).then(function(result){
			    	assertsObj.assertListContain(result,advertiserName,'Advertiser is not found in search list for new list page.');
			});
	});

	/**
	 * This method used to call createAdvertiserData by select Category/Keywords data type method of
	 * advertiserDataBase class.
	 * 
	 * @author vishalbha
	 */
	it('Create Advertiser data with javascript data pixel type and Category/Keywords Data Type:'+browser.user, function() {
		var advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(),browser.env, browser.language,'createadvertiserdata');
		advertiserDataBaseObj.gotoAdvertiserData();
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
				advertiserDataBaseObj.clearNewFilter();
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
				advertiserDataBaseObj.filterByAdvertiser(advertiserTestData.createdAdvertiserName);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2);					
		advertiserDataBaseObj.clickNewAdvertiserDataPageCancelButton();
		var advertiserDataName = advertiserTestData.advertiserDataName + utilityObj.getTimeStamp();
		advertiserDataBaseObj.createNewAdvertiserData(advertiserTestData,advertiserDataName,pathObj.getAdvertiserDataTexonomyDataFilePath(),"createadvertiserdata");
			utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserDataName);
			advertiserDataBaseObj.returnElementList().map(function (elm) {
			    return elm.getText();
			}).then(function(result){
			    	assertsObj.assertListContain(result,advertiserDataName,'Advertiser data is not found in search list.');
			});
	});
	
	/**
	 * This method used to call createAdvertiserData by select Default data type method of
	 * advertiserDataBase class.
	 * 
	 * @author vishalbha
	 */
	it('Create Advertiser data with iFrame data pixel type and Default Data Type:'+browser.user, function() {
		var advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(),browser.env, browser.language,'CreateIframeAdvertiserData');
		var advertiserDataName = advertiserTestData.advertiserDataName + utilityObj.getTimeStamp();
		advertiserDataBaseObj.createNewAdvertiserData(advertiserTestData,advertiserDataName,pathObj.getAdvertiserDataTexonomyDataFilePath(),"CreateIframeAdvertiserData");
				utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserDataName);
			advertiserDataBaseObj.returnElementList().map(function (elm) {
			    return elm.getText();
			}).then(function(result){
			    	assertsObj.assertListContain(result,advertiserDataName,'Advertiser data is not found in search list.');
			});
		advertiserDataBaseObj.deleteAdvertiser('ADVERTISER DATA');
			utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserDataName);
			advertiserDataBaseObj.returnElementList().map(function (elm) {
			    return elm.getText();
			}).then(function(result){
			    	assertsObj.assertListNotContain(result,advertiserDataName,'Advertiser data is found in search list.');
			});
	});
	
	/**
	 * This method used to call createAdvertiserData by select Click data type method of
	 * advertiserDataBase class.
	 * 
	 * @author vishalbha
	 */
	it('Create Advertiser data with Image data pixel type and Click Data Type:'+browser.user, function() {
		var advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(),browser.env, browser.language,'CreateImageAdvertiserData');
		var advertiserDataName = advertiserTestData.advertiserDataName + utilityObj.getTimeStamp();
		advertiserDataBaseObj.createNewAdvertiserData(advertiserTestData,advertiserDataName,pathObj.getAdvertiserDataTexonomyDataFilePath(),"CreateImageAdvertiserData",advertiserTestData.dataPixelType);
			utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserDataName);
			advertiserDataBaseObj.returnElementList().map(function (elm) {
			    return elm.getText();
			}).then(function(result){
			    	assertsObj.assertListContain(result,advertiserDataName,'Advertiser data is not found in search list.');
			});
		advertiserDataBaseObj.deleteAdvertiser('ADVERTISER DATA');
			utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserDataName);
			advertiserDataBaseObj.returnElementList().map(function (elm) {
			    return elm.getText();
			}).then(function(result){
			    	assertsObj.assertListNotContain(result,advertiserDataName,'Advertiser data is found in search list.');
			});
	});
	
	/**
	 * This method used to call createAdvertiserData by select Site Interaction data type method of
	 * advertiserDataBase class.
	 * 
	 * @author vishalbha
	 */
	it('Create Advertiser data with Javascript data pixel type and Site Interaction Data Type:'+browser.user, function() {
		var advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(),browser.env, browser.language,'CreateSiteInteractionAdvertiserData');
		var advertiserDataName = advertiserTestData.advertiserDataName + utilityObj.getTimeStamp();
		advertiserDataBaseObj.createNewAdvertiserData(advertiserTestData,advertiserDataName,pathObj.getAdvertiserDataTexonomyDataFilePath(),"CreateSiteInteractionAdvertiserData",advertiserTestData.dataPixelType);
			utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserDataName);
			advertiserDataBaseObj.returnElementList().map(function (elm) {
			    return elm.getText();
			}).then(function(result){
			    	assertsObj.assertListContain(result,advertiserDataName,'Advertiser data is not found in search list.');
			});
		advertiserDataBaseObj.deleteAdvertiser('ADVERTISER DATA');
			utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserDataName);
			advertiserDataBaseObj.returnElementList().map(function (elm) {
			    return elm.getText();
			}).then(function(result){
			    	assertsObj.assertListNotContain(result,advertiserDataName,'Advertiser data is found in search list.');
			});
	});
	
	/**
	 * This method is use to call obtain Pixels method from
	 * AdvertiserDataBase class and assert use to verify obtain pixels Header Text.
	 * 
	 * @author VishalBhatt
	 */
	it('Obtain Pixels of Advertiser Data:'+browser.user, function(){
		var advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(), browser.env, browser.language,'createadvertiserdata');
		advertiserDataBaseObj.gotoAdvertiserData();
		assertsObj.assertEqual(advertiserDataBaseObj.gotoObtainPixels(),advertiserTestData.obtainName,"Advertiser data obtain Pixels Header Text is not match with actual advertiser data obtain pixels Header Text.");
	});
	
	/**
	 * This method is use to call Market Filter method from Advertiserdatabase class
	 * and assert verify those advertisers which was already created on selected market.  
	 * 
	 * @author vishalbha
	 */
	it('Filter Market of Advertiser Data:'+browser.user, function(){
		var advertiserFilterData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(), browser.env, browser.language,'createadvertiserdata');
		var userType = browser.user;
		if((browser.user).toUpperCase()=='CS'){
			browser.waitForAngular();
			advertiserDataBaseObj.clearSearch();
			browser.waitForAngular();
			utilityObj.browserWaitforseconds(2);
		advertiserDataBaseObj.clearNewFilter();
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(2);
		advertiserDataBaseObj.selectMarketFilter(advertiserFilterData.market);
		utilityObj.browserWaitforseconds(2);
		var advertiserdata = (advertiserFilterData.advertiserDataForTurn).split(";");
		var j=0;
		for(var i=0 ;i<advertiserdata.length;i++){
				utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserdata[i]);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(3);
				advertiserDataBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				}).then(function(result){
				    	assertsObj.assertListContain(result,advertiserdata[j],'Advertiser data is not found in search list.');
				    	j++;
				});
			}
		}
		else{
		logger.info(userType + " user doesn't contain market filter.");
		};
	});
		/**
		 * This method is use to call sorting method from AdvertiserDataBase class and
		 *  assert used to verify sorting performed or not by getting boolean value.
		 * 
		 * @author vishalbha
		 */
		
		it('Sorting functionality of Advertiser Data:'+browser.user, function(){
			advertiserDataBaseObj.clearSearch();
			advertiserDataBaseObj.sortByAdvertiserName();
			utilityObj.browserWaitforseconds(2);
			assertsObj.assertNotNull(advertiserDataBaseObj.assertNameColumn(),"Sorting on name column is not perform.");
		});
		
		/**
		 * This method is use to call select advertiser Filter of advertiser data base class
		 * and assert that search selected advertiser's advertiser data on list page.  
		 * 
		 * @author vishalbha
		 */
		it('Filter Advertiser of Advertiser Data:'+browser.user, function(){
			var advertiserFilterData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(), browser.env, browser.language,'createadvertiserdata');
			advertiserDataBaseObj.clearSearch();
				advertiserDataBaseObj.clearNewFilter();
				utilityObj.browserWaitforseconds(2);
				advertiserDataBaseObj.filterByAdvertiser(advertiserFilterData.createdAdvertiserName);
				browser.waitForAngular();
				utilityObj.browserWaitforseconds(2);
				utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserFilterData.createdAdvertiserDataName);
				utilityObj.browserWaitforseconds(3);
				advertiserDataBaseObj.returnElementList().map(function (elm) {
				    return elm.getText();
				}).then(function(result){
				    	assertsObj.assertListContain(result,advertiserFilterData.createdAdvertiserDataName,'Advertiser data is not found in search list of Advertiser Data.');
				});
		});
	/**
	 * This method used to call editAdvertiserData method of
	 * advertiserDataBase class.
	 * 
	 * @author narottamc
	 */
	it('Edit created Advertiser data:'+browser.user,function(){
		var advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(),browser.env,browser.language,'createadvertiserdata');
		advertiserDataBaseObj.gotoAdvertiserData();
		utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserTestData.createdAdvertiserDataName);
		var updatedAdevertiserDataName=null;
		updatedAdevertiserDataName=advertiserTestData.createdAdvertiserDataName+"_Edit";
		utilityObj.browserWaitforseconds(3);
		advertiserDataBaseObj.editAdvertiserData(updatedAdevertiserDataName,"createadvertiserdata");
			utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),updatedAdevertiserDataName);
			advertiserDataBaseObj.returnElementList().map(function (elm) {
			    return elm.getText();
			}).then(function(result){
			    	assertsObj.assertListContain(result,updatedAdevertiserDataName,'Advertiser data is found in search list.');
			});
	 });	
	
	/**
	 * This method used to call viewAdvertiserDataName method of
	 * advertiserDataBase class.
	 * 
	 * @author narottamc
	 */
	it('View created Advertiser data:'+browser.user,function(){
		var advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(),browser.env,browser.language,'createadvertiserdata');
		advertiserDataBaseObj.gotoAdvertiserData();
		utilityObj.browserWaitforseconds(3);
		utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserTestData.updatedAdvertiserDataName);
		advertiserDataBaseObj.gotoViewAdvertiserData();
		assertsObj.assertEqual(advertiserDataBaseObj.viewAdvertiserDataName(advertiserTestData.updatedAdvertiserDataName),advertiserTestData.updatedAdvertiserDataName,
			"Advertiser data name is not match with actual advertiser data in view advertiser data page.");
	});
	
	/**
	 * This method used to call deleteAdvertiser method of
	 * advertiserDataBase class.
	 * 
	 * @author narottamc
	 */
	it('Delete created Advertiser data and Advertiser:'+browser.user,function(){
		var advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(),browser.env,browser.language,'createadvertiserdata');
		advertiserDataBaseObj.gotoAdvertiserData();
		utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserTestData.updatedAdvertiserDataName);
		utilityObj.browserWaitforseconds(3);
		advertiserDataBaseObj.deleteAdvertiser('ADVERTISER DATA');
			utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserTestData.updatedAdvertiserDataName);
			utilityObj.browserWaitforseconds(3);
			advertiserDataBaseObj.returnElementList().map(function (elm) {
			    return elm.getText();
			}).then(function(result){
			    	assertsObj.assertListNotContain(result,advertiserTestData.updatedAdvertiserDataName,'Advertiser data is not found in search list.');
			});
		advertiserDataBaseObj.gotoAdvertiser();
		browser.waitForAngular();
		utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserTestData.createdAdvertiserName);
		advertiserDataBaseObj.deleteAdvertiser('ADVERTISER');
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(5);
			utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserTestData.updatedAdvertiserDataName);
			utilityObj.browserWaitforseconds(3);
			advertiserDataBaseObj.returnElementListAdv().map(function (elm) {
			    return elm.getText();
			}).then(function(result){
			    	assertsObj.assertListNotContain(result,advertiserTestData.updatedAdvertiserDataName,'Advertiser data is found in search list.');
			});
	});
	
	 /**
	  * This method is use to call goto advertiser data page and click Show deleted advertiser data method 
	  * of AdvertiserDataBase and assert that deleted advertiser data is available in search list or not.
	  * 
	  * @author vishalbha
	  */
	
	
	it('Show Deleted Advertiser data:'+browser.user, function() {
		var advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(), browser.env, browser.language,'createadvertiserdata');
		advertiserDataBaseObj.gotoAdvertiserData();
		advertiserDataBaseObj.clickShowDeletedAdvertiser();
			utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserTestData.updatedAdvertiserDataName);
			utilityObj.browserWaitforseconds(3);
			advertiserDataBaseObj.returnElementList().map(function (elm) {
			    return elm.getText();
			}).then(function(result){
			    	assertsObj.assertListContain(result,advertiserTestData.updatedAdvertiserDataName,'Deleted Advertiser data Name is not found in list.');
			});
	});
	
	 /**
	  * This method is use to call gotoViewAuditLog method and search audit log field name 
	  * method of Advertiser data base and assert that edited field name is available or not.
	  * 
	  * @author vishalbha
	  */
	it('View Audit Log of Advertiser Data:'+browser.user, function(){
		var advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(), browser.env, browser.language,'createadvertiserdata');	
		advertiserDataBaseObj.gotoViewAuditLog();
		utilityObj.browserWaitforseconds(1);
		advertiserDataBaseObj.searchAuditLog(advertiserTestData.fieldNameValue);
		assertsObj.assertEqual(advertiserDataBaseObj.getFieldNameValue(),advertiserTestData.fieldNameValue,"Advertiser Data: Field name is not found for audit log.");
		utilityObj.switchToMainWindow();
	});
	
		
	/**
	  * This method is use to call click Hide deleted advertiser data method 
	  * of Advertiser data base and assert that deleted advertiser data is available in search list or not.
	  * 
	  * @author vishalbha
	  */
	it('Hide Deleted Advertiser data:'+browser.user,function() {
		var advertiserTestData = turnDataFilterObj.getDataProvider(pathObj.getAdvertiserDataTestDataFilePath(), browser.env, browser.language, 'createadvertiserdata');
		advertiserDataBaseObj.clickHideDeletedAdvertiserData();
		utilityObj.browserWaitforseconds(3);
			utilityObj.findList(advertiserDataBaseObj.returnSearchElement(),advertiserTestData.updatedAdvertiserDataName);
			utilityObj.browserWaitforseconds(3);
			advertiserDataBaseObj.returnElementList().map(function (elm) {
			    return elm.getText();
			}).then(function(result){
			    	assertsObj.assertListNotContain(result,advertiserTestData.updatedAdvertiserDataName,'Deleted Advertiser Name is found in list.');
			});
	});
	
	xit('Lazy Loading:'+browser.user,function() {
		//TODO
	});
		 
	/**
	 * This method used to call logout method of
	 * advertiserDataBase class.
	 * 
	 * @author narottamc
	 */
	it('Turn Logout Advertiser data tab:'+browser.user,function(){
		loginBaseObj.logout(browser.user);
		isAngularSite(false);
		assertsObj.assertTrue(loginBaseObj.isUserNamePresent(),"Username Textbox is not found for login page.");
	});
		
});
