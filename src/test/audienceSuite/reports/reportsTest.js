/**
* Copyright (C) 2014 Turn, Inc. All Rights Reserved.
* Proprietary and confidential.
*/

describe('Turn audience suite Report Tab:'+browser.user,function(){
	var pathObj  =require( '../../../testData/audienceSuite/path.js');
	var utilityObj = require('../../../lib/utility.js');
	var loginBaseObj = require('../../../base/audienceSuite/loginBase.js');
	var reportBaseObj = require('../../../base/audienceSuite/reportsBase.js');
	var turnDataFilterObj = require('../../../lib/turnDataProvider.js');
	var assertsObj = require('../../../lib/assert.js');
	
	
	beforeEach(function() {
		isAngularSite(false);
	});
	browser.ignoreSynchronization = true;	
	if((browser.user).toUpperCase()=='CS'){
	/**
	 * This method used to login 'CS' and redirecting Reporting to other Sub tab. 
	 * 
	 * @author vishalbha
	 */
	it('Turn Login Page with CS User Reports tab',function(){
		var loginTestData = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env,browser.language,'CS');
		utilityObj.geturl(loginTestData.url);
		browser.waitForAngular();
		loginBaseObj.login(loginTestData);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		loginBaseObj.isLogoutUserBoxPresent("CS");		
		loginBaseObj.redirectToAudienceSuite();
		browser.waitForAngular();
		utilityObj.geturl(loginTestData.url);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		assertsObj.assertTrue(reportBaseObj.checkReportingtabisPresent(),"Not redirecting to Reporting tab");
		reportBaseObj.gotoReportingTab();
		reportBaseObj.clickOnSubTab('CS');
		loginBaseObj.logout('CS');
	});
	
	/**
	 * This method used to login 'DPMWITHACCESS' and redirecting Reporting to other Sub tab. 
	 * 
	 * @author vishalbha
	 */
	it('Turn Login Page with DPM User with Access to New Insights',function(){
		var loginTestData = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env,browser.language,'DPMWITHACCESS');
		utilityObj.geturl(loginTestData.url);
		browser.waitForAngular();
		loginBaseObj.login(loginTestData);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		loginBaseObj.isLogoutUserBoxPresent("DPMWITHACCESS");
		loginBaseObj.redirectToAudienceSuite();
		browser.waitForAngular();
		utilityObj.geturl(loginTestData.url);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		assertsObj.assertTrue(reportBaseObj.checkReportingtabisPresent(),"Not redirecting to Reporting tab");
		reportBaseObj.gotoReportingTab();
		reportBaseObj.clickOnSubTab('DPMWITHACCESS');
		loginBaseObj.logout('DPMWITHACCESS');
	});
	
	/**
	 * This method used to login 'DPM' and redirecting Reporting to other Sub tab. 
	 * 
	 * @author vishalbha
	 */
	it('Turn Login Page with DPM User without Access to New Insights',function(){
		var loginTestData = turnDataFilterObj.getDataProvider(pathObj.getLoginTestDataFilePath(),browser.env,browser.language,'DPM');
		utilityObj.geturl(loginTestData.url);
		browser.waitForAngular();
		loginBaseObj.login(loginTestData);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		loginBaseObj.isLogoutUserBoxPresent("DPM");
		loginBaseObj.redirectToAudienceSuite();
		browser.waitForAngular();
		utilityObj.geturl(loginTestData.url);
		browser.waitForAngular();
		utilityObj.browserWaitforseconds(3);
		assertsObj.assertTrue(reportBaseObj.checkReportingtabisPresent(),"Not redirecting to Reporting tab");
		reportBaseObj.gotoReportingTab();
		reportBaseObj.clickOnSubTab('DPM');
		loginBaseObj.logout('DPM');
	});
	}
});