var path = function() {

	// JSON Path
	this.getAdvertiserTestDataFilePath = function() {

		return './src/testData/audienceSuite/advertiser.json';
	};

	this.getAdvertiserDataTestDataFilePath = function() {

		return './src/testData/audienceSuite/advertiserData.json';
	};
	
	this.getLoginTestDataFilePath = function() {
		return './src/testData/audienceSuite/login.json';
	};

	this.getDataProviderTestDataFilePath = function(){
		return './src/testData/audienceSuite/dataProvider.json';
	};
	
	this.getSharedPageDataProviderTestDataFilePath=function(){
	return './src/testData/audienceSuite/sharedPageDataProvider.json';
	};
	
	this.getAdvertiserDataTexonomyDataFilePath = function(){
		return '../testData/audienceSuite/taxonomy.xls';
	};
	this.getMediaProviderTestDataFilePath = function() {

		return './src/testData/audienceSuite/mediaProvider.json';
	};
	this.getMediaProviderLogoPath = function() {

		return '../testData/audienceSuite/Penguins2.png';
	};
	
	this.getLogoPath = function(){
		return '../testData/audienceSuite/logo.png';
	};

	this.getSegmentTestDataFilePath = function(){
		return './src/testData/audienceSuite/segment.json';
	};
	

	this.getDataContractsTestDataFilePath = function(){
		return './src/testData/audienceSuite/dataContract.json';
	};
   
   this.getSharedPageDataContractTestDataFilePath=function(){
        return './src/testData/audienceSuite/sharedDataContract.json';
   };
   
	this.getCookieSpaceDataProviderTestDataFilePath = function(){
		return './src/testData/audienceSuite/cookieSpaceDataProvider.json';
	};
	
	this.getSharedPageSegmentTestDataFilePath = function(){
		return './src/testData/audienceSuite/sharedSegment.json';
	};

	this.getCookieSpaceDataContractsTestDataFilePath = function(){
		return './src/testData/audienceSuite/cookieSpaceDataContracts.json';
	};
	
	this.getCookieSpaceMediaProviderTestDataFilePath = function(){
		return './src/testData/audienceSuite/cookieSpaceMediaProvider.json';
	};
};

module.exports = new path();