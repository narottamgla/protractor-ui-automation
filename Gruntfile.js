module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

	shell: {
      options: {
        stdout: true
      },
      npm_install: {
        command: 'npm install'
      },
      protractor_install: {
        command: 'node ./node_modules/protractor/bin/webdriver-manager update'
      }    
    },
    clean:{
    	reports:'testresults',
    	logs:'logs'
    },
 	
    jshint: {
      files: ['Gruntfile.js', 'src/*.*'],
      options: {
        // options here to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }
    },
   
	protractor: {	
		stg1:{
			options: {
		        keepAlive: true,
		        configFile: "protractorConfs/audienceSuite.js",
		        args:{
		        	params:{
		        		user:"CS",
		        		language:"English",
		        		env:"stg1",
		        		console:"NEW"
		        	}
		        }
		      },
		      singlerun: {},
		      auto: {
		        keepAlive: true,
		        options: {
		          args: {
		            seleniumPort: 4444
		          }
		        }
		      }
		},
		stg1dpmmm:{
			options: {
		        keepAlive: true,
		        configFile: "protractorConfs/audienceSuite.js",
		        args:{
		        	params:{
		        		user:"DPMMM",
		        		language:"English",
		        		env:"stg1",
		        		console:"NEW"
		        	}
		        }
		      },
		      singlerun: {},
		      auto: {
		        keepAlive: true,
		        options: {
		          args: {
		            seleniumPort: 4444
		          }
		        }
		      }
		},
      qa2: {		
    	    
	      options: {
	        keepAlive: true,
	        configFile: "protractorConfs/audienceSuite.js",
	        args:{
	        	params:{
	        		user:"CS",
	        		language:"English",
	        		env:"qa2",
	        		console:"NEW"
	        	}
	        }
	      },
	      singlerun: {},
	      auto: {
	        keepAlive: true,
	        options: {
	          args: {
	            seleniumPort: 4444
	          }
	        }
	      }
    	},
    	qa2dpmmm: {		
    	    
  	      options: {
  	        keepAlive: true,
  	        configFile: "protractorConfs/audienceSuite.js",
  	        args:{
  	        	params:{
  	        		user:"DPMMM",
  	        		language:"English",
  	        		env:"qa2",
  	        		console:"NEW"
  	        	}
  	        }
  	      },
  	      singlerun: {},
  	      auto: {
  	        keepAlive: true,
  	        options: {
  	          args: {
  	            seleniumPort: 4444
  	          }
  	        }
  	      }
      	}
	}
  
	
  });
  grunt.loadNpmTasks('grunt-shell-spawn');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-protractor-runner');
  grunt.loadNpmTasks('grunt-contrib-clean');
  
  grunt.registerTask('install', ['shell:npm_install', 'shell:protractor_install']);
  grunt.registerTask('default', ['clean','jshint','protractor:stg1','protractor:stg1dpmmm']);
  grunt.registerTask('cs', ['clean','jshint', 'protractor:stg1']);
  grunt.registerTask('dpmmm', ['clean','jshint', 'protractor:stg1dpmmm']);
  grunt.registerTask('qa2', ['clean','jshint', 'protractor:qa2']);
  grunt.registerTask('qa2dpmmm', ['clean','jshint', 'protractor:qa2dpmmm']);
  

};